
!start.

+!start : .my_name(Name)
   <- createWorkspace("org"); 
      joinWorkspace("org",Wid); 
      // Creation of the organisation
      makeArtifact("org","ora4mas.nopl.OrgBoard",["./src/org/t5.xml"],AId);
      focus(AId);
      createGroup(mg,maingroup,MGId);
      focus(MGId);        
	
	  // Creation of the groups
      createGroup(wg,wpgroup,GId);
      focus(GId);
      setParentGroup(mg)[artifact_id(GId)];
      createGroup(rg,rpgroup,RGId);
      focus(RGId);
      setParentGroup(mg)[artifact_id(RGId)];      
      createGroup(ag,wagroup,AGId);
      focus(AGId);
      setParentGroup(mg)[artifact_id(AGId)];      
      
      // Creation of the schemes
      createScheme(ws,writePaperSch,SId);
      focus(SId);
      createScheme(rs,readPaperSch,RSId);
      focus(RSId);
      createScheme(aws,accountWritePaperSch,ASId);
      focus(ASId);
 	  !!connect_schemes(GId,RGId,AGId);
      
      // Adopt the roles
      adoptRole(writer)[artifact_id(GId)];
 	.     
    
 +!connect_schemes(GId,RGId,AGId) <-    
      addSchemeWhenFormationOk(ws)[artifact_name("wg")];
      focusWhenAvailable("wg.ws");	  
      addSchemeWhenFormationOk(rs)[artifact_name("rg")];
      focusWhenAvailable("rg.rs");
      doSubscribeDFP(ws)[artifact_name("rg.rs")];
      
      addSchemeWhenFormationOk(aws)[artifact_name("ag")];
      focusWhenAvailable("ag.aws");
      doSubscribeDFP(ws)[artifact_name("ag.aws")];
      doSubscribeDFP(rs)[artifact_name("ag.aws")]
    .

+!Goal[scheme(Scheme)] : true <- .print("Achieving ",Goal).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("$jacamoJar/templates/org-obedient.asl") }
   

!start.

 +!joinWorkspace(WName,Wid) : true <- joinWorkspace("org",Wid).
 -!joinWorkspace(WName,Wid) : true <- .wait(1000); !joinWorkspace(WName,Wid).

+!start : .my_name(Name)
   <- .print("Hello from ",Name);
   	   !joinWorkspace("org",Wid); 
      focusWhenAvailable(org);
      focusWhenAvailable(mg);
      focusWhenAvailable(rg);
      adoptRole(reader)[artifact_name("rg")];

      focusWhenAvailable(rs);
      focusWhenAvailable("rg.rs");
   .

+!rtitle[scheme(Scheme)] : true <- 
     .print("Providing account for wtitle");
     addFact("myfact")[artifact_name("rg.rs")].
     
+!rabs[scheme(Scheme)] : true <- 
	.print("Providing account for wabs"); 
	.send(ag5bis,tell,reason1(wtitle,wabs)).

+!rsectitles[scheme(Scheme)] : true <- 
	.print("Providing account for wsectitles"); 
	.send(ag5bis,tell,reason2(wtitle,wsectitles)).

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("$jacamoJar/templates/org-obedient.asl") }
   
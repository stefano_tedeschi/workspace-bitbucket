
!start.

 +!joinWorkspace(WName,Wid) : true <- joinWorkspace("org",Wid).
 -!joinWorkspace(WName,Wid) : true <- .wait(1000); !joinWorkspace(WName,Wid).
 
+!start : .my_name(Name)
   <- .print("Hello from ",Name);
      !joinWorkspace("org",Wid);
   	  focusWhenAvailable(org);
      focusWhenAvailable(mg);
	  focusWhenAvailable(ag); 
      adoptRole(editor)[artifact_name("ag")];
   .

+!a_wtitle[scheme(Scheme)] : reason1(wtitle,G) <- 
	.print("Accounting wtitle with reason1: going to reset goal ", G);
	resetGoal(G)[artifact_name(ws)].
+!a_wtitle[scheme(Scheme)] : reason2(wtitle,G) <- 
	 .print("Accounting wtitle with reason2: going to reset goal ",G);
	 resetGoal(G)[artifact_name(ws)]. 
+!a_wtitle[scheme(Scheme)] : true <- !a_wtitle[scheme(Scheme)].
 	 
+!a_wabs[scheme(Scheme)] : true <- .print("Accounting wabs: doing nothing").
+!a_wsectitles[scheme(Scheme)] : true <- .print("Accounting wsectitles: doing nothing").

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
{ include("$jacamoJar/templates/org-obedient.asl") }
   
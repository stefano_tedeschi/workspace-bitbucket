package hiring;

import java.awt.event.FocusAdapter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactConfig;
import cartago.ArtifactId;
import cartago.CartagoException;
import cartago.LINK;
import cartago.OPERATION;
import cartago.OpFeedbackParam;
import cartago.OperationException;
import cartago.WorkspaceArtifact;
import jason.stdlib.eval;
import jason.stdlib.fail;
import twocomm.core.BusinessArtifact;
import twocomm.core.IPlayer;
import twocomm.core.Role;
import twocomm.core.RoleId;

public class WorkspaceManager extends BusinessArtifact {
	
	protected Logger logger = LogManager.getLogger(WorkspaceManager.class);
	
	public static String ARTIFACT_TYPE = "WorkspaceManager";
	
	public static String HIRER_ROLE = "hirer";
	public static String EVALUATOR_ROLE = "evaluator";
	public static String CANDIDATE_ROLE = "candidate";
	
	private RoleId hirer;
	private ArrayList<RoleId> evaluators = new ArrayList<>();
	private ArrayList<RoleId> candidates = new ArrayList<>();
	
	private static AtomicLong counter = new AtomicLong();
	
	private ArtifactId normativeArtifact;
	private ArtifactId position;
	private ArrayList<ArtifactId> applications = new ArrayList<>();
	
	static {
		addEnabledRole(HIRER_ROLE, Hirer.class);
		addEnabledRole(EVALUATOR_ROLE, Evaluator.class);
		addEnabledRole(CANDIDATE_ROLE, Candidate.class);
	}
	
	@OPERATION
	@Override
	protected void enact(String roleName) {

		if(roleName.equals("hirer")) {
			
			if(hirer != null) {
				failed("There cannot be multiple hirers!");
			}
			
			else {
				
				super.enact(roleName);
				Role r = enactedRoles.get(enactedRoles.size()-1);
				hirer = r.getRoleId();
				
				try {
					normativeArtifact = makeArtifact("hiringNormativeState", HiringNormativeState.class.getName(), ArtifactConfig.DEFAULT_CONFIG);
					defineObsProperty("normativeStateId", normativeArtifact);
				} catch (OperationException e) {
					failed("Cannot initialize normative artifact");
				}
				
				try {
					position = makeArtifact("position", Position.class.getName(), ArtifactConfig.DEFAULT_CONFIG);
					execLinkedOp(position, "initNormativeArtifact");
					execLinkedOp(position, "initWorkspaceManager");
					execLinkedOp(position, "setHirer", hirer);
					defineObsProperty("positionId", position);
				} catch (OperationException e) {
					failed("Cannot initialize position artifact");
				}
				
			}
			
		}
		
		else if(hirer != null) {
			
			if(roleName.equals("evaluator")) {
			
				super.enact(roleName);
			
				Role r = enactedRoles.get(enactedRoles.size()-1);
				RoleId evaluator = r.getRoleId();
				
				try {
					execLinkedOp(position, "addEvaluator", evaluator);
				} catch (OperationException e) {
					failed("Cannot add evaluator!");
				}
			
				evaluators.add(evaluator);
			
			}
			
			else if(roleName.equals("candidate")) {
				
				super.enact(roleName);
				
				Role r = enactedRoles.get(enactedRoles.size()-1);
				RoleId candidate = r.getRoleId();
				
				candidates.add(candidate);
				
			}
			
			if (candidates.size() == evaluators.size() && applications.size() != candidates.size()) {
				
				int missingApplications = candidates.size() - applications.size();
				
				for(int i = 0; i < missingApplications; i++) {
				
					try {
						
						RoleId evaluator = evaluators.get(evaluators.size()-missingApplications+i);
						RoleId candidate = candidates.get(candidates.size()-missingApplications+i);
						
						ArtifactId application = makeArtifact("application" + counter.incrementAndGet(), Application.class.getName(), ArtifactConfig.DEFAULT_CONFIG);
						applications.add(application);
						execLinkedOp(application, "initNormativeArtifact");
						execLinkedOp(application, "initWorkspaceManager");
						execLinkedOp(application, "setEvaluator", evaluator);
						execLinkedOp(application, "setCandidate", candidate);
						
						defineObsProperty("applicationId", application, evaluator.toString(), candidate.toString());
						
						execLinkedOp(normativeArtifact, "initCommitments", hirer, evaluator, candidate);
					
					} catch (OperationException e) {
						failed("Cannot initialize application artifact");
					}
				
				}
				
			}
			
		}
		else {
			failed("Enactment failed!");
		}
		
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}
	
	@LINK
	public void getPlayerRoleId(String playerName, OpFeedbackParam<RoleId> r) {
		r.set(getRoleIdByPlayerName(playerName));
	}
	
	@LINK
	public void getRoleIdByName(String roleIdName, OpFeedbackParam<RoleId> r) {
		r.set(getRoleIdByRoleName(roleIdName));
	}

	
	// INNER CLASSES for ROLES
	// hirer role
	public class Hirer extends PARole {

		public Hirer(String playerName, IPlayer player) {
			super(HIRER_ROLE, player);
		}
	}

	// evaluator role
	public class Evaluator extends PARole {
		public Evaluator(String playerName, IPlayer player) {
			super(EVALUATOR_ROLE, player);
		}
	}
	
	// candidate role
	public class Candidate extends PARole {
		public Candidate(String playerName, IPlayer player) {
			super(CANDIDATE_ROLE, player);
		}
	}
	
	public interface HirerObserver extends ProtocolObserver {	}

	public interface EvaluatorObserver extends ProtocolObserver {	}
	
	public interface CandidateObserver extends ProtocolObserver {	}
	
}

{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

/* Plans */

+!start : true
	<- 	.wait(500); 
		enact("candidate").
		
+enacted(Id,"candidate",My_Role_Id)
	<-	+enactment_id(My_Role_Id).
	
+applicationId(AppId,_,My_Role_Id) : enactment_id(My_Role_Id) & normativeStateId(NSId)
	<- focus(AppId);
	   focus(NSId).
	   
+postJob(Hirer) : enactment_id(My_Role_Id) & applicationId(AppId,_,My_Role_Id)
	<- apply[artifact_id(AppId)].

{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

/* Plans */

+!start : true
	<- 	.wait(500);
		enact("evaluator").
		
+enacted(Id,"evaluator",My_Role_Id)
	<-	+enactment_id(My_Role_Id).
	
+applicationId(AppId,My_Role_Id,_) : enactment_id(My_Role_Id) & normativeStateId(NSId) & positionId(PosId)
	<- focus(PosId);
	   focus(AppId);
	   focus(NSId).

+cc(My_Role_Id,Candidate_Role_Id,_,InformOutcome,"DETACHED")
	 : enactment_id(My_Role_Id) & applicationId(_,My_Role_Id,Candidate_Role_Id) & positionStatus("POSITION_OPEN")
	<- screenInterview;
	   println("Fatto!").
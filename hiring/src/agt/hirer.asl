{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

/* Plans */

+!start : true
	<- .wait(100);	
	   enact("hirer").
	   //.
	
+enacted(Id,"hirer",My_Role_Id)
	<-	+enactment_id(My_Role_Id).
	
+positionId(PosId) : normativeStateId(NSId)
	<- focus(PosId);
	   focus(NSId);
	   !post_job.
		
+!post_job : positionId(PosId)
	<- .wait(1000);
	   postJob[artifact_id(PosId)].	   
	


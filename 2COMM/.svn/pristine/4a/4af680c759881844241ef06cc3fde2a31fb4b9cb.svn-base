package twocomm.core.typing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.LogicalExpressionType;


/**
 * Definizione dei requirements per il ruolo di un commitment protocol,
 * definito in termini di commitment che devono essere portati a true
 * (nel caso del debitore) o portati a detached (nel caso del creditore)
 * per poter fare l'enactment di quel ruolo.
 * @author Federico Capuzzimati
 *
 */
public abstract class RoleRequirements {

	protected static final Logger staticLogger = LogManager.getLogger(RoleRequirements.class);
	protected Logger logger = LogManager.getLogger(TypedProtocolArtifact.class);
	
	final private Set<Commitment> C_DEFINITION;
	final private Set<Commitment> D_DEFINITION;
	final private String ROLE_NAME;
	
	/**
	 * Due array di commitment: il primo per quelli in cui il ruolo compare come debitore,
	 * il secondo come creditore
	 * @param commitsCDefinition
	 * @param commitsDDefinition
	 */
	protected RoleRequirements(String roleName, Commitment[] commitsDDefinition, Commitment[] commitsCDefinition) {
		D_DEFINITION = new HashSet<Commitment>();
		for (Commitment c : commitsDDefinition) {
			D_DEFINITION.add(c);
		}
		
		C_DEFINITION = new HashSet<Commitment>();
		
		if (commitsCDefinition != null) {
			for (Commitment c : commitsCDefinition) {
				C_DEFINITION.add(c);
			}
		}
		
		this.ROLE_NAME = roleName;
	}
	
	/**
	 * Un solo array di commitment, considerando che il ruolo compaia solo come debitore
	 * nei commitment, oppure si voglia richiedere solo la gestione della debtor compliance.
	 * @param commitsDDefinition
	 */
	protected RoleRequirements(String roleName, Commitment[] commitsDDefinition) {
		C_DEFINITION = new HashSet<Commitment>();
		
		D_DEFINITION = new HashSet<Commitment>();
		for (Commitment c : commitsDDefinition) {
			D_DEFINITION.add(c);
		}
		this.ROLE_NAME = roleName;
	}
	
	
	
	
	public boolean equals(RoleRequirements t) {
		return this.isIncluded(t) && t.isIncluded(this);
	}
	
	private boolean isIncluded(RoleRequirements t) {
		// TODO Auto-generated method stub
		return false;
	}


	public Set<Commitment> getRoleCRequirements() {
		return this.C_DEFINITION;
	}
	
	public Set<Commitment> getRoleDRequirements() {
		return this.D_DEFINITION;
	}
	
		
	public String toString() {
		String str = "Type "+this.getClass().getSimpleName()+"\n";
		str += "CREDITOR DEFINITION\n--------------\n";
		for (Commitment c : C_DEFINITION) {
			str += c + "\n";
		}
		str += "DEBTOR DEFINITION\n--------------\n";
		for (Commitment c : D_DEFINITION) {
			str += c + "\n";
		}
		return str;
	}


	

	
	private void updateTree(TreeNode node, ArrayList<BType> bTypes, ArrayList<TreeNode> leafNodes) {
		PartialActualization newPa;
		for (BType b : bTypes) {
			// se trovo la PA corrente, creo i nuovi nodi con le PA uguali a quella precedente
			// aggiungendo l'azione offerta del BType, che diventa anche l'etichetta dell'arco
			if (b.getPartialActualization().equals(node.partialAct)) {
				newPa = node.partialAct.clone();
				try {
					newPa.addAction(b.getNewAction());
					TreeNode newChild = node.addChild(newPa, b.getNewAction());
					// richiamo l'aggiornamento sul nuovo nodo
					updateTree(newChild, bTypes, leafNodes);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else if (!node.isLeaf){
				leafNodes.add(node);
				node.isLeaf = true;
				logger.debug("Nodo foglia:" +node);
			}
		}
	}
	
	
	
	
	private boolean checkCommitmentSatisfied(Commitment c, ArrayList<BType> bTypes) {
		logger.debug("Checking satisfy for commitment "+c);
		
		// creo albero con radice la p.a. vuota e tanti figli quanti sono i BTypes corrispondenti
		TreeNode root = new TreeNode(new PartialActualization());
		ArrayList<TreeNode> leafNodes = new ArrayList<TreeNode>();
		
		// costruisco l'albero, tenendo traccia dei nodi foglia
		updateTree(root, bTypes, leafNodes);
		logger.debug("PartialActualization Tree:\n"+root);
		logger.debug("Nodi foglia: "+leafNodes.size());
		
		// l'albero ha nei nodi foglia le azioni offerte lungo quel ramo,
		// quindi le verifico come fossero in AND	
		
		// considero conseguenti come sicuramente logical expression, NON commitment annidati
		// controllo che almeno una delle log.exp. portino il conseguente a TRUE
		LogicalExpression le = (LogicalExpression)c.getConsequent();
		
		for (TreeNode t : leafNodes) {
			for (ProtocolAction pa : t.partialAct.getActionList()) {
				update(le, pa);
			}
			if (le.isVerified()) return true;
			le.resetVerifiedToFalse();
			
		}
		
		// se i nodi foglia finiscono senza che sia diventata true, allora restituisco false
		return false;
	}
		
	
	private boolean checkCommitmentDetachment(Commitment c, ArrayList<BType> bTypes) {
		logger.debug("Checking detachment for commitment "+c);
		
		return true;
	}
	
	
	// VERSIONE CON ATTUALIZZAZIONI 
	public boolean isEnactable(ArrayList<BType> bTypes) {
		// btype: commit, partial-actualization, protocol-action
		boolean debtOk = true, credOk = true;
		ArrayList<BType> arrNew;
		
		// costruisco una mappa <commit,b-type>
		HashMap<Commitment,ArrayList<BType>> mapCommitTypes = new HashMap<Commitment,ArrayList<BType>>();
		for (BType bt : bTypes) {
			for (Commitment c : bt.getCommitments()) {
				if (mapCommitTypes.containsKey(c))
					mapCommitTypes.get(c).add(bt);
				else {
					arrNew = new ArrayList<BType>();
					arrNew.add(bt);
					mapCommitTypes.put(c, arrNew);
				}			
			}
		}
				
		logger.debug("Commitment-Types map created: ");
		for (Commitment c : mapCommitTypes.keySet()) {
			logger.debug(c);
			for (BType bt : mapCommitTypes.get(c)) {
				logger.debug(bt);
			}
		}
		// valuto i commitment dei req lato creditore
		if (!C_DEFINITION.isEmpty()) {
			logger.debug("Checking creditor commitments");
			for (Commitment c : C_DEFINITION) {
				// se commitment non trattato, enactment fallisce
				if (!mapCommitTypes.containsKey(c)) return false;
				credOk &= checkCommitmentDetachment(c, mapCommitTypes.get(c));				
			}
		} else logger.debug("No Creditor commitments required for role "+this.getRoleName());
		logger.debug("Creditor commitments checked.");
		
		// valuto i commitment dei req lato debitore
		for (Commitment c : D_DEFINITION) {
			logger.debug("Checking debtor commitments");
			// se commitment non trattato, enactment fallisce
			
			if (!(mapCommitTypes.containsKey(c))) {
				logger.debug("Commitment "+c+" not found in Commitment-types map");	
				//return false;
			}
			logger.debug("Found entry for commitment: "+mapCommitTypes.get(c));
			debtOk &= checkCommitmentSatisfied(c, mapCommitTypes.get(c));
		}
		logger.debug("Debtor commitments checked.");
		
		if (debtOk && credOk) 
			logger.debug("\n--------------------------------\nThe role "+ROLE_NAME+" is enactable.\n--------------------------------");
		else 
			logger.debug("\n--------------------------------\nThe role "+ROLE_NAME+" is NOT enactable.\n--------------------------------");
		return debtOk && credOk;
	}
	
	public String getRoleName() {
		return this.ROLE_NAME;
	
	}
	
	
	private void update(LogicalExpression e, ProtocolAction action) {
		if (e.getLogExpType() == LogicalExpressionType.FACT) {
			if (((Fact) action.getElementChanged()).equals(e)) {
				logger.trace("Expression *" + e + "* verified.");
				e.setVerified(true);
			}
		} else {
			if (e.getLeft() != null && !e.getLeft().isVerified() && !e.getLeft().isViolated()) {
				update(e.getLeft(), action);
			}
			if (e.getRight() != null && !e.getRight().isVerified() && !e.getRight().isViolated()) {
				update(e.getRight(), action);
			}
			switch (((CompositeExpression) e).getOperator()) {
			case AND:
				if (e.getLeft().isVerified() && e.getRight().isVerified()) {
					logger.trace("AND Expression *" + e + "* verified.");
					e.setVerified(true);
				} else if (e.getLeft().isViolated() || e.getRight().isViolated()) {
					logger.trace("AND Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			case OR:
				if (e.getLeft().isVerified() || e.getRight().isVerified()) {
					logger.trace("OR Expression *" + e + "* verified.");
					e.setVerified(true);
				} else if (e.getLeft().isViolated() && e.getRight().isViolated()) {
					logger.trace("OR Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			case NOT:
				if (e.getLeft().isVerified()) {
					logger.trace("NOT Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			case THEN:
				if (e.getLeft().isVerified() && e.getRight().isVerified() && !e.isViolated()) {
					logger.trace("THEN Expression *" + e + "* verified.");
					e.setVerified(true);
				} else if (!e.getLeft().isVerified() && e.getRight().isVerified()) {
					logger.trace("THEN Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			default:
				break;
			}
		}
	}
	
	public class TreeNode implements Iterable<TreeNode> {

		PartialActualization partialAct;
		ProtocolAction parentAction;
	    TreeNode parent;
	    List<TreeNode> children;
	    private boolean isLeaf = false;

	    public TreeNode(PartialActualization data) {
	        this.partialAct = data;
	        this.children = new LinkedList<TreeNode>();
	    }

	    public TreeNode addChild(PartialActualization child, ProtocolAction parentAction) {
	        TreeNode childNode = new TreeNode(child);
	        childNode.parent = this;
	        childNode.parentAction = parentAction;
	        this.children.add(childNode);
	        return childNode;
	    }

	    public ProtocolAction getParentAction() {
	    	return this.parentAction;
	    }
	    
	    public void setPartentAction(ProtocolAction action) {
	    	this.parentAction = action;
	    }
	    
	    public String toString() {
	    	String str = this.partialAct+"\n\t";
	    	if (children.size() != 0) {
		    	for (TreeNode child : children) {
		    		str += "|-- action: "+ child.parentAction +"-->" + child ;
		    	}
	    	}
	    	return str;
	    }
	    
		@Override
		public Iterator<TreeNode> iterator() {
			// TODO Auto-generated method stub
			return null;
		}

	    // other features ...

	}
	
	
	
	
}

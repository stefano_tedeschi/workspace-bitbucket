package netbill.automated;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleAction;
import twocomm.core.RoleId;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;
import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;

public class NetbillProtocol extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(NetbillProtocol.class);
	public static String ARTIFACT_TYPE = "NETBILL";
	public static String CUSTOMER_ROLE = "Customer";
	public static String MERCHANT_ROLE = "Merchant";

	static {
		addEnabledRole(CUSTOMER_ROLE, Customer.class);
		addEnabledRole(MERCHANT_ROLE, Merchant.class);
	}

	public NetbillProtocol() {
		super();
		socialState = new AutomatedSocialState(this);
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	// ROLES OPERATIONS

	// merchant.sendQuote
	@OPERATION
	public void sendQuote(Item item, double price) {
		RoleId merchant = getRoleIdByRoleName(getOpUserName());
		RoleId customer = getRoleIdByGenericRoleName(CUSTOMER_ROLE).get(0);
		try {
			Commitment c = new Commitment(merchant, customer, new Fact("acceptedQuotation", item, price), new Fact("goods", item));
			createCommitment(c);
			if (this.socialState.existsFact(new Fact("goods", item))) {
				satisfyCommitment(c);
			}
			createCommitment(new Commitment(merchant, customer, new Fact("paid", item), new Fact("receipt", item)));
			assertFact(new Fact("sendQuote", item, price));
			assertFact(new Fact("quotation", item, price));
			logger.trace("OPERATION PERFORMED: SEND_QUOTE (" + item + ", " + price + ") by " + merchant);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// merchant.sendGoods
	@OPERATION
	public void sendGoods(Item item) {
		RoleId merchant = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("goods", item));
			logger.trace("OPERATION PERFORMED: SEND_GOODS (" + item + ") by " + merchant);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// merchant.sendReceipt
	@OPERATION
	public void sendReceipt(Item item) {
		RoleId merchant = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("receipt", item));
			logger.trace("OPERATION PERFORMED: SEND_RECEIPT (" + item + ") by " + merchant);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// customer.sendRequest
	@OPERATION
	public void sendRequest(Item item) {
		System.out.println("SEND REQUEST");
		RoleId customer = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("requestedQuote", item));
			logger.trace("OPERATION PERFORMED: SEND_REQUEST (" + item + ") by " + customer);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// customer.sendAccept
	@OPERATION
	public void sendAccept(Item item, double price) {
		RoleId customer = getRoleIdByRoleName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			assertFact(new Fact("acceptedQuotation", item, price));
			createCommitment(new Commitment(customer, merchant, new Fact("goods", item), new Fact("paid", item)));
			logger.trace("OPERATION PERFORMED: SEND_ACCEPT (" + item + ", " + price + ") by " + customer);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// customer.sendReject
	@OPERATION
	public void sendReject(Item item, double price) {
		RoleId customer = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("rejectedQuotation", item, price));
			logger.trace("OPERATION PERFORMED: SEND_REJECT (" + item + ", " + price + ") by " + customer);
			for(Commitment c : socialState.retrieveCommitmentsByCreditorRoleId(customer)) {
				releaseCommitment(c);
			}
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// customer.sendEPO
	@OPERATION
	public void sendEPO(Item item) {
		RoleId customer = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("paid", item));
			logger.trace("OPERATION PERFORMED: PAY (" + item + ") by " + customer);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void printSocialState() {
		logger.trace("OPERATION PERFORMED: PRINT_SOCIAL_STATE \n" + socialState);
	}

	// BEGIN INNER CLASSES for ROLES
	
	// netbill.Merchant role
	public class Merchant extends PARole {

		public Merchant(IPlayer player) {
			super(MERCHANT_ROLE, player);
		}
		
		@RoleAction
		public void sendQuote(Item item, double price) {
			try {
				doAction(this.getArtifactId(), new Op("sendQuote", item, price));
			} catch (ActionFailedException e) {
				logger.error("Action SEND_QUOTE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public void sendGoods(Item item) {
			try {
				doAction(this.getArtifactId(), new Op("sendGoods", item));
			} catch (ActionFailedException e) {
				logger.error("Action SEND_GOODS failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public void sendReceipt(Item item) {
			try {
				doAction(this.getArtifactId(), new Op("sendReceipt", item));
			} catch (ActionFailedException e) {
				logger.error("Action SEND_RECEIPT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}
		
		public void printSocialState() {
			try {
				doAction(this.getArtifactId(), new Op("printSocialState"));
			} catch (ActionFailedException e) {
				logger.error("Action PRINT_SOCIAL_STATE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}
		

	}

	
	// netbill.Customer role
	public class Customer extends PARole {

		public Customer(IPlayer player) {
			super(CUSTOMER_ROLE, player);
		}
		
		@RoleAction
		public void sendRequest(Item item) {
			try {
				System.out.println("SEND REQUEST " + this.getArtifactId());
				doAction(this.getArtifactId(), new Op("sendRequest", item));
			} catch (ActionFailedException e) {
				logger.error("Action SEND_REQUEST failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public void sendAccept(Item item, double price) {
			try {
				doAction(this.getArtifactId(), new Op("sendAccept", item, price));
			} catch (ActionFailedException e) {
				logger.error("Action SEND_ACCEPT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public void sendReject(Item item, double price) {
			try {
				doAction(this.getArtifactId(), new Op("sendReject", item, price));
			} catch (ActionFailedException e) {
				logger.error("Action SEND_REJECT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public void sendEPO(Item item) {
			try {
				doAction(this.getArtifactId(), new Op("sendEPO", item));
			} catch (ActionFailedException e) {
				logger.error("Action SEND_EPO failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
			
		}

	}

	public interface CustomerObserver extends ProtocolObserver {

	}

	public interface MerchantObserver extends ProtocolObserver {

	}

}

package twocomm.core.logic;

import java.util.Objects;

import javax.persistence.Embeddable;

import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import twocomm.core.SocialStateElement;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

@Embeddable
public class CompositeExpression extends LogicalExpression {

	private LogicalOperatorType operator;

	public CompositeExpression(LogicalOperatorType operator, LogicalExpression left, LogicalExpression right)
			throws MissingOperandException, WrongOperandsNumberException {
		super(LogicalExpressionType.COMPOSITE_EXPRESSION, left, right);
		if (operator == LogicalOperatorType.NOT) {
			if (right != null) {
				throw new WrongOperandsNumberException("You cannot use a NOT operator with two operands.");
			} else {
				this.operator = operator;
			}

		} else if (right == null) {
			throw new MissingOperandException("Missing some operands.");
		} else {
			this.operator = operator;
		}
	}

	public LogicalOperatorType getOperator() {
		return operator;
	}

	// i suoi elementi son sicuramente giusti
	public String toString() {
		if (operator != LogicalOperatorType.NOT) {
			return "(" + left + " " + operator + " " + right + ")";
		} else {
			return "(" + operator + " " + left + ")";
		}
	}

	public static CompositeExpression and(LogicalExpression left, LogicalExpression right)
			throws MissingOperandException, WrongOperandsNumberException {
		return new CompositeExpression(LogicalOperatorType.AND, left, right);
	}

	public static CompositeExpression or(LogicalExpression left, LogicalExpression right)
			throws MissingOperandException, WrongOperandsNumberException {
		return new CompositeExpression(LogicalOperatorType.OR, left, right);
	}

	public static CompositeExpression not(LogicalExpression left)
			throws MissingOperandException, WrongOperandsNumberException {
		return new CompositeExpression(LogicalOperatorType.NOT, left, null);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof SocialStateElement)
			return this.equals((SocialStateElement) o);
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 37 * hash + Objects.hashCode(this.operator);
		return hash;
	}

	public boolean equals(SocialStateElement el) {
		switch (el.getElType()) {
		case COMMITMENT:
			return false;
		case LOGICAL_EXPRESSION:
			switch (((LogicalExpression) el).getLogExpType()) {
			case FACT:
				return false;
			case COMPOSITE_EXPRESSION:
				CompositeExpression expToCompare = (CompositeExpression) el;
				if (this.getOperator() == expToCompare.getOperator()) {
					if (this.getOperator() == LogicalOperatorType.NOT) {
						return this.getLeft().equals(expToCompare.getLeft());
					} else {
						return this.getLeft().equals(expToCompare.getLeft())
								&& this.getRight().equals(expToCompare.getRight());
					}
				} else {
					return false;
				}
			}

		}
		return false;
	}

	@Override
	public Term toJasonTerm() {
		if(operator == LogicalOperatorType.NOT) {
			Structure struct = new Structure("NOT", 1);
			Term t = left.toJasonTerm();
			struct.addTerm(t);
			return struct;
		}
		else {
			Structure struct = new Structure(operator.toString(), 2);
			Term l = left.toJasonTerm();
			struct.addTerm(l);
			Term r = right.toJasonTerm();
			struct.addTerm(r);
			return struct;
		}
	}

}

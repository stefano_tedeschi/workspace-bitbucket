package netbill;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.BusinessArtifact;
import twocomm.core.RoleId;
import twocomm.core.logic.Fact;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.exception.MissingOperandException;
import cartago.*;

public class Purchase extends BusinessArtifact {

	protected Logger logger = LogManager.getLogger(Purchase.class);
	public static String ARTIFACT_TYPE = "Purchase";
	public static String MERCHANT_ROLE = "merchant";
	public static String CUSTOMER_ROLE = "customer";
	
	private ArtifactId normativeArtifact = null;

	static {
		addEnabledRole(MERCHANT_ROLE, Merchant.class);
		addEnabledRole(CUSTOMER_ROLE, Customer.class);
	}

	public Purchase() {
		super();
		socialState = new AutomatedSocialState(this);
	}
	
	@INTERNAL_OPERATION
	void createNormativeArtifact() {
		
		if (normativeArtifact == null) {
			//Create normativeArtifact artifact
			try {
				normativeArtifact = makeArtifact("netbillNormativeState", 
						"netbill.NetbillNormativeState", ArtifactConfig.DEFAULT_CONFIG);
				defineObsProperty("netBillNormativeStateId",      normativeArtifact);
			} catch (OperationException e) {
				failed("There was problem cresting the normstive artifact");				
			}
		}
	}
	
	@OPERATION
	public void init(String itemName, int maxQuantity)  {
        // observable properties   
		
        defineObsProperty("itemName",      itemName);
        defineObsProperty("maxQuantity",   maxQuantity);
        
        defineObsProperty("purchaseId",      this.getId());
        
        execInternalOp("createNormativeArtifact");
        
    }

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	// ROLES OPERATIONS

	// merchant.quote 
	@OPERATION
	public void quote(int price, int quantity, String customer) {
		
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customerId = getRoleIdByRoleName(customer);
		
				
		try {			
			assertFact(new Fact("quotation", price,quantity,customer));
			execLinkedOp(normativeArtifact, "quote", merchant, customerId, price, quantity);
			logger.trace("PURCHASE: QUOTATION ("+price+","+quantity+") by " + merchant.getPlayerName());
		} catch (MissingOperandException|OperationException e) {
			log(Arrays.toString(e.getStackTrace()));
		}
	}

	// merchant.ship
	@OPERATION
	public void ship(String customerString) {
		RoleId merchant = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {
			assertFact(new Fact("goods",customer.toString()));
			execLinkedOp(normativeArtifact, "ship", merchant, customer);
			logger.trace("PURCHASE: SHIP by " + merchant);
			
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// merchant.emitReceipt
	@OPERATION
	public void emitReceipt(String customerString) {
		RoleId merchant = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {			
			
			assertFact(new Fact("receipt", customer.toString()));
			execLinkedOp(normativeArtifact, "emitReceipt", merchant, customer);
			logger.trace("OPERATION PERFORMED: RECEIPT by " + merchant);
		} catch (MissingOperandException | OperationException e) {
			e.printStackTrace();
		}
	}
	
	// customer.request
	@OPERATION
	public void request(int quantity) {
		RoleId customer = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			//createNormativeArtifact();
			assertFact(new Fact("requestedQuote", quantity, customer.toString()));
			logger.trace("PURCHASE: REQUESTED QUOTE OF " + quantity + " by " + customer);
			execLinkedOp(normativeArtifact, "request", customer, merchant, quantity);
		} catch (MissingOperandException|OperationException e) {
			e.printStackTrace();
		}
	}

	// customer.accept
	@OPERATION
	public void accept(int price, int quantity) {
		RoleId customer = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			assertFact(new Fact("acceptedQuotation", price, quantity, customer.toString()));
			execLinkedOp(normativeArtifact, "accept", price, quantity, merchant, customer);
			logger.trace("PURCHASE: ACCEPTED_QUOTATION of " + price + " by " + customer);
		} catch (MissingOperandException | OperationException e) {
			e.printStackTrace();
		}
	}
	
	// customer.reject
	@OPERATION
	public void reject(String price) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			assertFact(new Fact("rejectedQuotation", price, customer.toString()));
			logger.trace("OPERATION PERFORMED: REJECT by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.sendEPO
	@OPERATION
	public void sendEPO(int creditCardNumber) {
		RoleId customer = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		try {			
			execLinkedOp(normativeArtifact, "sendEPO", creditCardNumber, customer);
			assertFact(new Fact("paid", customer.toString()));
			assertFact(new Fact("epo", creditCardNumber, customer.toString()));
		
			logger.trace("PURCHASE: EPO by " + customer);
		} catch (MissingOperandException | OperationException e) {
			e.printStackTrace();
		}
	}

	// INNER CLASSES for ROLES
	// merchant role
	public class Merchant extends PARole {

		public Merchant(String playerName, IPlayer player) {
			super(MERCHANT_ROLE, player);
		}
	}

	// customer role
	public class Customer extends PARole {
		public Customer(String playerName, IPlayer player) {
			super(CUSTOMER_ROLE, player);
		}
	}

	public interface MerchantObserver extends ProtocolObserver {	}

	public interface CustomerObserver extends ProtocolObserver {	}

}
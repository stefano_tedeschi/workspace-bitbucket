package twocomm.core;



import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.CommunicationArtifact.CARole;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.CommitmentOperationNotPerformableException;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import cartago.OPERATION;
import cartago.ObsProperty;

/*
 * This artifact extends the 'TupleSpace' artifact.
 * Communication is performed via tuplespace; besides that, an interaction state is mantained.
 * 
 * Every communication artifact has to add to static HashSet 'enabledRoles' the roles
 * an agent can enact, via method 'addEnabledRole(String roleName)'.
 * 
 */

public abstract class ProtocolArtifact extends CommunicationArtifact {

	protected Logger logger = LogManager.getLogger(ProtocolArtifact.class);
	
	/* 
	 * String constants for primitives on commitments and logical_expressions (facts)
	 */
	public static final String CREATE_COMMITMENT = "createCommitment";
	public static final String CANCEL_COMMITMENT = "cancelCommitment";
	public static final String RELEASE_COMMITMENT = "releaseCommitment";
	public static final String SUSPEND_COMMITMENT = "suspendCommitment";
	public static final String REACTIVATE_COMMITMENT = "reactivateCommitment";
	
	public static final String INTERACTION_STATE_CHANGED_COMMITMENT = "commit";
	
	public static final String ASSERT_FACT = "assertFact";
	
	private ArrayList<ProtocolObserver> obs = new ArrayList<ProtocolObserver>();
	protected boolean changed = false;
	
	protected SocialState socialState = new SocialState(this);
	
	private int counter = 0;
	
	public static String INTERACTION_STATE = "interactionState";
	
	static {
		addEnabledRole(PA_ROLE, PARole.class);		
	}
	
	/*
	 * Returns the HashSet of enabled roles for this artifact.
	 * @return 
	 */
	public static HashMap<String, Class<? extends Role>> getEnabledRoles() {
		return new HashMap<String, Class<? extends Role>>(enabledRoles);
	}
	
	@OPERATION
	protected void init(){
		super.init();
	}
	
	
	//************ UTILITY PROTECTED FUNCTIONS
	
	protected void defineObsPropertyFromCommitment(Commitment com) {
		logger.debug("Adding commitment, "+com+", time: "+counter++);
		defineObsProperty("cc", com.getDebtor().toString(), com.getCreditor().toString(),
				com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());		
		logger.debug(socialState+", time: "+counter++);
	}
	
	protected void updateCommitmentObsPropertyByStatus(Commitment c, LifeCycleState oldStatus, LifeCycleState newStatus) {
		logger.debug("Update Commitment began, "+c+", status: "+newStatus);
		// non posso chiamare l'update della property, poiche' la selezione della proprieta'
		// avviene solo tramite nome property
		removeObsPropertyByTemplate("cc", c.getDebtor().toString(), c.getCreditor().toString(), 
				c.getAntecedent().toString(), c.getConsequent().toString(), oldStatus.toString());	
		c.setStatus(newStatus);
		defineObsProperty("cc", c.getDebtor().toString(), c.getCreditor().toString(), 
				c.getAntecedent().toString(), c.getConsequent().toString(), newStatus.toString());
		try { signal("temp"); } catch (Exception e) {logger.debug("Eccezione in signal");}
		logger.debug(socialState);
	}
	
	protected void defineObsPropertyFromFact(Fact f) {
		defineObsProperty(f.getPredicate(), f.getArguments());
	}
	
	
	protected int countFacts(Fact f) {
		int factNumber = 0;
		List<Fact> allFacts = socialState.getFacts();
		for (Fact f1 : allFacts) {
			if (f1.hasSameName(f)) factNumber++;
		}
		return factNumber;
	}
	
	
	protected List<Fact> getAllFactsWithName(Fact f) {
		List<Fact> facts = new ArrayList<Fact>();
		List<Fact> allFacts = socialState.getFacts();
		for (Fact f1 : allFacts) {
			if (f1.hasSameName(f)) facts.add(f1);
		}
		return facts;
	}
	//*********** FACT MANAGEMENT
	protected void assertFact(Fact f) {
		try {
			socialState.assertFact(f);
		} catch (LogicalExpressionAlreadyAddedException e) {
			// TODO Auto-generated catch block
			logger.error("Fact already added: "+e.getLocalizedMessage());
		}
		signal("fact");
		setChanged();
		notifyObservers(new SocialEvent(f, SocialEventActionType.FACT_ASSERTED));
	}
	
	
	//************** protected operations for commitment management
	
	protected synchronized ArrayList<Commitment> createCommitment(Commitment c) {
		ArrayList<Commitment> comms = socialState.createCommitment(c);
		setChanged();
		notifyObservers(new SocialEvent(c, SocialEventActionType.CREATE_COMMITMENT));
		
		return comms;
	}
		
	protected ArrayList<Commitment> createAllCommitments(Commitment c) {
		ArrayList<Commitment> comms = socialState.createAllCommitments(c);
		for (Commitment com : comms) {
			setChanged();
			notifyObservers(new SocialEvent(com, SocialEventActionType.CREATE_COMMITMENT));
		}
		
		return comms;
	}
	
	protected synchronized void releaseCommitment(Commitment c) {
        socialState.changeCommitmentStatus(c, LifeCycleState.TERMINATED);		
		setChanged();
		notifyObservers(new SocialEvent(c, SocialEventActionType.RELEASE_COMMITMENT));
		logger.debug("Commitment released, "+c);
	}
	
	// TODO: IGNORATO SE OPERAZIONE NON E' ESEGUIBILE
	protected void cancelCommitment(Commitment c) {
		// TERMINATED if CONDITIONAL, VIOLATED if DETACHED
		if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL)
			socialState.changeCommitmentStatus(c, LifeCycleState.TERMINATED);
		else if (c.getLifeCycleStatus() == LifeCycleState.DETACHED)
			socialState.changeCommitmentStatus(c, LifeCycleState.VIOLATED);
		setChanged();
		notifyObservers(new SocialEvent(c, SocialEventActionType.CANCEL_COMMITMENT));
		logger.debug("Commitment canceled, "+c);
	}
	
	protected void satisfyCommitment(Commitment c) {
		socialState.changeCommitmentStatus(c, LifeCycleState.SATISFIED);	
		setChanged();
		notifyObservers(new SocialEvent(c, SocialEventActionType.SATISFY_COMMITMENT));
		logger.debug("Commitment satisfied, "+c);
	}
	
	protected void detachCommitment(Commitment c) {
		//socialState.detachCommitment(c);
        socialState.changeCommitmentStatus(c, LifeCycleState.DETACHED);
		setChanged();
		notifyObservers(new SocialEvent(c, SocialEventActionType.DETACH_COMMITMENT));
		logger.debug("Commitment detached, "+c);
	}
	
	

	
	
	
	
	public synchronized void addObserver(ProtocolObserver o) {
		logger.debug("Registering Observer "+o);
		if (o == null)
			throw new NullPointerException();
		if (!obs.contains(o))
			obs.add(o);
	}
	
	public synchronized void deleteObserver(ProtocolObserver o) {
		obs.remove(o);
	}
	
	public void notifyObservers(final SocialEvent e, final Object ... args) {
		
//		Thread t = new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
				ProtocolObserver[] obsLocal;
				synchronized(this) {
					if (!changed)
						return;
					obsLocal = obs.toArray(new ProtocolObserver[obs.size()]);
					
					for (int i=obsLocal.length-1;i>=0;i--)
						obsLocal[i].handleEvent(e, args);
					clearChanged();
				}
				
//				
//			}
//		});
//		t.setPriority(Thread.MAX_PRIORITY);
//		t.setName("NotifierThread");
//		t.start();		
	}
	
	public synchronized void deleteObservers() {
		obs.clear();
	}
	
	protected synchronized void setChanged() {
		changed = true;
	}
	
	protected synchronized void clearChanged() {
		changed = false;
	}
	
	public synchronized boolean hasChanged() {
		return changed;
	}
	
	public synchronized int countObservers() {
		return obs.size();
	}
	
	public void setSocialState(SocialState s) {
		socialState = s;
	}
	
	/**
	 * Role for interaction with social state
	 * @author fefo
	 *
	 */
	public class PARole extends CARole {
		
		public PARole(String roleName, IPlayer player) {
			super(roleName, player);
		}
		
		// methods for inspection of social state
		
		
		public List<Commitment> getCommitmentsInvolvedAsCreditor() {
			return socialState.retrieveCommitmentsByCreditorRoleId(getRoleId());
		}
		
		public List<Commitment> getCommitmentsInvolvedAsDebtor() {
			return socialState.retrieveCommitmentsByDebtorRoleId(getRoleId());
		}
		
		public boolean existsFact(Fact f) {
			return socialState.existsFact(f);
		}
		
		public boolean existsCommitment(Commitment c) {
			return socialState.hasCommitment(c);
		}
		
		
		
		
		
		public void startObserving(ProtocolObserver o) {
			addObserver(o);
		}
	}
	

	
	
	
	public interface ProtocolObserver {
		
		void handleEvent(SocialEvent e, Object ... args);
		
	}

}

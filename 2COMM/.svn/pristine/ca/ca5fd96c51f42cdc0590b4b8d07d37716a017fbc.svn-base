package twocomm.core;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.exception.MissingRequirementForRoleException;
import cartago.Artifact;
import cartago.ArtifactId;
import cartago.CartagoException;
import cartago.IEventFilter;
import cartago.Op;
import cartago.OpFeedbackParam;
import cartago.WorkspaceId;
import cartago.AgentCredential;
import cartago.util.agent.ActionFailedException;
import cartago.util.agent.ActionFeedback;
import cartago.util.agent.ArtifactObsProperty;
import cartago.util.agent.CartagoBasicContext;
import cartago.util.agent.Percept;

public abstract class Role {

	protected static final Logger staticLogger = LogManager.getLogger(Role.class);
	
	protected RoleId roleId;
	
	protected IPlayer player;
	protected ArtifactId artId;
	
	private CartagoBasicContext ctx;	
	private static CartagoBasicContext staticCtx = new CartagoBasicContext("agent-Abstract-Role","default");
	
	protected ArrayList<Method> roleActions = new ArrayList<Method>();
	

// 		only for JADE and typed behaviours	
//		Annotation annotation = this.getClass().getAnnotation(RoleDefinition.class);
//		if (!(annotation != null && annotation instanceof RoleDefinition)) {
//			throw new MissingRequirementForRoleException(this.getClass());
//		}		
	
	public Role (String roleName, IPlayer player, ArtifactId artId) {
		super();
		this.roleId = new RoleId(roleName, this);
		this.player = player;
		this.artId = artId;
		ctx = new CartagoBasicContext(this.roleId.toString(),"default");
	}
	
	public ArtifactId getArtifactId() {
		return this.artId;
	}
	
	public String toString() {
		return this.roleId.toString();
	}

	public RoleId getRoleId() {
		return roleId;
	}
	
	
	
	
	/** 
	 * Static method for creating artifacts.
	 * 
	 * @param artifactName The name of the artifact to be created.
	 * @param artifactClass The class of the artifact.
	 * @return
	 */
	public static ArtifactId createArtifact(String artifactName, Class<? extends Artifact> artifactClass) {
		
		try {
			staticLogger.debug("Creating artifact "+artifactName+" of class "+artifactClass);
			ArtifactId art = staticCtx.makeArtifact(artifactName, artifactClass.getName());
			return art;
			
		} catch (CartagoException e) {
			// if artifact already exists, try to retrieve it
			staticLogger.debug(e.getMessage());
			staticLogger.debug("Artifact exists, trying to retrieve artifact "+artifactName+" of class "+artifactClass);
			try {
				return staticCtx.lookupArtifact(artifactName);
			} catch (CartagoException e1) {
				staticLogger.debug("Error in retrieving artifact "+artifactName+" of class "+artifactClass);
				e1.printStackTrace();
				return null;
			}			
		}
	}
	
	/**
	 * Enacting must be a static method, a Behaviour cannot interact directly with artifacts indeed.
	 * To enacting as "User", offeredPlayerBehaviour can be null.
	 * @param roleName
	 * @param artifact
	 * @param offeredPlayerBehaviour
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Role> T enact(String roleName, ArtifactId artifact, IPlayer player) {
		OpFeedbackParam<Role> r = new OpFeedbackParam<Role>();
		try {
			staticCtx.doAction(artifact, new Op(CommunicationArtifact.ENACT, roleName, player, r));
			staticLogger.debug("Enactment completed succesfully.");
			T a = (T) r.get();
			a.fillRoleActions();			
			return a;
		} catch (CartagoException e) {
			staticLogger.error("Error in enacting artifact "+artifact);
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	
	
	protected void fillRoleActions() {
	    ArrayList<Method> methods = new ArrayList<Method>(Arrays.asList(this.getClass().getDeclaredMethods()));
        for (Method method : methods) {
            if (method.isAnnotationPresent(RoleAction.class)) {
               roleActions.add(method);               
            }
        }
	}
	

	private boolean checkActionInRoleDefinition(Op action) {
		boolean ok = false;
		for (Method m : roleActions) {
			if (m.getName().equals(action.getName()))
				ok = true;
		}
		return ok;
	}
	
	
	// Code from cartago.util.agent.Agent
	protected String getAgentName(){
		return ctx.getName();
	}

	protected ActionFeedback doActionAsync(Op op) throws CartagoException {
		return ctx.doActionAsync(op);
	}


	protected ActionFeedback doActionAsync(ArtifactId aid, Op op, long timeout) throws CartagoException {
		return ctx.doActionAsync(aid, op, timeout);
	}

	protected ActionFeedback doActionAsync(Op op, long timeout) throws CartagoException {
		return ctx.doActionAsync(op, timeout);
	}

	protected void doAction(Op op, long timeout) throws ActionFailedException, CartagoException {
		staticLogger.debug("Executing action "+op+" on context "+ctx.getName());
		ctx.doAction(op, timeout);
	}

	protected void doAction(Op op) throws ActionFailedException, CartagoException {
		this.doAction(op, -1);
	}

	protected void doAction(ArtifactId aid, Op op, long timeout) throws ActionFailedException, CartagoException {
		if (checkActionInRoleDefinition(op)) {
			staticLogger.debug("Executing action "+op+" on artifact "+aid.getName()+" and context "+ctx.getName());
			ctx.doAction(aid, op, timeout);
		}
		else
			staticLogger.debug("Action "+op+" on artifact "+aid.getName()+" and context "+ctx.getName()+" not executed: action not found for"
					+ "role "+this);
		
	}

	protected void doAction(ArtifactId aid, Op op) throws ActionFailedException, CartagoException {
		this.doAction(aid,op,-1);
	}

	protected Percept fetchPercept() throws InterruptedException {
		return ctx.fetchPercept();
	}

	protected Percept waitPercept() throws InterruptedException {
		return ctx.waitForPercept();
	}

	protected Percept waitForPercept(IEventFilter filter) throws InterruptedException {
		return ctx.waitForPercept(filter);
	}

	protected void log(String msg){
		System.out.println("["+ctx.getName()+"] "+msg);
	}


	//Utility methods

	protected WorkspaceId joinWorkspace(String wspName, AgentCredential cred) throws CartagoException {
		return ctx.joinWorkspace(wspName, cred);
	}

	protected WorkspaceId joinRemoteWorkspace(String wspName, String address, String roleName, AgentCredential cred)  throws CartagoException {
		return ctx.joinRemoteWorkspace(wspName, address, roleName, cred);
	}

	protected ArtifactId lookupArtifact(String artifactName) throws CartagoException {
		return ctx.lookupArtifact(artifactName);
	}

	protected ArtifactId makeArtifact(String artifactName, String templateName) throws CartagoException {
		return ctx.makeArtifact(artifactName, templateName);
	}

	protected ArtifactId makeArtifact(String artifactName, String templateName, Object[] params) throws CartagoException {
		return ctx.makeArtifact(artifactName, templateName, params);
	}

	protected void disposeArtifact(ArtifactId artifactId) throws CartagoException {
		ctx.disposeArtifact(artifactId);
	}

	protected void focus(ArtifactId artifactId) throws CartagoException {
		ctx.focus(artifactId);
	}

	protected void focus(ArtifactId artifactId, IEventFilter filter) throws CartagoException {
		ctx.focus(artifactId, filter);
	}

	protected void stopFocus(ArtifactId aid) throws CartagoException {
		ctx.stopFocus(aid);
	}
	
	protected ArtifactObsProperty getObsProperty(String name){
		return ctx.getObsProperty(name);
	}

}

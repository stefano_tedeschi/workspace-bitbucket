package twocomm.test.jade.persistence;

import twocomm.core.RoleId;
import twocomm.core.SQLiteSocialState;
import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoService;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.persistence.PersistentSocialState;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import twocomm.test.jade.automated.singleagent.AgentAutomated;
import twocomm.test.jade.automated.singleagent.AutomatedSocialStateTest;
import twocomm.test.jade.persistence.ProtocolArtifactPersistent;

/**
 *
 * @author Stefano Tedeschi
 */
public class PersistentSocialStateTest {

	public static Logger logger = LogManager.getLogger(PersistentSocialStateTest.class);

	public static void main(String[] args) {

		try {
			CartagoService.startNode();
			logger.trace("Inizio");
			Runtime rt = Runtime.instance();
			Profile p = new ProfileImpl();
			p.setParameter("gui", "false");
			ContainerController cc = rt.createMainContainer(p);
			AgentController agent = cc.createNewAgent("AgentPersistent-1", AgentPersistent.class.getCanonicalName(),
					null);
			agent.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

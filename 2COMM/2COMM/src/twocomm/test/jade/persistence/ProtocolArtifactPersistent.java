package twocomm.test.jade.persistence;

import static java.lang.Thread.sleep;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.CartagoService;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.JoSQLSocialState;
import twocomm.core.LifeCycleState;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;
import twocomm.core.SocialState;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.ProtocolArtifact.ProtocolObserver;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.persistence.PersistentSocialState;
import twocomm.core.typing.RoleType;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import twocomm.test.jade.persistence.ProtocolArtifactPersistent.Dummy;

public class ProtocolArtifactPersistent extends ProtocolArtifact {

	static Logger logger = LogManager.getLogger(ProtocolArtifactPersistent.class);
	
	// i nomi dei ruoli devono essere maiuscoli: saranno usati per il nome delle classi interne
	// a loro associati, ed esse iniziano sempre in maiuscolo
	public static String ARTIFACT_TYPE = "TEST";	
	public static String DUMMY_ROLE = "Dummy";
	
	static {
		addEnabledRole(DUMMY_ROLE, Dummy.class);
	}
	
	//durante la creazione dell'artefatto si modifica l'istanza di stato sociale associata
	//di default viene associata ad ogni ProtocolArtifact un'istanza di SocialState
	//in questo caso si sostituisce con un'istanza di AutomatedSocialState
	public ProtocolArtifactPersistent() {
		super();
		socialState = new PersistentSocialState(this);
	}
	
	// ROLES OPERATIONS
	
	@OPERATION
	public void assertSimpleFact(String fact) throws InterruptedException, MissingOperandException {
		Fact f = new Fact(fact);
		assertFact(f);
	}
	
	@OPERATION
	public void createCommitments() throws MissingOperandException, WrongOperandsNumberException {
		
		logger.trace("Inserimento dei commitment...");
		
		RoleId debtor;
	    RoleId creditor;
	    LogicalExpression antecedent;
	    LogicalExpression consequent;
	    Commitment c;

	    debtor = new RoleId("Caio");
	    creditor = new RoleId("Tizio");
	    antecedent = new CompositeExpression(LogicalOperatorType.AND, new Fact("A"), new Fact("B"));
	    consequent = new CompositeExpression(LogicalOperatorType.OR, new Fact("C"), new Fact("D"));
	    Commitment c1 = new Commitment(debtor, creditor, antecedent, consequent);
	    createCommitment(c1);

	    debtor = new RoleId("Lucano");
	    creditor = new RoleId("Petronio");
	    antecedent = new CompositeExpression(LogicalOperatorType.THEN, new Fact("X"), new Fact("Y"));
	    consequent = new CompositeExpression(LogicalOperatorType.AND, new Fact("W"), new Fact("Z"));
	    c = new Commitment(debtor, creditor, antecedent, consequent);
	    createCommitment(c);

	    debtor = new RoleId("Cesare");
	    creditor = new RoleId("Augusto");
	    antecedent = new CompositeExpression(LogicalOperatorType.OR, new Fact("X"), new Fact("Y"));
	    consequent = new CompositeExpression(LogicalOperatorType.NOT, new Fact("R"), null);
	    c = new Commitment(debtor, creditor, antecedent, consequent);
	    createCommitment(c);

	    debtor = new RoleId("Topolino");
	    creditor = new RoleId("Paperino");
	    antecedent = new CompositeExpression(LogicalOperatorType.OR, new CompositeExpression(LogicalOperatorType.OR, new Fact("A"), new CompositeExpression(LogicalOperatorType.THEN, new Fact("B"), new Fact("C"))), new CompositeExpression(LogicalOperatorType.NOT, new Fact("D"), null));
	    consequent = new CompositeExpression(LogicalOperatorType.THEN, new CompositeExpression(LogicalOperatorType.AND, new Fact("X"), new CompositeExpression(LogicalOperatorType.OR, new Fact("Y"), new Fact("Z"))), new Fact("W"));
	    c = new Commitment(debtor, creditor, antecedent, consequent);
	    createCommitment(c);
	    
	    socialState.changeCommitmentStatus(c1, LifeCycleState.SATISFIED);
		
	}
	
	@OPERATION
	public void saveAndLoad() throws MissingOperandException, InterruptedException, CommitmentNotFoundInInteractionStateException {
	    
		logger.trace("Salvataggio e caricamento...");
		
		((PersistentSocialState)socialState).saveSocialState();
	    ((PersistentSocialState)socialState).loadSocialState();	

	}
	
	@OPERATION
	public void printSocialState() throws InterruptedException {
		
		sleep(1000);
		System.out.println(socialState);

	}
	
	@Override
	public String getArtifactType() {
		// TODO Auto-generated method stub
		return null;
	}

	public class Dummy extends PARole {

		public Dummy(IPlayer player) {
			super(DUMMY_ROLE, player);
			// TODO Auto-generated constructor stub
		}
		
		public void assertSimpleFact(String fact) {
			try {
				doAction(this.getArtifactId(), new Op("assertSimpleFact", fact));
			} catch (ActionFailedException e) {
				logger.error("Action ASSERT-SIMPLE-FACT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void createCommitments() {
			try {
				doAction(this.getArtifactId(), new Op("createCommitments"));
			} catch (ActionFailedException e) {
				logger.error("Action CREATE-COMMITMENTS failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void saveAndLoad() {
			try {
				doAction(this.getArtifactId(), new Op("saveAndLoad"));
			} catch (ActionFailedException e) {
				logger.error("Action SAVE-AND-LOAD failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void printSocialState() {
			try {
				doAction(this.getArtifactId(), new Op("printSocialState"));
			} catch (ActionFailedException e) {
				logger.error("Action PRINT-SOCIAL-STATE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public SocialState getSocialState() {
			return socialState;
		}
		
	}
	
	public interface TestProtocolArtifactObserver extends ProtocolObserver {
		
	}
	
}

package twocomm.test.jade.base;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.awt.Rectangle;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import cartago.CartagoException;
import cartago.CartagoService;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.RoleMessage;
import twocomm.core.SocialEvent;
import twocomm.core.SocialState;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.Fact;
//import twocomm.core.typing.SocialPointBehaviourType;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import twocomm.exception.MissingOperandException;
import twocomm.test.jade.base.DummyProtocol.Dummy;
import twocomm.test.jade.base.DummyProtocol.TestProtocolArtifactObserver;

public class DummyAgent extends Agent {

	public static final String ARTIFACT_NAME = "TEST";
	public Logger logger = LogManager.getLogger(DummyAgent.class);

	private Dummy role = null;
	
	protected void setup() {
		addBehaviour(new DummyBehaviour());
	}

	//@SocialPointBehaviourType(agentSocialPoints = AgentTypeDummy.class)
	public class DummyBehaviour extends OneShotBehaviour implements TestProtocolArtifactObserver {

		@Override
		public void handleEvent(SocialEvent e, Object... args) {
			System.out.println("Sono "+this.getBehaviourName()+", ruolo "+role+", "
					+ "Evento "+e+", argomenti in numero "+args.length);
			
		}

		

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			ArtifactId art = Role.createArtifact(ARTIFACT_NAME,
					DummyProtocol.class);
			try {
				Thread.sleep((long) (Math.random()*1100));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			role = (Dummy) (Role.enact(DummyProtocol.DUMMY_ROLE,
					art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			System.out.println("Ruolo "+role);
			role.startObserving(this);
			
			role.assertSimpleFact("Fact Simple By "+this.getBehaviourName());
			
		}
		
		
			
		
		
	}

}

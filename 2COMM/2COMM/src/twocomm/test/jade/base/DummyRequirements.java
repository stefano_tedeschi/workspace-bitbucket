package twocomm.test.jade.base;

import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleRequirements;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class DummyRequirements extends RoleRequirements { 
	
	public DummyRequirements() throws MissingOperandException, WrongOperandsNumberException {			
		super("DummyRequirements", 
				new Commitment[]{
				new Commitment(DummyProtocol.DUMMY_ROLE,
						DummyProtocol.FAKE_ROLE,
						"test")
				}, null);
		
	}
	
}
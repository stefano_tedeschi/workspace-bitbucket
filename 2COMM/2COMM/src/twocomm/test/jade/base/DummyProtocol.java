package twocomm.test.jade.base;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.CartagoService;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.SocialState;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.ProtocolArtifact.ProtocolObserver;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
//import twocomm.core.typing.RoleType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import twocomm.test.jade.base.DummyProtocol.Dummy;

public class DummyProtocol extends ProtocolArtifact {

	static Logger logger = LogManager.getLogger(DummyProtocol.class);
	
	// i nomi dei ruoli devono essere maiuscoli: saranno usati per il nome delle classi interne
	// a loro associati, ed esse iniziano sempre in maiuscolo
	public static String ARTIFACT_TYPE = "TEST";	
	public static String DUMMY_ROLE = "Dummy";
	public static String FAKE_ROLE = "Fake";	
	
	static {
		addEnabledRole(DUMMY_ROLE, Dummy.class);
		addEnabledRole(FAKE_ROLE, Fake.class);
	}
	
	// ROLES OPERATIONS
	
	@OPERATION
	public void assertSimpleFact(String fact) {
		RoleId executor = getRoleIdByRoleName(getOpUserName());
		RoleId dest = new RoleId(DUMMY_ROLE, RoleId.GROUP_ROLE);		
		
		try {
			assertFact(new Fact(fact, executor));
			
			createAllCommitments(new Commitment(executor, dest, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			
			logger.trace("OPERATION PERFORMED: CFP by "+executor);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			logger.trace("Inizio");
			testAgents();
			
//			testExpressionTree();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "true");
		ContainerController cc = rt.createMainContainer(p);
		AgentController senderAgent1 = cc.createNewAgent("DummyAgent-1",
				DummyAgent.class.getCanonicalName(), null);
		AgentController senderAgent2 = cc.createNewAgent("DummyAgent-2",
				DummyAgent.class.getCanonicalName(), null);
		
		senderAgent1.start();
		senderAgent2.start();
	}
	
	@Override
	public String getArtifactType() {
		// TODO Auto-generated method stub
		return null;
	}

	
	//@RoleType(requirements = DummyRequirements.class)
	public class Dummy extends PARole {

		public Dummy(IPlayer player) {
			super(DUMMY_ROLE, player);
			// TODO Auto-generated constructor stub
		}
		
		public void assertSimpleFact(String fact) {
			try {
				doAction(this.getArtifactId(), new Op("assertSimpleFact", fact));
			} catch (ActionFailedException e) {
				logger.error("Action ASSERT-SIMPLE-FACT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public SocialState getSocialState() {
			return socialState;
		}
		
	}
	
	public class Fake extends PARole {

		public Fake(IPlayer player) {
			super(FAKE_ROLE, player);
			// TODO Auto-generated constructor stub
		}
		
	}
	
	public interface TestProtocolArtifactObserver extends ProtocolObserver {
		
	}
	
}

package twocomm.test.jade.automated.singleagent;

import static java.lang.Thread.sleep;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.CartagoService;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import twocomm.core.Commitment;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.*;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import twocomm.test.jade.automated.singleagent.AgentAutomated;
import twocomm.test.jade.automated.singleagent.ProtocolArtifactAutomated;

/**
 * @author Stefano Tedeschi
 */
public class AutomatedSocialStateTest {

	public static Logger logger = LogManager.getLogger(AutomatedSocialStateTest.class);

	public static void main(String[] args) {

		try {
			CartagoService.startNode();
			logger.trace("Inizio");
			Runtime rt = Runtime.instance();
			Profile p = new ProfileImpl();
			p.setParameter("gui", "false");
			ContainerController cc = rt.createMainContainer(p);
			AgentController agent = cc.createNewAgent("AgentAutomated-1",
					AgentAutomated.class.getCanonicalName(), null);
			agent.start();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}

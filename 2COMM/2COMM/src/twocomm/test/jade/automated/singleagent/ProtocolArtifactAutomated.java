package twocomm.test.jade.automated.singleagent;

import static java.lang.Thread.sleep;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.CartagoService;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;
import twocomm.core.SocialState;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.ProtocolArtifact.ProtocolObserver;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import twocomm.test.jade.automated.singleagent.ProtocolArtifactAutomated.Dummy;

public class ProtocolArtifactAutomated extends ProtocolArtifact {

	static Logger logger = LogManager.getLogger(ProtocolArtifactAutomated.class);
	
	// i nomi dei ruoli devono essere maiuscoli: saranno usati per il nome delle classi interne
	// a loro associati, ed esse iniziano sempre in maiuscolo
	public static String ARTIFACT_TYPE = "TEST";	
	public static String DUMMY_ROLE = "Dummy";
	
	static {
		addEnabledRole(DUMMY_ROLE, Dummy.class);
	}
	
	//durante la creazione dell'artefatto si modifica l'istanza di stato sociale associata
	//di default viene associata ad ogni ProtocolArtifact un'istanza di SocialState
	//in questo caso si sostituisce con un'istanza di AutomatedSocialState
	public ProtocolArtifactAutomated() {
		super();
		socialState = new AutomatedSocialState(this);
	}
	
	// ROLES OPERATIONS
	
	@OPERATION
	public void assertSimpleFact(String fact) throws InterruptedException, MissingOperandException {
		Fact f = new Fact(fact);
		assertFact(f);
	}
	
	@OPERATION
	public void createCommitments() throws MissingOperandException, WrongOperandsNumberException {
		
		logger.trace("Inserimento dei commitment...");
		
		LogicalExpression antecedent;
		LogicalExpression consequent;
		Commitment c;

		antecedent = new CompositeExpression(LogicalOperatorType.AND, new Fact("A"), new Fact("B"));
		consequent = new CompositeExpression(LogicalOperatorType.OR, new Fact("C"),	new Fact("D"));
		c = new Commitment(new RoleId("Tizio"), new RoleId("Caio"), antecedent, consequent);
		createCommitment(c);

		antecedent = new CompositeExpression(LogicalOperatorType.THEN, new Fact("X"), new Fact("Y"));
		consequent = new CompositeExpression(LogicalOperatorType.AND, new Fact("W"), new Fact("Z"));
		c = new Commitment(new RoleId("Lucano"), new RoleId("Petronio"), antecedent, consequent);
		createCommitment(c);

		antecedent = new CompositeExpression(LogicalOperatorType.OR, new Fact("X"), new Fact("Y"));
		consequent = new CompositeExpression(LogicalOperatorType.NOT, new Fact("R"), null);
		c = new Commitment(new RoleId("Cesare"), new RoleId("Augusto"), antecedent, consequent);
		createCommitment(c);

		antecedent = new CompositeExpression(LogicalOperatorType.OR,
				new CompositeExpression(LogicalOperatorType.OR, new Fact("A"),
						new CompositeExpression(LogicalOperatorType.THEN, new Fact("B"),
								new Fact("C"))),
				new CompositeExpression(LogicalOperatorType.NOT, new Fact("D"), null));
		consequent = new CompositeExpression(LogicalOperatorType.THEN,
				new CompositeExpression(LogicalOperatorType.AND,
						new Fact("X"), new CompositeExpression(LogicalOperatorType.OR,
								new Fact("Y"), new Fact("Z"))),
				new Fact("W", new Object[0]));
		c = new Commitment(new RoleId("Topolino"), new RoleId("Paperino"), antecedent, consequent);
		createCommitment(c);

		System.out.println(socialState);
		
	}
	
	@OPERATION
	public void assertOtherFacts() throws MissingOperandException, InterruptedException {
		
		Fact f;
		
		logger.trace("Asserzione di A...");
		f = new Fact("A");
		assertFact(f);
		
		sleep(1000);

		logger.trace("Asserzione di B...");
		f = new Fact("B");
		assertFact(f);
		
		sleep(1000);

		logger.trace("Asserzione di C...");
		f = new Fact("C");
		assertFact(f);
		
		sleep(1000);

		logger.trace("Asserzione di Y...");
		f = new Fact("Y");
		assertFact(f);
		
		sleep(1000);

		logger.trace("Asserzione di R...");
		f = new Fact("R");
		assertFact(f);
		
		sleep(1000);

		logger.trace("Asserzione di X...");
		f = new Fact("X");
		assertFact(f);
		
		sleep(1000);
		
		logger.trace("Asserzione di W...");
		f = new Fact("W");
		assertFact(f);		

	}
	
	@OPERATION
	public void finalPrints() throws InterruptedException, MissingOperandException {
		
		sleep(1000);
		
		System.out.println(socialState);
		System.out.println(socialState.retrieveCommitmentsContainingFact(new Fact("A")));

	}
	
	@Override
	public String getArtifactType() {
		// TODO Auto-generated method stub
		return null;
	}

	public class Dummy extends PARole {

		public Dummy(IPlayer player) {
			super(DUMMY_ROLE, player);
			// TODO Auto-generated constructor stub
		}
		
		public void assertSimpleFact(String fact) {
			try {
				doAction(this.getArtifactId(), new Op("assertSimpleFact", fact));
			} catch (ActionFailedException e) {
				logger.error("Action ASSERT-SIMPLE-FACT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void createCommitments() {
			try {
				doAction(this.getArtifactId(), new Op("createCommitments"));
			} catch (ActionFailedException e) {
				logger.error("Action CREATE-COMMITMENTS failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void assertOtherFacts() {
			try {
				doAction(this.getArtifactId(), new Op("assertOtherFacts"));
			} catch (ActionFailedException e) {
				logger.error("Action ASSERT-OTHER-FACTS failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void finalPrints() {
			try {
				doAction(this.getArtifactId(), new Op("finalPrints"));
			} catch (ActionFailedException e) {
				logger.error("Action FINAL-PRINTS failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public SocialState getSocialState() {
			return socialState;
		}
		
	}
	
	public interface TestProtocolArtifactObserver extends ProtocolObserver {
		
	}
	
}

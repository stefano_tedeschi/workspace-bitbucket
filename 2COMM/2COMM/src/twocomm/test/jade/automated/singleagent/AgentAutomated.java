package twocomm.test.jade.automated.singleagent;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.awt.Rectangle;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import cartago.CartagoException;
import cartago.CartagoService;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.RoleMessage;
import twocomm.core.SocialEvent;
import twocomm.core.SocialState;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.Fact;
import twocomm.core.typing.BehaviourType;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import twocomm.exception.MissingOperandException;
import twocomm.test.jade.automated.multiagent.AgentTypeDummyAutomated;
import twocomm.test.jade.automated.singleagent.ProtocolArtifactAutomated.Dummy;
import twocomm.test.jade.automated.singleagent.ProtocolArtifactAutomated.TestProtocolArtifactObserver;

public class AgentAutomated extends Agent {

	private static final long serialVersionUID = 1L;
	public static final String ARTIFACT_NAME = "TEST";
	public Logger logger = LogManager.getLogger(AgentAutomated.class);

	private Dummy role = null;

	protected void setup() {
		addBehaviour(new DummyBehaviour());
	}

	public class DummyBehaviour extends OneShotBehaviour implements TestProtocolArtifactObserver {

		@Override
		public void handleEvent(SocialEvent e, Object... args) {
			logger.trace("Evento " + e);
		}

		private static final long serialVersionUID = 1L;

		@Override
		public void action() {
			ArtifactId art = Role.createArtifact(ARTIFACT_NAME, ProtocolArtifactAutomated.class);
			try {
				Thread.sleep((long) (Math.random() * 1100));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			role = (Dummy) (Role.enact(ProtocolArtifactAutomated.DUMMY_ROLE, art,
					new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			role.startObserving(this);

			role.assertSimpleFact("Fatto 1");
			role.createCommitments();
			role.assertOtherFacts();
			role.finalPrints();

		}

	}

}

package twocomm.test.jade.automated.multiagent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.CartagoService;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.RoleAction;
import twocomm.core.RoleId;
import twocomm.core.SocialState;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleType;
import twocomm.core.typing.TypedProtocolArtifact;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class DummyProtocolAutomated extends TypedProtocolArtifact {

	static Logger logger = LogManager.getLogger(DummyProtocolAutomated.class);
	
	// i nomi dei ruoli devono essere maiuscoli: saranno usati per il nome delle classi interne
	// a loro associati, ed esse iniziano sempre in maiuscolo
	public static String ARTIFACT_TYPE = "DummyProtocolAutomated";	
	public static String DUMMY_ROLE = "Dummy";
	public static String FAKE_ROLE = "Fake";
	
	public static int count = 0;
	
	static {
		addEnabledRole(DUMMY_ROLE, Dummy.class);
		addEnabledRole(FAKE_ROLE, Fake.class);
	}
	
	//durante la creazione dell'artefatto si modifica l'istanza di stato sociale associata
	//di default viene associata ad ogni ProtocolArtifact un'istanza di SocialState
	//in questo caso si sostituisce con un'istanza di AutomatedSocialState
	public DummyProtocolAutomated() {
		super();
		socialState = new AutomatedSocialState(this);
	}
	
	// ROLES OPERATIONS
	
	@OPERATION
	public void assertSimpleFact(String fact) throws InterruptedException {
		RoleId executor = getRoleIdByRoleName(getOpUserName());
		
		try {
			assertFact(new Fact(fact, executor));
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.trace("OPERATION PERFORMED: "+fact+" by "+executor);
		Thread.sleep(1000);		
	}
	
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			logger.trace("Inizio");
			testAgents();
			
//			testExpressionTree();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "true");
		ContainerController cc = rt.createMainContainer(p);
		AgentController senderAgent1 = cc.createNewAgent("DummyAgent-1",
				DummyAgentAutomated.class.getCanonicalName(), null);
		AgentController senderAgent2 = cc.createNewAgent("DummyAgent-2",
				DummyAgentAutomated.class.getCanonicalName(), null);
		
		senderAgent1.start();
		senderAgent2.start();
		
//		AgentController senderAgent3 = cc.createNewAgent("DummyAgent-3",
//				DummyAgentAutomated.class.getCanonicalName(), null);
//		
//		senderAgent3.start();
//		
//		AgentController senderAgent4 = cc.createNewAgent("DummyAgent-4",
//				DummyAgentAutomated.class.getCanonicalName(), null);
//		
//		senderAgent4.start();
		
	}
	
	@Override
	public String getArtifactType() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@RoleType(requirements = DummyRequirementsAutomated.class)
	public class Dummy extends PARole {

		public Dummy(IPlayer player) {
			super(DUMMY_ROLE, player);
			// TODO Auto-generated constructor stub
		}
		
		@RoleAction
		public void assertSimpleFact(String fact) {
			try {
				doAction(this.getArtifactId(), new Op("assertSimpleFact", fact));
			} catch (ActionFailedException e) {
				logger.error("Action ASSERT-SIMPLE-FACT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public SocialState getSocialState() {
			return socialState;
		}
		
	}
	
	public class Fake extends PARole {

		public Fake(IPlayer player) {
			super(FAKE_ROLE, player);
			// TODO Auto-generated constructor stub
		}
		
	}
	
	public interface TestProtocolArtifactObserver extends ProtocolObserver {
		
	}
	
}

package twocomm.test.jade.automated.multiagent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import twocomm.core.Role;
import twocomm.core.SocialEvent;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.typing.BehaviourType;
import twocomm.test.jade.automated.multiagent.DummyProtocolAutomated.Dummy;
import twocomm.test.jade.automated.multiagent.DummyProtocolAutomated.TestProtocolArtifactObserver;

public class DummyAgentAutomated extends Agent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String ARTIFACT_NAME = "DUMMY";
	public Logger logger = LogManager.getLogger(DummyAgentAutomated.class);

	private Dummy role = null;
	
	protected void setup() {
		addBehaviour(new MasterBehaviour());
		
	}

	
	//************* BEHAVIOUR MASTER
	public class MasterBehaviour extends OneShotBehaviour {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			ArtifactId art = Role.createArtifact(ARTIFACT_NAME,
					DummyProtocolAutomated.class);
			try {
				Thread.sleep((long) (Math.random()*1100));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DummyBehaviour b1 = new DummyBehaviour();
			DummyBehaviour2 b2 = new DummyBehaviour2();
						
			role = (Dummy) (Role.enact(DummyProtocolAutomated.DUMMY_ROLE,
					art, new JadeBehaviourPlayer(new Behaviour[]{b1, b2}, myAgent.getAID())));			
			
			addBehaviour(b1);
			addBehaviour(b2);
		}		
	}
	
	
	
	
	//********** BEHAVIOUR TIPATI
	@BehaviourType(bType = AgentTypeDummyAutomated.class)
	public class DummyBehaviour extends OneShotBehaviour implements TestProtocolArtifactObserver {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void handleEvent(SocialEvent e, Object... args) {
			
			
		}
		
		@Override
		public void action() {
			role.startObserving(this);
			role.assertSimpleFact("Fact Simple By "+this.getBehaviourName());
		}
		
	}
	
	@BehaviourType(bType = AgentTypeDummyAutomated2.class)
	public class DummyBehaviour2 extends OneShotBehaviour implements TestProtocolArtifactObserver {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void handleEvent(SocialEvent e, Object... args) {
			
			
		}

		@Override
		public void action() {
			role.startObserving(this);
			role.assertSimpleFact("Fact Simple By "+this.getBehaviourName());
		}		
		
	}

}

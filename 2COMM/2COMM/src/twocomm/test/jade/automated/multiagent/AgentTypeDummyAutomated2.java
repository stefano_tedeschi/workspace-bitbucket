package twocomm.test.jade.automated.multiagent;

import twocomm.core.Commitment;
import twocomm.core.RoleId;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.BType;
import twocomm.core.typing.ProtocolAction;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class AgentTypeDummyAutomated2 extends BType {

	public AgentTypeDummyAutomated2() throws MissingOperandException, WrongOperandsNumberException {
		super(new Commitment[]{new Commitment(DummyProtocolAutomated.DUMMY_ROLE,
				DummyProtocolAutomated.FAKE_ROLE,
				"testAntecedent",
				"testConsequent")},
				new ProtocolAction("testConsequent-Another"));
	}
}

package twocomm.test.jade.automated.multiagent;

import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleRequirements;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class DummyRequirementsAutomated extends RoleRequirements { 
	
	public DummyRequirementsAutomated() throws MissingOperandException, WrongOperandsNumberException {			
		super(DummyProtocolAutomated.DUMMY_ROLE, 
				new Commitment[]{
						new Commitment(DummyProtocolAutomated.DUMMY_ROLE,
						DummyProtocolAutomated.FAKE_ROLE,
						"testAntecedent", "testConsequent")
				}, null);
		
	}
	
}
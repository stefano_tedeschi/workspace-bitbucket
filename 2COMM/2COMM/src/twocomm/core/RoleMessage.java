package twocomm.core;

import jade.lang.acl.ACLMessage;

import java.io.Serializable;

public class RoleMessage extends ACLMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RoleId sender;
	private RoleId receiver;
	private Object content;
	
	public RoleMessage() {
		super();
	}
	
	public RoleMessage(RoleId sender, RoleId receiver, Object content) {
		this.sender = sender;
		this.receiver = receiver;
		this.content = content;
	}

	public RoleId getRoleSender() {
		return sender;
	}

	public RoleId getRoleReceiver() {
		return receiver;
	}
	
	public Object getContents() {
		return this.content;
	}
	
	public void setRoleSender(RoleId sender) {
		this.sender = sender;
	}

	public void setRoleReceiver(RoleId receiver) {
		this.receiver = receiver;
	}

	public void setContents(Object content) {
		this.content = content;
	}
	
	public String toString() {
		return "Message from "+sender+" to "+ receiver+" with content: "+content+", performative: "+getPerformative();
	}
	
	
}

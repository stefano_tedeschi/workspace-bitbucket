package twocomm.core;


public class LifeCycleOperationResult {

	private String result;
	private SocialStateElement targetStateElement;
	private LifeCycleOperationResultType resultType;
	
	public LifeCycleOperationResult() {
		super();
	}
	
	public LifeCycleOperationResult(String result, LifeCycleOperationResultType resType, SocialStateElement el) {
		this.result = result;		
		this.targetStateElement = el;
		this.resultType = resType;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public SocialStateElement getTargetStateElement() {
		return targetStateElement;
	}

	public void setTargetStateElement(SocialStateElement targetStateElement) {
		this.targetStateElement = targetStateElement;
	}

	public LifeCycleOperationResultType getResultType() {
		return resultType;
	}

	public void setResultType(LifeCycleOperationResultType resultType) {
		this.resultType = resultType;
	}

}

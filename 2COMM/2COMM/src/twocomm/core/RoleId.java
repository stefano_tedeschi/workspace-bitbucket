package twocomm.core;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

import javax.persistence.Embeddable;

@Embeddable
public class RoleId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	public static final int INDIVIDUAL_ROLE = 1;
	public static final int GROUP_ROLE = 2;
	private static AtomicLong nextId = new AtomicLong();
	private String roleName;
	private int type;
	private Role myRole;
	
	public RoleId(String roleName, Role myRole, int type) {
		id = nextId.incrementAndGet();
		this.roleName = roleName;
		this.type = type;		
		this.myRole = myRole;
	}
	
	public RoleId(String roleName, int type) {
		id = nextId.incrementAndGet();
		this.roleName = roleName;
		this.type = type;		
		this.myRole = null;
	}
	
	
	public RoleId(String roleName, Role myRole) {
		id = nextId.incrementAndGet();
		this.roleName = roleName;
		this.type = INDIVIDUAL_ROLE;	
		this.myRole = myRole;
	}
	
	public RoleId(String roleName) {
		id = nextId.incrementAndGet();
		this.roleName = roleName;
		this.type = GROUP_ROLE;		
		this.myRole = null;
	}
	
	public long getId() {
		return id;
	}
	
	public int getType() {
		return type;
	}
	
	public String getRoleName() {
		return roleName;
	}
	
	public String getPlayerName() {
		return myRole.player.getPlayerName();
	}
	
	public String toString() {
		if (type == GROUP_ROLE) {
			return roleName;
		}
		else if (type == INDIVIDUAL_ROLE) {
			return roleName+id;
		}
		else return "";
	}
	
	public boolean equals(RoleId otherRoleId) {
		switch (this.type) {
		case INDIVIDUAL_ROLE:
			switch (otherRoleId.type) {
			case INDIVIDUAL_ROLE:
				return this.id == otherRoleId.id;
								
			case GROUP_ROLE:
				return this.getRoleName().equals(otherRoleId.getRoleName());
			}
		break;
		
		case GROUP_ROLE:
			switch (otherRoleId.type) {
			case INDIVIDUAL_ROLE:
				return this.getRoleName().equals(otherRoleId.getRoleName());
								
			case GROUP_ROLE:
				return this.getRoleName().equals(otherRoleId.getRoleName());
			}
		
		break;		
		
		}
		return false;		
	}

	public Role getRole() {
		return this.myRole;
	}
	
}

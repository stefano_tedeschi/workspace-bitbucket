package twocomm.core.jade;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import twocomm.core.IPlayer;

public class JadeBehaviourPlayer implements IPlayer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Behaviour[] jadeBehaviour;
	private AID playerAgentId;
	
	public JadeBehaviourPlayer(Behaviour[] jadeBehaviour, AID playerAgentId) {
		super();
		this.jadeBehaviour = jadeBehaviour;
		this.playerAgentId = playerAgentId;
	}

	public Behaviour[] getJadeBehaviour() {
		return jadeBehaviour;
	}

	public AID getPlayerAgentId() {
		return playerAgentId;
	}
	
	public String getPlayerName() {
		return playerAgentId.getLocalName();
	}
	
}

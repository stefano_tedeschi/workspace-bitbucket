package twocomm.core.automated;

import twocomm.core.ProtocolArtifact.ProtocolObserver;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.OPERATION;
import twocomm.core.*;
import twocomm.core.logic.*;

/**
 * @author Stefano Tedeschi
 */
public class AutomatedSocialState extends SocialState implements ProtocolObserver {

	private static Logger logger = LogManager.getLogger(AutomatedSocialState.class);

	public AutomatedSocialState(ProtocolArtifact art) {
		super(art);
		try {
			art.addObserver(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void handleEvent(SocialEvent e, Object... args) {
		EventHandler handler = new EventHandler(this, e);
		logger.trace("Automated handling for event "+e);
		handler.start();
	}

	class EventHandler extends Thread {

		private SocialState socialState;
		private SocialEvent socialEvent;

		public EventHandler(SocialState socialState, SocialEvent socialEvent) {
			this.socialState = socialState;
			this.socialEvent = socialEvent;
		}

		@Override
		// vengono presi in considerazione solo i commitment in stato
		// conditional o detached
		public void run() {
			if (socialEvent.getAction() == SocialEventActionType.FACT_ASSERTED) {
				logger.trace("Retrieving commitments for "+socialEvent.getElementChanged());
				ArrayList<Commitment> commitmentsByFact = socialState
						.retrieveCommitmentsContainingFact((Fact) socialEvent.getElementChanged());
				
				if (commitmentsByFact != null) {
					for (Commitment c : commitmentsByFact) {
						synchronized (c) {
							if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
								update((LogicalExpression) c.getAntecedent());
								if (((LogicalExpression) c.getAntecedent()).isVerified()) {
									socialState.changeCommitmentStatus(c, LifeCycleState.DETACHED);
									socialState.notifyObs(new SocialEvent(c, SocialEventActionType.DETACH_COMMITMENT));
									logger.trace("Commitment changed to detached: "+c);
								}
							}
							if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL
									|| c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
								update((LogicalExpression) c.getConsequent());
								if (((LogicalExpression) c.getConsequent()).isVerified()) {
									socialState.changeCommitmentStatus(c, LifeCycleState.SATISFIED);
									socialState.notifyObs(new SocialEvent(c, SocialEventActionType.SATISFY_COMMITMENT));
									logger.trace("Commitment changed to satisfied: "+c);
								} else if (((LogicalExpression) c.getConsequent()).isViolated()) {
									socialState.changeCommitmentStatus(c, LifeCycleState.VIOLATED);
									socialState.notifyObs(new SocialEvent(c, SocialEventActionType.CANCEL_COMMITMENT));
									logger.trace("Commitment changed to violated: "+c);
								}
							}
						}
					}
				}
			}
		}

		// metodo che aggiorna i flag delle espressioni logiche
		private void update(LogicalExpression e) {
			if (e.getLogExpType() == LogicalExpressionType.FACT) {
				if (((Fact) socialEvent.getElementChanged()).equals(e)) {
					logger.trace("Expression *" + e + "* verified.");
					e.setVerified(true);
				}
			} else {
				if (e.getLeft() != null && !e.getLeft().isVerified() && !e.getLeft().isViolated()) {
					update(e.getLeft());
				}
				if (e.getRight() != null && !e.getRight().isVerified() && !e.getRight().isViolated()) {
					update(e.getRight());
				}
				switch (((CompositeExpression) e).getOperator()) {
				case AND:
					if (e.getLeft().isVerified() && e.getRight().isVerified()) {
						logger.trace("AND Expression *" + e + "* verified.");
						e.setVerified(true);
					} else if (e.getLeft().isViolated() || e.getRight().isViolated()) {
						logger.trace("AND Expression *" + e + "* not satisfied.");
						e.setVerified(false);
					}
					break;
				case OR:
					if (e.getLeft().isVerified() || e.getRight().isVerified()) {
						logger.trace("OR Expression *" + e + "* verified.");
						e.setVerified(true);
					} else if (e.getLeft().isViolated() && e.getRight().isViolated()) {
						logger.trace("OR Expression *" + e + "* not satisfied.");
						e.setVerified(false);
					}
					break;
				case NOT:
					if (e.getLeft().isVerified()) {
						logger.trace("NOT Expression *" + e + "* not satisfied.");
						e.setVerified(false);
					}
					break;
				case THEN:
					if (e.getLeft().isVerified() && e.getRight().isVerified() && !e.isViolated()) {
						logger.trace("THEN Expression *" + e + "* verified.");
						e.setVerified(true);
					} else if (!e.getLeft().isVerified() && e.getRight().isVerified()) {
						logger.trace("THEN Expression *" + e + "* not satisfied.");
						e.setVerified(false);
					}
					break;
				default:
					break;
				}
			}
		}

	}

}

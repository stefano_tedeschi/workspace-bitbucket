package twocomm.core.automated;

import twocomm.core.ProtocolArtifact.ProtocolObserver;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.*;
import twocomm.core.logic.*;

/**
 * @author Stefano Tedeschi
 */
public class AutomatedSocialStateSingleThreaded extends SocialState implements ProtocolObserver {

	private static final long serialVersionUID = 1L;
	private static Logger logger = LogManager.getLogger(AutomatedSocialStateSingleThreaded.class);

	public AutomatedSocialStateSingleThreaded(ProtocolArtifact art) {
		super(art);
		try {
			art.addObserver(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleEvent(SocialEvent socialEvent, Object... args) {
		logger.trace("Automated handling for event " + socialEvent);
		if (socialEvent.getAction() == SocialEventActionType.FACT_ASSERTED) {
			logger.trace("Retrieving commitments for " + socialEvent.getElementChanged());
			ArrayList<Commitment> commitmentsByFact = retrieveCommitmentsContainingFact(
					(Fact) socialEvent.getElementChanged());
			if (commitmentsByFact != null) {
				for (Commitment c : commitmentsByFact) {
					if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
						update(socialEvent, (LogicalExpression) c.getAntecedent());
						if (((LogicalExpression) c.getAntecedent()).isVerified()) {
							changeCommitmentStatus(c, LifeCycleState.DETACHED);
							notifyObs(new SocialEvent(c, SocialEventActionType.DETACH_COMMITMENT));
							logger.trace("Commitment changed to detached: " + c);
						}
					}
					if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL
							|| c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
						update(socialEvent, (LogicalExpression) c.getConsequent());
						if (((LogicalExpression) c.getConsequent()).isVerified()) {
							changeCommitmentStatus(c, LifeCycleState.SATISFIED);
							notifyObs(new SocialEvent(c, SocialEventActionType.SATISFY_COMMITMENT));
							logger.trace("Commitment changed to satisfied: " + c);
						} else if (((LogicalExpression) c.getConsequent()).isViolated()) {
							changeCommitmentStatus(c, LifeCycleState.VIOLATED);
							logger.trace("Commitment changed to violated: " + c);
						}
					}
				}
			}
		}
	}

	// metodo che aggiorna i flag delle espressioni logiche
	private void update(SocialEvent socialEvent, LogicalExpression e) {
		if (e.getLogExpType() == LogicalExpressionType.FACT) {
			if (((Fact) socialEvent.getElementChanged()).equals(e)) {
				logger.trace("Expression *" + e + "* verified.");
				e.setVerified(true);
			}
		} else {
			if (e.getLeft() != null && !e.getLeft().isVerified() && !e.getLeft().isViolated()) {
				update(socialEvent, e.getLeft());
			}
			if (e.getRight() != null && !e.getRight().isVerified() && !e.getRight().isViolated()) {
				update(socialEvent, e.getRight());
			}
			switch (((CompositeExpression) e).getOperator()) {
			case AND:
				if (e.getLeft().isVerified() && e.getRight().isVerified()) {
					logger.trace("AND Expression *" + e + "* verified.");
					e.setVerified(true);
				} else if (e.getLeft().isViolated() || e.getRight().isViolated()) {
					logger.trace("AND Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			case OR:
				if (e.getLeft().isVerified() || e.getRight().isVerified()) {
					logger.trace("OR Expression *" + e + "* verified.");
					e.setVerified(true);
				} else if (e.getLeft().isViolated() && e.getRight().isViolated()) {
					logger.trace("OR Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			case NOT:
				if (e.getLeft().isVerified()) {
					logger.trace("NOT Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			case THEN:
				if (e.getLeft().isVerified() && e.getRight().isVerified() && !e.isViolated()) {
					logger.trace("THEN Expression *" + e + "* verified.");
					e.setVerified(true);
				} else if (!e.getLeft().isVerified() && e.getRight().isVerified()) {
					logger.trace("THEN Expression *" + e + "* not satisfied.");
					e.setVerified(false);
				}
				break;
			default:
				break;
			}
		}
	}

}

package twocomm.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.OpFeedbackParam;
import cartago.tools.AbstractTupleSpace;
import cartago.util.agent.ActionFailedException;
import twocomm.core.jacamo.JasonAgentPlayer;
import twocomm.core.jade.JadeBehaviourPlayer;

public abstract class CommunicationArtifact extends AbstractTupleSpace {

	protected static final Logger logger = LogManager.getLogger(CommunicationArtifact.class);
	
	public static final String ENACT = "enact";
	public static final String DEACT = "deact";
	public static String ARTIFACT_TYPE = "CommunicationArtifact";
	public static String CA_ROLE = "CARole";
	public static String PA_ROLE = "PARole";
	
	
	/**
	 * List of enabled Roles for this artifact.
	 */
	protected static HashMap<String, Class<? extends Role>> enabledRoles = new  HashMap<String, Class<? extends Role>>();
	
	static {
		addEnabledRole(CA_ROLE, CARole.class);	
	}
	
	/**
	 * List of currently enacted Roles for this artifact.
	 */
	protected ArrayList<Role> enactedRoles = new ArrayList<Role>();
	
	
	public CommunicationArtifact() {
		super();
	}
	
	protected RoleId getRoleIdByPlayerName(String playerName) {
		ArrayList<RoleId> arr = getEnactedRolesIds();
		for (RoleId r : arr) {
			if (r.getPlayerName().equals(playerName))
				return r;
		}
		return null;
	}
	
	protected RoleId getRoleIdByRoleName(String roleIdName) {
		ArrayList<RoleId> arr = getEnactedRolesIds();
		for (RoleId r : arr) {
			if (r.toString().equals(roleIdName))
				return r;
		}
		return null;
	}
	
	public ArrayList<RoleId> getRoleIdByGenericRoleName(String roleName) {
		ArrayList<RoleId> arr = new ArrayList<RoleId>();
		for (Role r : enactedRoles) {
			if (r.getRoleId().getRoleName().equals(roleName))
				arr.add(r.getRoleId());
		}
		return arr;
	}
	
	public ArrayList<RoleId> getEnactedRolesIds() {
		ArrayList<RoleId> arr = new ArrayList<RoleId>();
		for (Role r : enactedRoles) {
			arr.add(r.getRoleId());
		}
		return arr;
	}
	
	public abstract String getArtifactType();
	
	/**
	 * Method mandatory to add enabled Roles to this communication artifact.
	 * @param roleName
	 */
	protected static void addEnabledRole(String roleName, Class<? extends Role> roleType) {
		enabledRoles.put(roleName, roleType);
	}
	
	// ENACT per JADE
	@OPERATION 
	protected void enact(String roleName, IPlayer player, OpFeedbackParam<Role> role) {
		logger.debug("Enactment of ROLE: "+roleName+" for PLAYER: "+player.getPlayerName());
		if (roleName.equals(CA_ROLE)) {
			role.set(this.new CARole(CA_ROLE, player));
			enactedRoles.add(role.get());
			logger.debug("CA_ROLE detected: Enactment done.");
			return;
		}
		if (checkRoleRequirements(roleName, player)) {
			// set the requested role; need to load class and instantiate a role
			Class<? extends Role> a = enabledRoles.get(roleName);
			try {
				logger.debug("Retrieving instance for role "+a);
				Constructor<? extends Role> cons = a.getDeclaredConstructor(new Class[] {this.getClass(), IPlayer.class});
				Role newRole = cons.newInstance(this, player);
				
				role.set(newRole);
				enactedRoles.add(role.get());
				
				logger.trace("New role instantiated: "+newRole);
				logger.debug("Enactment done.");
				
				return;
				
				
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		else {
			logger.error("Enactment failed: Wrong Role Type.");
			role.set(null);
		}
	}
	
	// ENACT per Jason
	@OPERATION
	protected void enact(String roleName) {
		//String agentName = getOpUserId().getAgentName();
		String agentName = getCurrentOpAgentId().getAgentName();
		JasonAgentPlayer player = new JasonAgentPlayer(agentName);
		Role r;
		if (roleName.equals(CA_ROLE)) {
			r = this.new CARole(CA_ROLE, player);
			enactedRoles.add(r);
			logger.debug("Enactment done.");
			logger.trace("Enactment done, updated enacted roles set: ");
			for (RoleId id : getEnactedRolesIds()) {
				logger.trace(id.toString() + " - "+id.getRoleName() +" played by "+id.getPlayerName());
			}
			defineObsProperty(roleName, r.getRoleId().toString());
			return;
		}
		else if (checkRoleRequirements(roleName, player)) {
			// set the requested role; need to load class and instantiate a role
			Class<? extends Role> a = enabledRoles.get(roleName);
			try {
				Constructor<? extends Role> cons = a.getDeclaredConstructor(new Class[] {this.getClass(), String.class, IPlayer.class});
				Role newRole = cons.newInstance(this, agentName, player);
				
				enactedRoles.add(newRole);
				//signal(getOpUserId(), "enacted", getOpUserId().getAgentName(), roleName, newRole.toString());
				signal(getCurrentOpAgentId(), "enacted", getCurrentOpAgentId().getAgentName(), roleName, newRole.toString());
				logger.trace("Enactment done, updated enacted roles set: ");
				for (RoleId id : getEnactedRolesIds()) {
					logger.trace(id.toString() + " - "+id.getRoleName() +" played by "+id.getPlayerName()+" whose agentId is "+id.getPlayerName());
				}
				defineObsProperty(roleName, newRole.getRoleId().toString());
				return;
				
				
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		else {
			logger.error("Enactment failed: Wrong Role Type.");
			failed("Wrong role type.");
		}
	}
	
	@OPERATION
	protected void deact(String roleName, Role role) {
		
	}
	
	@OPERATION
	protected void send(RoleMessage message) {
		// must register the sender of the message
		logger.debug("Sending "+message);
		if (message.getRoleReceiver().getType() == RoleId.GROUP_ROLE) {
			for (Role r : enactedRoles)
				// conditions: do not send the message to yourself, and send message to roles with the right generic type
				if (!(r.getRoleId().equals(message.getSender())) && r.getRoleId().equals(message.getRoleReceiver()))
					outImpl(r.getRoleId()+"."+message.getPerformative(), message);
		}
		else
			outImpl(message.getRoleReceiver().toString()+"."+message.getPerformative(), message);
	}
	
//	@OPERATION
//	protected void receive(RoleId receiverRoleId, OpFeedbackParam<RoleMessage> message) {
//		inImpl(receiverRoleId.toString(), message);
//		logger.debug("Received from "+receiverRoleId +" message "+message.get());
//		
//	}
	
	@OPERATION
	protected void receive(RoleId receiverRoleId, int performative, OpFeedbackParam<RoleMessage> message) {
		inImpl(receiverRoleId+"."+performative, message);
		logger.debug("Received from "+receiverRoleId +" message "+message.get());
	}
	
	@OPERATION
	protected void receiveNoBlock(RoleId receiverRoleId, OpFeedbackParam<RoleMessage> message) {
		inpImpl(receiverRoleId.toString()+".11", message);
		logger.debug("Received non-blocking from "+receiverRoleId);		
	}
	
	@OPERATION
	protected void receiveNoBlockPerf(RoleId receiverRoleId, int performative, OpFeedbackParam<RoleMessage> message) {
		inpImpl(receiverRoleId+"."+performative, message);
		logger.debug("Received non-blocking from "+receiverRoleId);		
	}
	
	
	
	
	/*
	 * Test whether proposed Role match requirements for this artifact.
	 */
//	protected boolean checkRoleRequirements(String roleName, IPlayer player) {
//		
//		if (!(player instanceof JadeBehaviourPlayer)) return true;
//		
//		JadeBehaviourPlayer jPlayer = (JadeBehaviourPlayer) player;
//		Behaviour[] offeredPlayerBehaviours = new Behaviour[]{jPlayer.getJadeBehaviour()};
//		
//		logger.debug("Checking role requirements for "+roleName);
//		// check the requested Role Name
//		if (!enabledRoles.containsKey(roleName)) {
//			logger.debug("Role "+roleName+" not found among enabled roles.");
//			return false;
//		}
//		
//		// control is excluded for role "CA_Role"
//		
//		if (roleName.equals(CA_ROLE))
//			return true;
//		
//		
//		logger.debug("Retrieving annotations for behaviour classes...");
//		Class<? extends Behaviour> behClass;
//		ArrayList<Annotation> behaviourTypeAnnotations = new ArrayList<Annotation>();
//		Annotation behaviourTypeAnnotation;
//		for (Behaviour beh : offeredPlayerBehaviours) {
//			 behClass = beh.getClass();
//			 behaviourTypeAnnotation = behClass.getAnnotation(BehaviourType.class);
//			 if (behaviourTypeAnnotation == null) {
//				logger.debug("Annotation 'BehaviourType' is missing for behaviour");
//				return false;
//			 }
//			 behaviourTypeAnnotations.add(behaviourTypeAnnotation);
//		}		
//		
//		logger.debug("Found "+behaviourTypeAnnotations.size()+" annotations.");
//		logger.debug("Done. Retrieving annotations for role class...");
//		Class<?> roleClass;
//		try {
//			String roleClassName = (this.getClass().getName())+"$"+roleName;
//			logger.debug("Retrieving class for requested "+roleClassName);
//			roleClass = Class.forName(roleClassName);
//		} catch (ClassNotFoundException e) {
//			logger.debug("Bad role name.");
//			return false;
//		}
//		Annotation roleAnnotation = roleClass.getAnnotation(RoleType.class);
//		if (roleAnnotation == null) {
//			logger.debug("Annotation 'RoleType' is missing for role");
//			return false;
//		}
//		
//		// Both annotations retrieved
//		logger.debug("Both annotations retrieved. Checking requirements compliance...");
//		
//		// Getting instances for retrieved types
//		ArrayList<Type> typesToMerge = new ArrayList<Type>();
//		Type behaviourType;
//		Type roleType;
//		Type mergedType;
//		try {
//			for (Annotation ann : behaviourTypeAnnotations) {
//				behaviourType = ((BehaviourType)ann).capabilities().getDeclaredConstructor().newInstance();
//				typesToMerge.add(behaviourType);
//			}			
//			roleType = ((RoleType)roleAnnotation).requirements().getDeclaredConstructor().newInstance();
//			logger.debug("Behaviour Types size: "+typesToMerge.size());
//			logger.debug("Role Type: "+roleType);
//			logger.debug("Merging retrieved types...");
//			mergedType = Type.merge(typesToMerge);
//			return roleType.isIncluded(mergedType);
//		} catch (InstantiationException e) {
//			logger.error("Instatiation error when instancing requirements.");
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			logger.error("Illegal Access Exception.");
//			e.printStackTrace();
//		} catch (IllegalArgumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InvocationTargetException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (NoSuchMethodException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SecurityException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}
	
	// metodo di utilita' per ottenere come unica AgentAbilities l'insieme
	// dei metodi offerti per il ruolo
//	private AgentCapabilities mergeAgentCapabilities(List<Annotation> behaviourTypeAnnotations) {
//		ArrayList<AgentCapabilities> agentCapabilitiesToMerge = new ArrayList<AgentCapabilities>();
//		AgentCapabilities agCapabilites;
//		for (Annotation ann : behaviourTypeAnnotations) {
//			try {
//				agCapabilites = ((BehaviourType)ann).capabilities().getDeclaredConstructor().newInstance();
//				agentCapabilitiesToMerge.add(agCapabilites);
//			} catch (InstantiationException | IllegalAccessException
//					| IllegalArgumentException | InvocationTargetException
//					| NoSuchMethodException | SecurityException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//		}
//		return AgentCapabilities.merge(agentCapabilitiesToMerge);
//	}
	
	
	
	// spara eccezioni
	protected boolean checkRoleRequirements(String roleName, IPlayer player) {
		
		// per ora enactment solo per JADE
		if (!(player instanceof JadeBehaviourPlayer)) return true;
		
		logger.debug("Checking role name for ROLE: "+roleName);
		// check the requested Role Name: must exists in protocol enabled roles
		if (!enabledRoles.containsKey(roleName)) {
			logger.debug("Role "+roleName+" not found among enabled roles.");
			return false;
		}
		else return true;
	}
	
	/**
	 * Role "User", for basic send/receive communication artifact primitives
	 * @author fefo
	 *
	 */
	public class CARole extends Role {
		
		public CARole(String roleName, IPlayer player) {
			super(roleName, player, getId());
		}
		
		
		@RoleAction
		public void send(RoleMessage message) {
			try {
				logger.debug("Sending message: "+message);
				doAction(this.getArtifactId(), new Op("send", message));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		// receive the first message sent to this role
		@RoleAction
		public RoleMessage receive(int performative) {
			OpFeedbackParam<RoleMessage> message = new OpFeedbackParam<RoleMessage>();
			try {	
				doAction(this.getArtifactId(), new Op("receive", this.getRoleId(), performative, message));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return message.get();
		}
		
		// receive the first message sent to this role
		@RoleAction
		public RoleMessage receiveNoBlock(int performative) {
			OpFeedbackParam<RoleMessage> message = new OpFeedbackParam<RoleMessage>();
			try {	
				doAction(this.getArtifactId(), new Op("receiveNoBlockPerf", this.getRoleId(), performative, message));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				logger.error("Errore: "+e.getMessage());
				e.printStackTrace();
			} catch (CartagoException e) {
				logger.error("Errore: "+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return message.get();
		}

		
		
		// receive all messages sent to this role
		@RoleAction
		public ArrayList<RoleMessage> receiveAll() {
			OpFeedbackParam<RoleMessage> message = new OpFeedbackParam<RoleMessage>();
			ArrayList<RoleMessage> arr = new ArrayList<RoleMessage>();
			
			boolean noMoreMessages = false;
			
			while (!noMoreMessages) {
				try {	
					doAction(this.getArtifactId(), new Op("receiveNoBlock", this.getRoleId(), message));
					if (message.get() != null)
						arr.add(message.get());
					else
						noMoreMessages = true;
				} catch (ActionFailedException e) {
					logger.error("No more matches in receiving");
					noMoreMessages = true;					
				} catch (CartagoException e) {
					logger.error("Error in receiving, "+e.getMessage());
					e.printStackTrace();
				}
			}
			
			
			return arr;
		}
		
		// receive first messages sent to this role
		@RoleAction
		public RoleMessage receive() {
			OpFeedbackParam<RoleMessage> message = new OpFeedbackParam<RoleMessage>();
			
			try {	
				doAction(this.getArtifactId(), new Op("receiveNoBlock", this.getRoleId(), message));
				if (message.get() != null)
					return message.get();
				else
					return null;
			} catch (ActionFailedException e) {
				e.printStackTrace();
				logger.error("No more matches in receiving");
				return null;		
			} catch (CartagoException e) {
				logger.error("Error in receiving, "+e.getMessage());
				e.printStackTrace();
				return null;
			}					
		}
		
		@RoleAction
		public ArrayList<RoleMessage> receiveAll(int performative) {
			OpFeedbackParam<RoleMessage> message = new OpFeedbackParam<RoleMessage>();
			ArrayList<RoleMessage> arr = new ArrayList<RoleMessage>();
			
			boolean noMoreMessages = false;
			
			while (!noMoreMessages) {
				try {	
					doAction(this.getArtifactId(), new Op("receiveNoBlockPerf", this.getRoleId(), performative, message));
					logger.info("Richiesta la receiveNoBlock");
					if (message.get() != null)
						arr.add(message.get());
					else
						noMoreMessages = true;
				} catch (ActionFailedException e) {
					logger.error("No more matches in receiving");
					noMoreMessages = true;					
				} catch (CartagoException e) {
					logger.error("Error in receiveing, "+e.getMessage());
					e.printStackTrace();
				}
			}
			
			
			return arr;
		}
		
	}
	
}
  
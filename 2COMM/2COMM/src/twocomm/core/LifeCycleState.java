package twocomm.core;

/* 
 * Possible states of a commitment lifecycle, as illustrated in
 * Specifying and Verifying Cross-Organizational Business Models: An Agent-Oriented Approach
 */
public enum LifeCycleState {
	NULL, DETACHED, CONDITIONAL, EXPIRED, PENDING, TERMINATED, VIOLATED, SATISFIED
}

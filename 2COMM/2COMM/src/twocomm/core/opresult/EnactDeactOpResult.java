package twocomm.core.opresult;

/* 
 * Class for encapsulating enactment/deactment operation outcome.
 */
public class EnactDeactOpResult {

	public static final String ENACTMENT_SUCCESS = "enactSuccess";
	public static final String ENACTMENT_FAILURE = "enactFailure";
	
	private String result = "";
	private String message = "";
	
	public EnactDeactOpResult() {
		super();
	}
	
	public EnactDeactOpResult(String res, String msg) {
		this.result = res;
		this.message = msg;
	}


	public String getResult() {
		return result;
	}


	public void setResult(String result) {
		this.result = result;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	
	
}

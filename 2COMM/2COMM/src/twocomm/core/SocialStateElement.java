package twocomm.core;

import java.util.Date;

import javax.persistence.Embeddable;

import jason.asSyntax.Term;

@Embeddable
public abstract class SocialStateElement extends Object {

	protected SocialStateElementType elType;
	protected Date timestamp;

	public SocialStateElementType getElType() {
		return elType;
	}
	
	protected void setTimestamp() {
		timestamp = new Date();
	}

	public Date getTimestamp() {
		return this.timestamp;
	}
	
	public abstract String toString();
	
	public abstract Term toJasonTerm();

	//public abstract boolean equals(SocialStateElement el);

}

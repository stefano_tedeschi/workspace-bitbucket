package twocomm.core;

public abstract class CommitmentArtifact extends ProtocolArtifact {

	protected RoleId debtor;
	protected RoleId creditor;
	
	public class CommitmentRole extends PARole {

		public CommitmentRole(String roleName, IPlayer player) {
			super(roleName, player);
		}
		
		public RoleId getCreditor() {
			return creditor;
		}
		
		public RoleId getDebtor() {
			return debtor;
		}
		
	}

}

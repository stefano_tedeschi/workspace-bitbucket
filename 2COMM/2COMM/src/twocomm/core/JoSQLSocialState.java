package twocomm.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.exception.CommitmentAlreadyAddedException;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.CommitmentOperationNotPerformableException;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import twocomm.exception.RetrievalOperationNotPerformableException;

import org.josql.*;

/**
 *
 * @author Stefano Tedeschi
 */
public class JoSQLSocialState extends Observable {

    private static Logger logger = LogManager.getLogger(JoSQLSocialState.class);

    private ProtocolArtifact art;

    private List<Commitment> allCommitments = new ArrayList<>();
    private List<Fact> facts = new ArrayList<>();

    public JoSQLSocialState(ProtocolArtifact art) {
        super();
        this.art = art;
    }

    public boolean assertFact(Fact f) throws LogicalExpressionAlreadyAddedException {
        boolean added = true;
        boolean toAdd = true;
        for (Fact el : facts) {
            if (el.equals(f)) {
                toAdd = false;
            }
        }
        if (toAdd) {
            synchronized (facts) {
                facts.add(f);
            }
            art.defineObsPropertyFromFact(f);
        } else {
            throw new LogicalExpressionAlreadyAddedException(f);
        }
        // NEED TO CHECK IF SOME COMMITMENT BECOME DETACHED/SATISFIED
        // checkCommitmentsAfterInteractionStateElementAdding(le);
        logger.debug("Check finished, actual status: " + this);
        return added;
    }

    public boolean existsFact(Fact f) {
        for (Fact f1 : facts) {
            if (f1.equals(f)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasCommitment(Commitment c) throws RetrievalOperationNotPerformableException {
        List<Commitment> res = new ArrayList<>();
    	try {
        	Query q = new Query();
        	//la query con una sintassi simile a quella di SQL viene parsificata in modo da essere eseguita su una lista di oggetti di tipo Commitment
        	q.parse("SELECT * FROM twocomm.core.commit.Commitment WHERE debtor = '" + c.getDebtor() + "' AND creditor = '"
                + c.getCreditor() + "' AND antecedent = '" + c.getAntecedent() + "' AND consequent = '"
                + c.getConsequent() + "'");
        	//la query viene eseguita passando come parametro la struttura dati su cui si vuole eseguire la query
        	res = q.execute(allCommitments);
        }
        catch(QueryParseException | QueryExecutionException e) {
        	throw new RetrievalOperationNotPerformableException(e);
        }
        for (Commitment c1 : res) {
            if (c1.equals(c)) {
                return true;
            }
        }
        return false;
    }

    public List<Commitment> createAllCommitments(Commitment c) {
        List<Commitment> concreteCommitmentsAdded = new ArrayList<>();
        if (c.getCreditor().getType() == RoleId.GROUP_ROLE) {
            int numberOfCreditor = art.getEnactedRolesIds().size();
            int exceptionCount = 0;
            Commitment comm;
            for (RoleId r : art.getEnactedRolesIds()) {
                if (r.equals(c.getCreditor()) && !r.equals(c.getDebtor())) {
                    try {
                        System.out.println("dentro, " + r.toString() + " " + c.getCreditor().toString());
                        comm = new Commitment(c.getDebtor(), r, c.getAntecedent(), c.getConsequent());
                        createConcreteCommitmentInternal(comm);
                        concreteCommitmentsAdded.add(comm);
                        for (Commitment com : concreteCommitmentsAdded) {
							art.defineObsPropertyFromCommitment(com);
						}
						
						logger.debug("Commitment to reify added, "+c+", reified: "+concreteCommitmentsAdded.size());
                    } catch (CommitmentAlreadyAddedException e) {
                        exceptionCount++;
                    }
                }
            }
            if (exceptionCount == numberOfCreditor) {
                logger.debug("The commitment " + c + " is not reyfable.");
            }
        } // creditor roleid already particular
        else {
            System.out.println(
                    "The commitment " + c + " must be a GENERIC commitment, with a RoleId GENERIC as creditor.");
        }
        return concreteCommitmentsAdded;
    }

    public List<Commitment> createCommitment(Commitment c) {
        ArrayList<Commitment> concreteCommitmentsAdded = new ArrayList<>();
        if (c.getCreditor().getType() == RoleId.GROUP_ROLE) {
            try {
                createGroupCommitmentInternal(c);
                concreteCommitmentsAdded.add(c);
            } catch (CommitmentAlreadyAddedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                logger.debug("Error in adding group commitment " + c);
            }
        } // creditor roleid already particular
        else {
            try {
                createConcreteCommitmentInternal(c);
                concreteCommitmentsAdded.add(c);
            } catch (CommitmentAlreadyAddedException e) {
                e.printStackTrace();
                logger.debug("The commitment " + c + " is not reyfable.");
            }
        }
        for (Commitment com : concreteCommitmentsAdded) {
			art.defineObsPropertyFromCommitment(com);
		}
		logger.debug("Aggiunta del commitment completata, "+c);
        return concreteCommitmentsAdded;
    }

    private void createGroupCommitmentInternal(Commitment c) throws CommitmentAlreadyAddedException {
        // CHECK IF COMMITMENT IS ALREADY IN INTERACTION STATE
        for (Commitment actualC : allCommitments) {
            // errore se commitment uguali E se creditori sono ENTRAMBI
            // GROUP_ROLE
            // TODO: qua ignoro i commitment violati o terminati
            if (actualC.equals(c) && actualC.getCreditor().getType() == c.getCreditor().getType()
                    && actualC.getLifeCycleStatus() != LifeCycleState.VIOLATED
                    && actualC.getLifeCycleStatus() != LifeCycleState.TERMINATED) {
                throw new CommitmentAlreadyAddedException(c);
            }
        }
        // UPDATE COMMITMENT REPRESENTATION
        allCommitments.add(c);
    }

    private void createConcreteCommitmentInternal(Commitment c) throws CommitmentAlreadyAddedException {
        // CHECK IF COMMITMENT IS ALREADY IN INTERACTION STATE
        for (Commitment actualC : allCommitments) {
            if (actualC.equals(c)) {
                throw new CommitmentAlreadyAddedException(c);
            }
        }
        // UPDATE COMMITMENT REPRESENTATION
        allCommitments.add(c);
        // NEED TO CHECK IF SOME COMMITMENT BECOME DETACHED/SATISFIED
        // checkCommitmentsAfterInteractionStateElementAdding(c);
        logger.debug("Check finished, actual status: " + this);
    }

    /*
     * "Cancel a commitment" means that a detached commitment becomes violated,
     * while a conditional commitment becomes terminated
     */
    public boolean cancelCommitment(Commitment c)
            throws CommitmentNotFoundInInteractionStateException, CommitmentOperationNotPerformableException {
        synchronized (allCommitments) {
            // CHECK IF COMMITMENT IS IN INTERACTION STATE
            logger.debug("Canceling commitment " + c);
            Commitment cToCancel = null;
            Query q = new Query();
            List<Commitment> res = new ArrayList<>();
            try {
                q.parse("SELECT * FROM twocomm.core.Commitment WHERE debtor = '" + c.getDebtor()
                        + "' AND creditor = '" + c.getCreditor() + "' AND antecedent = '" + c.getAntecedent()
                        + "' AND consequent = '" + c.getConsequent() + "' AND lifeCycleStatus = '"
                        + c.getLifeCycleStatus() + "'");
                res = q.execute(allCommitments);
            } catch (QueryParseException | QueryExecutionException e) {
                throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);
            }
            if (res.isEmpty()) {
                throw new CommitmentNotFoundInInteractionStateException(c);
            }
            cToCancel = res.get(0);
			// TODO: QUI C'E' FORZATURA DEL CONTROLLO DEL LIFECYCLE, NON FACCIO
            // CONTROLLO
            // check lifecyclestatus of commitment
            // if (c.getLifeCycleStatus() != LifeCycleState.CONDITIONAL &&
            // c.getLifeCycleStatus() != LifeCycleState.DETACHED) {
            // throw new CommitmentOperationNotPerformableException(c,
            // ProtocolArtifact.CANCEL_COMMITMENT);
            // }

            // UPDATE STRUCTURE BY LIFECYCLE
            logger.trace("Commitment to cancel found");
            // logger.debug("Updating roles-commitments support
            // datastructures");
            // now change commitment status
            switch (c.getLifeCycleStatus()) {
                case CONDITIONAL:
                    cToCancel.setStatus(LifeCycleState.TERMINATED);
                    break;
                case DETACHED:
                    cToCancel.setStatus(LifeCycleState.VIOLATED);
                    break;
                default:
                    throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);

            }
            logger.debug("Cancel operation finished, actual status: " + this);
            return true;
        }
    }

    public boolean removeCommitment(Commitment c)
            throws CommitmentNotFoundInInteractionStateException, CommitmentOperationNotPerformableException {
        synchronized (allCommitments) {
            // CHECK IF COMMITMENT IS IN INTERACTION STATE
            logger.debug("Canceling commitment " + c);
            Commitment cToCancel = null;
            Query q = new Query();
            List<Commitment> res = new ArrayList<>();
            try {
                q.parse("SELECT * FROM twocomm.core.Commitment WHERE debtor = '" + c.getDebtor()
                        + "' AND creditor = '" + c.getCreditor() + "' AND antecedent = '" + c.getAntecedent()
                        + "' AND consequent = '" + c.getConsequent() + "' AND lifeCycleStatus = '"
                        + c.getLifeCycleStatus() + "'");
                res = q.execute(allCommitments);
            } catch (QueryParseException | QueryExecutionException e) {
                throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);
            }
            if (res.isEmpty()) {
                throw new CommitmentNotFoundInInteractionStateException(c);
            }
            cToCancel = res.get(0);
			// TODO: QUI C'E' FORZATURA DEL CONTROLLO DEL LIFECYCLE, NON FACCIO
            // CONTROLLO
            // check lifecyclestatus of commitment
            // if (c.getLifeCycleStatus() != LifeCycleState.CONDITIONAL &&
            // c.getLifeCycleStatus() != LifeCycleState.DETACHED) {
            // throw new CommitmentOperationNotPerformableException(c,
            // ProtocolArtifact.CANCEL_COMMITMENT);
            // }

            // UPDATE STRUCTURE BY LIFECYCLE
            logger.trace("Commitment to cancel found");
            // logger.debug("Updating roles-commitments support
            // datastructures");
            // now change commitment status
            allCommitments.remove(cToCancel);
            // logger.debug("Remove operation finished, actual status: " +
            // this);
            return true;
        }
    }

    /*
     * private void
     * checkCommitmentsAfterInteractionStateElementAdding(SocialStateElement
     * elAdded) { ArrayList<Commitment> condCommits =
     * commitments.get(LifeCycleState.CONDITIONAL); ArrayList<Commitment>
     * detachedCommits = commitments.get(LifeCycleState.DETACHED); switch
     * (elAdded.getElType()) { case COMMITMENT: logger.trace(
     * "Just added a COMMITMENT, reasoning on interaction state after adding " +
     * elAdded); Commitment c = (Commitment) elAdded; // search in conditional
     * commitment set if (condCommits != null) { logger.trace(
     * "Parsing CONDITIONAL commitments"); for (Commitment condComm :
     * condCommits) { logger.debug("Commitment antecedent: " +
     * condComm.getAntecedent()); if (condComm.getAntecedent().equals(c)) {
     * logger.debug("Commitment " + condComm +
     * " has to change its state. Now is\n " + condComm.getLifeCycleStatus() +
     * ", setting to DETACHED.");
     * 
     * condComm.setStatus(LifeCycleState.DETACHED); // try { //
     * condComm.setAntecedent(new Fact("true")); // } catch
     * (MissingOperandException e) { // // TODO Auto-generated catch block //
     * e.printStackTrace(); // } }
     * 
     * } } // search in detached commitment set if (detachedCommits != null) {
     * logger.trace("Parsing DETACHED commitments"); for (Commitment condComm :
     * detachedCommits) { if (condComm.getAntecedent().equals(c)) {
     * logger.debug("Commitment " + condComm +
     * " has to change its state. Now is\n " + condComm.getLifeCycleStatus() +
     * ", setting to DISCHARGED.");
     * condComm.setStatus(LifeCycleState.SATISFIED); try {
     * condComm.setAntecedent(new Fact("true")); } catch
     * (MissingOperandException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } }
     * 
     * } } break;
     * 
     * case LOGICAL_EXPRESSION: logger.trace(
     * "Just added a FACT, reasoning on interaction state after adding " +
     * elAdded); LogicalExpression el = (LogicalExpression) elAdded; if
     * (condCommits != null) { logger.trace("Parsing CONDITIONAL commitments");
     * for (Commitment condComm : condCommits) { logger.debug(
     * "Commitment antecedent: " + condComm.getAntecedent()); if
     * (condComm.getAntecedent().equals(el)) { logger.debug("Commitment " +
     * condComm + " has to change its state. Now is\n " +
     * condComm.getLifeCycleStatus() + ", setting to DETACHED.");
     * condComm.setStatus(LifeCycleState.DETACHED); // try { //
     * condComm.setAntecedent(new Fact("true")); // } catch
     * (MissingOperandException e) { // // TODO Auto-generated catch block //
     * e.printStackTrace(); // } }
     * 
     * } } // search in detached commitment set if (detachedCommits != null) {
     * logger.trace("Parsing DETACHED commitments"); for (Commitment condComm :
     * detachedCommits) { if (condComm.getAntecedent().equals(el)) {
     * logger.debug("Commitment " + condComm +
     * " has to change its state. Now is\n " + condComm.getLifeCycleStatus() +
     * ", setting to DISCHARGED.");
     * condComm.setStatus(LifeCycleState.SATISFIED); try {
     * condComm.setAntecedent(new Fact("true")); } catch
     * (MissingOperandException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } }
     * 
     * } } break; }
     * 
     * }
     */
    @Override
    public synchronized String toString() {
        String st = "";

        st += "\nFACTS:\n------------------\n";
        for (LogicalExpression f : facts) {
            st += "- " + f + "\n";
        }

        st += "\nCOMMITMENTS:\n------------------\n";
        for (Commitment c : allCommitments) {
            st += "- " + c + "\n";
        }
        return st + "\n";
    }

    public List<Commitment> retrieveAllCommitments() {
        return allCommitments;
    }

    public List<Commitment> retrieveCommitmentsByLifecycleStatus(LifeCycleState status)
            throws RetrievalOperationNotPerformableException {
        List<Commitment> res = null;
    	try {
        	Query q = new Query();
        	q.parse("SELECT * FROM twocomm.core.Commitment WHERE lifeCycleStatus = '" + status + "'");
        	res = q.execute(allCommitments);
        }
        catch(QueryParseException | QueryExecutionException e) {
        	throw new RetrievalOperationNotPerformableException(e);
        }
    	return res;
    }

    public List<Commitment> retrieveCommitmentsByCreditorRoleId(RoleId id)
            throws RetrievalOperationNotPerformableException {
        List<Commitment> res = null;
        try {
        	Query q = new Query();
        	q.parse("SELECT * FROM twocomm.core.Commitment WHERE creditor = '" + id + "'");
        	res = q.execute(allCommitments);
        }
        catch(QueryParseException | QueryExecutionException e) {
            throw new RetrievalOperationNotPerformableException(e);
        }
        return res;
    }

    public List<Commitment> retrieveCommitmentsByDebtorRoleId(RoleId id)
            throws RetrievalOperationNotPerformableException {
    	List<Commitment> res = null;
    	try {
    		Query q = new Query();
        	q.parse("SELECT * FROM twocomm.core.Commitment WHERE debtor = '" + id + "'");
        	res = q.execute(allCommitments);
    	}
    	catch(QueryParseException | QueryExecutionException e) {
            throw new RetrievalOperationNotPerformableException(e);
        }
        return res;
    }

    public List<Commitment> retrieveCommitmentsByAntecedent(Fact f)
            throws RetrievalOperationNotPerformableException {
    	List<Commitment> res = null;
    	try {
    		Query q = new Query();
        	q.parse("SELECT * FROM twocomm.core.Commitment WHERE antecedent = '" + f + "'");
        	res = q.execute(allCommitments);
    	}
    	catch(QueryParseException | QueryExecutionException e) {
            throw new RetrievalOperationNotPerformableException(e);
        }
        return res;
    }

    public List<Commitment> retrieveCommitmentsByConsequent(Fact f)
            throws RetrievalOperationNotPerformableException {
    	List<Commitment> res = null;
    	try {
    		Query q = new Query();
    		q.parse("SELECT * FROM twocomm.core.Commitment WHERE consequent = '" + f + "'");
    		res = q.execute(allCommitments);
    	}
    	catch(QueryParseException | QueryExecutionException e) {
            throw new RetrievalOperationNotPerformableException(e);
        }
        return res;
    }

    public List<Commitment> retrieveCommitmentsContainingFact(Fact f)
            throws RetrievalOperationNotPerformableException {
    	List<Commitment> res = null;
    	try {
        Query q = new Query();
        q.parse("SELECT * FROM twocomm.core.Commitment WHERE antecedent LIKE '%" + f + "%' OR consequent LIKE '%"
                + f + "%'");
        res = q.execute(allCommitments);
    	}
    	catch(QueryParseException | QueryExecutionException e) {
            throw new RetrievalOperationNotPerformableException(e);
        }
        return res;
    }

    private String printHashSet(ArrayList hs) {
        String st = "";
        int i = 0;
        for (Object o : hs) {
            st += i + ": " + o + "\n";
            i++;
        }
        return st;
    }

    /*
     // implementati metodi per modificare lo stato dei commitment
     public void releaseCommitment(Commitment c) {
     synchronized (c) {
     c.setStatus(LifeCycleState.TERMINATED);
     }
     }

     public void satisfyCommitment(Commitment c) {
     synchronized (c) {
     c.setStatus(LifeCycleState.SATISFIED);
     }
     }

     public void detachCommitment(Commitment c) {
     synchronized (c) {
     c.setStatus(LifeCycleState.DETACHED);
     }
     }

     public void violateCommitment(Commitment c) {
     synchronized (c) {
     c.setStatus(LifeCycleState.VIOLATED);
     }
     }
     */
    /**
     * Change the status of commitment c to the new status newstate. It removes
     * c from list of conditional or detached commitments, and updates the
     * structure maintaining the list of commitment with state newstate.
     *
     * @param c the commitment whose status has to be changed.
     * @param newState the new status, according to the commitment lifecycle
     * possible states.
     */
    public void changeCommitmentStatus(Commitment c, LifeCycleState newState) {
        logger.debug("Changing status of commitment: " + c);
        if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
            if (newState != LifeCycleState.CONDITIONAL) {
                synchronized (art) {
					art.updateCommitmentObsPropertyByStatus(c, c.getLifeCycleStatus(), newState);
				}
                synchronized (c) {
                    c.setStatus(newState);
                }
            }
        } else if (c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
            if (newState != LifeCycleState.DETACHED) {
                synchronized (art) {
					art.updateCommitmentObsPropertyByStatus(c, c.getLifeCycleStatus(), newState);
				}
                synchronized (c) {
                    c.setStatus(newState);
                }
            }
        }
        logger.debug("Status of commitment changed. New status: " + c);
    }

}

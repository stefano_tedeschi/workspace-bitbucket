package twocomm.core.logic;

import javax.persistence.Embeddable;

@Embeddable
public enum LogicalExpressionType {

	FACT, COMPOSITE_EXPRESSION
}

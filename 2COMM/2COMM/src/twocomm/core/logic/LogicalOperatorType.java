package twocomm.core.logic;

import javax.persistence.Embeddable;

@Embeddable
public enum LogicalOperatorType {

	AND, OR, NOT, THEN;

	@Override
	public String toString() {
		if (this == LogicalOperatorType.AND)
			return "AND";
		else if (this == LogicalOperatorType.OR)
			return "OR";
		else if (this == LogicalOperatorType.NOT)
			return "NOT";
		else 
			return "THEN";
	}

}

package twocomm.core.logic;

import twocomm.core.SocialStateElement;
import twocomm.core.SocialStateElementType;
import twocomm.exception.MissingOperandException;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Embeddable;

import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.ObjectTerm;
import jason.asSyntax.ObjectTermImpl;
import jason.asSyntax.StringTerm;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

@Embeddable
public class Fact extends LogicalExpression {

	private String predicate;
	private Object[] arguments;

	public Fact(String p, Object... arg) throws MissingOperandException {
		super(LogicalExpressionType.FACT);
		predicate = p;
		arguments = arg;
		if(p.equals("true") && arg.length == 0) {
			this.setVerified(true);
		}
		else if(p.equals("false") && arg.length == 0) {
			this.setVerified(false);
		}
	}

	public String getPredicate() {
		return predicate;
	}

	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	public Object[] getArguments() {
		return arguments;
	}

	public void setArguments(Object... arguments) {
		this.arguments = arguments;
	}

	public String toString() {
		String str = predicate;
		if (arguments.length != 0) {
			str += "(";
			for (int i = 0; i < arguments.length; i++) {
				str += arguments[i] + ",";
			}
			str = str.substring(0, str.lastIndexOf(","));
			str += ")";
		}
		return str;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof SocialStateElement)
			return this.equals((SocialStateElement) o);
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 23 * hash + Objects.hashCode(this.predicate);
		hash = 23 * hash + Arrays.deepHashCode(this.arguments);
		return hash;
	}

	
	public boolean hasSameName(Fact f) {
		return this.getPredicate().equals(f.getPredicate());
	}
	
	public boolean equals(SocialStateElement el) {
		// Must be a Logical Expression, not a commitment
		if (el.getElType() != SocialStateElementType.LOGICAL_EXPRESSION) {
			return false;
		}
		LogicalExpression le = (LogicalExpression) el;
		// Must be a Fact, not a Logical Expression
		if (le.getLogExpType() != LogicalExpressionType.FACT) {
			return false;
		}
		// we have a fact, now compare it with this
		Fact f = (Fact) le;
		// compare predicate
		if (!getPredicate().equals(f.getPredicate())) {
			return false;
		}
		// compare numer of predicate arguments
		if (getArguments().length != f.getArguments().length) {
			return false;
		}
		// now, compare each argument. Must be in the same order too
		for (int i = 0; i < getArguments().length; i++) {
			if (!getArguments()[i].equals(f.getArguments()[i])) {
				return false;
			}
		}

		return true;
	}

	@Override
	public Term toJasonTerm() {
		
		Structure struct = new Structure(predicate, arguments.length);
		
		for(Object o : arguments) {
			
			if(o instanceof Number) {
				NumberTerm n = new NumberTermImpl(((Number) o).doubleValue());
				struct.addTerm(n);
			}
			
			else if(o instanceof String) {
				StringTerm s = new StringTermImpl((String) o);
				struct.addTerm(s);
			}
			
			else {
				ObjectTerm t = new ObjectTermImpl(o);
				struct.addTerm(t);
			}
		
		}
		
		return struct;
	
	}
}

package twocomm.core.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Embeddable;

import twocomm.core.SocialStateElement;
import twocomm.core.SocialStateElementType;
import twocomm.exception.MissingOperandException;

@Embeddable
public abstract class LogicalExpression extends SocialStateElement {

	public LogicalExpressionType logExpType;

	protected LogicalExpression left;
	protected LogicalExpression right;

	// aggiunto flag per tenere traccia delle espressioni logiche che sono
	// verificate nello stato sociale + setter e getter
	private Boolean verified;

	public LogicalExpression(LogicalExpressionType type) throws MissingOperandException {
		if (type == LogicalExpressionType.FACT) {
			this.logExpType = type;
			this.elType = SocialStateElementType.LOGICAL_EXPRESSION;
			this.verified = null;
		} else {
			throw new MissingOperandException("Wrong type: requested a FACT.");
		}
	}

	// left != null sempre: nel caso del NOT, e'l'unico elemento a non essere
	// null
	public LogicalExpression(LogicalExpressionType type, LogicalExpression left, LogicalExpression right)
			throws MissingOperandException {
		if (type == LogicalExpressionType.COMPOSITE_EXPRESSION && left != null) {
			this.logExpType = type;
			this.elType = SocialStateElementType.LOGICAL_EXPRESSION;
			this.left = left;
			this.right = right;
			this.verified = null;
		} else {
			throw new MissingOperandException("Wrong type: requested a LOGICAL_EXPRESSION.");
		}
	}

	public LogicalExpression getLeft() {
		return left;
	}

	public void setLeft(LogicalExpression left) {
		this.left = left;
	}

	public LogicalExpression getRight() {
		return right;
	}

	public void setRight(LogicalExpression right) {
		this.right = right;
	}

	public LogicalExpressionType getLogExpType() {
		return this.logExpType;
	}

	private List<Fact> getAllFactsInt(List<Fact> facts) {
		if (this.getLogExpType() == LogicalExpressionType.FACT) {
			if (!facts.contains(this))
				facts.add((Fact)this);
		}
		else {
			facts = left.getAllFactsInt(facts);
			if (right != null)
				facts = right.getAllFactsInt(facts);
		}
		return facts;
	}
	
	// restituisce lista di tutti singoli fatti che compongono l'espressione
	public List<Fact> getAllFacts() {
		List<Fact> facts = new ArrayList<Fact>();
		return getAllFactsInt(facts);
	}
	
	public boolean isVerified() {
		if (verified != null) {
			return verified;
		}
		return false;
	}

	public boolean isViolated() {
		if (verified != null) {
			return !verified;
		}
		return false;
	}

	public void setVerified(boolean verified) {
		this.verified = new Boolean(verified);
	}
	
	public void resetVerifiedToFalse() {
		if (getLogExpType() == LogicalExpressionType.FACT) {
			setVerified(false);
		} else {
			if (getLeft() != null) {
				getLeft().resetVerifiedToFalse();
			}
			if (getRight() != null) {
				getRight().resetVerifiedToFalse();
			}
			resetVerifiedToFalse();						
		}
	}	

}

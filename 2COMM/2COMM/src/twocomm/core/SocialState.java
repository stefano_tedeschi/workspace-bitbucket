package twocomm.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.CompositeExpression;
import twocomm.exception.CommitmentAlreadyAddedException;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.CommitmentOperationNotPerformableException;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import twocomm.exception.MissingOperandException;

@Entity
public class SocialState extends Observable implements Serializable {

	@Id
	@GeneratedValue
	private int id;

	private static Logger logger = LogManager.getLogger(SocialState.class);

	@Transient
	private ProtocolArtifact art;

	// facts and commitments
	protected List<Commitment> allCommitments = new ArrayList<>();
	@Transient
	private ConcurrentHashMap<LifeCycleState, List<Commitment>> commitmentsByStatus = new ConcurrentHashMap<>();

	protected List<Fact> facts = new ArrayList<>();

	// ordinati per debitore e per creditore
	@Transient
	private ConcurrentHashMap<RoleId, List<Commitment>> commitByDebtor = new ConcurrentHashMap<>();
	@Transient
	private ConcurrentHashMap<RoleId, List<Commitment>> commitByCreditor = new ConcurrentHashMap<>();

	// indicizzati in base ai fatti presenti in antecedente e/o conseguente
	@Transient
	private ConcurrentHashMap<Fact, ArrayList<Commitment>> commitByFact = new ConcurrentHashMap<>();

	public SocialState(ProtocolArtifact art) {
		super();
		this.art = art;
		initializeStatusHashMap();
	}

	private void initializeStatusHashMap() {
		commitmentsByStatus.put(LifeCycleState.CONDITIONAL, new ArrayList<Commitment>());
		commitmentsByStatus.put(LifeCycleState.DETACHED, new ArrayList<Commitment>());
		commitmentsByStatus.put(LifeCycleState.EXPIRED, new ArrayList<Commitment>());
		commitmentsByStatus.put(LifeCycleState.PENDING, new ArrayList<Commitment>());
		commitmentsByStatus.put(LifeCycleState.SATISFIED, new ArrayList<Commitment>());
		commitmentsByStatus.put(LifeCycleState.TERMINATED, new ArrayList<Commitment>());
		commitmentsByStatus.put(LifeCycleState.VIOLATED, new ArrayList<Commitment>());
	}

	public List<Fact> getFacts() {
		return facts;
	}

	public void setFacts(ArrayList<Fact> facts) {
		this.facts = facts;
	}

	public boolean assertFact(Fact f) throws LogicalExpressionAlreadyAddedException {
		boolean added = true;
		boolean toAdd = true;
		f.setTimestamp();
		for (Fact el : facts) {
			if (el.equals(f)) {
				toAdd = false;
			}
		}
		if (toAdd) {
			synchronized (facts) {
				facts.add(f);
			}
			art.defineObsPropertyFromFact(f);
		} else {
			throw new LogicalExpressionAlreadyAddedException(f);
		}
		// NEED TO CHECK IF SOME COMMITMENT BECOME DETACHED/SATISFIED
		// checkCommitmentsAfterInteractionStateElementAdding(le);
		logger.debug("Added fact: "+f);
		return added;
	}

	public boolean existsFact(Fact f) {
		for (Fact f1 : facts) {
			if (f1.equals(f)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * For a commitmend involving as a creditor a Group Role, it reifies x commitment,
	 * where x is the number of agents currently playing that role.
	 * @param c
	 * @return
	 */
	
	public ArrayList<Commitment> createAllCommitments(Commitment c) {
		ArrayList<Commitment> concreteCommitmentsAdded = new ArrayList<Commitment>();
		if (c.getCreditor().getType() == RoleId.GROUP_ROLE) {
			int numberOfCreditor = art.getEnactedRolesIds().size();
			int exceptionCount = 0;
			Commitment comm;
			for (RoleId r : art.getEnactedRolesIds()) {
				if (r.equals(c.getCreditor()) && !r.equals(c.getDebtor())) {
					try {
						comm = new Commitment(c.getDebtor(), r, c.getAntecedent(), c.getConsequent());
						createConcreteCommitmentInternal(comm);
						
						
						logger.debug("Commitment to reify added, "+c+", reified: "+concreteCommitmentsAdded.size());
					} catch (CommitmentAlreadyAddedException e) {
						exceptionCount++;
					}
				}
			}
			if (exceptionCount == numberOfCreditor) {
				logger.debug("The commitment " + c + " is not reyfable.");
			}
		} // creditor roleid already particular
		else {
			logger.error(
					"The commitment " + c + " must be a GENERIC commitment, with a RoleId GROUP as creditor.");
		}
		return concreteCommitmentsAdded;
	}

	/**
	 * For a commitment involving as a creditor a Group Role, it creates
	 * one commitment, with a generic creditor of specified type.
	 * If the creditor is a specific role, it creates the commitment.
	 * @param c
	 * @return
	 */
	public ArrayList<Commitment> createCommitment(Commitment c) {
		ArrayList<Commitment> concreteCommitmentsAdded = new ArrayList<Commitment>();
		if (c.getCreditor().getType() == RoleId.GROUP_ROLE) {
			try {
				createGroupCommitmentInternal(c);
			} catch (CommitmentAlreadyAddedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Error in adding group commitment " + c);
			}
		} // creditor roleid already particular
		else {
			try {
				createConcreteCommitmentInternal(c);				
			} catch (CommitmentAlreadyAddedException e) {
				e.printStackTrace();
				logger.error("The commitment " + c + " is not reyfable.");
			}
		}
		
		logger.debug("Aggiunta del commitment completata, "+c);
		return concreteCommitmentsAdded;
	}

	private void createGroupCommitmentInternal(Commitment c) throws CommitmentAlreadyAddedException {
		// CHECK IF COMMITMENT IS ALREADY IN INTERACTION STATE
		for (Commitment actualC : allCommitments) {
			// errore se commitment uguali E
			//se creditori sono ENTRAMBI GROUP_ROLE
			// TODO: qua ignoro i commitment violati o terminati
			logger.trace("CREATE GROUP COMMITMENT INTERNAL");
			logger.trace("Checking commitments:\nTO ADD: "+c+"\nACTUAL: "+actualC);
			if (actualC.equals(c) && actualC.getCreditor().getType() == c.getCreditor().getType()
					&& actualC.getLifeCycleStatus() != LifeCycleState.VIOLATED
					&& actualC.getLifeCycleStatus() != LifeCycleState.TERMINATED) {					
				throw new CommitmentAlreadyAddedException(c);
			}
		}

		allCommitments.add(c);
		art.defineObsPropertyFromCommitment(c);
		logger.trace("Commitment added: "+c);

		// UPDATE COMMITMENT LIST BY CREDITOR
		List<Commitment> actualCommitsCreditor = commitByCreditor.get(c.getCreditor());
		if (actualCommitsCreditor != null) {
			actualCommitsCreditor.add(c);
		} else {
			actualCommitsCreditor = new ArrayList<Commitment>();
			actualCommitsCreditor.add(c);
			commitByCreditor.put(c.getCreditor(), actualCommitsCreditor);
		}

		// UPDATE COMMITMENT LIST BY DEBTOR
		List<Commitment> actualCommitsDebtor = commitByDebtor.get(c.getDebtor());
		if (actualCommitsDebtor != null) {
			actualCommitsDebtor.add(c);
		} else {
			actualCommitsDebtor = new ArrayList<Commitment>();
			actualCommitsDebtor.add(c);
			commitByDebtor.put(c.getDebtor(), actualCommitsDebtor);
		}

		// UPDATE COMMITMENT LIST BY LIFECYCLE STATUS
		List<Commitment> actualCommits = commitmentsByStatus.get(c.getLifeCycleStatus());
		actualCommits.add(c);
		
		// UPDATE COMMITMENT LIST BY FACTS
		updateCommitsByFact(c.getAntecedent(), c);
		updateCommitsByFact(c.getConsequent(), c);

		// NEED TO CHECK IF SOME COMMITMENT BECOME DETACHED/SATISFIED
		// checkCommitmentsAfterInteractionStateElementAdding(c);
	}

	private void updateCommitsByFact(SocialStateElement el, Commitment c) {
		if (el instanceof Fact) {
			ArrayList<Commitment> factCommits = commitByFact.get(((Fact) el));
			if (factCommits != null) {
				factCommits.add(c);
			} else {
				ArrayList<Commitment> toAddC = new ArrayList<Commitment>();
				toAddC.add(c);
				commitByFact.put(((Fact) el), toAddC);
			}
		} else if (el instanceof CompositeExpression) {
			if (((CompositeExpression) el).getLeft() != null) {
				updateCommitsByFact(((CompositeExpression) el).getLeft(), c);
			}
			if (((CompositeExpression) el).getRight() != null) {
				updateCommitsByFact(((CompositeExpression) el).getRight(), c);
			}
		}
	}

	private void createConcreteCommitmentInternal(Commitment c) throws CommitmentAlreadyAddedException {
		// CHECK IF COMMITMENT IS ALREADY IN INTERACTION STATE
		for (Commitment actualC : allCommitments) {
			logger.trace("CREATE CONCRETE COMMITMENT INTERNAL");
			logger.trace("Checking commitments:\nTO ADD: "+c+"\nACTUAL: "+actualC);
			if (actualC.equals(c)) {
				
				return;
			}
		}
		c.setTimestamp();
		allCommitments.add(c);
		art.defineObsPropertyFromCommitment(c);
		
		logger.trace("Commitment added: "+c);
		
		// UPDATE COMMITMENT LIST BY CREDITOR
		List<Commitment> actualCommitsCreditor = commitByCreditor.get(c.getCreditor());
		if (actualCommitsCreditor != null) {
			actualCommitsCreditor.add(c);
		} else {
			actualCommitsCreditor = new ArrayList<Commitment>();
			actualCommitsCreditor.add(c);
			commitByCreditor.put(c.getCreditor(), actualCommitsCreditor);
		}

		// UPDATE COMMITMENT LIST BY DEBTOR
		List<Commitment> actualCommitsDebtor = commitByDebtor.get(c.getDebtor());
		if (actualCommitsDebtor != null) {
			actualCommitsDebtor.add(c);
		} else {
			actualCommitsDebtor = new ArrayList<Commitment>();
			actualCommitsDebtor.add(c);
			commitByDebtor.put(c.getDebtor(), actualCommitsDebtor);
		}

		// UPDATE COMMITMENT LIS BY LIFECYCLE STATUS
		List<Commitment> actualCommits = commitmentsByStatus.get(c.getLifeCycleStatus());
		actualCommits.add(c);
		

		// UPDATE COMMITMENT LIST BY FACTS
		updateCommitsByFact(c.getAntecedent(), c);
		updateCommitsByFact(c.getConsequent(), c);

		// NEED TO CHECK IF SOME COMMITMENT BECOME DETACHED/SATISFIED
		// checkCommitmentsAfterInteractionStateElementAdding(c);
	}

	/*
	 * "Cancel a commitment" means that a detached commitment becomes violated,
	 * while a conditional commitment becomes terminated
	 */
	public synchronized boolean cancelCommitment(Commitment c)
			throws CommitmentNotFoundInInteractionStateException, CommitmentOperationNotPerformableException {
		// CHECK IF COMMITMENT IS IN INTERACTION STATE
		logger.debug("Canceling commitment " + c);
		Commitment cToCancel = null;
		for (Commitment actualC : allCommitments) {
			if (actualC.equals(c)) {
				cToCancel = actualC;
			}
		}
		if (cToCancel == null) {
			throw new CommitmentNotFoundInInteractionStateException(c);
		}
		// TODO: QUI C'E' FORZATURA DEL CONTROLLO DEL LIFECYCLE, NON FACCIO
		// CONTROLLO
		// check lifecyclestatus of commitment
		// if (c.getLifeCycleStatus() != LifeCycleState.CONDITIONAL &&
		// c.getLifeCycleStatus() != LifeCycleState.DETACHED) {
		// throw new CommitmentOperationNotPerformableException(c,
		// ProtocolArtifact.CANCEL_COMMITMENT);
		// }

		// UPDATE STRUCTURE BY LIFECYCLE
		
		// UPDATE COMMITMENT LIFECYCLE STATUS
		List<Commitment> actualCommits = commitmentsByStatus.get(c.getLifeCycleStatus());
		if (!actualCommits.isEmpty()) {
			if (actualCommits.contains(c)) {
				actualCommits.remove(c);

				// now change commitment status
				switch (c.getLifeCycleStatus()) {
				case CONDITIONAL:
					c.setStatus(LifeCycleState.TERMINATED);
					break;
				case DETACHED:
					c.setStatus(LifeCycleState.VIOLATED);
					break;
				default:
					throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);
				}

				// UPDATE COMMITMENT LIST BY LIFECYCLE STATUS
				actualCommits = commitmentsByStatus.get(c.getLifeCycleStatus());
				actualCommits.add(c);
				
			}
		} else {
			throw new CommitmentNotFoundInInteractionStateException(c);
		}

		logger.debug("Cancel operation finished, actual status: " + this);
		return true;
	}

	public boolean removeCommitment(Commitment c) throws CommitmentNotFoundInInteractionStateException {
		boolean allOk = false;
		Commitment cToCancel = null;
		for (Commitment actualC : allCommitments) {
			if (actualC.equals(c)) {
				cToCancel = actualC;
			}
		}
		if (cToCancel == null) {
			throw new CommitmentNotFoundInInteractionStateException(c);
		}
        synchronized(allCommitments) {
            allOk = allCommitments.remove(c);
        }
		List<Commitment> actualCommitsCreditor = commitByCreditor.get(c.getCreditor());
		if (actualCommitsCreditor != null) {
			actualCommitsCreditor.remove(c);
		}
		

		// UPDATE COMMITMENT LIST BY DEBTOR
		List<Commitment> actualCommitsDebtor = commitByDebtor.get(c.getDebtor());
		if (actualCommitsDebtor != null) {
			actualCommitsDebtor.remove(c);
		}

		// UPDATE COMMITMENT LIS BY LIFECYCLE STATUS
		List<Commitment> actualCommits = commitmentsByStatus.get(c.getLifeCycleStatus());
		if (!actualCommits.isEmpty()) {
			actualCommits.remove(c);			
		} else {
			actualCommits.add(c);
		}

		return allOk;
	}

	private void checkCommitmentsAfterInteractionStateElementAdding(SocialStateElement elAdded) {
		List<Commitment> condCommits = commitmentsByStatus.get(LifeCycleState.CONDITIONAL);
		List<Commitment> detachedCommits = commitmentsByStatus.get(LifeCycleState.DETACHED);
		switch (elAdded.getElType()) {
		case COMMITMENT:
			logger.trace("Just added a COMMITMENT, reasoning on interaction state after adding " + elAdded);
			Commitment c = (Commitment) elAdded;
			// search in conditional commitment set
			if (!condCommits.isEmpty()) {
				logger.trace("Parsing CONDITIONAL commitments");
				for (Commitment condComm : condCommits) {
					logger.debug("Commitment antecedent: " + condComm.getAntecedent());
					if (condComm.getAntecedent().equals(c)) {
						logger.debug("Commitment " + condComm + " has to change its state. Now is\n "
								+ condComm.getLifeCycleStatus() + ", setting to DETACHED.");

						condComm.setStatus(LifeCycleState.DETACHED);
						// try {
						// condComm.setAntecedent(new Fact("true"));
						// } catch (MissingOperandException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
					}

				}
			}
			// search in detached commitment set
			if (!detachedCommits.isEmpty()) {
				logger.trace("Parsing DETACHED commitments");
				for (Commitment condComm : detachedCommits) {
					if (condComm.getAntecedent().equals(c)) {
						logger.debug("Commitment " + condComm + " has to change its state. Now is\n "
								+ condComm.getLifeCycleStatus() + ", setting to DISCHARGED.");
						condComm.setStatus(LifeCycleState.SATISFIED);
						try {
							condComm.setAntecedent(new Fact("true"));
						} catch (MissingOperandException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}
			break;

		case LOGICAL_EXPRESSION:
			logger.trace("Just added a FACT, reasoning on interaction state after adding " + elAdded);
			LogicalExpression el = (LogicalExpression) elAdded;
			if (!condCommits.isEmpty()) {
				logger.trace("Parsing CONDITIONAL commitments");
				for (Commitment condComm : condCommits) {
					logger.debug("Commitment antecedent: " + condComm.getAntecedent());
					if (condComm.getAntecedent().equals(el)) {
						logger.debug("Commitment " + condComm + " has to change its state. Now is\n "
								+ condComm.getLifeCycleStatus() + ", setting to DETACHED.");
						condComm.setStatus(LifeCycleState.DETACHED);
						// try {
						// condComm.setAntecedent(new Fact("true"));
						// } catch (MissingOperandException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
					}

				}
			}
			// search in detached commitment set
			if (!detachedCommits.isEmpty()) {
				logger.trace("Parsing DETACHED commitments");
				for (Commitment condComm : detachedCommits) {
					if (condComm.getAntecedent().equals(el)) {
						logger.debug("Commitment " + condComm + " has to change its state. Now is\n "
								+ condComm.getLifeCycleStatus() + ", setting to DISCHARGED.");
						condComm.setStatus(LifeCycleState.SATISFIED);
						try {
							condComm.setAntecedent(new Fact("true"));
						} catch (MissingOperandException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}
			break;
		}

	}

	public String toString() {
		String st = "";
//		st += "\nROLES IN DEBTOR STATUS:\n------------------\n";
//		st += printRolesCommitment(commitByDebtor);
//		st += "\nROLES IN CREDITOR STATUS:\n-------------------\n";
//		st += printRolesCommitment(commitByCreditor);
		st += "\nFACTS:\n------------------\n";
		for (LogicalExpression f : facts) {
			st += "- " + f + "\n";
		}

		st += "\nCOMMITMENTS:\n------------------\n";
		for (Commitment c : allCommitments) {
			st += "- " + c + "\n";
		}
		return st + "\n";
	}

	public List<Commitment> retrieveAllCommitments() {
		return allCommitments;
	}

	public List<Fact> retrieveAllFacts() {
		return facts;
	}

	public List<Commitment> retrieveCommitmentsByLifecycleStatus(LifeCycleState status) {
		return commitmentsByStatus.get(status);
	}

	public List<Commitment> retrieveCommitmentsByCreditorRoleId(RoleId id) {
		return commitByCreditor.get(id);
	}

	public List<Commitment> retrieveCommitmentsByDebtorRoleId(RoleId id) {
		return commitByDebtor.get(id);
	}

	public ArrayList<Commitment> retrieveCommitmentsByConsequent(Fact f) {
		ArrayList<Commitment> retComm = new ArrayList<Commitment>();
		for (Commitment c : allCommitments) {
			if (c.getConsequent().equals(f)) {
				retComm.add(c);
			}
		}
		return retComm;
	}

	public ArrayList<Commitment> retrieveCommitmentsByAntecedent(Fact f) {
		ArrayList<Commitment> retComm = new ArrayList<Commitment>();
		for (Commitment c : allCommitments) {
			if (c.getAntecedent().equals(f)) {
				retComm.add(c);
			}
		}
		return retComm;
	}

	public ArrayList<Commitment> retrieveCommitmentsContainingFact(Fact f) {
		return commitByFact.get(f);
	}

	public boolean hasCommitment(Commitment c) {
		boolean present = false;
		for (Commitment com : allCommitments) {
			if (com.equals(c)) {
				present = true;
			}
		}
		return present;
	}

	private String printRolesCommitment(ConcurrentHashMap<RoleId, ArrayList<Commitment>> mp) {
		String ret = "";
		Collection<RoleId> clRoles = mp.keySet();
		for (RoleId r : clRoles) {
			ret += r + ", commitments: ";
			ArrayList<Commitment> commitsByRole = mp.get(r);
			for (Commitment c : commitsByRole) {
				ret += c + "\n";
			}
		}
		return ret;
	}

	private String printStatusCommitment(ConcurrentHashMap<LifeCycleState, ArrayList<Commitment>> mp) {
		String ret = "";
		Collection<LifeCycleState> clStates = mp.keySet();
		for (LifeCycleState r : clStates) {
			ret += r + ", commitments: ";
			ArrayList<Commitment> commitsByRole = mp.get(r);
			for (Commitment c : commitsByRole) {
				ret += c + "\n";
			}
		}
		return ret;
	}

	private String printFactCommitment(ConcurrentHashMap<Fact, ArrayList<Commitment>> mp) {
		String ret = "";
		Collection<Fact> clStates = mp.keySet();
		for (Fact r : clStates) {
			ret += r + ", commitments: ";
			ArrayList<Commitment> commitsByRole = mp.get(r);
			for (Commitment c : commitsByRole) {
				ret += c + "\n";
			}
		}
		return ret;
	}

	private String printHashSet(ArrayList hs) {
		String st = "";
		int i = 0;
		for (Object o : hs) {
			st += i + ": " + o + "\n";
			i++;
		}
		return st;
	}

    /*
	// implementati metodi per modificare lo stato dei commitment
	public void releaseCommitment(Commitment c) {
		synchronized (commitmentsByStatus) {
			if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				ArrayList<Commitment> conditionalCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.CONDITIONAL);
				conditionalCommitments.remove(c);
			} else {
				ArrayList<Commitment> detachedCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.DETACHED);
				detachedCommitments.remove(c);
			}
			synchronized (c) {
				c.setStatus(LifeCycleState.TERMINATED);
			}
			ArrayList<Commitment> terminatedCommitments = retrieveCommitmentsByLifecycleStatus(
					LifeCycleState.TERMINATED);
			terminatedCommitments.add(c);
		}
	}

	public void satisfyCommitment(Commitment c) {
		synchronized (commitmentsByStatus) {
			if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				ArrayList<Commitment> conditionalCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.CONDITIONAL);
				conditionalCommitments.remove(c);
			} else {
				ArrayList<Commitment> detachedCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.DETACHED);
				detachedCommitments.remove(c);
			}
			synchronized (c) {
				c.setStatus(LifeCycleState.SATISFIED);
			}
			ArrayList<Commitment> satisfiedCommitments = retrieveCommitmentsByLifecycleStatus(LifeCycleState.SATISFIED);
			satisfiedCommitments.add(c);
		}
	}

	public void detachCommitment(Commitment c) {
		synchronized (commitmentsByStatus) {
			ArrayList<Commitment> conditionalCommitments = retrieveCommitmentsByLifecycleStatus(
					LifeCycleState.CONDITIONAL);
			conditionalCommitments.remove(c);
			synchronized (c) {
				c.setStatus(LifeCycleState.DETACHED);
			}
			ArrayList<Commitment> detachedCommitments = retrieveCommitmentsByLifecycleStatus(LifeCycleState.DETACHED);
			detachedCommitments.add(c);
		}
	}

	public void violateCommitment(Commitment c) {
		synchronized (commitmentsByStatus) {
			if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				ArrayList<Commitment> conditionalCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.CONDITIONAL);
				conditionalCommitments.remove(c);
			} else {
				ArrayList<Commitment> detachedCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.DETACHED);
				detachedCommitments.remove(c);
			}
			synchronized (c) {
				c.setStatus(LifeCycleState.VIOLATED);
			}
			ArrayList<Commitment> violatedCommitments = retrieveCommitmentsByLifecycleStatus(LifeCycleState.VIOLATED);
			violatedCommitments.add(c);
		}
	}
    */
	
	/**
	 * Change the status of commitment c to the new status newstate. It removes
	 * c from list of conditional or detached commitments, and updates
	 * the structure maintaining the list of commitment with state newstate.
	 * @param c the commitment whose status has to be changed.
	 * @param newState the new status, according to the commitment lifecycle possible states.
	 */
	public void changeCommitmentStatus(Commitment c, LifeCycleState newState) {
		logger.debug("Changing status of commitment: "+c);
		synchronized (commitmentsByStatus) {
			if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				List<Commitment> conditionalCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.CONDITIONAL);
				if (newState != LifeCycleState.CONDITIONAL) {
					synchronized (art) {
						art.updateCommitmentObsPropertyByStatus(c, c.getLifeCycleStatus(), newState);
					}
					conditionalCommitments.remove(c);
					synchronized (c) {
						c.setStatus(newState);
					}
					List<Commitment> comms = retrieveCommitmentsByLifecycleStatus(newState);
					comms.add(c);
				}
					
			} else if (c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
				List<Commitment> detachedCommitments = retrieveCommitmentsByLifecycleStatus(
						LifeCycleState.DETACHED);
				if (newState != LifeCycleState.DETACHED) {
					synchronized (art) {
						art.updateCommitmentObsPropertyByStatus(c, c.getLifeCycleStatus(), newState);
					}
					detachedCommitments.remove(c);
					synchronized (c) {
						c.setStatus(newState);
					}
					List<Commitment> comms = retrieveCommitmentsByLifecycleStatus(newState);
					comms.add(c);
				}
			}
		}
		logger.debug("New status: "+c);
	}
	
	public void notifyObs(SocialEvent e, Object... args) {
		art.setChanged();
		art.notifyObservers(e, args);
	}

}

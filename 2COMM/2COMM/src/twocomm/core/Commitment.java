package twocomm.core;

import javax.persistence.Embeddable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.LiteralImpl;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.LogicalExpressionType;
import twocomm.exception.MissingOperandException;

@Embeddable
public class Commitment extends SocialStateElement {

	// Standard static logger
	private static Logger logger = LogManager.getLogger(Commitment.class);

	private RoleId debtor;
	private RoleId creditor;
	private SocialStateElement antecedent;
	private SocialStateElement consequent;
	private LifeCycleState status;

	/*
	 * Constructors for ConditionalCommitment and Commitment. The two Roles are:
	 * the first as Debtor, the second as Creditor. The two
	 * InteractionStateElement are the antecedent and consequent conditions. The
	 * constructor has no scope qualifier, because a Commitment instance can be
	 * created only by InteractionState (that is in the same package).
	 */
	public Commitment(RoleId d, RoleId c, SocialStateElement ant, SocialStateElement cons) {
		super();
		this.elType = SocialStateElementType.COMMITMENT;
		debtor = d;
		creditor = c;
		antecedent = ant;
		consequent = cons;
		status = LifeCycleState.CONDITIONAL;
		if(antecedent instanceof LogicalExpression) {
			LogicalExpression p = (LogicalExpression) antecedent;
			if(p.isVerified()) {
				status = LifeCycleState.DETACHED;
			}
		}
		if(consequent instanceof LogicalExpression) {
			LogicalExpression q = (LogicalExpression) consequent;
			if(q.isVerified()) {
				status = LifeCycleState.SATISFIED;
			}
			else if(q.isViolated() && antecedent instanceof LogicalExpression && ((LogicalExpression) antecedent).isVerified()) {
				status = LifeCycleState.VIOLATED;
			}
		}
	}

	public Commitment(RoleId d, RoleId c, String ant, SocialStateElement cons) {
		super();
		this.elType = SocialStateElementType.COMMITMENT;
		debtor = d;
		creditor = c;
		try {
			antecedent = new Fact(ant);
		} catch (MissingOperandException e) {
			logger.error("Error in creating a commitment with " + ant + " antecedend" + e.getCause());
			e.printStackTrace();
		}
		consequent = cons;
		status = LifeCycleState.CONDITIONAL;
		if(antecedent instanceof LogicalExpression) {
			LogicalExpression p = (LogicalExpression) antecedent;
			if(p.isVerified()) {
				status = LifeCycleState.DETACHED;
			}
		}
		if(consequent instanceof LogicalExpression) {
			LogicalExpression q = (LogicalExpression) consequent;
			if(q.isVerified()) {
				status = LifeCycleState.SATISFIED;
			}
			else if(q.isViolated() && antecedent instanceof LogicalExpression && ((LogicalExpression) antecedent).isVerified()) {
				status = LifeCycleState.VIOLATED;
			}
		}
	}

	public Commitment(RoleId d, RoleId c, SocialStateElement ant, String cons) {
		super();
		this.elType = SocialStateElementType.COMMITMENT;
		debtor = d;
		creditor = c;
		antecedent = ant;
		try {
			consequent = new Fact(cons);
		} catch (MissingOperandException e) {
			logger.error("Error in creating a commitment with " + cons + " consequent" + e.getCause());
			e.printStackTrace();
		}
		status = LifeCycleState.CONDITIONAL;
		if(antecedent instanceof LogicalExpression) {
			LogicalExpression p = (LogicalExpression) antecedent;
			if(p.isVerified()) {
				status = LifeCycleState.DETACHED;
			}
		}
		if(consequent instanceof LogicalExpression) {
			LogicalExpression q = (LogicalExpression) consequent;
			if(q.isVerified()) {
				status = LifeCycleState.SATISFIED;
			}
			else if(q.isViolated() && antecedent instanceof LogicalExpression && ((LogicalExpression) antecedent).isVerified()) {
				status = LifeCycleState.VIOLATED;
			}
		}
	}

	public Commitment(RoleId d, RoleId c, String ant, String cons) {
		super();
		this.elType = SocialStateElementType.COMMITMENT;
		debtor = d;
		creditor = c;

		try {
			antecedent = new Fact(ant);
			consequent = new Fact(cons);
		} catch (MissingOperandException e) {
			logger.error("Error in creating a commitment with " + ant + " antecend and " + cons + " consequent"
					+ e.getCause());
			e.printStackTrace();
		}
		status = LifeCycleState.CONDITIONAL;
		if(antecedent instanceof LogicalExpression) {
			LogicalExpression p = (LogicalExpression) antecedent;
			if(p.isVerified()) {
				status = LifeCycleState.DETACHED;
			}
		}
		if(consequent instanceof LogicalExpression) {
			LogicalExpression q = (LogicalExpression) consequent;
			if(q.isVerified()) {
				status = LifeCycleState.SATISFIED;
			}
			else if(q.isViolated() && antecedent instanceof LogicalExpression && ((LogicalExpression) antecedent).isVerified()) {
				status = LifeCycleState.VIOLATED;
			}
		}
	}

	public Commitment(RoleId d, RoleId c, SocialStateElement o) {
		super();
		this.elType = SocialStateElementType.COMMITMENT;
		debtor = d;
		creditor = c;
		try {
			antecedent = new Fact("true");
		} catch (Exception e) {
			logger.error("Error in creating a commitment with true antecedend" + e.getCause());
			e.printStackTrace();
		}
		consequent = o;
		status = LifeCycleState.DETACHED;
		if(consequent instanceof LogicalExpression) {
			LogicalExpression q = (LogicalExpression) consequent;
			if(q.isVerified()) {
				status = LifeCycleState.SATISFIED;
			}
			else if(q.isViolated() && antecedent instanceof LogicalExpression && ((LogicalExpression) antecedent).isVerified()) {
				status = LifeCycleState.VIOLATED;
			}
		}
	}

	public Commitment(RoleId d, RoleId c, String o) {
		super();
		this.elType = SocialStateElementType.COMMITMENT;
		debtor = d;
		creditor = c;
		try {
			antecedent = new Fact("true");
			consequent = new Fact(o);
		} catch (Exception e) {
			logger.error("Error in creating a commitment with true antecedend" + e.getCause());
			e.printStackTrace();
		}
		status = LifeCycleState.DETACHED;
		if(consequent instanceof LogicalExpression) {
			LogicalExpression q = (LogicalExpression) consequent;
			if(q.isVerified()) {
				status = LifeCycleState.SATISFIED;
			}
			else if(q.isViolated() && antecedent instanceof LogicalExpression && ((LogicalExpression) antecedent).isVerified()) {
				status = LifeCycleState.VIOLATED;
			}
		}
	}

	// Useful constructors for commitment requirements
	public Commitment(String d, String c, String o) {
		this(new RoleId(d, RoleId.GROUP_ROLE), new RoleId(c, RoleId.GROUP_ROLE), o);
	}

	public Commitment(String d, String c, String ant, String cons) {
		this(new RoleId(d, RoleId.GROUP_ROLE), new RoleId(c, RoleId.GROUP_ROLE), ant, cons);
	}

	public Commitment(String d, String c, String ant, SocialStateElement cons) {
		this(new RoleId(d, RoleId.GROUP_ROLE), new RoleId(c, RoleId.GROUP_ROLE), ant, cons);
	}

	public Commitment(String d, String c, SocialStateElement ant, String cons) {
		this(new RoleId(d, RoleId.GROUP_ROLE), new RoleId(c, RoleId.GROUP_ROLE), ant, cons);
	}

	public Commitment(RoleId debtor2, RoleId creditor2, SocialStateElement antecedent2, SocialStateElement consequent2,
			LifeCycleState status) {
		this(debtor2, creditor2, antecedent2, consequent2);
		this.setStatus(status);
	}

	
	/*
	 * Getter and Setter methods.
	 * 
	 */
	public void setStatus(LifeCycleState st) {
		status = st;
	}

	public LifeCycleState getLifeCycleStatus() {
		return status;
	}

	public RoleId getDebtor() {
		return debtor;
	}

	public void setDebtor(RoleId debtor) {
		this.debtor = debtor;
	}

	public RoleId getCreditor() {
		return creditor;
	}

	public void setCreditor(RoleId creditor) {
		this.creditor = creditor;
	}

	public SocialStateElement getAntecedent() {
		return antecedent;
	}

	public void setAntecedent(SocialStateElement antecedent) {
		this.antecedent = antecedent;
	}

	public SocialStateElement getConsequent() {
		return consequent;
	}

	public void setConsequent(SocialStateElement consequent) {
		this.consequent = consequent;
	}

	/*
	 * STAMPA PER ORA APPROSSIMATIVA
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "cc(" + getDebtor() + ", " + getCreditor() + ", " + getAntecedent() + ", " + getConsequent()
				+ ", " + getLifeCycleStatus() + ")";
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other == this) return true;
		if (!(other instanceof SocialStateElement)) return false;
		
		SocialStateElement el = (SocialStateElement)other;
		if (el.getElType() != SocialStateElementType.COMMITMENT) {
			return false;
		}
		Commitment c = (Commitment) el;
		boolean equals = (this.getCreditor().equals(c.getCreditor()) && this.getDebtor().equals(c.getDebtor())
				&& this.getAntecedent().equals(c.getAntecedent()) && this.getConsequent().equals(c.getConsequent()));
		return equals;
	}
	
	@Override
	public int hashCode() {
		return this.antecedent.hashCode() + this.consequent.hashCode();
	}

	@Override
	public Term toJasonTerm() {
		
		Structure struct = new Structure("cc", 5);
		
		Literal d = new LiteralImpl(debtor.toString());
		struct.addTerm(d);
		
		Literal c = new LiteralImpl(creditor.toString());
		struct.addTerm(c);
		
		Term ant = antecedent.toJasonTerm();
		struct.addTerm(ant);
		
		Term cons = consequent.toJasonTerm();
		struct.addTerm(cons);
		
		Literal stat = new LiteralImpl(status.toString());
		struct.addTerm(stat);
		
		return struct;
		
	}

}

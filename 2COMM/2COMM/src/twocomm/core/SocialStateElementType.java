package twocomm.core;

import javax.persistence.Embeddable;

@Embeddable
public enum SocialStateElementType {

	COMMITMENT, LOGICAL_EXPRESSION
}

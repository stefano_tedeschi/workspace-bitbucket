package twocomm.core.typing;



import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.CommunicationArtifact.CARole;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.CommitmentOperationNotPerformableException;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import cartago.OPERATION;
import cartago.ObsProperty;
import jade.core.behaviours.Behaviour;

/*
 * This artifact extends the 'TupleSpace' artifact.
 * Communication is performed via tuplespace; besides that, an interaction state is mantained.
 * 
 * Every communication artifact has to add to static HashSet 'enabledRoles' the roles
 * an agent can enact, via method 'addEnabledRole(String roleName)'.
 * 
 */

public abstract class TypedProtocolArtifact extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(TypedProtocolArtifact.class);
	

	@Override
	protected boolean checkRoleRequirements(String roleName, IPlayer player) {
		if (super.checkRoleRequirements(roleName, player)) {
			JadeBehaviourPlayer jPlayer = (JadeBehaviourPlayer) player;
			Behaviour[] offeredPlayerBehaviours = jPlayer.getJadeBehaviour();
			
			logger.debug("Retrieving annotations for behaviour classes...");			
			
			//******************* ESTRAZIONE BEHAVIOURSTYPES - AGENTE
			Class<? extends Behaviour> behClass;
			ArrayList<Annotation> behaviourTypeAnnotations = new ArrayList<Annotation>();
			Annotation behaviourTypeAnnotation;
			for (Behaviour beh : offeredPlayerBehaviours) {
				 behClass = beh.getClass();
				 behaviourTypeAnnotation = behClass.getAnnotation(BehaviourType.class);
				 if (behaviourTypeAnnotation == null) {
					logger.debug("Annotation 'BehaviourType' is missing for behaviour");
					return false;
				 }
				 behaviourTypeAnnotations.add(behaviourTypeAnnotation);
			}		
			
			logger.debug("AGENT BEHAVIOURS: Found "+behaviourTypeAnnotations.size()+" annotations.");
			logger.debug("Retrieving annotations for role class...");
			
			//****************** ESTRAZIONE REQUIREMENTS - RUOLO
			Class<?> roleClass;
			String roleClassName = null;
			try {
				roleClassName = (this.getClass().getName())+"$"+roleName;
				logger.debug("Retrieving class for requested "+roleClassName);
				roleClass = Class.forName(roleClassName);
			} catch (ClassNotFoundException e) {
				logger.debug("Bad role name: "+roleClassName+", error: "+e.getMessage());
				return false;
			}
			Annotation roleAnnotation = roleClass.getAnnotation(RoleType.class);
			if (roleAnnotation == null) {
				logger.debug("Annotation 'RoleType' is missing for role");
				return false;
			}
			
			// Both annotations retrieved
			logger.debug("Both annotations retrieved. Checking requirements compliance...");
			
			// Getting instances for retrieved types
			
			RoleRequirements roleReq = null;
			try {
				roleReq = ((RoleType)roleAnnotation).requirements().getDeclaredConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				logger.error("Error in retrieving RoleRequirments: "+e.getMessage());
				e.printStackTrace();
			}
			logger.debug("Role Type: "+roleReq);
			
			ArrayList<BType> overallBTypes = mergeBTypes(behaviourTypeAnnotations);
			logger.debug("Overall Offered Types: "+overallBTypes);	
			
			// passo tutti i requirements di ruolo e l'insieme dei btypes offerti dall'agente
			return roleReq.isEnactable(overallBTypes);
		}
		else return false;
	}
	
	
	// serve creare un array di btypes, istanziando tutte quelle che ho trovato
	// tramite camp dell'annotazione
	protected ArrayList<BType> mergeBTypes(List<Annotation> behaviourTypeAnnotations) {
		ArrayList<BType> overallBTypes = new ArrayList<BType>();
		BType bT;
		for (Annotation ann : behaviourTypeAnnotations) {
			try {
				bT = ((BehaviourType)ann).bType().getDeclaredConstructor().newInstance();
				overallBTypes.add(bT);
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return overallBTypes;
	}

}

package twocomm.core.typing;

import java.util.ArrayList;

import twocomm.core.Commitment;
import twocomm.core.SocialEvent;

public abstract class BType {

	private Commitment[] commitments;
	private PartialActualization partialActualization = new PartialActualization(this);
	private ProtocolAction newAction;
	
	public BType(Commitment[] c, PartialActualization pAct, ProtocolAction newAct) {
		commitments = c;
		partialActualization = pAct;
		newAction = newAct;
	}
	
	public BType(Commitment[] c, ProtocolAction newAct) {
		commitments = c;
		try {
			this.newAction = newAct;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public BType(BType previousBType, ProtocolAction newAct) {
		this.commitments = previousBType.getCommitments();
		this.partialActualization = previousBType.getPartialActualization().clone();
		try {
			this.partialActualization.addAction(previousBType.getNewAction());
			this.newAction = newAct;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	


	public Commitment[] getCommitments() {
		return commitments;
	}

	public void setCommitment(Commitment[] commitment) {
		this.commitments = commitment;
	}

	public PartialActualization getPartialActualization() {
		return partialActualization;
	}

	public void setPartialActualization(PartialActualization partialActualization) {
		this.partialActualization = partialActualization;
	}

	public ProtocolAction getNewAction() {
		return newAction;
	}

	public void setNewAction(ProtocolAction newAction) {
		this.newAction = newAction;
	}
	
	public String toString() {
		String str = "\nBehaviourType: "+this.getClass()+"\n";
		str += "Commitments: ";
		for (Commitment c : commitments) {
			str += c+"\n";
		}
		str += "Partial Actualization: "+partialActualization;
		str += "Protocol Action: "+newAction+"\n";
		return str;
		
	}
}
package twocomm.core.typing;

import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;

/**
 * Questa classe rappresenta un'azione del protocollo,
 * intesa come evento sociale di fatto aggiunto.
 * TODO: valutare se ha senso cosi oppure meglio
 * utilizzare una tassonomia di azioni offerta
 * direttamente dal protocollo.
 * @author elron
 *
 */

public class ProtocolAction extends SocialEvent {

	public ProtocolAction(Fact f) {
		super(f, SocialEventActionType.FACT_ASSERTED);
	}
	
	public ProtocolAction(String action) throws MissingOperandException {
		super(new Fact(action), SocialEventActionType.FACT_ASSERTED);
	}
	
	public String toString() {
		return this.getElementChanged().toString();
	}
	
	public boolean equals(Object other) {
		return this.getElementChanged().equals(((ProtocolAction)other).getElementChanged());
	}
}

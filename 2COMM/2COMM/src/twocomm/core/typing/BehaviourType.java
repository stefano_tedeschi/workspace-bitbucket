package twocomm.core.typing;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


/**
 * Classe di annotazione per il tipaggio. Si utilizza nel behaviour (JADE)
 * di cui si vuole definire il tipo:
 * @BehaviourType(bType=
 * @author elron
 *
 */

@Retention(RetentionPolicy.RUNTIME)

@Target({ElementType.TYPE})
public @interface BehaviourType {

	Class<? extends BType> bType();
	
}

package twocomm.core.typing;

import java.util.ArrayList;

import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;

/**
 * Un'attualizzazione consiste una sequenza di eventi sociali che portano un commitment
 * a rendere true il suo conseguente, ovvero soddisfano il commitment (stato satisfied).
 * Un'attualizzazione parziale e' semplicemente una sottosequenza di una (presunta)
 * attualizzazione.
 * @author elron
 *
 */

public class PartialActualization {

	private ArrayList<ProtocolAction> actionList = new ArrayList<ProtocolAction>();
	private BType containerBType;
	
	public PartialActualization() {
		super();
	}
	
	public PartialActualization(BType bt) {
		this.containerBType = bt;
	}
	
	public PartialActualization(ProtocolAction se, BType bt) throws Exception {
		if (se.getAction() != SocialEventActionType.FACT_ASSERTED)
			throw new Exception("Wrong Social Event");
		this.actionList.add(se);
		this.containerBType = bt;
	}
	
	public void addAction(ProtocolAction se) throws Exception {
		if (se.getAction() != SocialEventActionType.FACT_ASSERTED)
			throw new Exception("Wrong Social Event");
		this.actionList.add(se);
	}
	
	public PartialActualization clone() {
		PartialActualization pa = new PartialActualization(containerBType);
		pa.setEventList(new ArrayList<ProtocolAction>(this.getActionList()));
		return pa;
	}

	public ArrayList<ProtocolAction> getActionList() {
		return actionList;
	}

	public void setEventList(ArrayList<ProtocolAction> eventList) {
		this.actionList = eventList;
	}
	
	public boolean isEmpty() {
		return actionList.isEmpty();
	}
	
	public BType getContainerBType() {
		return this.containerBType;
	}
	
	public String toString() {
		String str = "";
		if (actionList.isEmpty()) return "Empty";
		for (ProtocolAction pa : actionList) {
			str += pa+"-";
		}
		return str;
	}
	
	public int hashCode() {
		int hash = 0;
		for (ProtocolAction p : actionList) {
			hash += p.getElementChanged().toString().length();
		}
		hash += actionList.size();
		return hash;
	}
	
	/**
	 * Due PartialActualization sono uguali se sono uguali e nello stesso ordine
	 * tutte le Protocol Action che le compongono
	 */
	public boolean equals(Object other) {
		PartialActualization paOther = (PartialActualization)other;
		if (this.getActionList().size() != paOther.getActionList().size()) return false;
		for (int i=0;i<this.getActionList().size();i++) {
			if (!(this.getActionList().get(i).equals(paOther.getActionList().get(i)))) return false;
		}
		return true;
		
	}
}

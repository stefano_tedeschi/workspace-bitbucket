package twocomm.core.typing.old;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import twocomm.core.logic.Fact;

/**
 * Class for agent typing based on facts
 * the agent declares to make true.
 * @author Federico Capuzzimati
 *
 */

public class AgentCapabilities {
	final private Set<Fact> DEFINITION;
	
	protected AgentCapabilities(Fact[] factDefinition) {
		DEFINITION = new HashSet<Fact>();
		for (Fact c : factDefinition) {
			DEFINITION.add(c);
		}
	}
	
	public Set<Fact> getCapabilities() {
		return this.DEFINITION;
	}
	
	public boolean containsCapability(Fact fe) {
		return DEFINITION.contains(fe);
	}
	
	public static AgentCapabilities merge(List<AgentCapabilities> agentAbilitiesToMerge) {
		ArrayList<Fact> factsToMerge = new ArrayList<Fact>();
		for (AgentCapabilities t : agentAbilitiesToMerge) {
			for (Fact f : t.DEFINITION) {
				if (!factsToMerge.contains(f)) {
					factsToMerge.add(f);
				}
			}
		}
		AgentCapabilities t = new AgentCapabilities(factsToMerge.toArray(new Fact[factsToMerge.size()]));
		return t;
	}
	
	public String toString() {
		String str = "AgentAbilities for "+this.getClass().getTypeName()+"\n";
		for (Fact c : DEFINITION) {
			str += c + "\n";
		}
		return str;
	}
}

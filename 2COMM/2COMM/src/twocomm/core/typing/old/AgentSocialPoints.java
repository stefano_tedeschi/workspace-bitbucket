package twocomm.core.typing.old;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import twocomm.core.Commitment;
import twocomm.core.logic.Fact;

/**
 * Class for agent typing based on stages of a commitment lifecycle that
 * the agent declares to manage.
 * @author Federico Capuzzimati
 *
 */

public class AgentSocialPoints {
	final private Set<SocialPoint> DEFINITION;
	
	private String agentName = "UKNOWN";
	
	protected AgentSocialPoints(SocialPoint[] agentSP) {
		DEFINITION = new HashSet<SocialPoint>();
		for (SocialPoint c : agentSP) {
			DEFINITION.add(c);
		}
	}
	
	public void setAgentName(String name) {
		this.agentName = name;
	}
	
	public String getAgentName() {
		return this.agentName;
	}
	
	public Set<SocialPoint> getSocialPoints() {
		return this.DEFINITION;
	}
	
	public List<SocialPoint> getSocialPointsForCommitment(Commitment c) {
		ArrayList<SocialPoint> sps = new ArrayList<SocialPoint>();
		for (SocialPoint sp : DEFINITION) {
			if (sp.getCommitment().equals((c)))
				sps.add(sp);
		}
		return sps;
	}
	
	/**
	 * Prende una lista di AgentSocialPoints (quindi, associati a behaviour)
	 * e ne restituisce uno che ne rappresenta l'unione.
	 * 
	 * @param commitmentsManagedToMerge
	 * @return
	 */
	public static AgentSocialPoints merge(List<AgentSocialPoints> agSocPointsToMerge) {
		ArrayList<SocialPoint> mergingAgSocPoints = new ArrayList<SocialPoint>();
		for (AgentSocialPoints agSP : agSocPointsToMerge) {
			for (SocialPoint mc : agSP.DEFINITION) {
				if (!mergingAgSocPoints.contains(mc)) {
					// DA RIVEDERE QUESTO PASSAGGIO
					mergingAgSocPoints.add(mc);
				}
			}
		}
		AgentSocialPoints t = new AgentSocialPoints(mergingAgSocPoints.toArray(new SocialPoint[mergingAgSocPoints.size()]));
		return t;
	}
	
	public String toString() {
		String str = "Social points for "+this.agentName+"\n";
		for (SocialPoint c : DEFINITION) {
			str += c + "\n";
		}
		return str;
	}
}

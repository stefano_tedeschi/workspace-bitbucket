package twocomm.core.typing.old;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.SocialStateElementType;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.LogicalExpressionType;
import twocomm.core.typing.old.AgentCapabilities;
import twocomm.core.typing.old.CommitmentsManaged;
import twocomm.core.typing.old.ManagedCommitment;


/**
 * An object of type RoleRequirements denotes which
 * @author Federico Capuzzimati
 *
 */
public class RoleRequirements {

	protected static final Logger staticLogger = LogManager.getLogger(RoleRequirements.class);
	
	final private Set<Commitment> DEFINITION;
	
	protected RoleRequirements(Commitment[] commitsDefinition) {
		DEFINITION = new HashSet<Commitment>();
		for (Commitment c : commitsDefinition) {
			DEFINITION.add(c);
		}
	}
	
	
	public boolean equals(RoleRequirements t) {
		return this.isIncluded(t) && t.isIncluded(this);
	}
	
	public Set<Commitment> getRoleRequirements() {
		return this.DEFINITION;
	}
	
	public boolean isIncluded(RoleRequirements includerType)  {
		
		boolean included = true;
		for (Commitment c : this.DEFINITION) {
			if (included) {
				included = false;
				for (Commitment d : includerType.DEFINITION) {
					if (c.equals(d)) {
						included = true;
						break;
					}
				}
			}
			else break;
		}
		return included;
	}
	
	public String toString() {
		String str = "Type "+this.getClass().getSimpleName()+"\n";
		for (Commitment c : DEFINITION) {
			str += c + "\n";
		}
		return str;
	}


	public static RoleRequirements merge(List<RoleRequirements> typesToMerge) {
		ArrayList<Commitment> commitmentsToMerge = new ArrayList<Commitment>();
		for (RoleRequirements t : typesToMerge) {
			for (Commitment c : t.DEFINITION) {
				if (!commitmentsToMerge.contains(c)) {
					commitmentsToMerge.add(c);
				}
			}
		}
		RoleRequirements t = new RoleRequirements(commitmentsToMerge.toArray(new Commitment[commitmentsToMerge.size()]));
		return t;
	}
	
	// restituisce tutti i conseguenti dei commitments che compongono i requirements.
	// utile per la verifica che sia enactable un agente con certe AgentCapabilities
	private List<LogicalExpression> returnAllConsequents() {
		List<LogicalExpression> consequents = new ArrayList<LogicalExpression>();
		for (Commitment c : DEFINITION) {
			// deve essere una logical expression, NON un commitment a sua volta
			if (c.getConsequent().getElType() == SocialStateElementType.LOGICAL_EXPRESSION) {
				LogicalExpression le = (LogicalExpression)c.getConsequent();
				consequents.add(le);
			}
			
		}
		return consequents;
	}
	
	private static boolean enactmentFunction(LogicalExpression le, AgentCapabilities capabilities) {
		// caso base: le e' un fatto, restituisce true se e' contenuto nelle abilities
		if (le.getLogExpType() == LogicalExpressionType.FACT) {
			return capabilities.containsCapability((Fact)le);
		} else { // e' una COMPOSITE EXPRESSION, valuto i casi
			CompositeExpression ce = (CompositeExpression)le;
			switch (ce.getOperator()) {
			case AND:
				return enactmentFunction(ce.getLeft(), capabilities) && enactmentFunction(ce.getRight(), capabilities);				
			case NOT:
				return true;
			case OR:
				return enactmentFunction(ce.getLeft(), capabilities) || enactmentFunction(ce.getRight(), capabilities);
			case THEN:
				return enactmentFunction(ce.getLeft(), capabilities) && enactmentFunction(ce.getRight(), capabilities);
			default:
				return false;
			
			}
		}
		
	}
	
	
	// VERSIONE CON CONTROLLO FACTS
	public static boolean isEnactable(AgentCapabilities capabilities, RoleRequirements req) {
		boolean ok = true;
		List<LogicalExpression> roleRequirementsConsequents = req.returnAllConsequents();
		for (LogicalExpression le : roleRequirementsConsequents) {
			staticLogger.trace("Actual logical expression of Role Requirements:\n"+le);
			ok = enactmentFunction(le, capabilities);
		}
		return ok;
	}
	
	// VERSIONE CON CONTROLLO COMMITMENTS
	public static boolean isEnactable(CommitmentsManaged commsManaged, RoleRequirements req) {
		ManagedCommitment mc = null;
		boolean notAllManaged = true;
		for (Commitment c : req.DEFINITION) {
			mc = commsManaged.getCommitmentManaged(c);
			// basta che ne trovi uno che non c'e', e posso restituire false
			if (mc == null) return false;
			ArrayList<LifeCycleState> st = mc.getManagedStates();
			notAllManaged = ManagedCommitment.areAllStatesManaged(st);
			if (!notAllManaged) return false;
		}
		return notAllManaged;
	}
}

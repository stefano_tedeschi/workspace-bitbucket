package twocomm.core.typing.old;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import twocomm.core.typing.RoleRequirements; 

@Retention(RetentionPolicy.RUNTIME)

@Target({ElementType.TYPE})
public @interface RoleType {

	Class<? extends RoleRequirements> requirements();
	int interactionCardinality() default 1;
}

package twocomm.core.typing.old;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;




public class SocialPoint {
	
	public static final String DEBTOR = "debtor";
	public static final String CREDITOR = "creditor";
	
	protected static final Logger staticLogger = LogManager.getLogger(SocialPoint.class);
	
	
	
	private Commitment commitment;
	private String roleInCommitment;
	private LifeCycleState managedState;
	
	public SocialPoint(Commitment c, String r, LifeCycleState st) {
		commitment = c;
		roleInCommitment = r;
		managedState = st;
	}

	public Commitment getCommitment() {
		return commitment;
	}

	public void setCommitment(Commitment commitment) {
		this.commitment = commitment;
	}

	public String getRoleInCommitment() {
		return roleInCommitment;
	}

	public void setRoleInCommitment(String roleInCommitment) {
		this.roleInCommitment = roleInCommitment;
	}

	public LifeCycleState getManagedState() {
		return managedState;
	}

	public void setManagedState(LifeCycleState managedState) {
		this.managedState = managedState;
	}
	
	public String toString() {
		return "SocialPoint for commitment: "+commitment+" with role: "+roleInCommitment+" and for state: "+managedState;
	}
	
	
}

package twocomm.core.typing.old;

import twocomm.core.Commitment;

/**
 * Questa rappresenta i social point con tutti gli stadi del ciclo di vita.
 * @author elron
 *
 */
public class CompleteSocialPoint {

	private Commitment commitment;
	private String roleInCommitment;
	
	public CompleteSocialPoint(Commitment c, String r) {
		commitment = c;
		roleInCommitment = r;
	}
	
	public CompleteSocialPoint(SocialPoint sp) {
		commitment = sp.getCommitment();
		roleInCommitment = sp.getRoleInCommitment();
	}

	public Commitment getCommitment() {
		return commitment;
	}

	public void setCommitment(Commitment commitment) {
		this.commitment = commitment;
	}

	public String getRoleInCommitment() {
		return roleInCommitment;
	}

	public void setRoleInCommitment(String roleInCommitment) {
		this.roleInCommitment = roleInCommitment;
	}
	
	public String toString() {
		return "Complete Social Point for commitment: "+commitment+", required role: "+roleInCommitment;
	}
}

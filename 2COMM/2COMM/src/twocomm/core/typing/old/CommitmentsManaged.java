package twocomm.core.typing.old;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import twocomm.core.Commitment;
import twocomm.core.logic.Fact;

/**
 * Class for agent typing based on stages of a commitment lifecycle that
 * the agent declares to manage.
 * @author Federico Capuzzimati
 *
 */

public class CommitmentsManaged {
	final private Set<ManagedCommitment> DEFINITION;
	
	protected CommitmentsManaged(ManagedCommitment[] commitmentStatesManaged) {
		DEFINITION = new HashSet<ManagedCommitment>();
		for (ManagedCommitment c : commitmentStatesManaged) {
			DEFINITION.add(c);
		}
	}
	
	public Set<ManagedCommitment> getCommitmentsManaged() {
		return this.DEFINITION;
	}
	
	public ManagedCommitment getCommitmentManaged(Commitment c) {
		for (ManagedCommitment mc : DEFINITION) {
			if (mc.isCommitmentManaged(c))
				return mc;
		}
		return null;
	}
	
	/**
	 * Prende una lista di CommitmentsManaged (quindi, associati a behaviour)
	 * e ne restituisce uno che ne rappresenta l'unione.
	 * 
	 * @param commitmentsManagedToMerge
	 * @return
	 */
	public static CommitmentsManaged merge(List<CommitmentsManaged> commitmentsManagedToMerge) {
		ArrayList<ManagedCommitment> managedCommitsToMerge = new ArrayList<ManagedCommitment>();
		for (CommitmentsManaged commitsManaged : commitmentsManagedToMerge) {
			for (ManagedCommitment mc : commitsManaged.DEFINITION) {
				if (!managedCommitsToMerge.contains(mc)) {
					managedCommitsToMerge.add(mc);
				}
			}
		}
		CommitmentsManaged t = new CommitmentsManaged(managedCommitsToMerge.toArray(new ManagedCommitment[managedCommitsToMerge.size()]));
		return t;
	}
	
	public String toString() {
		String str = "CommitmentsManaged for "+this.getClass().getTypeName()+"\n";
		for (ManagedCommitment c : DEFINITION) {
			str += c + "\n";
		}
		return str;
	}
}

package twocomm.core.typing.old;

import java.util.ArrayList;
import java.util.List;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;

public class ManagedCommitment {

	private Commitment commitment;
	private ArrayList<LifeCycleState> managedStates = new ArrayList<LifeCycleState>();
	
	public ManagedCommitment(Commitment c, LifeCycleState ...cycleStates ) {
		commitment = c;
		for (LifeCycleState st : cycleStates) {
			managedStates.add(st);
		}
	}
	
	/**
	 * Add the parameter to the list of managed states of this managed commitment.
	 * @param st
	 */
	public void addManagedState(LifeCycleState st) {
		if (!managedStates.contains(st))
			managedStates.add(st);
	}
	
	public Commitment getManagedCommitment() {
		return this.commitment;
	}
	
	public ArrayList<LifeCycleState> getManagedStates() {
		return this.managedStates;
	}
	
	public boolean isCommitmentStateManaged(LifeCycleState st) {
		return managedStates.contains(st);
	}
	
	public boolean isCommitmentManaged(Commitment c) {
		return this.commitment.equals(c);
	}
	
	public void mergeStatesManagedCommitment(ManagedCommitment mc) {
		ArrayList<LifeCycleState> managedSt = mc.getManagedStates();
		for (LifeCycleState st : managedSt) {
			this.addManagedState(st);
		}
	}
	
	
	public String toString() {
		String str = commitment.toString() + "\nSTATES MANAGED:\n ";
		for (LifeCycleState st : managedStates) {
			str += st + "\n";
		}
		return str;
	}
	
	/**
	 * Return false if the commitments are different; if commitments are equal,
	 * return false if the lists of managed states are different.
	 */
	public boolean equals(Object mg) {
		if (! (mg instanceof ManagedCommitment)) return false;
		ManagedCommitment mc = (ManagedCommitment)mg;
		if (!(mc.getManagedCommitment().equals(this.getManagedCommitment()))) return false;
		else {
			ArrayList<LifeCycleState> otherStates = mc.getManagedStates();
			for (LifeCycleState st : this.managedStates) {
				if (!(otherStates.contains(st))) return false;
			}
			for (LifeCycleState st : otherStates) {
				if (!(this.managedStates.contains(st))) return false;
			}
			return true;
		}
	}
	
	/**
	 * Utility method to check if a list of commitment lifecycle states
	 * contains all the devised states
	 * @param states
	 * @return
	 */
	public static boolean areAllStatesManaged(List<LifeCycleState> states) {
		// manca PENDING dalla lista completa
		boolean cond = false, det = false, exp = false, sat = false, term = false, viol = false;
		for (LifeCycleState st : states) {
			if (st == LifeCycleState.CONDITIONAL) cond = true;
			if (st == LifeCycleState.DETACHED) det = true;
			if (st == LifeCycleState.EXPIRED) exp = true;
			if (st == LifeCycleState.SATISFIED) sat = true;
			if (st == LifeCycleState.TERMINATED) term = true;
			if (st == LifeCycleState.VIOLATED) viol = true;
		}
		return (cond && det && exp && sat && term && viol);
	}
}

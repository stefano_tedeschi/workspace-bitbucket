package twocomm.core;


public class SocialEvent {
	
	private SocialStateElement elementChanged;
	private SocialEventActionType action;
	
	public SocialEvent (SocialStateElement el, SocialEventActionType action) {
		this.elementChanged = el;
		this.action = action;
	}
	
	public SocialStateElement getElementChanged() {
		return this.elementChanged;
	}
	
	public SocialEventActionType getAction() {
		return this.action;
	}
	
	public String toString() {
		return elementChanged+ ", op: "+action;
	}
}

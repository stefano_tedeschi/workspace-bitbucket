package twocomm.core;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.exception.CommitmentAlreadyAddedException;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.CommitmentOperationNotPerformableException;
import twocomm.exception.LogicalExpressionAlreadyAddedException;

@Entity
public class SQLiteSocialState extends Observable implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    
    private static Logger logger = LogManager.getLogger(SQLiteSocialState.class);


    protected List<Commitment> allCommitments = new ArrayList<>();

    private List<Fact> facts = new ArrayList<>();
    
    @Transient
    private ProtocolArtifact art;

    @Transient
    private final String dbUrl = "jdbc:sqlite::memory:";
    @Transient
    private Connection conn;

    @Transient
    private int count;

    //l'indicizzazione viene effettuata mediante una tabella in un database sqlite che contiene una rappresentazione di tutti i commitment presenti nello stato sociale
    public SQLiteSocialState(ProtocolArtifact art) {
        super();
        this.art = art;
        count = 0;
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            conn = DriverManager.getConnection(dbUrl);
            Statement st = conn.createStatement();
            st.execute("DROP TABLE IF EXISTS COMMITMENTS");
            st.execute(
                    "CREATE TABLE COMMITMENTS(DEBTOR TEXT, CREDITOR TEXT, ANTECEDENT TEXT, CONSEQUENT TEXT, STATUS TEXT, POS INT PRIMARY KEY)");
            st.execute("CREATE INDEX DEBTOR_INDEX ON COMMITMENTS(DEBTOR)");
            st.execute("CREATE INDEX CREDITOR_INDEX ON COMMITMENTS(CREDITOR)");
            st.execute("CREATE INDEX ANTECEDENT_INDEX ON COMMITMENTS(ANTECEDENT)");
            st.execute("CREATE INDEX CONSEQUENT_INDEX ON COMMITMENTS(CONSEQUENT)");
            st.execute("CREATE INDEX STATUS_INDEX ON COMMITMENTS(STATUS)");
            st.close();
        } catch (SQLException ex) {
            logger.error("Unable to connect to SQLite DB: " + ex.getMessage());
            System.exit(0);
        }
    }
    
    public List<Fact> getFacts() {
		return facts;
	}

	public void setFacts(List<Fact> facts) {
		this.facts = facts;
	}

    public boolean assertFact(Fact f) throws LogicalExpressionAlreadyAddedException {
        boolean added = true;
        boolean toAdd = true;
        for (Fact el : facts) {
            if (el.equals(f)) {
                toAdd = false;
            }
        }
        if (toAdd) {
            synchronized (facts) {
                facts.add(f);
            }
            art.defineObsPropertyFromFact(f);
        } else {
            throw new LogicalExpressionAlreadyAddedException(f);
        }
        // NEED TO CHECK IF SOME COMMITMENT BECOME DETACHED/SATISFIED
        // checkCommitmentsAfterInteractionStateElementAdding(le);
        // logger.debug("Check finished, actual status: " + this);
        logger.debug("Added fact: " + f);
        return added;
    }

    public boolean existsFact(Fact f) {
		for (Fact f1 : facts) {
			if (f1.equals(f)) {
				return true;
			}
		}
		return false;
	}

    //le operazioni di ricerca e recupero dei commitment possono essere svolte effettuando una query sul database che restituisce la posizione dei commitment cercati nella lista allCommitments
    public boolean hasCommitment(Commitment c) {
        boolean present = false;
        try {
            Statement st = conn.createStatement();
            String query = "SELECT * FROM COMMITMENTS WHERE CREDITOR='" + c.getCreditor().toString() + "' AND DEBTOR='"
                    + c.getDebtor().toString() + "' AND ANTECEDENT='" + c.getAntecedent().toString()
                    + "' AND CONSEQUENT='" + c.getConsequent().toString() + "'";
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                present = true;
            }
        } catch (SQLException ex) {
            logger.error("Unable to retrieve commitment: " + ex.getMessage());
        }
        return present;
    }

    public List<Commitment> createAllCommitments(Commitment c) {
        List<Commitment> concreteCommitmentsAdded = new ArrayList<>();
        if (c.getCreditor().getType() == RoleId.GROUP_ROLE) {
            int numberOfCreditor = art.getEnactedRolesIds().size();
            int exceptionCount = 0;
            Commitment comm;
            for (RoleId r : art.getEnactedRolesIds()) {
                if (r.equals(c.getCreditor()) && !r.equals(c.getDebtor())) {
                    try {
                        comm = new Commitment(c.getDebtor(), r, c.getAntecedent(), c.getConsequent());
                        createConcreteCommitmentInternal(comm);
                        concreteCommitmentsAdded.add(comm);
                        for (Commitment com : concreteCommitmentsAdded) {
							art.defineObsPropertyFromCommitment(com);
						}
						logger.debug("Commitment to reify added, "+c+", reified: "+concreteCommitmentsAdded.size());
                    } catch (CommitmentAlreadyAddedException e) {
                        exceptionCount++;
                    }
                }
            }
            if (exceptionCount == numberOfCreditor) {
                logger.debug("The commitment " + c + " is not reyfable.");
            }
        } // creditor roleid already particular
        else {
            System.out.println(
                    "The commitment " + c + " must be a GENERIC commitment, with a RoleId GENERIC as creditor.");
        }
        return concreteCommitmentsAdded;
    }

    public ArrayList<Commitment> createCommitment(Commitment c) {
        ArrayList<Commitment> concreteCommitmentsAdded = new ArrayList<>();
        if (c.getCreditor().getType() == RoleId.GROUP_ROLE) {
            try {
                createGroupCommitmentInternal(c);
                concreteCommitmentsAdded.add(c);
            } catch (CommitmentAlreadyAddedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                logger.debug("Error in adding group commitment " + c);
            }
        } // creditor roleid already particular
        else {
            try {
                createConcreteCommitmentInternal(c);
                concreteCommitmentsAdded.add(c);
            } catch (CommitmentAlreadyAddedException e) {
                e.printStackTrace();
                logger.error("The commitment " + c + " is not reyfable.");
            }
        }
		for (Commitment com : concreteCommitmentsAdded) {
			art.defineObsPropertyFromCommitment(com);
		}
		logger.debug("Aggiunta del commitment completata, "+c);
		return concreteCommitmentsAdded;
    }

    private void createGroupCommitmentInternal(Commitment c) throws CommitmentAlreadyAddedException {
        // CHECK IF COMMITMENT IS ALREADY IN INTERACTION STATE
        try {
            Statement st = conn.createStatement();
            String query = "SELECT * FROM COMMITMENTS WHERE CREDITOR='" + c.getCreditor().toString() + "' AND DEBTOR='"
                    + c.getDebtor().toString() + "' AND ANTECEDENT='" + c.getAntecedent().toString()
                    + "' AND CONSEQUENT='" + c.getConsequent().toString() + "'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                Commitment actualC = allCommitments.get(rs.getInt("POS"));
                if (actualC.getCreditor().getType() == c.getCreditor().getType()
                        && actualC.getLifeCycleStatus() != LifeCycleState.VIOLATED
                        && actualC.getLifeCycleStatus() != LifeCycleState.TERMINATED) {
                    throw new CommitmentAlreadyAddedException(c);
                }
            }
        } catch (SQLException ex) {
            logger.error("Unable to add commitment: " + ex.getMessage());
        }
        // UPDATE COMMITMENT REPRESENTATION
        // logger.debug("Updating roles-commitments support datastructures");
        try {
            Statement st = conn.createStatement();
            st.execute("INSERT INTO COMMITMENTS(CREDITOR, DEBTOR, ANTECEDENT, CONSEQUENT, STATUS, POS) VALUES ('"
                    + c.getCreditor().toString() + "', '" + c.getDebtor().toString() + "', '"
                    + c.getAntecedent().toString() + "', '" + c.getConsequent().toString() + "', '"
                    + c.getLifeCycleStatus().toString() + "', " + count + ")");
            st.close();
            count++;
            allCommitments.add(c);
            logger.trace("Commitment added: " + c);
        } catch (SQLException ex) {
            logger.error("Unable to add the commitment");
        }
    }

    private void createConcreteCommitmentInternal(Commitment c) throws CommitmentAlreadyAddedException {
        // CHECK IF COMMITMENT IS ALREADY IN INTERACTION STATE
        try {
            Statement st = conn.createStatement();
            String query = "SELECT * FROM COMMITMENTS WHERE CREDITOR='" + c.getCreditor().toString() + "' AND DEBTOR='"
                    + c.getDebtor().toString() + "' AND ANTECEDENT='" + c.getAntecedent().toString()
                    + "' AND CONSEQUENT='" + c.getConsequent().toString() + "'";
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                throw new CommitmentAlreadyAddedException(c);
            }
        } catch (SQLException ex) {
            logger.error("Unable to add commitment: " + ex.getMessage());
        }
        // UPDATE COMMITMENT REPRESENTATION
        // logger.debug("Updating roles-commitments support datastructures");
        try {
            Statement st = conn.createStatement();
            st.execute("INSERT INTO COMMITMENTS(CREDITOR, DEBTOR, ANTECEDENT, CONSEQUENT, STATUS, POS) VALUES ('"
                    + c.getCreditor().toString() + "', '" + c.getDebtor().toString() + "', '"
                    + c.getAntecedent().toString() + "', '" + c.getConsequent().toString() + "', '"
                    + c.getLifeCycleStatus() + "', " + count + ")");
            st.close();
            count++;
            allCommitments.add(c);
            logger.trace("Commitment added:" + c);
        } catch (SQLException ex) {
            logger.error("Unable to add the commitment");
        }
        // NEED TO CHECK IF SOME COMMITMENT BECOME DETACHED/SATISFIED
        // checkCommitmentsAfterInteractionStateElementAdding(c);
        // logger.debug("Check finished, actual status: " + this);
    }

    /*
     * "Cancel a commitment" means that a detached commitment becomes violated,
     * while a conditional commitment becomes terminated
     */
    public boolean cancelCommitment(Commitment c)
            throws CommitmentNotFoundInInteractionStateException, CommitmentOperationNotPerformableException {
        logger.debug("Canceling commitment " + c);
        synchronized (allCommitments) {
            // CHECK IF COMMITMENT IS IN INTERACTION STATE
            // logger.debug("Canceling commitment " + c);
            Commitment cToCancel = null;
            int pos = -1;
            try {
                Statement st = conn.createStatement();
                String query = "SELECT POS FROM COMMITMENTS WHERE CREDITOR='" + c.getCreditor().toString()
                        + "' AND DEBTOR='" + c.getDebtor().toString() + "' AND ANTECEDENT='"
                        + c.getAntecedent().toString() + "' AND CONSEQUENT='" + c.getConsequent().toString() + "'";
                ResultSet rs = st.executeQuery(query);
                if (rs.next()) {
                    pos = rs.getInt("POS");
                }
                rs.close();
                st.close();
            } catch (SQLException ex) {
                throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);
            }
            if (pos == -1) {
                throw new CommitmentNotFoundInInteractionStateException(c);
            }
            cToCancel = allCommitments.get(pos);
			// TODO: QUI C'E' FORZATURA DEL CONTROLLO DEL LIFECYCLE, NON FACCIO
            // CONTROLLO
            // check lifecyclestatus of commitment
            // if (c.getLifeCycleStatus() != LifeCycleState.CONDITIONAL &&
            // c.getLifeCycleStatus() != LifeCycleState.DETACHED) {
            // throw new CommitmentOperationNotPerformableException(c,
            // ProtocolArtifact.CANCEL_COMMITMENT);
            // }

            // UPDATE STRUCTURE BY LIFECYCLE
            logger.trace("Commitment to cancel found");
            // logger.debug("Updating roles-commitments support
            // datastructures");
            // now change commitment status
            switch (c.getLifeCycleStatus()) {
                case CONDITIONAL:
                    try {
                        Statement st = conn.createStatement();
                        String query = "UPDATE COMMITMENTS SET STATUS='" + LifeCycleState.TERMINATED + "' WHERE POS=" + pos;
                        st.executeUpdate(query);
                        st.close();
                        cToCancel.setStatus(LifeCycleState.TERMINATED);
                    } catch (SQLException ex) {
                        throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);
                    }
                    break;
                case DETACHED:
                    try {
                        Statement st = conn.createStatement();
                        String query = "UPDATE COMMITMENTS SET STATUS='" + LifeCycleState.VIOLATED + "' WHERE POS=" + pos;
                        st.executeUpdate(query);
                        st.close();
                        cToCancel.setStatus(LifeCycleState.VIOLATED);
                    } catch (SQLException ex) {
                        throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);
                    }
                    break;
                default:
                    throw new CommitmentOperationNotPerformableException(c, ProtocolArtifact.CANCEL_COMMITMENT);
            }
            // logger.debug("Cancel operation finished, actual status: " +
            // this);
            return true;
        }
    }

    public boolean removeCommitment(Commitment c) throws CommitmentNotFoundInInteractionStateException {
        synchronized (allCommitments) {
            int pos = -1;
            try {
                Statement st = conn.createStatement();
                String query = "SELECT POS FROM COMMITMENTS WHERE CREDITOR='" + c.getCreditor().toString()
                        + "' AND DEBTOR='" + c.getDebtor().toString() + "' AND ANTECEDENT='"
                        + c.getAntecedent().toString() + "' AND CONSEQUENT='" + c.getConsequent().toString() + "'";
                ResultSet rs = st.executeQuery(query);
                if (rs.next()) {
                    pos = rs.getInt("POS");
                }
                rs.close();
                st.close();
            } catch (SQLException ex) {
                logger.error("Unable to remove commitment: " + ex.getMessage());
                return false;
            }
            if (pos == -1) {
                throw new CommitmentNotFoundInInteractionStateException(c);
            }
            try {
                Statement st = conn.createStatement();
                String query = "DELETE FROM COMMITMENTS WHERE POS=" + pos;
                st.execute(query);
                if (pos != allCommitments.size() - 1) {
                    query = "UPDATE COMMITMENTS SET POS=" + pos + " WHERE POS=(SELECT MAX(POS) FROM COMMITMENTS)";
                    st.executeUpdate(query);
                }
                st.close();
            } catch (SQLException ex) {
                throw new CommitmentNotFoundInInteractionStateException(c);
            }
            allCommitments.remove(pos);
            if (pos != allCommitments.size()) {
                allCommitments.add(pos, allCommitments.get(allCommitments.size() - 1));
                allCommitments.remove(allCommitments.size() - 1);
                count--;
            }
            return true;
        }
    }

    /*
     * private void
     * checkCommitmentsAfterInteractionStateElementAdding(SocialStateElement
     * elAdded) { ArrayList<Commitment> condCommits =
     * commitments.get(LifeCycleState.CONDITIONAL); ArrayList<Commitment>
     * detachedCommits = commitments.get(LifeCycleState.DETACHED); switch
     * (elAdded.getElType()) { case COMMITMENT: logger.trace(
     * "Just added a COMMITMENT, reasoning on interaction state after adding " +
     * elAdded); Commitment c = (Commitment) elAdded; // search in conditional
     * commitment set if (condCommits != null) { logger.trace(
     * "Parsing CONDITIONAL commitments"); for (Commitment condComm :
     * condCommits) { logger.debug("Commitment antecedent: " +
     * condComm.getAntecedent()); if (condComm.getAntecedent().equals(c)) {
     * logger.debug("Commitment " + condComm +
     * " has to change its state. Now is\n " + condComm.getLifeCycleStatus() +
     * ", setting to DETACHED.");
     * 
     * condComm.setStatus(LifeCycleState.DETACHED); // try { //
     * condComm.setAntecedent(new Fact("true")); // } catch
     * (MissingOperandException e) { // // TODO Auto-generated catch block //
     * e.printStackTrace(); // } }
     * 
     * } } // search in detached commitment set if (detachedCommits != null) {
     * logger.trace("Parsing DETACHED commitments"); for (Commitment condComm :
     * detachedCommits) { if (condComm.getAntecedent().equals(c)) {
     * logger.debug("Commitment " + condComm +
     * " has to change its state. Now is\n " + condComm.getLifeCycleStatus() +
     * ", setting to DISCHARGED.");
     * condComm.setStatus(LifeCycleState.SATISFIED); try {
     * condComm.setAntecedent(new Fact("true")); } catch
     * (MissingOperandException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } }
     * 
     * } } break;
     * 
     * case LOGICAL_EXPRESSION: logger.trace(
     * "Just added a FACT, reasoning on interaction state after adding " +
     * elAdded); LogicalExpression el = (LogicalExpression) elAdded; if
     * (condCommits != null) { logger.trace("Parsing CONDITIONAL commitments");
     * for (Commitment condComm : condCommits) { logger.debug(
     * "Commitment antecedent: " + condComm.getAntecedent()); if
     * (condComm.getAntecedent().equals(el)) { logger.debug("Commitment " +
     * condComm + " has to change its state. Now is\n " +
     * condComm.getLifeCycleStatus() + ", setting to DETACHED.");
     * condComm.setStatus(LifeCycleState.DETACHED); // try { //
     * condComm.setAntecedent(new Fact("true")); // } catch
     * (MissingOperandException e) { // // TODO Auto-generated catch block //
     * e.printStackTrace(); // } }
     * 
     * } } // search in detached commitment set if (detachedCommits != null) {
     * logger.trace("Parsing DETACHED commitments"); for (Commitment condComm :
     * detachedCommits) { if (condComm.getAntecedent().equals(el)) {
     * logger.debug("Commitment " + condComm +
     * " has to change its state. Now is\n " + condComm.getLifeCycleStatus() +
     * ", setting to DISCHARGED.");
     * condComm.setStatus(LifeCycleState.SATISFIED); try {
     * condComm.setAntecedent(new Fact("true")); } catch
     * (MissingOperandException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } }
     * 
     * } } break; }
     * 
     * }
     */
    @Override
    public synchronized String toString() {
        String st = "";

        st += "\nFACTS:\n------------------\n";
        for (LogicalExpression f : facts) {
            st += "- " + f + "\n";
        }

        st += "\nCOMMITMENTS:\n------------------\n";
        for (Commitment c : allCommitments) {
            st += "- " + c + "\n";
        }
        return st + "\n";
    }

    public List<Commitment> retrieveAllCommitments() {
        return allCommitments;
    }

    public List<Commitment> retrieveCommitmentsByLifecycleStatus(LifeCycleState status) {
        int pos = -1;
        List<Commitment> temp = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = "SELECT POS FROM COMMITMENTS WHERE STATUS='" + status + "'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                pos = rs.getInt("POS");
                temp.add(allCommitments.get(pos));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            return null;
        }
        if (pos == -1) {
            return null;
        }
        return temp;
    }

    public List<Commitment> retrieveCommitmentsByCreditorRoleId(RoleId id) {
        int pos = -1;
        List<Commitment> temp = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = "SELECT POS FROM COMMITMENTS WHERE CREDITOR='" + id.toString() + "'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                pos = rs.getInt("POS");
                temp.add(allCommitments.get(pos));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            return null;
        }
        if (pos == -1) {
            return null;
        }
        return temp;
    }

    public List<Commitment> retrieveCommitmentsByDebtorRoleId(RoleId id) {
        int pos = -1;
        List<Commitment> temp = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = "SELECT POS FROM COMMITMENTS WHERE DEBTOR='" + id.toString() + "'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                pos = rs.getInt("POS");
                temp.add(allCommitments.get(pos));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            return null;
        }
        if (pos == -1) {
            return null;
        }
        return temp;
    }

    public List<Commitment> retrieveCommitmentsByAntecedent(Fact f) {
        int pos = -1;
        List<Commitment> temp = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = "SELECT POS FROM COMMITMENTS WHERE ANTECEDENT='" + f.toString() + "'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                pos = rs.getInt("POS");
                temp.add(allCommitments.get(pos));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            return null;
        }
        if (pos == -1) {
            return null;
        }
        return temp;
    }

    public List<Commitment> retrieveCommitmentsByConsequent(Fact f) {
        int pos = -1;
        List<Commitment> temp = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = "SELECT POS FROM COMMITMENTS WHERE CONSEQUENT='" + f.toString() + "'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                pos = rs.getInt("POS");
                temp.add(allCommitments.get(pos));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            return null;
        }
        if (pos == -1) {
            return null;
        }
        return temp;
    }

    public List<Commitment> retrieveCommitmentsContainingFact(Fact f) {
        int pos = -1;
        List<Commitment> temp = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = "SELECT POS FROM COMMITMENTS WHERE ANTECEDENT LIKE '%" + f.toString()
                    + "%' OR CONSEQUENT LIKE '%" + f.toString() + "%'";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                pos = rs.getInt("POS");
                temp.add(allCommitments.get(pos));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            return null;
        }
        if (pos == -1) {
            return null;
        }
        return temp;
    }

    private String printHashSet(ArrayList hs) {
        String st = "";
        int i = 0;
        for (Object o : hs) {
            st += i + ": " + o + "\n";
            i++;
        }
        return st;
    }

    public String printDatabaseTable() {
        String s = "";
        try {
            Statement st = conn.createStatement();
            String query = "SELECT * FROM COMMITMENTS";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                s = s + "CC(" + rs.getString("DEBTOR") + ", " + rs.getString("CREDITOR") + ", "
                        + rs.getString("ANTECEDENT") + ", " + rs.getString("CONSEQUENT") + ", STATUS: "
                        + rs.getString("STATUS") + ") -- " + rs.getInt("POS") + "\n";
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            logger.debug(ex.getMessage());
            return null;
        }
        return s;
    }

    /*
    // implementati metodi per modificare lo stato dei commitment
    public void releaseCommitment(Commitment c) {
        synchronized (c) {
            try {
                Statement st = conn.createStatement();
                String query = "UPDATE COMMITMENTS SET STATUS='" + LifeCycleState.TERMINATED + "' WHERE CREDITOR='"
                        + c.getCreditor().toString() + "' AND DEBTOR='" + c.getDebtor().toString()
                        + "' AND ANTECEDENT='" + c.getAntecedent().toString() + "' AND CONSEQUENT='"
                        + c.getConsequent().toString() + "'";
                st.executeUpdate(query);
                st.close();
            } catch (SQLException ex) {
                logger.error("Unable to release commitment: " + ex.getMessage());
                return;
            }
            c.setStatus(LifeCycleState.TERMINATED);
        }
    }

    public void satisfyCommitment(Commitment c) {
        synchronized (c) {
            try {
                Statement st = conn.createStatement();
                String query = "UPDATE COMMITMENTS SET STATUS='" + LifeCycleState.SATISFIED + "' WHERE CREDITOR='"
                        + c.getCreditor().toString() + "' AND DEBTOR='" + c.getDebtor().toString()
                        + "' AND ANTECEDENT='" + c.getAntecedent().toString() + "' AND CONSEQUENT='"
                        + c.getConsequent().toString() + "'";
                st.executeUpdate(query);
                st.close();
            } catch (SQLException ex) {
                logger.error("Unable to satisfy commitment: " + ex.getMessage());
                return;
            }
            c.setStatus(LifeCycleState.SATISFIED);
        }
    }

    public void detachCommitment(Commitment c) {
        synchronized (c) {
            try {
                Statement st = conn.createStatement();
                String query = "UPDATE COMMITMENTS SET STATUS='" + LifeCycleState.DETACHED + "' WHERE CREDITOR='"
                        + c.getCreditor().toString() + "' AND DEBTOR='" + c.getDebtor().toString()
                        + "' AND ANTECEDENT='" + c.getAntecedent().toString() + "' AND CONSEQUENT='"
                        + c.getConsequent().toString() + "'";
                st.executeUpdate(query);
                st.close();
            } catch (SQLException ex) {
                logger.error("Unable to detach commitment: " + ex.getMessage());
                return;
            }
            c.setStatus(LifeCycleState.DETACHED);
        }
    }

    public void violateCommitment(Commitment c) {
        synchronized (c) {
            try {
                Statement st = conn.createStatement();
                String query = "UPDATE COMMITMENTS SET STATUS='" + LifeCycleState.VIOLATED + "' WHERE CREDITOR='"
                        + c.getCreditor().toString() + "' AND DEBTOR='" + c.getDebtor().toString()
                        + "' AND ANTECEDENT='" + c.getAntecedent().toString() + "' AND CONSEQUENT='"
                        + c.getConsequent().toString() + "'";
                st.executeUpdate(query);
                st.close();
            } catch (SQLException ex) {
                logger.error("Unable to violate commitment: " + ex.getMessage());
                return;
            }
            c.setStatus(LifeCycleState.VIOLATED);
        }
    }
    */

    /**
     * Change the status of commitment c to the new status newstate. It removes
     * c from list of conditional or detached commitments, and updates the
     * structure maintaining the list of commitment with state newstate.
     *
     * @param c the commitment whose status has to be changed.
     * @param newState the new status, according to the commitment lifecycle
     * possible states.
     */
    public void changeCommitmentStatus(Commitment c, LifeCycleState newState) {
        logger.debug("Changing status of commitment: " + c);
        if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
            if (newState != LifeCycleState.CONDITIONAL) {
                synchronized (c) {
                    try {
                        Statement st = conn.createStatement();
                        String query = "UPDATE COMMITMENTS SET STATUS='" + newState + "' WHERE CREDITOR='"
                                + c.getCreditor().toString() + "' AND DEBTOR='" + c.getDebtor().toString()
                                + "' AND ANTECEDENT='" + c.getAntecedent().toString() + "' AND CONSEQUENT='"
                                + c.getConsequent().toString() + "'";
                        st.executeUpdate(query);
                        st.close();
                    } catch (SQLException ex) {
                        logger.error("Unable to change commitment status: " + ex.getMessage());
                        return;
                    }
                    synchronized (art) {
						art.updateCommitmentObsPropertyByStatus(c, c.getLifeCycleStatus(), newState);
					}
                    c.setStatus(newState);
                }
            }

        } else if (c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
            if (newState != LifeCycleState.DETACHED) {
                synchronized (c) {
                    try {
                        Statement st = conn.createStatement();
                        String query = "UPDATE COMMITMENTS SET STATUS='" + newState + "' WHERE CREDITOR='"
                                + c.getCreditor().toString() + "' AND DEBTOR='" + c.getDebtor().toString()
                                + "' AND ANTECEDENT='" + c.getAntecedent().toString() + "' AND CONSEQUENT='"
                                + c.getConsequent().toString() + "'";
                        st.executeUpdate(query);
                        st.close();
                    } catch (SQLException ex) {
                        logger.error("Unable to change commitment status: " + ex.getMessage());
                        return;
                    }
                    synchronized (art) {
						art.updateCommitmentObsPropertyByStatus(c, c.getLifeCycleStatus(), newState);
					}
                    c.setStatus(newState);
                }
            }
        }
        logger.debug("Status of commitment changed. New status: " + c);
    }

}

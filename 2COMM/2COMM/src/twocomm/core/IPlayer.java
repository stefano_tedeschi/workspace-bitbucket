package twocomm.core;

import java.io.Serializable;

public interface IPlayer extends Serializable{

	public String getPlayerName();
	
}

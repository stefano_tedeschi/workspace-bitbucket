package twocomm.core.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import twocomm.core.Commitment;
import twocomm.core.ProtocolArtifact;
import twocomm.core.SocialState;
import twocomm.core.logic.Fact;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;

/**
 *
 * @author Stefano Tedeschi
 */
//il dbms utilizzato e' objectDB
@Entity
public class PersistentSocialState extends SocialState implements Serializable {

	public PersistentSocialState(ProtocolArtifact art) {
		super(art);
	}

	//metodo che salva l'istanza corrente di stato sociale
	public void saveSocialState() {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("socialState.odb");
		EntityManager em = emf.createEntityManager();

		TypedQuery<SocialState> query = em.createQuery("SELECT s FROM SocialState s", SocialState.class);
		List<SocialState> results = query.getResultList();

		em.getTransaction().begin();
		for (SocialState sToCancel : results) {
			em.remove(sToCancel);
		}
		em.getTransaction().commit();

		em.getTransaction().begin();
		em.persist(this);
		em.getTransaction().commit();

		em.close();
		emf.close();

	}

	//metodo che ripristina un'istanza di stato sociale precedentemente salvata
	public void loadSocialState() throws CommitmentNotFoundInInteractionStateException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("socialState.odb");
		EntityManager em = emf.createEntityManager();

		TypedQuery<SocialState> query = em.createQuery("SELECT s FROM SocialState s", SocialState.class);
		List<SocialState> results = query.getResultList();

		while (!allCommitments.isEmpty())
			this.removeCommitment(allCommitments.get(0));
		this.setFacts(new ArrayList<Fact>());

		if (results.size() > 0) {
			SocialState temp = results.get(0);
			for (Commitment c : temp.retrieveAllCommitments()) {
				this.createCommitment(c);
			}
			for (Fact f : temp.getFacts())
				this.facts.add(f);
		}

		em.close();
		emf.close();
	}

}

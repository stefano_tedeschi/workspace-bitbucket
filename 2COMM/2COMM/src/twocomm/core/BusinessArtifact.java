package twocomm.core;



import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.CommunicationArtifact.CARole;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpression;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.CommitmentOperationNotPerformableException;
import twocomm.exception.LogicalExpressionAlreadyAddedException;
import cartago.OPERATION;
import cartago.ObsProperty;

/*
 * This artifact extends the 'TupleSpace' artifact.
 * Communication is performed via tuplespace; besides that, an interaction state is mantained.
 * 
 * Every communication artifact has to add to static HashSet 'enabledRoles' the roles
 * an agent can enact, via method 'addEnabledRole(String roleName)'.
 * 
 */

public abstract class BusinessArtifact extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(BusinessArtifact.class);
	
	

}

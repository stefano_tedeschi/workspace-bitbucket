package twocomm.core;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


/**
 * Classe di annotazione per taggare un metodo RoleAction.
 * @RoleAction
 * @author elron
 *
 */

@Retention(RetentionPolicy.RUNTIME)

@Target({ElementType.METHOD})
public @interface RoleAction {
	
}

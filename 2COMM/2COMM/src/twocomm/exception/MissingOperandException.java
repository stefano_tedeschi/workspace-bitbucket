package twocomm.exception;

public class MissingOperandException extends Exception {
	
	public MissingOperandException (String message) {
		super(message);
	}
	
}

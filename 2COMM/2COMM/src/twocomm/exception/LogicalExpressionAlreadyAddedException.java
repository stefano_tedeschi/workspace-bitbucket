package twocomm.exception;

import twocomm.core.logic.LogicalExpression;

public class LogicalExpressionAlreadyAddedException extends Exception {

	private LogicalExpression le = null;
	
	public LogicalExpressionAlreadyAddedException(LogicalExpression le) {
		super("Logical Expression already present in interaction state: "+le);
		this.le = le;
	}
	
	public LogicalExpression getLogicalExpression() {
		return le;
	}

}

package twocomm.exception;

import twocomm.core.Commitment;

public class CommitmentAlreadyAddedException extends Exception {

	private Commitment c;
	
	public CommitmentAlreadyAddedException(Commitment c) {
		super("Commitment already present in interaction state: "+c);
		// TODO Auto-generated constructor stub
	}
	
	public Commitment getCommitment() {
		return c;
	}

}

package twocomm.exception;

public class MissingRequirementForRoleException extends Exception {

	public MissingRequirementForRoleException() {
		// TODO Auto-generated constructor stub
	}

	public MissingRequirementForRoleException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MissingRequirementForRoleException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MissingRequirementForRoleException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	
	public MissingRequirementForRoleException(Class roleClass) {
		super("Role "+roleClass.getCanonicalName()+" is missing requirements.");
		// TODO Auto-generated constructor stub
	}

}

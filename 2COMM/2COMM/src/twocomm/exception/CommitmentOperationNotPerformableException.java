package twocomm.exception;

import twocomm.core.Commitment;

public class CommitmentOperationNotPerformableException extends Exception {

	private Commitment c;
	private String operation;
	
	public CommitmentOperationNotPerformableException(Commitment c, String operation) {
		super("Unable to perform operation "+operation+ " on commitment "+c+" with lifecyclestatus "+c.getLifeCycleStatus());
		// TODO Auto-generated constructor stub
	}
	
	public Commitment getCommitment() {
		return c;
	}

}

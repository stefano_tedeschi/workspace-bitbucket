package twocomm.exception;

public class WrongProtocolRoleRequestEnactmentException extends Exception {

	public WrongProtocolRoleRequestEnactmentException(String stringToParse) {
		super("Wrong request for "+stringToParse+". The correct form is 'protocolName.roleName'.");
	}
}

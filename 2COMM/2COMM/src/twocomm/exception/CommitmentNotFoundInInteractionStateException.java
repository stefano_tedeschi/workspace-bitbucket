package twocomm.exception;

import twocomm.core.Commitment;

public class CommitmentNotFoundInInteractionStateException extends Exception {

	private Commitment c;
	
	public CommitmentNotFoundInInteractionStateException(Commitment c) {
		super("Commitment not found in interaction state: "+c);
		this.c = c;
	}
	
	public Commitment getCommitment() {
		return c;
	}

}

package cartago.tools;

import twocomm.core.RoleId;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.*;

public abstract class AbstractTupleSpace extends Artifact {
	
	Logger log = LogManager.getLogger(AbstractTupleSpace.class);
	
	TupleSet tset;
	
	protected void init(){
		tset = new TupleSet();
	}
	
	protected void outImpl(String name, Object... args) {
		Tuple t = new Tuple(name, args);
		tset.add(t);
		///log.info("OUT on "+this.getClass() + "::"+getId()+" "+printTupleSpace());
	}
	
	protected void outImpl(RoleId destRole, Object... args) {
		outImpl(destRole.toString(), args);		
	}
	
	protected void inImpl(String name, Object... params){
		TupleTemplate tt = new TupleTemplate(name,params);
		await("foundMatch",tt);
		Tuple t = tset.removeMatching(tt);
		bind(tt,t);
		//log.info("IN on "+this.getClass() + "::"+getId()+" "+printTupleSpace());
	}
	
	protected void inpImpl(String name, Object... params){
		TupleTemplate tt = new TupleTemplate(name,params);
		if (foundMatch(tt)){
			Tuple t = tset.removeMatching(tt);
			bind(tt,t);
		} else {
			failed("no_match");
		}
		log.info("INP on "+this.getClass() + "::"+getId()+" "+printTupleSpace());
	}
	
	protected String printTupleSpace() {
		return tset.getActualTupleSet();
	}
	
	
	@OPERATION void out(String name, Object... args){
		outImpl(name, args);
	}
	
	@OPERATION protected void in(String name, Object... params){
		inImpl(name, params);
	}

	@OPERATION void inp(String name, Object... params){
		inpImpl(name, params);
	}

	@INTERNAL_OPERATION void rd(String name, Object... params){
		TupleTemplate tt = new TupleTemplate(name,params);
		await("foundMatch",tt);
		Tuple t = tset.readMatching(tt);
		bind(tt,t);
	}

	@INTERNAL_OPERATION void rdp(String name, Object... params){
		TupleTemplate tt = new TupleTemplate(name,params);
		if (foundMatch(tt)){
			Tuple t = tset.readMatching(tt);
			bind(tt,t);
		} else {
			failed("no_match");
		}
	}
	
	private void bind(TupleTemplate tt, Tuple t){
		Object[] tparams = t.getContents();
		int index = 0;
		for (Object p: tt.getContents()){
			if (p instanceof OpFeedbackParam<?>){
				((OpFeedbackParam) p).set(tparams[index]);
			}
			index++;
		}
	}
	
	@GUARD boolean foundMatch(TupleTemplate tt){
		return tset.hasTupleMatching(tt);
	}
}

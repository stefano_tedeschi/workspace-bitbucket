package cnpCommits;

import java.util.ArrayList;

import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.Type;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class InitiatorRequirements extends Type {
	
	public InitiatorRequirements() throws MissingOperandException, WrongOperandsNumberException {
		super(new Commitment[]{
			new Commitment(CNPArtifactTimerMaxPart.INITIATOR_ROLE, CNPArtifactTimerMaxPart.PARTICIPANT_ROLE, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))) 
		});
	}	
}
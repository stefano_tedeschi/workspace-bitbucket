package cnpCommits;

import twocomm.core.logic.Fact;
import twocomm.core.typing.AgentAbilities;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class TypeInitiator extends AgentAbilities {

	public TypeInitiator() throws MissingOperandException, WrongOperandsNumberException {
		
		super(new Fact[]
				{new Fact("accept"), new Fact("reject")
				});
	}
}

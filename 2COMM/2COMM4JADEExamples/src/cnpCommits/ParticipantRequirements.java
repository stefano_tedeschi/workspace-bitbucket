package cnpCommits;

import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.Type;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class ParticipantRequirements extends Type { 
	
	public ParticipantRequirements() throws MissingOperandException, WrongOperandsNumberException {			
		super(new Commitment[]{
				new Commitment(CNPArtifactTimerMaxPart.PARTICIPANT_ROLE, CNPArtifactTimerMaxPart.INITIATOR_ROLE, "accept", new CompositeExpression(
							LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))
				});
		
	}
	
}
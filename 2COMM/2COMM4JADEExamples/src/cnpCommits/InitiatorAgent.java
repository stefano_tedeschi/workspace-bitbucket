package cnpCommits;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.RoleMessage;
import twocomm.core.logic.Fact;
import twocomm.core.typing.BehaviourType;
import twocomm.exception.MissingOperandException;
import cnpCommits.CNPArtifactTimerMaxPart.Initiator;

public class InitiatorAgent extends Agent {

	public static final String ARTIFACT_NAME = "CNP-1";
	public Logger logger = LogManager.getLogger(InitiatorAgent.class);

	public int satisfyCommitToAcceptOrReject = 0;
	
	protected void setup() {
		addBehaviour(new InitiatorBehaviourImpl());
	}

	@BehaviourType(capabilities = TypeInitiator.class)
	public class InitiatorBehaviourImpl extends InitiatorBehaviour  {

		public InitiatorBehaviourImpl() {
			super(ARTIFACT_NAME);
			// TODO Auto-generated constructor stub
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			super.action();
			
		}

		@Override
		public Behaviour commitToAcceptOrRejectIfPropose() {
			return new CommitToAcceptOrRejectIfPropose(initiator);
		}

		@Override
		public Behaviour satisfyCommitToAcceptOrReject() {
			// TODO Auto-generated method stub
			return new SatisfyCommitToAcceptOrReject(initiator);
		}

		@Override
		public Behaviour fulfilledCommitToDoneOrFailure() {
			// TODO Auto-generated method stub
			return new FulfilledCommitToDoneOrFailure(initiator);
		}

		
	}

	public class CommitToAcceptOrRejectIfPropose extends OneShotBehaviour {
		Initiator initiator = null;
		Task t = new Task("Decisione");

		public CommitToAcceptOrRejectIfPropose(Initiator initiator) {
			this.initiator = initiator;
		}

		@Override
		public void action() {
			try {
				// wait from participants
				Thread.sleep(5000);
			} catch (Exception e) {
			}
			initiator.cfp(t);

		}

	}

	public class SatisfyCommitToAcceptOrReject extends OneShotBehaviour {

		Initiator initiator = null;
		ArrayList<Proposal> proposals = new ArrayList<Proposal>();

		public SatisfyCommitToAcceptOrReject(Initiator initiator) {
			super();
			this.initiator = initiator;
		}

		@Override
		public void action() {
			ArrayList<RoleMessage> propos = initiator
					.receiveAll(ACLMessage.PROPOSE);
			for (RoleMessage p : propos) {
				proposals.add((Proposal) (p.getContents()));
			}

			initiator.accept(proposals.get(0));
			System.out.println("Accettata la prima proposta");
			for (int i = 1; i < proposals.size(); i++) {
				initiator.reject(proposals.get(i));
			}
		}
	}

	public class FulfilledCommitToDoneOrFailure extends OneShotBehaviour {

		Initiator initiator = null;

		public FulfilledCommitToDoneOrFailure(Initiator initiator) {
			super();
			this.initiator = initiator;
		}

		@Override
		public void action() {
			logger.debug("Parsing results...");
			try {
				if (initiator.existsFact(new Fact("done")))
					logger.debug("DONE percepted, work done.");
				else if (initiator.existsFact(new Fact("failure")))
					logger.debug("FAILURE percepted, work done.");
			} catch (MissingOperandException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Error in composing fact to match");
			}

		}

	}

}

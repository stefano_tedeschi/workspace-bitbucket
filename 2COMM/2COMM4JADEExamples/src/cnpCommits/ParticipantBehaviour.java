package cnpCommits;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.ArtifactId;
import cnpCommits.CNPArtifactTimerMaxPart.CNPInitiatorObserver;
import cnpCommits.CNPArtifactTimerMaxPart.Participant;

public abstract class ParticipantBehaviour extends OneShotBehaviour implements
				CNPInitiatorObserver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(ParticipantBehaviour.class);
	protected Participant participant;
	public String artifactName;
	public int satisfiedCommitToAcceptOrReject=0;
	protected Task t;
	protected Proposal p = null;
	
	public abstract Behaviour commitToDoneOrFailureIfAccept();

	public abstract Behaviour fulfilledCommitToAcceptOrReject();
	
	
	public ParticipantBehaviour(String artifactName) {
		this.artifactName = artifactName;
	}
	
	
	public Task getTask() { return t; }
	public void setTask(Task t) { this.t = t; }
	
	public Proposal getProposal() { return p; }
	public void setProposal(Proposal p) { this.p = p; }
	
	@Override
	public void action() {
		ArtifactId art = Role.createArtifact(artifactName,
				CNPArtifactTimerMaxPart.class);
		participant = (Participant) (Role.enact(
				CNPArtifactTimerMaxPart.PARTICIPANT_ROLE, art, new JadeBehaviourPlayer(this, myAgent.getAID())));
		participant.startObserving(this);
		
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		//logger.debug("Participant Handle by: "+participant.getRoleId()+"\n"+e);
		if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				try {
					if (c.getCreditor().equals(participant.getRoleId())
							&& c.getDebtor().equals(
									new RoleId(
											CNPArtifactTimerMaxPart.INITIATOR_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("accept"), new Fact(
													"reject")))) {
						myAgent.addBehaviour(commitToDoneOrFailureIfAccept());
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			else if (c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
				try {
					if (c.getDebtor().equals(participant.getRoleId())
							&& c.getCreditor().equals(
									new RoleId(
											CNPArtifactTimerMaxPart.INITIATOR_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("done"), new Fact(
													"failure")))) {
						myAgent.addBehaviour(fulfilledCommitToAcceptOrReject());
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			} else {
				// altri stati a cui reagire
			}
		} else { // event is a SocialFact assertion

		}

	}
}

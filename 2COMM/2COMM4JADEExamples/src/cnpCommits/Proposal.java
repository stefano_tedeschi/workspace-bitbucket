package cnpCommits;

import twocomm.core.RoleId;

public class Proposal {

	private String proposalContent = "";
	private RoleId role;
	
	public Proposal(String prop) {
		this.proposalContent = prop;
	}
	
	public void setRoleId(RoleId role) {
		this.role = role;
	}
	
	public RoleId getRoleId() {
		return this.role;
	}
	
	public String getProposalContent() {
		return proposalContent;
	}
	
	public String toString() {
		return "Proposal "+proposalContent;
	}
	
}

package cnpCommits;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.RoleMessage;
import twocomm.core.typing.BehaviourType;
import cnpCommits.CNPArtifactTimerMaxPart.CNPParticipantObserver;
import cnpCommits.CNPArtifactTimerMaxPart.Participant;

public class ParticipantAgent extends Agent {

	public static final String ARTIFACT_NAME = "CNP-1";
	public Logger logger = LogManager.getLogger(ParticipantAgent.class);

	protected void setup() {
		addBehaviour(new ParticipantBehaviourImpl());
	}

	@BehaviourType(capabilities = TypeParticipant.class)
	public class ParticipantBehaviourImpl extends ParticipantBehaviour implements
			CNPParticipantObserver {

		public ParticipantBehaviourImpl() {
			super(ARTIFACT_NAME);
			// TODO Auto-generated constructor stub
		}
				
		@Override
		public void action() {
			super.action();
		}
		
		@Override
		public Behaviour commitToDoneOrFailureIfAccept() {
			return new CommitToDoneOrFailureIfAccept(participant, this);
		}
		
		@Override
		public Behaviour fulfilledCommitToAcceptOrReject() {
			return new FulfilledCommitToAcceptOrReject(participant, getTask());
		}
	
	}
	
	public class CommitToDoneOrFailureIfAccept extends OneShotBehaviour {
		Participant participant;
		Task t;
		Proposal p;
		ParticipantBehaviour bh;
		
		public CommitToDoneOrFailureIfAccept(Participant participant, ParticipantBehaviour bh) {
			this.participant = participant;		
			this.bh = bh;
		}

		@Override
		public void action() {
			try {
				// wait from participants
				Thread.sleep(1000);
			} catch (Exception e) {
			}
			RoleMessage cfp = participant.receive(ACLMessage.CFP);
			t = (Task) (cfp.getContents());
			bh.setTask(t);
			p = createProposal(t);
			participant.propose(p, cfp.getRoleSender());

		}
		
		private Proposal createProposal(Task t) {
			logger.debug("Creating Proposal for task " + t);
			return new Proposal("Proposal " + getBehaviourName());
		}


	}

	public class FulfilledCommitToAcceptOrReject extends OneShotBehaviour {

		Participant participant = null;
		Task task;
		
		public FulfilledCommitToAcceptOrReject(Participant participant, Task task) {
			super();
			this.participant = participant;
			this.task = task;
		}

		@Override
		public void action() {
			try {
				// wait from participants
				Thread.sleep(2000);
			} catch (Exception e) {
			}
			RoleMessage accept = participant
					.receive(ACLMessage.ACCEPT_PROPOSAL);
			if (accept != null) {
				if (accept.getPerformative() == ACLMessage.ACCEPT_PROPOSAL)
					if (elaborateTask(task))
						participant.done(task, accept.getRoleSender());
					else
						participant.failure(accept.getRoleSender());
			}
			

		}
		
		private boolean elaborateTask(Task t) {
			logger.debug("Elaborating task " + t);
			int r = (int)(Math.random()*100);
			if (r < 50) {
				t.setResult("Done");
				return true;
			}
			else {
				t.setResult("Failure");
				return false;
			}
			
		}

	}

}

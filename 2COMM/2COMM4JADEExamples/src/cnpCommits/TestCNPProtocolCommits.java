package cnpCommits;

import java.util.ArrayList;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import cartago.CartagoException;
import cartago.CartagoService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class TestCNPProtocolCommits {
	
	private static Logger logger = LogManager.getLogger(TestCNPProtocolCommits.class);
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			testAgents();
			
//			testExpressionTree();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		final Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "true");
		ContainerController cc = rt.createMainContainer(p);
		
		java.lang.Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		        rt.shutDown();
		    }
		});
		
		AgentController initiator = cc.createNewAgent("Initiator",
				InitiatorAgent.class.getCanonicalName(), null);
		
		ArrayList<AgentController> participants = new ArrayList<AgentController>();
		for (int i=0;i<2;i++) {
			participants.add(cc.createNewAgent("Participant-"+i,
				ParticipantAgent.class.getCanonicalName(), null));
		}
		
		for (AgentController part : participants) {
			part.start();
		}
		
		
		initiator.start();
	}
	
	private static void testExpressionTree() throws MissingOperandException, WrongOperandsNumberException, CartagoException {
		
		CompositeExpression le1 = 
				CompositeExpression.and(new Fact("pippo", new Fact("pippoInside")), 
						CompositeExpression.or(new Fact("ripippo"), 
							CompositeExpression.not(new Fact("ripippo")))
						);
		
		logger.info(le1.toString());		
		logger.error("pippo");
				
	}
	
	
}

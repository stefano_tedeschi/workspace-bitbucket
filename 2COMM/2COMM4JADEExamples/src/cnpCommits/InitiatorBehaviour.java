package cnpCommits;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.ArtifactId;
import cnpCommits.CNPArtifactTimerMaxPart.CNPInitiatorObserver;
import cnpCommits.CNPArtifactTimerMaxPart.Initiator;

public abstract class InitiatorBehaviour extends OneShotBehaviour implements
				CNPInitiatorObserver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(InitiatorBehaviour.class);
	protected Initiator initiator;
	public String artifactName;
	public int satisfiedCommitToAcceptOrReject=0;
	
	public abstract Behaviour commitToAcceptOrRejectIfPropose();

	public abstract Behaviour satisfyCommitToAcceptOrReject();
	
	public abstract Behaviour fulfilledCommitToDoneOrFailure();
	
	
	public InitiatorBehaviour(String artifactName) {
		this.artifactName = artifactName;
	}
	
	@Override
	public void action() {
		ArtifactId art = Role.createArtifact(artifactName,
				CNPArtifactTimerMaxPart.class);
		initiator = (Initiator) (Role.enact(CNPArtifactTimerMaxPart.INITIATOR_ROLE,
				art, new JadeBehaviourPlayer(this, myAgent.getAID())));

		initiator.startObserving(this);
		
		myAgent.addBehaviour(this.commitToAcceptOrRejectIfPropose());
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		logger.debug("Handle by: "+initiator.getRoleId()+"\n"+e);
		if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			if (c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
				try {
					if (c.getDebtor().equals(initiator.getRoleId())
							&& c.getCreditor().equals(
									new RoleId(
											CNPArtifactTimerMaxPart.PARTICIPANT_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("accept"), new Fact(
													"reject")))) {
						myAgent.addBehaviour(satisfyCommitToAcceptOrReject());
						logger.debug("SatisfyCommitToAcceptOrReject: "+(++satisfiedCommitToAcceptOrReject));
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			else if (c.getLifeCycleStatus() == LifeCycleState.SATISFIED) {
				try {
					if (c.getCreditor().equals(initiator.getRoleId())
							&& c.getDebtor().equals(
									new RoleId(
											CNPArtifactTimerMaxPart.PARTICIPANT_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("done"), new Fact(
													"failure")))) {
						myAgent.addBehaviour(fulfilledCommitToDoneOrFailure());
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			} else {
				// altri stati a cui reagire
			}
		} else { // event is a SocialFact assertion

		}

	}
}

package cnpCommits;

import twocomm.core.logic.Fact;
import twocomm.core.typing.AgentAbilities;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class TypeParticipant extends AgentAbilities {

	public TypeParticipant() throws MissingOperandException, WrongOperandsNumberException {
		
		super(new Fact[]
				{new Fact("done"), new Fact("failure")
				});
	}
}

package netbill.automated.customer;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import netbill.automated.Item;
import netbill.automated.NetbillProtocol;
import netbill.automated.NetbillProtocol.Customer;
import netbill.automated.NetbillProtocol.CustomerObserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.Fact;
import cartago.ArtifactId;

public abstract class CustomerBehaviour extends OneShotBehaviour implements
				CustomerObserver {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(CustomerBehaviour.class);
	protected Customer customer;
	public String artifactName;
	
	public abstract Behaviour cSendsRequest();
	public abstract Behaviour cAcceptsOrRejects(Item i, double price);
	public abstract Behaviour cSendsEPO(Item i);
	
	
	public CustomerBehaviour(String artifactName) {
		this.artifactName = artifactName;
	}
	
	@Override
	public void action() {
		ArtifactId art = Role.createArtifact(artifactName,
				NetbillProtocol.class);
		customer = (Customer) (Role.enact(NetbillProtocol.CUSTOMER_ROLE,
				art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
		customer.startObserving(this);
		myAgent.addBehaviour(cSendsRequest());
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if (e.getElementChanged().getElType() == SocialStateElementType.LOGICAL_EXPRESSION) {
			Fact f = (Fact) e.getElementChanged();
			if(f.getPredicate().equals("quotation")) {
				Item i = (Item) f.getArguments()[0];
				double price = (double) f.getArguments()[1];
				myAgent.addBehaviour(cAcceptsOrRejects(i, price));
			}
		}
		else if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			if(c.getDebtor().equals(customer.getRoleId())) {
				if(c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
					Fact f = (Fact) c.getConsequent();
					if(f.getPredicate().equals("paid")) {
						
						Item i = (Item) f.getArguments()[0];
						myAgent.addBehaviour(cSendsEPO(i));
					}
				}
			}
		}
	}
}

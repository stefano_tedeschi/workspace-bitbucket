package netbill.automated.merchant;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import netbill.automated.Item;
import netbill.automated.NetbillProtocol;
import netbill.automated.NetbillProtocol.Merchant;
import netbill.automated.NetbillProtocol.MerchantObserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.Fact;
import cartago.ArtifactId;

public abstract class MerchantBehaviour extends OneShotBehaviour implements
				MerchantObserver {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(MerchantBehaviour.class);
	protected Merchant merchant;
	public String artifactName;
	
	public abstract Behaviour mSendsQuote(Item i);
	public abstract Behaviour mSendsGoods(Item i);
	public abstract Behaviour mSendsReceipt(Item i);
	public abstract Behaviour mPrintsSocialState();
	
	
	public MerchantBehaviour(String artifactName) {
		this.artifactName = artifactName;
	}

	@Override
	public void action() {
		System.out.println("Sono qua");
		ArtifactId art = Role.createArtifact(artifactName,
				NetbillProtocol.class);
		merchant = (Merchant) (Role.enact(
				NetbillProtocol.MERCHANT_ROLE, art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
		merchant.startObserving(this);
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if (e.getElementChanged().getElType() == SocialStateElementType.LOGICAL_EXPRESSION) {
			Fact f = (Fact) e.getElementChanged();
			if(f.getPredicate().equals("requestedQuote")) {
				Item i = (Item) f.getArguments()[0];
				myAgent.addBehaviour(mSendsQuote(i));
			}
			else if(f.getPredicate().equals("receipt") || f.getPredicate().equals("rejectedQuotation")) {
				myAgent.addBehaviour(mPrintsSocialState());
			}
		}
		else if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			if(c.getDebtor().equals(merchant.getRoleId())) {
				if(c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
					Fact f = (Fact) c.getConsequent();
					if(f.getPredicate().equals("goods")) {
						Item i = (Item) f.getArguments()[0];
						myAgent.addBehaviour(mSendsGoods(i));
					}
					else if(f.getPredicate().equals("receipt")) {
						Item i = (Item) f.getArguments()[0];
						myAgent.addBehaviour(mSendsReceipt(i));
					}
				}
			}
		}
	}
}

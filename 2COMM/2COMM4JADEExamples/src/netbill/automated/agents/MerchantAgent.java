package netbill.automated.agents;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import netbill.automated.Item;
import netbill.automated.NetbillProtocol.Merchant;
import netbill.automated.NetbillProtocol.MerchantObserver;
import netbill.automated.merchant.MerchantBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MerchantAgent extends Agent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String ARTIFACT_NAME = "NETBILL-1";
	public Logger logger = LogManager.getLogger(MerchantAgent.class);

	protected void setup() {
		addBehaviour(new MerchantBehaviourImpl());
	}

	public class MerchantBehaviourImpl extends MerchantBehaviour implements MerchantObserver {

		private static final long serialVersionUID = 1L;

		public MerchantBehaviourImpl() {
			super(ARTIFACT_NAME);
		}

		@Override
		public void action() {
			super.action();
		}

		@Override
		public Behaviour mSendsQuote(Item i) {
			return new MSendsQuote(merchant, i);
		}

		@Override
		public Behaviour mSendsGoods(Item i) {
			return new MSendsGoods(merchant, i);
		}

		@Override
		public Behaviour mSendsReceipt(Item i) {
			return new MSendsReceipt(merchant, i);
		}

		@Override
		public Behaviour mPrintsSocialState() {
			return new MPrintsSocialState(merchant);
		}

	}

	public class MSendsQuote extends OneShotBehaviour {

		private static final long serialVersionUID = 1L;
		private Merchant merchant;
		private Item i;

		public MSendsQuote(Merchant merchant, Item item) {
			this.merchant = merchant;
			this.i = item;
		}

		@Override
		public void action() {
			merchant.sendQuote(i, 5000);
		}

	}

	public class MSendsGoods extends OneShotBehaviour {

		private static final long serialVersionUID = 1L;
		private Merchant merchant;
		private Item i;

		public MSendsGoods(Merchant merchant, Item item) {
			this.merchant = merchant;
			this.i = item;
		}

		@Override
		public void action() {
			merchant.sendGoods(i);
		}

	}

	public class MSendsReceipt extends OneShotBehaviour {

		private static final long serialVersionUID = 1L;
		private Merchant merchant;
		private Item i;

		public MSendsReceipt(Merchant merchant, Item item) {
			this.merchant = merchant;
			this.i = item;
		}

		@Override
		public void action() {
			merchant.sendReceipt(i);
		}

	}

	public class MPrintsSocialState extends OneShotBehaviour {

		private static final long serialVersionUID = 1L;
		private Merchant merchant;

		public MPrintsSocialState(Merchant merchant) {
			this.merchant = merchant;
		}

		@Override
		public void action() {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			merchant.printSocialState();
		}

	}

}

package netbill.automated.agents;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import cartago.CartagoService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestNetbill {
	
	private static Logger logger = LogManager.getLogger(TestNetbill.class);
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			CartagoService.createWorkspace("default");
			testAgents();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		final Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "false");
		ContainerController cc = rt.createMainContainer(p);
		
		java.lang.Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		        rt.shutDown();
		    }
		});
		
		AgentController merchant = cc.createNewAgent("Merchant",
				MerchantAgent.class.getCanonicalName(), null);

		merchant.start();
		
		AgentController customer = cc.createNewAgent("Customer",
				CustomerAgent.class.getCanonicalName(), null);

		customer.start();
		
	}	
}

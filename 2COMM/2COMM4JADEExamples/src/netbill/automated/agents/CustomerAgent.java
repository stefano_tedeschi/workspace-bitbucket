package netbill.automated.agents;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import netbill.automated.Item;
import netbill.automated.NetbillProtocol.Customer;
import netbill.automated.customer.CustomerBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CustomerAgent extends Agent {

	private static final long serialVersionUID = 1L;
	public static final String ARTIFACT_NAME = "NETBILL-1";
	public Logger logger = LogManager.getLogger(CustomerAgent.class);
	
	protected void setup() {
		addBehaviour(new CustomerBehaviourImpl());
	}

	public class CustomerBehaviourImpl extends CustomerBehaviour  {

		public CustomerBehaviourImpl() {
			super(ARTIFACT_NAME);
		}

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			super.action();
			
		}

		@Override
		public Behaviour cSendsRequest() {
			return new CSendsRequest(customer);
		}

		@Override
		public Behaviour cAcceptsOrRejects(Item i, double price) {
			return new CAcceptsOrRejects(customer, i, price);
		}

		@Override
		public Behaviour cSendsEPO(Item i) {
			return new CSendsEPO(customer, i);
		}
		
	}

	public class CSendsRequest extends OneShotBehaviour {
		
		private static final long serialVersionUID = 1L;
		private Customer customer = null;
		private Item i = new Item("item");

		public CSendsRequest(Customer customer) {
			this.customer = customer;
		}

		@Override
		public void action() {
			customer.sendRequest(i);

		}

	}

	public class CAcceptsOrRejects extends OneShotBehaviour {

		private static final long serialVersionUID = 1L;
		private Customer customer;
		private Item i;
		private double price;

		public CAcceptsOrRejects(Customer customer, Item i, double price) {
			super();
			this.customer = customer;
			this.i = i;
			this.price = price;
		}

		@Override
		public void action() {
			//l'agente sceglie in modo casuale se accettare il prezzo proposto oppure no
			if(Math.round(Math.random()*100)%2==0) {
				customer.sendAccept(i,price);
			}
			else {
				customer.sendReject(i, price);
			}		
		}
	}
	
	public class CSendsEPO extends OneShotBehaviour {
		
		private static final long serialVersionUID = 1L;
		private Customer customer;
		private Item i;

		public CSendsEPO(Customer customer, Item i) {
			this.customer = customer;
			this.i = i;
		}

		@Override
		public void action() {
			customer.sendEPO(i);
		}

	}

}

package agent;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.CommunicationArtifact;
import twocomm.core.CommunicationArtifact.CARole;
import twocomm.core.Role;
import twocomm.core.RoleMessage;
import twocomm.core.jade.JadeBehaviourPlayer;
import cartago.ArtifactId;
import def.CommunicationArtifactImpl;

public class ReceiverAgent extends Agent {

	public static final String ARTIFACT_NAME = "SendReceiveArtifact";
	public Logger logger = LogManager.getLogger(ReceiverAgent.class);
	
	protected void setup() {
		addBehaviour(new OneShotBehaviour() {
			
			@Override
			public void action() {
				
				ArtifactId art = Role.createArtifact(ARTIFACT_NAME, CommunicationArtifactImpl.class);
				CARole user = (CARole)(Role.enact(CommunicationArtifact.CA_ROLE, art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					logger.error("Interrupted exception in receive");
					e.printStackTrace();
				}
				logger.info("Inizio a ricevere");
				ArrayList<RoleMessage> receive = user.receiveAll(ACLMessage.AGREE);
				logger.info("Message received: "+receive.size());
				if (receive.size() != 0) {
					// respond to message
					RoleMessage answer = new RoleMessage(user.getRoleId(),receive.get(0).getRoleSender(),"Ti rispondo alla prova!");
					user.send(answer);
				}
				
				
				
			}
		});
	}
	

}

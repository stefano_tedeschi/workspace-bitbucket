package agent;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import cartago.CartagoException;
import cartago.CartagoService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class TestSendReceiveAgents {
	
	private static Logger logger = LogManager.getLogger(TestSendReceiveAgents.class);
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			testAgents();
			
//			testExpressionTree();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "true");
		ContainerController cc = rt.createMainContainer(p);
		AgentController senderAgent1 = cc.createNewAgent("SenderAgent-1",
				SenderAgent.class.getCanonicalName(), null);
		AgentController senderAgent2 = cc.createNewAgent("SenderAgent-2",
				SenderAgent.class.getCanonicalName(), null);
		AgentController senderAgent3 = cc.createNewAgent("SenderAgent-3",
				SenderAgent.class.getCanonicalName(), null);
		AgentController receiverAgent1 = cc.createNewAgent("ReceiverAgent-1",
				ReceiverAgent.class.getCanonicalName(), null);
//		AgentController receiverAgent2 = cc.createNewAgent("ReceiverAgent-2",
//				ReceiverAgent.class.getCanonicalName(), null);
//		AgentController receiverAgent3 = cc.createNewAgent("ReceiverAgent-3",
//				ReceiverAgent.class.getCanonicalName(), null);
		
		senderAgent1.start();
		senderAgent2.start();
		senderAgent3.start();
		
		receiverAgent1.start();
//		receiverAgent2.start();
//		receiverAgent3.start();
	}
	
	private static void testExpressionTree() throws MissingOperandException, WrongOperandsNumberException, CartagoException {
		
		CompositeExpression le1 = 
				CompositeExpression.and(new Fact("pippo", new Fact("pippoInside")), 
						CompositeExpression.or(new Fact("ripippo"), 
							CompositeExpression.not(new Fact("ripippo")))
						);
		
		logger.info(le1.toString());		
		logger.error("pippo");
				
	}
	
	
}

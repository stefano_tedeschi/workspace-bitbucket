package agent;

import twocomm.core.CommunicationArtifact;
import twocomm.core.Role;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.CommunicationArtifact.CARole;
import twocomm.core.jade.JadeBehaviourPlayer;
import cartago.ArtifactId;
import def.CommunicationArtifactImpl;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class SenderAgent extends Agent {

	public static final String ARTIFACT_NAME = "SendReceiveArtifact";
		
	protected void setup() {
		addBehaviour(new OneShotBehaviour() {
			
			@Override
			public void action() {
				ArtifactId art = Role.createArtifact(ARTIFACT_NAME, CommunicationArtifactImpl.class);
				CARole user = (CARole)(Role.enact(CommunicationArtifact.CA_ROLE, art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
				RoleId receiver = new RoleId(CommunicationArtifact.CA_ROLE, RoleId.GROUP_ROLE);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				RoleMessage test = new RoleMessage();
				RoleMessage test1 = new RoleMessage();
				test.setContents("Prova "+user.getRoleId());
				test.setRoleSender(user.getRoleId());
				test.setRoleReceiver(receiver);
				int r = (int) (Math.random()*100);
				if (r < 50)
					test.setPerformative(ACLMessage.INFORM);
				else
					test.setPerformative(ACLMessage.AGREE);
												
				user.send(test);
								
			}
		});
	}
	

}

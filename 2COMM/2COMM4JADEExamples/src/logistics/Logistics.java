package logistics;

import java.util.ArrayList;

import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;
import logistics.C1.CreditorC1;
import logistics.C1.DebtorC1;
import logistics.C1.SSMC1;
import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.ProtocolArtifact.ProtocolObserver;
import twocomm.core.RoleAction;
import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;

public class Logistics extends ProtocolArtifact {

	private String packageName;
	private Location currentLocation;

	public static String LOGISTIC_ROLE = "logisticRole";

	private enum Location {
		A, B, C, D
	}

	static {
		addEnabledRole(LOGISTIC_ROLE, LogisticRole.class);
	}

	@OPERATION
	public void init(String packageName) {
		this.packageName = packageName;
		defineObsProperty("packageName", packageName);
	}

	@OPERATION
	public void pay(int what, String who) {
		defineObsProperty("pay", what, who);
		setChanged();
		try {
			notifyObservers(new SocialEvent(new Fact("payL", what, who), SocialEventActionType.FACT_ASSERTED));
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@OPERATION
	public void move(String pkg, String where) {
		if (!packageName.equals(pkg)) {
			failed("Wrong package name");
		} else if (where.equals("A")) {
			currentLocation = Location.A;
			defineObsProperty("currentLocation", Location.A.toString());
			setChanged();
			try {
				notifyObservers(new SocialEvent(new Fact("atL", pkg, where), SocialEventActionType.FACT_ASSERTED));
			} catch (MissingOperandException e) {
				e.printStackTrace();
			}
		} else if (where.equals("B")) {
			if (currentLocation == Location.A) {
				currentLocation = Location.B;
				removeObsProperty("currentLocation");
				defineObsProperty("currentLocation", Location.B.toString());
				setChanged();
				try {
					notifyObservers(new SocialEvent(new Fact("atL", pkg, where), SocialEventActionType.FACT_ASSERTED));
				} catch (MissingOperandException e) {
					e.printStackTrace();
				}
			} else {
				failed("Wrong position");
			}
		} else if (where.equals("C")) {
			if (currentLocation == Location.B) {
				currentLocation = Location.C;
				removeObsProperty("currentLocation");
				defineObsProperty("currentLocation", Location.C.toString());
				setChanged();
				try {
					notifyObservers(new SocialEvent(new Fact("atL", pkg, where), SocialEventActionType.FACT_ASSERTED));
				} catch (MissingOperandException e) {
					e.printStackTrace();
				}
			} else {
				failed("Wrong position");
			}
		} else if (where.equals("D")) {
			if (currentLocation == Location.C) {
				currentLocation = Location.D;
				removeObsProperty("currentLocation");
				defineObsProperty("currentLocation", Location.D.toString());
				setChanged();
				try {
					notifyObservers(new SocialEvent(new Fact("atL", pkg, where), SocialEventActionType.FACT_ASSERTED));
				} catch (MissingOperandException e) {
					e.printStackTrace();
				}
			} else {
				failed("Wrong position");
			}
		}
	}

	@Override
	public String getArtifactType() {
		// TODO Auto-generated method stub
		return null;
	}

	public class LogisticRole extends PARole {

		public LogisticRole(IPlayer player) {
			super(LOGISTIC_ROLE, player);
			// TODO Auto-generated constructor stub
		}

		@RoleAction
		public void init(String packageName) {
			try {
				doAction(this.getArtifactId(), new Op("init", packageName));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}

		@RoleAction
		public void pay(int what, String who) {
			try {
				doAction(this.getArtifactId(), new Op("pay", what, who));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}

		@RoleAction
		public void move(String pkg, String where) {
			try {
				doAction(this.getArtifactId(), new Op("move", pkg, where));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}

	}
	
	public interface LogisticRoleObserver extends ProtocolObserver {

	}

}

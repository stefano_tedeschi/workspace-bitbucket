package logistics;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.OpFeedbackParam;
import cartago.util.agent.ActionFailedException;
import twocomm.core.Commitment;
import twocomm.core.CommitmentArtifact;
import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.ProtocolArtifact.ProtocolObserver;
import twocomm.core.Role;
import twocomm.core.RoleAction;
import twocomm.core.RoleId;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class C6 extends CommitmentArtifact {

	protected Logger logger = LogManager.getLogger(C6.class);
	public static String ARTIFACT_TYPE = "C6";
	public static String DEBTOR_ROLE = "debtorC6";
	public static String CREDITOR_ROLE = "creditorC6";
	public static String SOCIAL_STATE_MANAGER_ROLE = "ssmC6";

	static {
		addEnabledRole(DEBTOR_ROLE, DebtorC6.class);
		addEnabledRole(CREDITOR_ROLE, CreditorC6.class);
		addEnabledRole(SOCIAL_STATE_MANAGER_ROLE,SSMC6.class);
	}

	public C6() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);

	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	@Override
	protected void enact(String roleName) {
		super.enact(roleName);
		if (roleName.equals("debtorC6")) {
			debtor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		} else if (roleName.equals("creditorC6")) {
			creditor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		}
	}
	
	@Override
	protected void enact(String roleName, IPlayer player, OpFeedbackParam<Role> role) {
		super.enact(roleName, player, role);
		if (roleName.equals("debtorC6")) {
			debtor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		} else if (roleName.equals("creditorC6")) {
			creditor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		}
	}

	// ROLES OPERATIONS

	@OPERATION
	public void createC6() {
		RoleId agent = getRoleIdByRoleName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can create a commitment");
		}
		try {
			createCommitment(new Commitment(debtor, creditor, new CompositeExpression(LogicalOperatorType.AND, new Fact("at", "pkg", "B"), new Fact("pay", "200", debtor.toString())), new Fact("at", "pkg", "D")));
			logger.trace("OPERATION PERFORMED: createC6 by " + agent.toString());
		} catch (MissingOperandException e) {
			log(Arrays.toString(e.getStackTrace()));
		} catch (WrongOperandsNumberException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	public void releaseC6() {
		RoleId agent = getRoleIdByRoleName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(creditor)) {
			failed("Only the creditor can release a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		releaseCommitment(c);
		logger.trace("OPERATION PERFORMED: releaseC6 by " + agent.toString());
	}
	
	@OPERATION
	public void cancelC6() {
		RoleId agent = getRoleIdByRoleName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can cancel a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		cancelCommitment(c);
		logger.trace("OPERATION PERFORMED: cancelC6 by " + agent.toString());
	}
	
	@OPERATION
	public void assertPay(String pred, String who) {
		try {
			assertFact(new Fact("pay", pred, who));
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void assertAt(String what, String where) {
		try {
			assertFact(new Fact("at", what, where));
		} catch (

		MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// BEGIN INNER CLASSES for ROLES
	// Debtor role

	public class DebtorC6 extends CommitmentRole {

		public DebtorC6(IPlayer player) {
			super(DEBTOR_ROLE, player);
		}
		
		@RoleAction
		public void createC6() {
			try {
				doAction(this.getArtifactId(), new Op("createC6"));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}
		
		@RoleAction
		public void cancelC6() {
			try {
				doAction(this.getArtifactId(), new Op("cancelC6"));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}
		
	}

	// Creditor role

	public class CreditorC6 extends CommitmentRole {

		public CreditorC6(IPlayer player) {
			super(CREDITOR_ROLE, player);
		}
		
		@RoleAction
		public void releaseC6() {
			try {
				doAction(this.getArtifactId(), new Op("releaseC6"));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}

	}
	
	public class SSMC6 extends CommitmentRole {

		public SSMC6(IPlayer player) {
			super(SOCIAL_STATE_MANAGER_ROLE, player);
		}
		
		@RoleAction
		public void assertPay(String pred, String who) {
			try {
				doAction(this.getArtifactId(), new Op("assertPay", pred, who));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}
		
		@RoleAction
		public void assertAt(String what, String where) {
			try {
				doAction(this.getArtifactId(), new Op("assertAt", what, where));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}

	}

	public interface DebtorC6Observer extends ProtocolObserver {

	}

	public interface CreditorC6Observer extends ProtocolObserver {

	}
	
	public interface SSMC6Observer extends ProtocolObserver {

	}

}

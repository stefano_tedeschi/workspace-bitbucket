package logistics;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.OpFeedbackParam;
import cartago.util.agent.ActionFailedException;
import twocomm.core.Commitment;
import twocomm.core.CommitmentArtifact;
import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.ProtocolArtifact.ProtocolObserver;
import twocomm.core.Role;
import twocomm.core.RoleAction;
import twocomm.core.RoleId;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;

public class C4 extends CommitmentArtifact {

	protected Logger logger = LogManager.getLogger(C4.class);
	public static String ARTIFACT_TYPE = "C4";
	public static String DEBTOR_ROLE = "debtorC4";
	public static String CREDITOR_ROLE = "creditorC4";
	public static String SOCIAL_STATE_MANAGER_ROLE = "ssmC4";

	static {
		addEnabledRole(DEBTOR_ROLE, DebtorC4.class);
		addEnabledRole(CREDITOR_ROLE, CreditorC4.class);
		addEnabledRole(SOCIAL_STATE_MANAGER_ROLE,SSMC4.class);
	}

	public C4() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);

	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	@Override
	protected void enact(String roleName) {
		super.enact(roleName);
		if (roleName.equals("debtorC4")) {
			debtor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		} else if (roleName.equals("creditorC4")) {
			creditor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		}
	}
	
	@Override
	protected void enact(String roleName, IPlayer player, OpFeedbackParam<Role> role) {
		super.enact(roleName, player, role);
		if (roleName.equals("debtorC4")) {
			debtor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		} else if (roleName.equals("creditorC4")) {
			creditor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		}
	}

	// ROLES OPERATIONS

	@OPERATION
	public void createC4() {
		RoleId agent = getRoleIdByRoleName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can create a commitment");
		}
		try {
			createCommitment(new Commitment(debtor, creditor, new Fact("at", "pkg", "B"), new Fact("pay", "20", creditor.toString())));
			logger.trace("OPERATION PERFORMED: createC4 by " + agent.toString());
		} catch (MissingOperandException e) {
			log(Arrays.toString(e.getStackTrace()));
		}
	}

	@OPERATION
	public void releaseC4() {
		RoleId agent = getRoleIdByRoleName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(creditor)) {
			failed("Only the creditor can release a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		releaseCommitment(c);
		logger.trace("OPERATION PERFORMED: releaseC4 by " + agent.toString());
	}
	
	@OPERATION
	public void cancelC4() {
		RoleId agent = getRoleIdByRoleName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can cancel a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		cancelCommitment(c);
		logger.trace("OPERATION PERFORMED: cancelC4 by " + agent.toString());
	}

	@OPERATION
	public void assertPay(String pred, String who) {
		try {
			assertFact(new Fact("pay", pred, who));
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void assertAt(String what, String where) {
		try {
			assertFact(new Fact("at", what, where));
		} catch (

		MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	// BEGIN INNER CLASSES for ROLES
	// Debtor role

	public class DebtorC4 extends PARole {

		public DebtorC4(IPlayer player) {
			super(DEBTOR_ROLE, player);
		}
		
		@RoleAction
		public void createC4() {
			try {
				doAction(this.getArtifactId(), new Op("createC4"));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}
		
		@RoleAction
		public void cancelC4() {
			try {
				doAction(this.getArtifactId(), new Op("cancelC4"));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}
		
		public RoleId getCreditor() {
			return creditor;
		}
		
		public RoleId getDebtor() {
			return debtor;
		}
		
	}

	// Creditor role

	public class CreditorC4 extends CommitmentRole {

		public CreditorC4(IPlayer player) {
			super(CREDITOR_ROLE, player);
		}

		@RoleAction
		public void releaseC4() {
			try {
				doAction(this.getArtifactId(), new Op("releaseC4"));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public class SSMC4 extends CommitmentRole {

		public SSMC4(IPlayer player) {
			super(SOCIAL_STATE_MANAGER_ROLE, player);
		}
		
		@RoleAction
		public void assertPay(String pred, String who) {
			try {
				doAction(this.getArtifactId(), new Op("assertPay", pred, who));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}
		
		@RoleAction
		public void assertAt(String what, String where) {
			try {
				doAction(this.getArtifactId(), new Op("assertAt", what, where));
			} catch (ActionFailedException e) {
				e.printStackTrace();
			} catch (CartagoException e) {
				e.printStackTrace();
			}
		}

	}

	public interface DebtorC4Observer extends ProtocolObserver {

	}

	public interface CreditorC4Observer extends ProtocolObserver {

	}
	
	public interface SSMC4Observer extends ProtocolObserver {

	}

}

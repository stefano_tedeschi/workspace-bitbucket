package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.introspection.AddedBehaviour;
import logistics.C1.SSMC1;
import logistics.C1.SSMC1Observer;
import logistics.C2.SSMC2;
import logistics.C2.SSMC2Observer;
import logistics.C3.SSMC3;
import logistics.C3.SSMC3Observer;
import logistics.C4.SSMC4;
import logistics.C4.SSMC4Observer;
import logistics.C5.SSMC5;
import logistics.C5.SSMC5Observer;
import logistics.C6.SSMC6;
import logistics.C6.SSMC6Observer;
import logistics.C7.SSMC7;
import logistics.C7.SSMC7Observer;
import logistics.C8.SSMC8;
import logistics.C8.SSMC8Observer;
import logistics.Logistics.LogisticRole;
import logistics.Logistics.LogisticRoleObserver;
import twocomm.core.SocialEvent;
import twocomm.core.SocialEventActionType;
import twocomm.core.logic.Fact;

public abstract class SSMBehaviour extends OneShotBehaviour implements SSMC1Observer,
																	   SSMC2Observer,
																	   SSMC3Observer,
																	   SSMC4Observer,
																	   SSMC5Observer,
																	   SSMC6Observer,
																	   SSMC7Observer,
																	   SSMC8Observer,
																	   LogisticRoleObserver {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(SSMBehaviour.class);
	
	protected SSMC1 roleC1;
	protected SSMC2 roleC2;
	protected SSMC3 roleC3;
	protected SSMC4 roleC4;
	protected SSMC5 roleC5;
	protected SSMC6 roleC6;
	protected SSMC7 roleC7;
	protected SSMC8 roleC8;
	protected LogisticRole logRole;
	
	public SSMBehaviour() {

	}
	
	@Override
	public void action() {

		
	}
	
	public abstract Behaviour iAssertPay(String what, String who);
	
	public abstract Behaviour iAssertAt(String pkg, String where);
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if(e.getAction() == SocialEventActionType.FACT_ASSERTED) {
			Fact f = (Fact) e.getElementChanged();
			if(f.getPredicate().equals("payL")) {
				String what = f.getArguments()[0].toString();
				String who = f.getArguments()[1].toString();
				myAgent.addBehaviour(iAssertPay(what, who));
			}
			else if(f.getPredicate().equals("atL")) {
				String pkg = f.getArguments()[0].toString();
				String where = f.getArguments()[1].toString();
				myAgent.addBehaviour(iAssertAt(pkg, where));
			}
		}
	}
	
}

package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C1.CreditorC1;
import logistics.C1.CreditorC1Observer;
import logistics.C2.DebtorC2;
import logistics.C2.DebtorC2Observer;
import logistics.Logistics.LogisticRole;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;

public abstract class CustomerBehaviour extends OneShotBehaviour implements CreditorC1Observer, DebtorC2Observer {

	private static final long serialVersionUID = 1L;

	protected CreditorC1 roleC1;
	protected DebtorC2 roleC2;
	protected LogisticRole logRole;

	public Logger logger = LogManager.getLogger(CustomerBehaviour.class);

	public CustomerBehaviour() {

	}

	@Override
	public void action() {

	}

	public abstract Behaviour iDetachC1(String seller);

	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			try {
				if (c.getCreditor().toString().equals(roleC1.toString())
						&& c.getAntecedent().equals(new Fact("pay", "500", c.getDebtor().toString()))
						&& c.getConsequent().equals(new Fact("at", "pkg", "D"))
						&& c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
					myAgent.addBehaviour(iDetachC1(c.getDebtor().toString()));
				}
				if (c.getCreditor().toString().equals(roleC1.toString())
						&& c.getAntecedent().equals(new Fact("pay", "500", c.getDebtor().toString()))
						&& c.getConsequent().equals(new Fact("at", "pkg", "D"))
						&& c.getLifeCycleStatus() == LifeCycleState.SATISFIED) {
					System.out.println("CUSTOMER: well done!");
				}
			} catch (MissingOperandException e1) {
				e1.printStackTrace();
			}
		}
	}
}

package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.OneShotBehaviour;
import logistics.C8.CreditorC8;
import logistics.C8.CreditorC8Observer;
import logistics.Logistics.LogisticRole;
import twocomm.core.SocialEvent;

public abstract class Trk4Behaviour extends OneShotBehaviour implements CreditorC8Observer {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(Trk4Behaviour.class);
	
	protected CreditorC8 roleC8;
	protected LogisticRole logRole;
	
	public Trk4Behaviour() {

	}
	
	@Override
	public void action() {

		
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {

	}
}

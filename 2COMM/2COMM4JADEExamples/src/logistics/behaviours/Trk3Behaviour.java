package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C7.DebtorC7;
import logistics.C7.DebtorC7Observer;
import logistics.Logistics.LogisticRole;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public abstract class Trk3Behaviour extends OneShotBehaviour implements DebtorC7Observer {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(Trk3Behaviour.class);
	
	protected DebtorC7 roleC7;
	protected LogisticRole logRole;
	
	public Trk3Behaviour() {

	}
	
	@Override
	public void action() {

		
	}
	
	public abstract Behaviour iSatisfyC7();
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if(e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			try {
				if(c.getDebtor().toString().equals(roleC7.toString()) &&
				   c.getAntecedent().equals(new CompositeExpression(LogicalOperatorType.AND, new Fact("at","pkg","C"), new Fact("pay", "50", c.getDebtor().toString()))) &&
				   c.getConsequent().equals(new Fact("at", "pkg", "D")) &&
				   c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
					myAgent.addBehaviour(iSatisfyC7());
				}
			} catch (MissingOperandException e1) {
				e1.printStackTrace();
			} catch (WrongOperandsNumberException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}

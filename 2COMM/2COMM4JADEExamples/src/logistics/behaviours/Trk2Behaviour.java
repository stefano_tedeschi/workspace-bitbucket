package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.OneShotBehaviour;
import logistics.C4.CreditorC4;
import logistics.C4.CreditorC4Observer;
import logistics.Logistics.LogisticRole;
import twocomm.core.SocialEvent;

public abstract class Trk2Behaviour extends OneShotBehaviour implements CreditorC4Observer {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(Trk2Behaviour.class);
	
	protected CreditorC4 roleC4;
	protected LogisticRole logRole;
	
	public Trk2Behaviour() {

	}
	
	@Override
	public void action() {

		
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {

	}
}

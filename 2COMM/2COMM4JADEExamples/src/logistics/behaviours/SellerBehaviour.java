package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C1.DebtorC1;
import logistics.C1.DebtorC1Observer;
import logistics.C2.CreditorC2;
import logistics.C2.CreditorC2Observer;
import logistics.C3.CreditorC3;
import logistics.C3.CreditorC3Observer;
import logistics.C4.DebtorC4;
import logistics.C4.DebtorC4Observer;
import logistics.C5.CreditorC5;
import logistics.C5.CreditorC5Observer;
import logistics.C6.CreditorC6;
import logistics.C6.CreditorC6Observer;
import logistics.C8.DebtorC8;
import logistics.C8.DebtorC8Observer;
import logistics.Logistics.LogisticRole;
import logistics.Logistics.LogisticRoleObserver;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;

public abstract class SellerBehaviour extends OneShotBehaviour implements DebtorC1Observer,
																	   CreditorC2Observer,
																	   CreditorC3Observer,
																	   DebtorC4Observer,
																	   CreditorC5Observer,
																	   CreditorC6Observer,
																	   DebtorC8Observer,
																	   LogisticRoleObserver {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(SellerBehaviour.class);
	
	protected DebtorC1 roleC1;
	protected CreditorC2 roleC2;
	protected CreditorC3 roleC3;
	protected DebtorC4 roleC4;
	protected CreditorC5 roleC5;
	protected CreditorC6 roleC6;
	protected DebtorC8 roleC8;
	protected LogisticRole logRole;
	
	public SellerBehaviour() {

	}
	
	@Override
	public void action() {

		
	}
	
	public abstract Behaviour iSatisfyC1();
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if(e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			try {
				if(c.getDebtor().toString().equals(roleC1.toString()) &&
				   c.getAntecedent().equals(new Fact("pay", "500", c.getDebtor().toString())) &&
				   c.getConsequent().equals(new Fact("at", "pkg", "D")) &&
				   c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
				   myAgent.addBehaviour(iSatisfyC1());
				}
			} catch (MissingOperandException e1) {
				e1.printStackTrace();
			}
		}
	}
}

package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C3.DebtorC3;
import logistics.C3.DebtorC3Observer;
import logistics.Logistics.LogisticRole;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public abstract class Trk1Behaviour extends OneShotBehaviour implements DebtorC3Observer {

	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(Trk1Behaviour.class);
	
	protected DebtorC3 roleC3;
	protected LogisticRole logRole;
	
	public Trk1Behaviour() {

	}
	
	@Override
	public void action() {

		
	}
	
	public abstract Behaviour iSatisfyC3();
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if(e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			try {
				if(c.getDebtor().toString().equals(roleC3.toString()) &&
				   c.getAntecedent().equals(new CompositeExpression(LogicalOperatorType.AND, new Fact("at","pkg","A"), new Fact("pay", "50", c.getDebtor().toString()))) &&
				   c.getConsequent().equals(new Fact("at", "pkg", "B")) &&
				   c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
					myAgent.addBehaviour(iSatisfyC3());
				}
			} catch (MissingOperandException e1) {
				e1.printStackTrace();
			} catch (WrongOperandsNumberException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}
}

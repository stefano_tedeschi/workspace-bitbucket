package logistics.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C5.DebtorC5;
import logistics.C5.DebtorC5Observer;
import logistics.C6.DebtorC6;
import logistics.C6.DebtorC6Observer;
import logistics.C7.CreditorC7;
import logistics.C7.CreditorC7Observer;
import logistics.Logistics.LogisticRole;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public abstract class Pln1Behaviour extends OneShotBehaviour
		implements DebtorC5Observer, DebtorC6Observer, CreditorC7Observer {

	private static final long serialVersionUID = 1L;

	public Logger logger = LogManager.getLogger(Pln1Behaviour.class);

	protected DebtorC5 roleC5;
	protected DebtorC6 roleC6;
	protected CreditorC7 roleC7;
	protected LogisticRole logRole;

	public Pln1Behaviour() {

	}

	@Override
	public void action() {

	}

	public abstract Behaviour iCreateC6();
	
	public abstract Behaviour iSatisfyC6();

	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			try {
				if (c.getCreditor().toString().equals(roleC7.toString())
						&& c.getAntecedent()
								.equals(new CompositeExpression(LogicalOperatorType.AND, new Fact("at", "pkg", "C"),
										new Fact("pay", "50", c.getDebtor().toString())))
						&& c.getConsequent().equals(new Fact("at", "pkg", "D"))
						&& c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
					myAgent.addBehaviour(iCreateC6());
				}
				if (c.getDebtor().toString().equals(roleC6.toString())
						&& c.getAntecedent()
								.equals(new CompositeExpression(LogicalOperatorType.AND, new Fact("at", "pkg", "B"),
										new Fact("pay", "200", c.getDebtor().toString())))
						&& c.getConsequent().equals(new Fact("at", "pkg", "D"))
						&& c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
					myAgent.addBehaviour(iSatisfyC6());
				}
			} catch (MissingOperandException | WrongOperandsNumberException e1) {
				e1.printStackTrace();
			}
		}
	}
}

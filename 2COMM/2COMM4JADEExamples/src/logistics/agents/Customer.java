package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C1;
import logistics.C1.CreditorC1;
import logistics.C2;
import logistics.C2.DebtorC2;
import logistics.Logistics;
import logistics.Logistics.LogisticRole;
import logistics.behaviours.CustomerBehaviour;
import twocomm.core.Role;
import twocomm.core.jade.JadeBehaviourPlayer;

public class Customer extends Agent {

	private static final long serialVersionUID = 1L;

	public Logger logger = LogManager.getLogger(Customer.class);
	
	protected void setup() {
		addBehaviour(new CustomerBehaviourImpl());
	}

	public class CustomerBehaviourImpl extends CustomerBehaviour  {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			
			super.action();
			
			ArtifactId log = Role.createArtifact("log", Logistics.class);
			ArtifactId c1 = Role.createArtifact("c1", C1.class);
			ArtifactId c2 = Role.createArtifact("c2", C2.class);
			
			roleC1 = (CreditorC1) (Role.enact(C1.CREDITOR_ROLE,
					c1, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC1.startObserving(this);
			roleC2 = (DebtorC2) (Role.enact(C2.DEBTOR_ROLE,
					c2, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC2.startObserving(this);
			logRole = (LogisticRole) (Role.enact(Logistics.LOGISTIC_ROLE,
					log, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			logRole.startObserving(this);
			
			
		}

		@Override
		public Behaviour iDetachC1(String seller) {
			return new DetachC1(seller);
		}
		
		public class DetachC1 extends OneShotBehaviour {

			private String seller;
			
			public DetachC1(String seller) {
				this.seller = seller;
			}

			@Override
			public void action() {
				System.out.println("CUSTOMER: paying 500 to the seller...");
				logRole.pay(500, seller);
				
			}
			
		}
	
	}

}

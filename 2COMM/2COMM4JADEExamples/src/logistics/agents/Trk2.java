package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import logistics.C4;
import logistics.C4.CreditorC4;
import logistics.Logistics;
import logistics.Logistics.LogisticRole;
import logistics.behaviours.Trk2Behaviour;
import twocomm.core.Role;
import twocomm.core.jade.JadeBehaviourPlayer;

public class Trk2 extends Agent {

	private static final long serialVersionUID = 1L;

	public Logger logger = LogManager.getLogger(Trk2.class);
	
	protected void setup() {
		addBehaviour(new Trk2BehaviourImpl());
	}

	public class Trk2BehaviourImpl extends Trk2Behaviour  {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			
			super.action();
			
			ArtifactId log = Role.createArtifact("log", Logistics.class);
			ArtifactId c4 = Role.createArtifact("c4", C4.class);
			
			roleC4 = (CreditorC4) (Role.enact(C4.CREDITOR_ROLE,
					c4, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC4.startObserving(this);
			logRole = (LogisticRole) (Role.enact(Logistics.LOGISTIC_ROLE,
					log, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			logRole.startObserving(this);
			
		}
	
	}

}

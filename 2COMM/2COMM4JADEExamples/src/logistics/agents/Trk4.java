package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import logistics.C8;
import logistics.C8.CreditorC8;
import logistics.Logistics;
import logistics.Logistics.LogisticRole;
import logistics.behaviours.Trk4Behaviour;
import twocomm.core.Role;
import twocomm.core.jade.JadeBehaviourPlayer;

public class Trk4 extends Agent {

	private static final long serialVersionUID = 1L;

	public Logger logger = LogManager.getLogger(Trk4.class);
	
	protected void setup() {
		addBehaviour(new Trk4BehaviourImpl());
	}

	public class Trk4BehaviourImpl extends Trk4Behaviour  {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			
			super.action();
			
			ArtifactId log = Role.createArtifact("log", Logistics.class);
			ArtifactId c8 = Role.createArtifact("c8", C8.class);
			
			roleC8 = (CreditorC8) (Role.enact(C8.CREDITOR_ROLE,
					c8, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC8.startObserving(this);
			logRole = (LogisticRole) (Role.enact(Logistics.LOGISTIC_ROLE,
					log, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			logRole.startObserving(this);
			
		}
	
	}

}

package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C3;
import logistics.C3.DebtorC3;
import logistics.Logistics;
import logistics.Logistics.LogisticRole;
import logistics.behaviours.Trk1Behaviour;
import twocomm.core.Role;
import twocomm.core.jade.JadeBehaviourPlayer;

public class Trk1 extends Agent {

	private static final long serialVersionUID = 1L;

	public Logger logger = LogManager.getLogger(Trk1.class);
	
	protected void setup() {
		addBehaviour(new Trk1BehaviourImpl());
	}

	public class Trk1BehaviourImpl extends Trk1Behaviour  {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			
			super.action();
			
			ArtifactId log = Role.createArtifact("log", Logistics.class);
			ArtifactId c3 = Role.createArtifact("c3", C3.class);
			
			roleC3 = (DebtorC3) (Role.enact(C3.DEBTOR_ROLE,
					c3, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC3.startObserving(this);
			logRole = (LogisticRole) (Role.enact(Logistics.LOGISTIC_ROLE,
					log, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			logRole.startObserving(this);
			
			doWait(1000);
			
			roleC3.createC3();
			
		}

		@Override
		public Behaviour iSatisfyC3() {
			return new SatisfyC3();
		}
		
		public class SatisfyC3 extends OneShotBehaviour {

			@Override
			public void action() {
				
				System.out.println("TRK1: Moving pkg to B...");
				logRole.move("pkg", "B");
			}
			
		}
	
	}

}

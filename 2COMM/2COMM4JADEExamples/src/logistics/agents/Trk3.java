package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C7;
import logistics.C7.DebtorC7;
import logistics.Logistics;
import logistics.Logistics.LogisticRole;
import logistics.behaviours.Trk3Behaviour;
import twocomm.core.Role;
import twocomm.core.jade.JadeBehaviourPlayer;

public class Trk3 extends Agent {

	private static final long serialVersionUID = 1L;

	public Logger logger = LogManager.getLogger(Trk3.class);

	protected void setup() {
		addBehaviour(new Trk3BehaviourImpl());
	}

	public class Trk3BehaviourImpl extends Trk3Behaviour {

		private static final long serialVersionUID = 1L;

		@Override
		public void action() {

			super.action();

			ArtifactId log = Role.createArtifact("log", Logistics.class);
			ArtifactId c7 = Role.createArtifact("c7", C7.class);

			roleC7 = (DebtorC7) (Role.enact(C7.DEBTOR_ROLE, c7,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC7.startObserving(this);
			logRole = (LogisticRole) (Role.enact(Logistics.LOGISTIC_ROLE,
					log, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			logRole.startObserving(this);

			doWait(1000);
			
			roleC7.createC7();

		}

		@Override
		public Behaviour iSatisfyC7() {
			return new SatisfyC7();
		}
		
		public class SatisfyC7 extends OneShotBehaviour {

			@Override
			public void action() {
				System.out.println("TRK3: Moving package to D...");
				logRole.move("pkg", "D");
				
			}
			
		}

	}

}

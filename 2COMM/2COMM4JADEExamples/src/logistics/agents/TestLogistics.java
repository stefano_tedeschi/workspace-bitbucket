package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.CartagoService;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

public class TestLogistics {
	
	private static Logger logger = LogManager.getLogger(TestLogistics.class);
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			CartagoService.createWorkspace("default");
			testAgents();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		final Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "true");
		ContainerController cc = rt.createMainContainer(p);
		
		java.lang.Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		        rt.shutDown();
		    }
		});
		
		AgentController ssm = cc.createNewAgent("Social State Manager",
				SocialStateManager.class.getCanonicalName(), null);
		
		ssm.start();
		
		Thread.sleep(1000);
		
		AgentController seller = cc.createNewAgent("Seller",
				Seller.class.getCanonicalName(), null);
		seller.start();
		
		AgentController customer = cc.createNewAgent("Customer",
				Customer.class.getCanonicalName(), null);
		customer.start();
		
		AgentController pln1 = cc.createNewAgent("Pln1",
				Pln1.class.getCanonicalName(), null);
		pln1.start();
		
		AgentController trk1 = cc.createNewAgent("Trk1",
				Trk1.class.getCanonicalName(), null);
		trk1.start();
		
		AgentController trk2 = cc.createNewAgent("Trk2",
				Trk2.class.getCanonicalName(), null);
		trk2.start();
		
		AgentController trk3 = cc.createNewAgent("Trk3",
				Trk3.class.getCanonicalName(), null);
		trk3.start();
		
		AgentController trk4 = cc.createNewAgent("Trk4",
				Trk4.class.getCanonicalName(), null);
		trk4.start();
		
	}	
}

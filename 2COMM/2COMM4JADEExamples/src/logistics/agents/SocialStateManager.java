package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C1;
import logistics.C1.SSMC1;
import logistics.C2;
import logistics.C2.SSMC2;
import logistics.C3;
import logistics.C3.SSMC3;
import logistics.C4;
import logistics.C4.SSMC4;
import logistics.C5;
import logistics.C5.SSMC5;
import logistics.C6;
import logistics.C6.SSMC6;
import logistics.C7;
import logistics.C7.SSMC7;
import logistics.C8;
import logistics.C8.SSMC8;
import logistics.Logistics;
import logistics.Logistics.LogisticRole;
import logistics.behaviours.SSMBehaviour;
import twocomm.core.Role;
import twocomm.core.jade.JadeBehaviourPlayer;

public class SocialStateManager extends Agent {

	private static final long serialVersionUID = 6852502916085149507L;

	public Logger logger = LogManager.getLogger(SocialStateManager.class);

	protected void setup() {
		addBehaviour(new SSMBehaviourImpl());
	}

	public class SSMBehaviourImpl extends SSMBehaviour {

		public SSMBehaviourImpl() {
			super();
		}

		private static final long serialVersionUID = 1L;

		@Override
		public void action() {

			super.action();
			ArtifactId log = Role.createArtifact("log", Logistics.class);
			ArtifactId c1 = Role.createArtifact("c1", C1.class);
			ArtifactId c2 = Role.createArtifact("c2", C2.class);
			ArtifactId c3 = Role.createArtifact("c3", C3.class);
			ArtifactId c4 = Role.createArtifact("c4", C4.class);
			ArtifactId c5 = Role.createArtifact("c5", C5.class);
			ArtifactId c6 = Role.createArtifact("c6", C6.class);
			ArtifactId c7 = Role.createArtifact("c7", C7.class);
			ArtifactId c8 = Role.createArtifact("c8", C8.class);

			roleC1 = (SSMC1) (Role.enact(C1.SOCIAL_STATE_MANAGER_ROLE, c1,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC1.startObserving(this);
			roleC2 = (SSMC2) (Role.enact(C2.SOCIAL_STATE_MANAGER_ROLE, c2,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC2.startObserving(this);
			roleC3 = (SSMC3) (Role.enact(C3.SOCIAL_STATE_MANAGER_ROLE, c3,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC3.startObserving(this);
			roleC4 = (SSMC4) (Role.enact(C4.SOCIAL_STATE_MANAGER_ROLE, c4,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC4.startObserving(this);
			roleC5 = (SSMC5) (Role.enact(C5.SOCIAL_STATE_MANAGER_ROLE, c5,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC5.startObserving(this);
			roleC6 = (SSMC6) (Role.enact(C6.SOCIAL_STATE_MANAGER_ROLE, c6,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC6.startObserving(this);
			roleC7 = (SSMC7) (Role.enact(C7.SOCIAL_STATE_MANAGER_ROLE, c7,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC7.startObserving(this);
			roleC8 = (SSMC8) (Role.enact(C8.SOCIAL_STATE_MANAGER_ROLE, c8,
					new JadeBehaviourPlayer(new Behaviour[] { this }, myAgent.getAID())));
			roleC8.startObserving(this);
			logRole = (LogisticRole) (Role.enact(Logistics.LOGISTIC_ROLE,
					log, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			logRole.startObserving(this);
			logRole.init("pkg");

		}

		@Override
		public Behaviour iAssertPay(String what, String who) {
			return new AssertPayBehaviour(what, who);
		}

		@Override
		public Behaviour iAssertAt(String pkg, String where) {
			return new AssertAtBehaviour(pkg, where);
		}

		public class AssertPayBehaviour extends OneShotBehaviour {

			private static final long serialVersionUID = 1L;
			
			String what;
			String who;

			public AssertPayBehaviour(String what, String who) {
				this.what = what;
				this.who = who;
			}

			@Override
			public void action() {
				roleC1.assertPay(what, who);
				roleC2.assertPay(what, who);
				roleC3.assertPay(what, who);
				roleC4.assertPay(what, who);
				roleC5.assertPay(what, who);
				roleC6.assertPay(what, who);
				roleC7.assertPay(what, who);
				roleC8.assertPay(what, who);
			}
		
		}
		
		public class AssertAtBehaviour extends OneShotBehaviour {

			private static final long serialVersionUID = 1L;
			String what;
			String where;

			public AssertAtBehaviour(String what, String where) {
				this.what = what;
				this.where = where;
			}

			@Override
			public void action() {
				roleC1.assertAt(what, where);
				roleC2.assertAt(what, where);
				roleC3.assertAt(what, where);
				roleC4.assertAt(what, where);
				roleC5.assertAt(what, where);
				roleC6.assertAt(what, where);
				roleC7.assertAt(what, where);
				roleC8.assertAt(what, where);
			}

		}
	
	}

}

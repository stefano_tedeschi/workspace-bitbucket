package logistics.agents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import logistics.C1;
import logistics.C1.DebtorC1;
import logistics.C2;
import logistics.C2.CreditorC2;
import logistics.C3;
import logistics.C3.CreditorC3;
import logistics.C4;
import logistics.C4.DebtorC4;
import logistics.C5;
import logistics.C5.CreditorC5;
import logistics.C6;
import logistics.C6.CreditorC6;
import logistics.C8;
import logistics.C8.DebtorC8;
import logistics.Logistics;
import logistics.Logistics.LogisticRole;
import logistics.behaviours.SellerBehaviour;
import twocomm.core.Role;
import twocomm.core.jade.JadeBehaviourPlayer;

public class Seller extends Agent {

	private static final long serialVersionUID = 1L;

	public Logger logger = LogManager.getLogger(Seller.class);
	
	protected void setup() {
		addBehaviour(new SellerBehaviourImpl());
	}

	public class SellerBehaviourImpl extends SellerBehaviour  {

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			
			super.action();
			
			ArtifactId log = Role.createArtifact("log", Logistics.class);
			ArtifactId c1 = Role.createArtifact("c1", C1.class);
			ArtifactId c2 = Role.createArtifact("c2", C2.class);
			ArtifactId c3 = Role.createArtifact("c3", C3.class);
			ArtifactId c4 = Role.createArtifact("c4", C4.class);
			ArtifactId c5 = Role.createArtifact("c5", C5.class);
			ArtifactId c6 = Role.createArtifact("c6", C6.class);
			ArtifactId c8 = Role.createArtifact("c8", C8.class);
			
			roleC1 = (DebtorC1) (Role.enact(C1.DEBTOR_ROLE,
					c1, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC1.startObserving(this);
			roleC2 = (CreditorC2) (Role.enact(C2.CREDITOR_ROLE,
					c2, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC2.startObserving(this);
			roleC3 = (CreditorC3) (Role.enact(C3.CREDITOR_ROLE,
					c3, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC3.startObserving(this);
			roleC4 = (DebtorC4) (Role.enact(C4.DEBTOR_ROLE,
					c4, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC4.startObserving(this);
			roleC5 = (CreditorC5) (Role.enact(C5.CREDITOR_ROLE,
					c5, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC5.startObserving(this);
			roleC6 = (CreditorC6) (Role.enact(C6.CREDITOR_ROLE,
					c6, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC6.startObserving(this);
			roleC8 = (DebtorC8) (Role.enact(C8.DEBTOR_ROLE,
					c8, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			roleC8.startObserving(this);
			logRole = (LogisticRole) (Role.enact(Logistics.LOGISTIC_ROLE,
					log, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			logRole.startObserving(this);
			doWait(2000);
			
			System.out.println("SELLER: Creating C1...");
			roleC1.createC1();
			
		}

		@Override
		public Behaviour iSatisfyC1() {
			return new SatisfyC1();
		}
		
		public class SatisfyC1 extends OneShotBehaviour {

			@Override
			public void action() {
				
				System.out.println("SELLER: Moving pkg to A...");
				logRole.move("pkg", "A");
				System.out.println("SELLER: paying 50 to trk1...");
				logRole.pay(50, roleC3.getDebtor().toString());
				System.out.println("SELLER: paying 200 to pln1...");
				logRole.pay(200, roleC6.getDebtor().toString());
			}
			
		}
	
	}

}

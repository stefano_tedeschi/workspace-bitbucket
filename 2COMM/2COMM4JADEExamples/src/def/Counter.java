package def;
import java.io.Serializable;

import cartago.*;

public class Counter extends Artifact implements Serializable {

	private int internalCounter = 0;
	
	@OPERATION void init(){
		defineObsProperty("count",0);
	}
	    
	@OPERATION void inc(){
		ObsProperty prop = getObsProperty("count");
		prop.updateValue(prop.intValue()+1);
		signal("tick");
	}
	
	public int getInternalCounter() {
		return internalCounter;
	}
}
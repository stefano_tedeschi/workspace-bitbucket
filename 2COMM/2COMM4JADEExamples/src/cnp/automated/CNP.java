package cnp.automated;

import java.util.ArrayList;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.LifeCycleState;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleAction;
import twocomm.core.RoleId;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;
import cnp.automated.Proposal;
import cnp.automated.Task;

public class CNP extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(CNP.class);
	public static String ARTIFACT_TYPE = "CNP";	
	public static String INITIATOR_ROLE = "Initiator";
	public static String PARTICIPANT_ROLE = "Participant";	
	private long millis;
	private int numberMaxProposals = 3;
	private int actualProposals = 0;
	private boolean acceptingProposals = true;
	private Task t = null;
	private ArrayList<Proposal> proposals = new ArrayList<Proposal>();
	
	static {
		addEnabledRole(INITIATOR_ROLE, Initiator.class);
		addEnabledRole(PARTICIPANT_ROLE, Participant.class);
	}
	
	public CNP() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}	
	
	
	// ROLES OPERATIONS
	
//	@OPERATION
//	public void init()  {
//		System.out.println("Test");
//	}
	
	
	@OPERATION
	public void cfp(Task task) {
		millis = new Date().getTime();
		RoleId initiator = getRoleIdByRoleName(getOpUserName());
		RoleId dest = new RoleId(PARTICIPANT_ROLE, RoleId.GROUP_ROLE);
		try {
			t = task;
			assertFact(new Fact("cfp", initiator, task));
			for(RoleId r : getEnactedRolesIds()) {
				if (r.equals(dest) && !r.equals(initiator)) {
					Commitment c = new Commitment(initiator, r, new Fact("propose", r), new CompositeExpression(LogicalOperatorType.OR, new Fact("accept", r), new Fact("reject", r)));
					createCommitment(c);
				}
			}
			//createAllCommitments(new Commitment (initiator, dest, "propose", new CompositeExpression(LogicalOperatorType.OR, new Fact("accept", dest), new Fact("reject", dest))));
			logger.trace("OPERATION PERFORMED: CFP by " + initiator);
		}
		catch(MissingOperandException | WrongOperandsNumberException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void accept(Proposal proposal) {
		RoleId initiator = getRoleIdByRoleName(getOpUserName());
		RoleId participant = proposal.getRoleId();
		try {
			assertFact(new Fact("accept", participant));
			logger.trace("OPERATION PERFORMED: ACCEPT by " + initiator);
		}
		catch(MissingOperandException e) {
			e.printStackTrace();
		}

	}
	
	@OPERATION
	public void reject(Proposal proposal) {
		RoleId initiator = getRoleIdByRoleName(getOpUserName());
		RoleId participant = proposal.getRoleId();
		try {
			assertFact(new Fact("reject", participant));
			//System.out.println("Termino " + c);
			//releaseCommitment(new Commitment(participant, initiator, new Fact("accept", participant), new CompositeExpression(
			//		LogicalOperatorType.OR, new Fact("done", participant), new Fact("failure", participant))));
			for(Commitment c : socialState.retrieveCommitmentsByAntecedent(new Fact("accept", participant))) {
				releaseCommitment(c);
			}
			logger.trace("OPERATION PERFORMED: REJECT by " + initiator);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void propose(Proposal prop, RoleId initiator) {
		if(!acceptingProposals)
			failed("No more proposals allowed.");
		RoleId participant = getRoleIdByRoleName(getOpUserName());
		proposals.add(prop);
		try {
			assertFact(new Fact("propose", participant));
			createCommitment(new Commitment(participant, initiator, new Fact("accept", participant), new CompositeExpression(LogicalOperatorType.OR, new Fact("done", participant), new Fact("failure", participant))));
			//TODO controllare questo pezzo
			actualProposals++;
			if (actualProposals == numberMaxProposals) {
				acceptingProposals = false;
				RoleId groupParticipant = new RoleId(PARTICIPANT_ROLE, RoleId.GROUP_ROLE);
				createCommitment(new Commitment(initiator, groupParticipant, new CompositeExpression(
						LogicalOperatorType.OR, new Fact("accept", groupParticipant), new Fact("reject", groupParticipant))));
			}
			logger.trace("OPERATION PERFORMED: PROPOSE by " + participant);
		} catch(WrongOperandsNumberException | MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	
	@OPERATION
	public void refuse(RoleId participant, RoleId initiator) {
		try {
			releaseCommitment(new Commitment(initiator, participant, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			assertFact(new Fact("refuse"));
			logger.trace("OPERATION PERFORMED: REFUSE by "+participant);
		} catch (MissingOperandException | WrongOperandsNumberException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void done(Task task, RoleId initiator) {
		RoleId participant = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("done", participant));
			logger.trace("OPERATION PERFORMED: DONE by "+participant);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void failure(RoleId initiator) {
		RoleId participant = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("failure", participant));
			logger.trace("OPERATION PERFORMED: FAILURE by "+participant);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void printSocialState() {
		System.out.println(socialState);
	}
	
	
	// BEGIN INNER CLASSES for ROLES
	// cnp.Initiator role
	
	public class Initiator extends PARole {
		
		public Initiator(IPlayer player) {
			super(INITIATOR_ROLE, player);
		}


		@RoleAction
		public void cfp(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("cfp", task));
			} catch (ActionFailedException e) {
				logger.error("Action CFP failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		// accepting to particular role
		@RoleAction
		public void accept(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("accept", proposal));
			} catch (ActionFailedException e) {
				logger.error("Action ACCEPT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		@RoleAction
		public void reject(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("reject", proposal));
			} catch (ActionFailedException e) {
				logger.error("Action REJECT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void printSocialState() {
			try {
				doAction(this.getArtifactId(), new Op("printSocialState"));
			} catch (ActionFailedException e) {
				logger.error("Action PRINT SOCIAL STATE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public boolean existsDone() throws MissingOperandException {
			for(RoleId r : getEnactedRolesIds()) {
				if(existsFact(new Fact("done", r)))
					return true;
			}
			return false;
		}
		
		public boolean existsFailure() throws MissingOperandException {
			for(RoleId r : getEnactedRolesIds()) {
				if(existsFact(new Fact("failure", r)))
					return true;
			}
			return false;
		}
		
		
		public ArrayList<Proposal> getProposals() {
			return proposals;
		}
		
	}
	
	// cnp.Participant role
	
	public class Participant extends PARole {
		
		public Participant(IPlayer player) {
			super(PARTICIPANT_ROLE, player);
		}

		public Task getTask() {
			return t;
		}
		
		@RoleAction
		public void propose(Proposal proposal, RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("propose", proposal, initiator));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public void refuse(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("refuse", task, this.getRoleId()));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		@RoleAction
		public void done(Task task, RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("done", task, initiator));
			} catch (ActionFailedException e) {
				logger.error("Action DONE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		@RoleAction
		public void failure(RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("failure", initiator));
			} catch (ActionFailedException e) {
				logger.error("Action FAILURE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
	}
	
	public interface CNPInitiatorObserver extends ProtocolObserver {
		
	}
	
	public interface CNPParticipantObserver extends ProtocolObserver {
		
	}

}

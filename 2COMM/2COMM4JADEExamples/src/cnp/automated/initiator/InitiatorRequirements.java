package cnp.automated.initiator;

import java.util.ArrayList;

import cnp.base.CNPArtifact;
import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleRequirements;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class InitiatorRequirements extends RoleRequirements {
	
	public InitiatorRequirements() throws MissingOperandException, WrongOperandsNumberException {
		super("Initiator",
				new Commitment[]{
				new Commitment(CNPArtifact.INITIATOR_ROLE, CNPArtifact.PARTICIPANT_ROLE, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))) 
		}, null);
	}	
}
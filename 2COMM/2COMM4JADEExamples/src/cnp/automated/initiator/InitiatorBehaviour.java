package cnp.automated.initiator;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.ArtifactId;
import cnp.automated.CNP;
import cnp.automated.CNP.CNPInitiatorObserver;
import cnp.automated.CNP.Initiator;

public abstract class InitiatorBehaviour extends OneShotBehaviour implements
				CNPInitiatorObserver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(InitiatorBehaviour.class);
	protected Initiator initiator;
	public String artifactName;
	public int proposals=0;
	public int maxProposals;
	
	public abstract Behaviour iCommitToAcceptOrRejectIfPropose();

	public abstract Behaviour iSatisfyCommitToAcceptOrReject();
	
	public abstract Behaviour iFulfilledCommitToDoneOrFailure();
	
	
	public InitiatorBehaviour(String artifactName, int maxProposals) {
		this.artifactName = artifactName;
		this.maxProposals = maxProposals;
	}
	
	@Override
	public void action() {
		// qua inizialmente c'era la enact, ma non mi piace piu', anche in relazione
		// al tipaggio. non ha tanto senso che la parte offerta dal protocollo
		// decida come e quando fare la enact
		
		
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		// considering commitments
		if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			// FIRST CASE: accepting a proposal, refusing others
			Commitment c = (Commitment) e.getElementChanged();
			// if the commitment that changed becomes detached
			if (c.getLifeCycleStatus() == LifeCycleState.DETACHED && c.getCreditor().getType() == RoleId.INDIVIDUAL_ROLE) {				try {
				// if I am the debtor and the creditor is participant as group
				RoleId r = 	new RoleId(CNP.PARTICIPANT_ROLE, RoleId.GROUP_ROLE);
				if (c.getDebtor().equals(initiator.getRoleId())
							/*&& c.getCreditor().equals(r)*/
							// and the consequent is "accept OR reject"
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("accept", c.getCreditor()), new Fact(
													"reject", c.getCreditor())))) {
						// must check if it is the last proposal I wait
						proposals++;
						if (proposals >= maxProposals) {
							logger.trace("Maximum number of proposals reached.");
							myAgent.addBehaviour(iSatisfyCommitToAcceptOrReject());
						}
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			else if (c.getLifeCycleStatus() == LifeCycleState.SATISFIED) {
				try {
					if (c.getCreditor().equals(initiator.getRoleId())
							&& c.getDebtor().equals(
									new RoleId(
											CNP.PARTICIPANT_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("done", c.getDebtor()), new Fact(
													"failure", c.getDebtor())))) {
						myAgent.addBehaviour(iFulfilledCommitToDoneOrFailure());
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			} else {
				// altri stati a cui reagire
			}
		} else { // event is a SocialFact assertion

		}

	}
}

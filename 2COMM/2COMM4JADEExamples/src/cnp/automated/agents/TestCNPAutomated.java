package cnp.automated.agents;

import java.util.ArrayList;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import cartago.CartagoService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestCNPAutomated {
	
	private static Logger logger = LogManager.getLogger(TestCNPAutomated.class);
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			CartagoService.createWorkspace("default");
			testAgents();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		final Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "true");
		ContainerController cc = rt.createMainContainer(p);
		
		java.lang.Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		        rt.shutDown();
		    }
		});
		
		AgentController initiator = cc.createNewAgent("Initiator",
				InitiatorAgent.class.getCanonicalName(), null);
		
		ArrayList<AgentController> participants = new ArrayList<AgentController>();
		for (int i=0;i<3;i++) {
			participants.add(cc.createNewAgent("Participant"+i,
				ParticipantAgent.class.getCanonicalName(), null));
		}
		
		for (AgentController part : participants) {
			part.start();
		}
		
		
		initiator.start();
	}	
}

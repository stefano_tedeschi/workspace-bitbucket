package cnp.automated.agents;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import twocomm.core.Role;
import twocomm.core.RoleId;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.typing.BehaviourType;
import cnp.automated.CNP;
import cnp.automated.Proposal;
import cnp.automated.Task;
import cnp.automated.CNP.CNPParticipantObserver;
import cnp.automated.CNP.Participant;
import cnp.automated.participant.ParticipantBehaviour;

public class ParticipantAgent extends Agent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String ARTIFACT_NAME = "CNP-1";
	public Logger logger = LogManager.getLogger(ParticipantAgent.class);

	protected void setup() {
		addBehaviour(new ParticipantBehaviourImpl());
	}

	@BehaviourType(bType = TypeParticipant.class)
	public class ParticipantBehaviourImpl extends ParticipantBehaviour implements
			CNPParticipantObserver {

		private static final long serialVersionUID = 1L;

		public ParticipantBehaviourImpl() {
			super(ARTIFACT_NAME);
			
		}
				 
		@Override
		public void action() {
			super.action();
			ArtifactId art = Role.createArtifact(artifactName,
					CNP.class);
			participant = (Participant) (Role.enact(
					CNP.PARTICIPANT_ROLE, art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));
			participant.startObserving(this);
		}
		
		@Override
		public Behaviour pCommitToDoneOrFailureIfAccept(RoleId initiator) {
			return new PCommitToDoneOrFailureIfAccept(participant, this, initiator);
		}
		
		@Override
		public Behaviour pFulfilledCommitToAcceptOrReject(RoleId initiator) {
			return new PFulfilledCommitToAcceptOrReject(participant, getTask(), initiator);
		}
	
	}
	
	public class PCommitToDoneOrFailureIfAccept extends OneShotBehaviour {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Participant participant;
		Task t;
		Proposal p;
		ParticipantBehaviour bh;
		RoleId initiator;
		
		public PCommitToDoneOrFailureIfAccept(Participant participant, ParticipantBehaviour bh, RoleId initiator) {
			this.participant = participant;		
			this.bh = bh;
			this.initiator = initiator;
		}

		@Override
		public void action() {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
			
			t = participant.getTask();
			bh.setTask(t);
			p = createProposal(t);
			participant.propose(p, initiator);

		}
		
		private Proposal createProposal(Task t) {
			logger.debug("Creating Proposal for task " + t);
			Proposal p = new Proposal("Proposal " + getBehaviourName());
			p.setRoleId(participant.getRoleId());
			return p;
		}


	}

	public class PFulfilledCommitToAcceptOrReject extends OneShotBehaviour {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Participant participant = null;
		Task task;
		RoleId initiator;
		
		public PFulfilledCommitToAcceptOrReject(Participant participant, Task task, RoleId initiator) {
			super();
			this.participant = participant;
			this.task = task;
			this.initiator = initiator;
		}

		@Override
		public void action() {
			try {
				// wait from participants
				//Thread.sleep(2000);
			} catch (Exception e) {
			}
			
			if (elaborateTask(task))
				participant.done(task, initiator);
			else
				participant.failure(initiator);
			
			

		}
		
		private boolean elaborateTask(Task t) {
			logger.debug("Elaborating task " + t);
			int r = (int)(Math.random()*100);
			if (r < 80) {
				t.setResult("Done");
				return true;
			}
			else {
				t.setResult("Failure");
				return false;
			}
			
		}

	}

}

package cnp.automated.agents;

import cnp.automated.CNP;
import twocomm.core.Commitment;
import twocomm.core.RoleId;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.BType;
import twocomm.core.typing.ProtocolAction;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class TypeParticipant extends BType {

	public TypeParticipant() throws MissingOperandException, WrongOperandsNumberException {
		
		super(new Commitment[]{new Commitment(new RoleId(CNP.PARTICIPANT_ROLE, RoleId.GROUP_ROLE),
				new RoleId(CNP.INITIATOR_ROLE, RoleId.GROUP_ROLE),
				new Fact("accept"),
				new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))},
				new ProtocolAction("done"));
	}
}

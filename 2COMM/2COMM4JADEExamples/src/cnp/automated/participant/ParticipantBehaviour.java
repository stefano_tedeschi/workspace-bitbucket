package cnp.automated.participant;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.ArtifactId;
import cnp.automated.CNP;
import cnp.automated.Proposal;
import cnp.automated.Task;
import cnp.automated.CNP.CNPInitiatorObserver;
import cnp.automated.CNP.Participant;

public abstract class ParticipantBehaviour extends OneShotBehaviour implements
				CNPInitiatorObserver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(ParticipantBehaviour.class);
	protected Participant participant;
	public String artifactName;
	public int satisfiedCommitToAcceptOrReject=0;
	protected Task t;
	protected Proposal p = null;
	
	public abstract Behaviour pCommitToDoneOrFailureIfAccept(RoleId initiator);

	public abstract Behaviour pFulfilledCommitToAcceptOrReject(RoleId initiator);
	
	
	public ParticipantBehaviour(String artifactName) {
		this.artifactName = artifactName;
	}
	
	
	public Task getTask() { return t; }
	public void setTask(Task t) { this.t = t; }
	
	public Proposal getProposal() { return p; }
	public void setProposal(Proposal p) { this.p = p; }
	
	@Override
	public void action() {
				
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		// CONSIDERING COMMITMENT CASES
		if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			Commitment c = (Commitment) e.getElementChanged();
			// FIRST CASE: initiator's commitment to accept or reject a proposal C(I,P,PROPOSAL,ACCEPT OR REJECT)
			if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				try {
					if (c.getCreditor().equals(participant.getRoleId())
							&& c.getDebtor().equals(
									new RoleId(
											CNP.INITIATOR_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("accept", participant.getRoleId()), new Fact(
													"reject", participant.getRoleId())))) {
						myAgent.addBehaviour(pCommitToDoneOrFailureIfAccept(c.getDebtor()));
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			// SECOND CASE: initiator accepted my proposal C(P,I,ACCEPT,DONE OR FAILURE)
			else if (c.getLifeCycleStatus() == LifeCycleState.DETACHED) {
				try {
					if (c.getDebtor().equals(participant.getRoleId())
							&& c.getCreditor().equals(
									new RoleId(
											CNP.INITIATOR_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("done", participant.getRoleId()), new Fact(
													"failure", participant.getRoleId())))) {
						myAgent.addBehaviour(pFulfilledCommitToAcceptOrReject(c.getCreditor()));
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			} else {
				// altri stati a cui reagire
			}
		} else { // event is a SocialFact assertion

		}

	}
}

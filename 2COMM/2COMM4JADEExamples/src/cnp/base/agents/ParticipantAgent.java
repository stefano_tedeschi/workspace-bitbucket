package cnp.base.agents;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.typing.BehaviourType;
import cnp.base.CNPArtifact;
import cnp.base.Proposal;
import cnp.base.Task;
import cnp.base.CNPArtifact.CNPParticipantObserver;
import cnp.base.CNPArtifact.Participant;
import cnp.base.participant.ParticipantBehaviour;

public class ParticipantAgent extends Agent {

	public static final String ARTIFACT_NAME = "CNP-1";
	public Logger logger = LogManager.getLogger(ParticipantAgent.class);

	protected void setup() {
		addBehaviour(new ParticipantBehaviourImpl());
	}

	@BehaviourType(bType = TypeParticipant.class)
	public class ParticipantBehaviourImpl extends ParticipantBehaviour implements
			CNPParticipantObserver {

		public ParticipantBehaviourImpl() {
			super(ARTIFACT_NAME);
			// TODO Auto-generated constructor stub
		}
				
		@Override
		public void action() {
			super.action();
		}
		
		@Override
		public Behaviour pCommitToDoneOrFailureIfAccept(RoleId initiator) {
			return new PCommitToDoneOrFailureIfAccept(participant, this, initiator);
		}
		
		@Override
		public Behaviour pFulfilledCommitToAcceptOrReject(RoleId initiator) {
			return new PFulfilledCommitToAcceptOrReject(participant, getTask(), initiator);
		}
	
	}
	
	public class PCommitToDoneOrFailureIfAccept extends OneShotBehaviour {
		Participant participant;
		Task t;
		Proposal p;
		ParticipantBehaviour bh;
		RoleId initiator;
		
		public PCommitToDoneOrFailureIfAccept(Participant participant, ParticipantBehaviour bh, RoleId initiator) {
			this.participant = participant;		
			this.bh = bh;
			this.initiator = initiator;
		}

		@Override
		public void action() {
			try {
				//Thread.sleep(1000);
			} catch (Exception e) {
			}
			
			t = participant.getTask();
			bh.setTask(t);
			p = createProposal(t);
			participant.propose(p, initiator);

		}
		
		private Proposal createProposal(Task t) {
			logger.debug("Creating Proposal for task " + t);
			Proposal p = new Proposal("Proposal " + getBehaviourName());
			p.setRoleId(participant.getRoleId());
			return p;
		}


	}

	public class PFulfilledCommitToAcceptOrReject extends OneShotBehaviour {

		Participant participant = null;
		Task task;
		RoleId initiator;
		
		public PFulfilledCommitToAcceptOrReject(Participant participant, Task task, RoleId initiator) {
			super();
			this.participant = participant;
			this.task = task;
			this.initiator = initiator;
		}

		@Override
		public void action() {
			try {
				// wait from participants
				//Thread.sleep(2000);
			} catch (Exception e) {
			}
			
			if (elaborateTask(task))
				participant.done(task, initiator);
			else
				participant.failure(initiator);
			
			

		}
		
		private boolean elaborateTask(Task t) {
			logger.debug("Elaborating task " + t);
			int r = (int)(Math.random()*100);
			if (r < 80) {
				t.setResult("Done");
				return true;
			}
			else {
				t.setResult("Failure");
				return false;
			}
			
		}

	}

}

package cnp.base.agents;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cartago.ArtifactId;
import twocomm.core.Role;
import twocomm.core.RoleMessage;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.Fact;
import twocomm.core.typing.BehaviourType;
import twocomm.exception.MissingOperandException;
//import cnp.automated.CNP;
import cnp.base.CNPArtifact;
import cnp.base.Proposal;
import cnp.base.Task;
import cnp.base.CNPArtifact.Initiator;
import cnp.base.initiator.InitiatorBehaviour;

public class InitiatorAgent extends Agent {

	public static final String ARTIFACT_NAME = "CNP-1";
	public static final int MAX_PROPOSALS = 3;
	public Logger logger = LogManager.getLogger(InitiatorAgent.class);

	public int countSatisfyCommitToAcceptOrReject = 0;
	
	protected void setup() {
		addBehaviour(new InitiatorBehaviourImpl());
	}

	@BehaviourType(bType = TypeInitiator.class)
	public class InitiatorBehaviourImpl extends InitiatorBehaviour  {

		public InitiatorBehaviourImpl() {
			super(ARTIFACT_NAME, MAX_PROPOSALS);
			// TODO Auto-generated constructor stub
		}

		private static final long serialVersionUID = 1L;
		
		@Override
		public void action() {
			super.action();
			ArtifactId art = Role.createArtifact(artifactName,
					CNPArtifact.class);
			initiator = (Initiator) (Role.enact(CNPArtifact.INITIATOR_ROLE,
					art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));

			myAgent.addBehaviour(this.iCommitToAcceptOrRejectIfPropose());
			
		}

		@Override
		public Behaviour iCommitToAcceptOrRejectIfPropose() {
			return new ICommitToAcceptOrRejectIfPropose(initiator);
		}

		@Override
		public Behaviour iSatisfyCommitToAcceptOrReject() {
			// TODO Auto-generated method stub
			return new ISatisfyCommitToAcceptOrReject(initiator);
		}

		@Override
		public Behaviour iFulfilledCommitToDoneOrFailure() {
			// TODO Auto-generated method stub
			return new IFulfilledCommitToDoneOrFailure(initiator);
		}

		
	}

	public class ICommitToAcceptOrRejectIfPropose extends OneShotBehaviour {
		Initiator initiator = null;
		Task t = new Task("Decisione");

		public ICommitToAcceptOrRejectIfPropose(Initiator initiator) {
			this.initiator = initiator;
		}

		@Override
		public void action() {
			try {
				// wait from participants
				Thread.sleep(1000);
			} catch (Exception e) {
			}
			initiator.cfp(t);

		}

	}

	public class ISatisfyCommitToAcceptOrReject extends OneShotBehaviour {

		Initiator initiator = null;
		ArrayList<Proposal> proposals = new ArrayList<Proposal>();

		public ISatisfyCommitToAcceptOrReject(Initiator initiator) {
			super();
			this.initiator = initiator;
		}

		@Override
		public void action() {
			proposals = initiator.getProposals();

			initiator.accept(proposals.get(0));
			System.out.println("The winner is "+proposals.get(0).getRoleId());
			for (int i = 1; i < proposals.size(); i++) {
				initiator.reject(proposals.get(i));
			}
		}
	}

	public class IFulfilledCommitToDoneOrFailure extends OneShotBehaviour {

		Initiator initiator = null;

		public IFulfilledCommitToDoneOrFailure(Initiator initiator) {
			super();
			this.initiator = initiator;
		}

		@Override
		public void action() {
			logger.debug("Parsing results...");
			try {
				if (initiator.existsFact(new Fact("done")))
					logger.debug("DONE percepted, work done.");
				else if (initiator.existsFact(new Fact("failure")))
					logger.debug("FAILURE percepted, work done.");
			} catch (MissingOperandException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("Error in composing fact to match");
			}

		}

	}

}

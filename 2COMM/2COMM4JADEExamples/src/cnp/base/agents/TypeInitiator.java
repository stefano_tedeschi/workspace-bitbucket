package cnp.base.agents;

import cnp.base.CNPArtifact;
import twocomm.core.Commitment;
import twocomm.core.RoleId;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalExpressionType;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.BType;
import twocomm.core.typing.PartialActualization;
import twocomm.core.typing.ProtocolAction;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class TypeInitiator extends BType {

	public TypeInitiator() throws MissingOperandException, WrongOperandsNumberException {
		
		super(new Commitment[]{new Commitment(new RoleId(CNPArtifact.INITIATOR_ROLE, RoleId.GROUP_ROLE),
				new RoleId(CNPArtifact.PARTICIPANT_ROLE, RoleId.GROUP_ROLE),
				new Fact("propose"),
				new CompositeExpression(LogicalOperatorType.OR, new Fact("accept"), new Fact("reject")))},
				new ProtocolAction("accept"));
	}
}

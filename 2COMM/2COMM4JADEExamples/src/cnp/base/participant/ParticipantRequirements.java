package cnp.base.participant;

import cnp.base.CNPArtifact;
import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleRequirements;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;

public class ParticipantRequirements extends RoleRequirements { 
	
	public ParticipantRequirements() throws MissingOperandException, WrongOperandsNumberException {			
		super("Participant",
				new Commitment[]{
				new Commitment(CNPArtifact.PARTICIPANT_ROLE, CNPArtifact.INITIATOR_ROLE, "accept", new CompositeExpression(
							LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))
				}, null);
		
	}
	
}
package cnp.base;

import java.util.ArrayList;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cnp.base.initiator.InitiatorRequirements;
import cnp.base.participant.ParticipantRequirements;
import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.LifeCycleState;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.SocialEvent;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleType;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.CartagoException;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;

public class CNPArtifactTimer extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(CNPArtifactTimer.class);
	public static String ARTIFACT_TYPE = "CNP.ArtifactTimer";	
	public static String INITIATOR_ROLE = "Initiator";
	public static String PARTICIPANT_ROLE = "Participant";
	
	private long millis;
	private long maxTime = 1000;
	private int numberMaxProposals = 3;
	private int actualProposals = 0;
	private boolean acceptingProposals = true;
	private RoleId initiatorId;
	
	static {
		addEnabledRole(INITIATOR_ROLE, Initiator.class);
		addEnabledRole(PARTICIPANT_ROLE, Participant.class);
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}	
	
	
	// ROLES OPERATIONS
	
	@OPERATION
	public void cfp(Task task, RoleId initiator) {
		
 		RoleMessage cfp = new RoleMessage();
 		RoleId dest = new RoleId(PARTICIPANT_ROLE, RoleId.GROUP_ROLE);
		cfp.setContents(task);
		cfp.setRoleSender(initiator);
		cfp.setRoleReceiver(dest);
		cfp.setPerformative(ACLMessage.CFP);
		send(cfp);
		initiatorId = initiator;
		try {
			// createCommitment comporta la new di un commitment. Non ho garanzie se mi baso sui riferimenti al commitment
			// poiche siamo in un ambiente distribuito. Quindi ci vanno meccanismi di matching sullo stato sociale:
			// la release ad es. viene fatta creando un nuovo commitment, quello che si vuole porre in stato released.
			
			// togliere new commitment e mettere i singoli paramtri di costruzione
			
			createAllCommitments(new Commitment(initiator, dest, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			assertFact(new Fact("cfp", initiator, task));
			logger.trace("OPERATION PERFORMED: CFP by "+initiator);
			execInternalOp("startTiming");
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@INTERNAL_OPERATION void startTiming() {
		await_time(maxTime);
		acceptingProposals = false;
		logger.debug("Not accepting proposals anymore");
		RoleId groupParticipant = new RoleId(PARTICIPANT_ROLE, RoleId.GROUP_ROLE);
		try {
			createCommitment(new Commitment(initiatorId, groupParticipant, new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
		} catch (MissingOperandException | WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void accept(Proposal proposal, RoleId initiator) {
		
		RoleMessage accept = new RoleMessage();
 		RoleId dest = proposal.getRoleId();
 		accept.setRoleSender(initiator);
 		accept.setRoleReceiver(dest);
 		accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
		send(accept);
		
		try {
			assertFact(new Fact("accept"));
			//**** DEBUG: CAMBIO STATO DI VITA A COMMITMENT GIUSTO, SARA' IL SOCIAL STATE CHE DOVRA' GESTIRLO
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(initiator)) {
				if (com.getAntecedent().equals(new Fact("accept")) && com.getLifeCycleStatus().equals(LifeCycleState.CONDITIONAL)) {
					arrComm.add(com);
				}
			}
			Commitment cToAdd;
			for (Commitment com : arrComm) {
				cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), new Fact("true"), com.getConsequent(), LifeCycleState.DETACHED);	
				socialState.removeCommitment(com);
				logger.debug("Commitment da rimuovere: "+com);
				createCommitment(cToAdd);
			}
			logger.trace("OPERATION PERFORMED: ACCEPT by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommitmentNotFoundInInteractionStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void reject(Proposal proposal, RoleId initiator) {
		
		RoleMessage reject = new RoleMessage();
 		RoleId dest = proposal.getRoleId();
 		reject.setRoleSender(initiator);
 		reject.setRoleReceiver(dest);
 		reject.setPerformative(ACLMessage.REJECT_PROPOSAL);
		send(reject);
		
		try {
			releaseCommitment(new Commitment(proposal.getRoleId(), initiator, "accept", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			logger.trace("OPERATION PERFORMED: REJECT by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void propose(Proposal prop, RoleId participant, RoleId initiator) {
		if (!acceptingProposals) {
			logger.debug("No more proposals allowed.");
			return;
		}
		prop.setRoleId(participant);
		RoleMessage proposal = new RoleMessage();
 		proposal.setContents(prop);
 		proposal.setRoleSender(participant);
 		proposal.setRoleReceiver(initiator);
 		proposal.setPerformative(ACLMessage.PROPOSE);
		send(proposal);
		
		try {
			assertFact(new Fact("propose", participant, prop));
			createCommitment(new Commitment(participant, initiator, new Fact("accept"), new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			//**** DEBUG: CAMBIO STATO DI VITA A COMMITMENT GIUSTO, SARA' IL SOCIAL STATE CHE DOVRA' GESTIRLO
			for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(participant)) {
				if (com.getAntecedent().equals(new Fact("propose")) && com.getLifeCycleStatus().equals(LifeCycleState.CONDITIONAL)) {
					Commitment cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), new Fact("true"), com.getConsequent(), LifeCycleState.DETACHED);	
					socialState.removeCommitment(com);
					logger.debug("Commitment da rimuovere: "+com);
					createCommitment(cToAdd);	
					actualProposals++;						
				}
			}
			
//				defineObsProperty("cc", initiator.getCanonicalName(), "GroupParticipant",
//						"true", "(accept OR reject)", "DETACHED");
				
			
			
			logger.trace("OPERATION PERFORMED: PROPOSE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommitmentNotFoundInInteractionStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@OPERATION
	public void refuse(RoleId participant, RoleId initiator) {
		RoleMessage refusal = new RoleMessage();
		refusal.setRoleSender(participant);
		refusal.setRoleReceiver(initiator);
		refusal.setPerformative(ACLMessage.REFUSE);
		send(refusal);
		
		try {
			releaseCommitment(new Commitment(initiator, participant, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			assertFact(new Fact("refuse", participant));
			logger.trace("OPERATION PERFORMED: REFUSE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void done(Task task, RoleId participant, RoleId initiator) {
		RoleMessage done = new RoleMessage();
 		done.setContents(task);
 		done.setRoleSender(participant);
 		done.setRoleReceiver(initiator);
 		done.setPerformative(ACLMessage.INFORM);
		send(done);
		
		try {
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(participant)) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
					arrComm.add(com);
				}
			}
			for (Commitment com :arrComm) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
					Commitment cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), com.getAntecedent(), com.getConsequent(), LifeCycleState.SATISFIED);	
					socialState.removeCommitment(com);
					logger.debug("Commitment da rimuovere: "+com);
					createCommitment(cToAdd);				
				}
			}		
			
			assertFact(new Fact("done"));
			logger.trace("OPERATION PERFORMED: DONE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommitmentNotFoundInInteractionStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	@OPERATION
	public void failure(RoleId participant, RoleId initiator) {
		RoleMessage failure = new RoleMessage();
		failure.setRoleSender(participant);
		failure.setRoleReceiver(initiator);
		failure.setPerformative(ACLMessage.FAILURE);
		send(failure);
		try {
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(participant)) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
					arrComm.add(com);
				}
			}
			for (Commitment com :arrComm) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
					Commitment cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), com.getAntecedent(), com.getConsequent(), LifeCycleState.SATISFIED);	
					socialState.removeCommitment(com);
					logger.debug("Commitment da rimuovere: "+com);
					createCommitment(cToAdd);				
				}
			}	
			assertFact(new Fact("failure"));
			logger.trace("OPERATION PERFORMED: FAILURE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommitmentNotFoundInInteractionStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	// BEGIN INNER CLASSES for ROLES
	// cnp.Initiator role
	
	@RoleType(requirements = InitiatorRequirements.class)
	public class Initiator extends PARole {
		
		public Initiator(IPlayer player) {
			super(INITIATOR_ROLE, player);
		}


		public void cfp(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("cfp", task, getRoleId()));
			} catch (ActionFailedException e) {
				logger.error("Action CFP failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		// accepting to particular role
		public void accept(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("accept", proposal, getRoleId()));
			} catch (ActionFailedException e) {
				logger.error("Action ACCEPT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		
		public void reject(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("reject", proposal, getRoleId()));
			} catch (ActionFailedException e) {
				logger.error("Action REJECT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
	}
	
	// cnp.Participant role
	
	@RoleType(requirements = ParticipantRequirements.class)
	public class Participant extends PARole {
		
		public Participant(IPlayer player) {
			super(PARTICIPANT_ROLE, player);
		}


		public void propose(Proposal proposal, RoleId proposalSender) {
			try {
				doAction(this.getArtifactId(), new Op("propose", proposal, this.getRoleId(), proposalSender));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void refuse(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("refuse", task, this.getRoleId()));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		
		public void done(Task task, RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("done", task, getRoleId(), initiator));
			} catch (ActionFailedException e) {
				logger.error("Action DONE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		
		public void failure(RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("failure", getRoleId(), initiator));
			} catch (ActionFailedException e) {
				logger.error("Action FAILURE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
	}
	
	public interface CNPInitiatorObserver extends ProtocolObserver {
		
	}
	
	public interface CNPParticipantObserver extends ProtocolObserver {
		
	}

}

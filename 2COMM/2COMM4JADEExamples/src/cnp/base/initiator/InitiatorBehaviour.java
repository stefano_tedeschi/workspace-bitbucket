package cnp.base.initiator;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.Role;
import twocomm.core.RoleId;
import twocomm.core.SocialEvent;
import twocomm.core.SocialStateElementType;
import twocomm.core.jade.JadeBehaviourPlayer;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.ArtifactId;
import cnp.base.CNPArtifact;
import cnp.base.CNPArtifact.CNPInitiatorObserver;
import cnp.base.CNPArtifact.Initiator;

public abstract class InitiatorBehaviour extends OneShotBehaviour implements
				CNPInitiatorObserver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Logger logger = LogManager.getLogger(InitiatorBehaviour.class);
	protected Initiator initiator;
	public String artifactName;
	public int proposals=0;
	public int maxProposals;
	
	public abstract Behaviour iCommitToAcceptOrRejectIfPropose();

	public abstract Behaviour iSatisfyCommitToAcceptOrReject();
	
	public abstract Behaviour iFulfilledCommitToDoneOrFailure();
	
	
	public InitiatorBehaviour(String artifactName, int maxProposals) {
		this.artifactName = artifactName;
		this.maxProposals = maxProposals;
	}
	
	@Override
	public void action() {
		ArtifactId art = Role.createArtifact(artifactName,
				CNPArtifact.class);
		initiator = (Initiator) (Role.enact(CNPArtifact.INITIATOR_ROLE,
				art, new JadeBehaviourPlayer(new Behaviour[]{this}, myAgent.getAID())));

		initiator.startObserving(this);
		
		myAgent.addBehaviour(this.iCommitToAcceptOrRejectIfPropose());
	}
	
	@Override
	public void handleEvent(SocialEvent e, Object... args) {
		// considering commitments
		if (e.getElementChanged().getElType() == SocialStateElementType.COMMITMENT) {
			// FIRST CASE: accepting a proposal, refusing others
			Commitment c = (Commitment) e.getElementChanged();
			// if the commitment that changed becames detached
			if (c.getLifeCycleStatus() == LifeCycleState.DETACHED && c.getCreditor().getType() == RoleId.INDIVIDUAL_ROLE) {
				try {
					// if I am the debtor and the creditor is participant as group
					if (c.getDebtor().equals(initiator.getRoleId())
							&& c.getCreditor().equals(
									new RoleId(
											CNPArtifact.PARTICIPANT_ROLE,
											RoleId.GROUP_ROLE))
							// and the consequent is "accept OR reject"
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("accept"), new Fact(
													"reject")))) {
						// must check if it is the last proposal I wait
						proposals++;
						if (proposals >= maxProposals) {
							logger.debug("Maximum number of proposals reached.");
							myAgent.addBehaviour(iSatisfyCommitToAcceptOrReject());
						}
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			else if (c.getLifeCycleStatus() == LifeCycleState.SATISFIED) {
				try {
					if (c.getCreditor().equals(initiator.getRoleId())
							&& c.getDebtor().equals(
									new RoleId(
											CNPArtifact.PARTICIPANT_ROLE,
											RoleId.GROUP_ROLE))
							&& c.getConsequent().equals(
									new CompositeExpression(
											LogicalOperatorType.OR,
											new Fact("done"), new Fact(
													"failure")))) {
						myAgent.addBehaviour(iFulfilledCommitToDoneOrFailure());
					}
				} catch (MissingOperandException
						| WrongOperandsNumberException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			} else {
				// altri stati a cui reagire
			}
		} else { // event is a SocialFact assertion

		}

	}
}

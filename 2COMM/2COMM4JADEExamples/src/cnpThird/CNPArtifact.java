package cnpThird;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.ProtocolArtifact;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;

public class CNPArtifact extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(CNPArtifact.class);
	public static String ARTIFACT_TYPE = "CNP.Artifact";	
	public static String INITIATOR_ROLE = "CNP.Initiator";
	public static String PARTICIPANT_ROLE = "CNP.Participant";
	
	
	static {
		addEnabledRole(INITIATOR_ROLE, Initiator.class);
		addEnabledRole(PARTICIPANT_ROLE, Participant.class);
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}	
	
	
	// ROLES OPERATIONS
	
	@OPERATION
	public void cfp(Task task, RoleId initiator) {
		
 		RoleMessage cfp = new RoleMessage();
 		RoleId dest = new RoleId(PARTICIPANT_ROLE, RoleId.GENERIC_ROLE);
		cfp.setContents(task);
		cfp.setRoleSender(initiator);
		cfp.setRoleReceiver(dest);
		cfp.setPerformative(ACLMessage.CFP);
		send(cfp);
		
		try {
			// createCommitment comporta la new di un commitment. Non ho garanzie se mi baso sui riferimenti al commitment
			// poiche siamo in un ambiente distribuito. Quindi ci vanno meccanismi di matching sullo stato sociale:
			// la release ad es. viene fatta creando un nuovo commitment, quello che si vuole porre in stato released.
			
			// togliere new commitment e mettere i singoli paramtri di costruzione
			
			createCommitment(new Commitment(initiator, dest, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			assertFact(new Fact("cfp", initiator, task));
			logger.trace("OPERATION PERFORMED: CFP by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void accept(Proposal proposal, RoleId initiator) {
		
		RoleMessage accept = new RoleMessage();
 		RoleId dest = proposal.getRoleId();
 		accept.setRoleSender(initiator);
 		accept.setRoleReceiver(dest);
 		accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
		send(accept);
		
		try {
			assertFact(new Fact("accept"));
			logger.trace("OPERATION PERFORMED: ACCEPT by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void reject(Proposal proposal, RoleId initiator) {
		
		RoleMessage reject = new RoleMessage();
 		RoleId dest = proposal.getRoleId();
 		reject.setRoleSender(initiator);
 		reject.setRoleReceiver(dest);
 		reject.setPerformative(ACLMessage.REJECT_PROPOSAL);
		send(reject);
		
		try {
			releaseCommitment(new Commitment(proposal.getRoleId(), initiator, "accept", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			logger.trace("OPERATION PERFORMED: REJECT by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void propose(Proposal prop, RoleId participant, RoleId initiator) {
		
		prop.setRoleId(participant);
		RoleMessage proposal = new RoleMessage();
 		proposal.setContents(prop);
 		proposal.setRoleSender(participant);
 		proposal.setRoleReceiver(initiator);
 		proposal.setPerformative(ACLMessage.PROPOSE);
		send(proposal);
		
		try {
			createCommitment(new Commitment(participant, new RoleId(INITIATOR_ROLE, RoleId.GENERIC_ROLE), "accept", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			assertFact(new Fact("propose", participant, prop));
			logger.trace("OPERATION PERFORMED: PROPOSE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@OPERATION
	public void refuse(String message) {
//		releaseCommitment(new Commitment(d, c, new Fact("propose", "proposal"), new CompositeExpression(
//				LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
	}
	
	@OPERATION
	public void done(Task task, RoleId participant, RoleId initiator) {
		RoleMessage done = new RoleMessage();
 		done.setContents(task);
 		done.setRoleSender(participant);
 		done.setRoleReceiver(initiator);
 		done.setPerformative(ACLMessage.INFORM);
		send(done);
		
		try {
			assertFact(new Fact("done", initiator, task));
			logger.trace("OPERATION PERFORMED: DONE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	@OPERATION
	public void failure(RoleId participant, RoleId initiator) {
		RoleMessage failure = new RoleMessage();
		failure.setRoleSender(participant);
		failure.setRoleReceiver(initiator);
		failure.setPerformative(ACLMessage.FAILURE);
		send(failure);
		try {
			assertFact(new Fact("failure"));
			logger.trace("OPERATION PERFORMED: FAILURE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	// BEGIN INNER CLASSES for ROLES
	// cnp.Initiator role
	
	@RoleDefinition(requirements = InitiatorRequirements.class)
	public class Initiator extends PARole {
		
	
		public Initiator(Behaviour player, AID agent) {
			super(INITIATOR_ROLE, player, agent);
		}


		
		
//		@Requirements(
//				commitments = (Initiator.class, Participant.class, "propose", "accept OR reject")
//				)
		public void cfp(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("cfp", task, getRoleId()));
			} catch (ActionFailedException e) {
				logger.error("Action CFP failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		// accepting to particular role
		public void accept(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("accept", proposal, getRoleId()));
			} catch (ActionFailedException e) {
				logger.error("Action ACCEPT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		
		public void reject(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("reject", proposal, getRoleId()));
			} catch (ActionFailedException e) {
				logger.error("Action REJECT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
	}
	
	// cnp.Participant role
	
	@RoleDefinition(requirements = ParticipantRequirements.class)
	public class Participant extends PARole {
		
//		
		public Participant(Behaviour player, AID agent) {
			super(PARTICIPANT_ROLE, player, agent);
		}


//		@Requirements(
//				commitments = (Initiator.class, Participant.class, "propose", "accept OR reject")
//				)
		public void propose(Proposal proposal, RoleId proposalSender) {
			try {
				doAction(this.getArtifactId(), new Op("propose", proposal, this.getRoleId(), proposalSender));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void refuse(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("refuse", task, this.getRoleId()));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		
		public void done(Task task, RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("done", task, getRoleId(), initiator));
			} catch (ActionFailedException e) {
				logger.error("Action DONE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		
		public void failure(RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("failure", getRoleId(), initiator));
			} catch (ActionFailedException e) {
				logger.error("Action FAILURE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
	}
	
	public interface InitiatorRequirements  extends Requirements { }

	public interface ParticipantRequirements extends Requirements { }


}

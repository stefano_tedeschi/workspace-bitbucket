package cnpThird;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Role;
import twocomm.jade.examples.cnpThird.CNPArtifact.Initiator;
import twocomm.jade.examples.cnpThird.CNPArtifact.InitiatorRequirements;
import cartago.ArtifactId;

public class InitiatorAgent extends Agent {

	public static final String ARTIFACT_NAME = "CNP-1";
	public Logger logger = LogManager.getLogger(InitiatorAgent.class);
	
	protected void setup() {
		addBehaviour(new InitiatorBehaviour());
	}
	
	public class InitiatorBehaviour extends OneShotBehaviour implements InitiatorRequirements {
		
		Initiator initiator = null;
		Task t = new Task("Decisione");
		ArrayList<Proposal> proposals = new ArrayList<Proposal>();
		int maximumProposals = 3;
		boolean maximumReached = false;
	
		
		@Override
		public void action() {
			ArtifactId art = Role.createArtifact(ARTIFACT_NAME, CNPArtifact.class);
			initiator = (Initiator)(Role.enact(CNPArtifact.INITIATOR_ROLE, art, this, myAgent.getAID()));
			
			try {
				// wait from participants
				Thread.sleep(5000);
			}
			catch (Exception e) {}
			initiator.cfp(t);
			
			try {
				// wait for proposals
				Thread.sleep(5000);
			}
			catch (Exception e) {}
			
			logger.debug("Retrieving all proposals...");
			
			ArrayList<CAMessage> proposalsMsg = initiator.receiveAll();
			ArrayList<Proposal> proposals = new ArrayList<Proposal>();
			for (CAMessage msg : proposalsMsg)
				proposals.add((Proposal)(msg.getContents()));
			
			// some criterion for choosing proposals
			initiator.accept(proposals.get(0));
			for (int i=1;i<proposals.size();i++) {
				initiator.reject(proposals.get(i));
			}
			
			CAMessage done = initiator.receive(ACLMessage.INFORM);
			logger.debug("DONE: "+done);
			
		}
	}
	

}

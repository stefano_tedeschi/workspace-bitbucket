package cnpThird;

import it.unito.di.artifact.Role;
import it.unito.di.artifact.CAMessage;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.jade.examples.cnpThird.CNPArtifact.Participant;
import twocomm.jade.examples.cnpThird.CNPArtifact.ParticipantRequirements;
import cartago.ArtifactId;

public class ParticipantAgent extends Agent {

	public static final String ARTIFACT_NAME = "CNP-1";
	public Logger logger = LogManager.getLogger(ParticipantAgent.class);

	protected void setup() {
		addBehaviour(new ParticipantBehaviour());
	}

	public class ParticipantBehaviour extends OneShotBehaviour implements
			ParticipantRequirements {

		Task t = null;
		Proposal p = null;
		Participant participant = null;

		@Override
		public void action() {
			ArtifactId art = Role.createArtifact(ARTIFACT_NAME,
					CNPArtifact.class);
			participant = (Participant) (Role.enact(
					CNPArtifact.PARTICIPANT_ROLE, art, this, myAgent.getAID()));

			//
			// cos� invece ricevo solo il messaggio
			CAMessage cfp = participant.receive(ACLMessage.CFP);
			// analyze taskMessage
			logger.debug("Received task message " + cfp.getContent());
			t = (Task) (cfp.getContents());
			// sending proposal
			participant.propose(createProposal(t), cfp.getRoleSender());

			// receveing answer
			CAMessage accept = participant
					.receive(ACLMessage.ACCEPT_PROPOSAL);
			if (elaborateTask(t))
				participant.done(t, accept.getRoleSender());
			else
				participant.failure(accept.getRoleSender());

		}

		private Proposal createProposal(Task t) {
			logger.debug("Creating Proposal for task " + t);
			return new Proposal("Proposal " + getBehaviourName());
		}

		private boolean elaborateTask(Task t) {
			logger.debug("Elaborating task " + t);
			// performing work
			t.setResult("Done");
			return false;
		}

	}

}

package cnpThird;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.CartagoException;
import cartago.CartagoService;

public class TestCNPProtocolThird {
	
	private static Logger logger = LogManager.getLogger(TestCNPProtocolThird.class);
	
	public static void main(String[] args)  {
		try {
			CartagoService.startNode();
			testAgents();
			
//			testExpressionTree();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
				
	}
	
	private static void testAgents() throws Exception {
		
		final Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "false");
		ContainerController cc = rt.createMainContainer(p);
		
		java.lang.Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		        rt.shutDown();
		    }
		});
		
		AgentController initiator = cc.createNewAgent("Initiator",
				InitiatorAgent.class.getCanonicalName(), null);
		AgentController participant1 = cc.createNewAgent("Participant-1",
				ParticipantAgent.class.getCanonicalName(), null);
		AgentController participant2 = cc.createNewAgent("Participant-2",
				ParticipantAgent.class.getCanonicalName(), null);
		AgentController participant3 = cc.createNewAgent("Participant-3",
				ParticipantAgent.class.getCanonicalName(), null);
		
		
		participant1.start();
		participant2.start();
		participant3.start();
		
		initiator.start();
	}
	
	private static void testExpressionTree() throws MissingOperandException, WrongOperandsNumberException, CartagoException {
		
		CompositeExpression le1 = 
				CompositeExpression.and(new Fact("pippo", new Fact("pippoInside")), 
						CompositeExpression.or(new Fact("ripippo"), 
							CompositeExpression.not(new Fact("ripippo")))
						);
		
		logger.info(le1.toString());		
		logger.error("pippo");
				
	}
	
	
}

{ include("$jacamoJar/templates/common-cartago.asl") }

!participate.

/* Plans */

+!participate
	<- 	.wait(1000); // serve per evitare l'errore di concurrentmodification cartago
		lookupArtifact("C1",ArtId);
		focus(ArtId);
		enact("creditorC1").

+enacted(_,"creditorC1",My_Role_Id)[artifact_id(Id)]
	<-	+enactment_id("C1",Id,My_Role_Id); // serve perche' enact e' una signal e va aggiunta come mental note
		println("Role creditorC1 successfully enacted!");
		makeArtifact("C2","art.C2",[],ArtId);
	    focus(ArtId);
	    enact("debtorC2")[artifact_id(ArtId)];
		.

+enacted(_,"debtorC2",My_Role_Id)[artifact_id(Id)]
	<- +enactment_id("C2",Id,My_Role_Id);
	   println("Role debtorC2 successfully enacted!").

+cc(Initiator_Role_Id, My_Role_Id, "propose", "(accept OR reject)","CONDITIONAL")
	<-	!proposeORrefuse.

+cc(My_Role_Id, Initiator_Role_Id, "accept", "(done OR failure)", "DETACHED")
	<-	.print("******* I AM THE WINNER *********");
	    !doneORfailure.   

+!proposeORrefuse
	<-	!compute_result(Result);
		if (Result == "ok") {
			createC2;
			propose;
		}
		else {
			releaseC1;
		}.
	
+!doneORfailure
	<-	!compute_result(Result);
		.print("The result is: ",Result);
		if (Result == "ko") {
			failure;
		}
		else {
			done;
		}.

+!compute_result(Result)
	<-	.random(Succ);
		if (Succ < 0.5) {
			.concat("ko",Result);
		}
		else {
			.concat("ok",Result);
		}.
		

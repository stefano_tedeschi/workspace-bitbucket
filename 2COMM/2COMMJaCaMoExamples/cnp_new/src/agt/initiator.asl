{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

+!start
	<- makeArtifact("C1","art.C1",[],ArtId);
	   focus(ArtId);
	   enact("debtorC1")[artifact_id(ArtId)].

+enacted(_,"debtorC1",Role_Id)[artifact_id(Id)]
	<-	+enactment_id("C1",Id,Role_Id);
		println("Role debtorC1 successfully enacted!");
		.wait(3000);
    	lookupArtifact("C2",ArtId);
		focus(ArtId);
		enact("creditorC2")[artifact_id(ArtId)];
		.

+enacted(_,"creditorC2",Role_Id)[artifact_id(Id)]
	 : enactment_id("C1",_,C1_Role_Id)
	<- +enactment_id("C2",Id,Role_Id);
	   println("Role creditorC2 successfully enacted!");
	   createC1.

+cc(My_Role_Id, _, "propose", "(accept OR reject)", "DETACHED") 
	 : enactment_id("C1",_,My_Role_Id)
	<- !acceptORreject.

+!acceptORreject
	<-	!compute_result(Result);
		.print("The result is: ",Result);
		if (Result == "accept") {
			accept;
		}
		else {
			releaseC2;
		}.

+!compute_result(Result)
	<-	.random(Succ);
		if (Succ < 0.5) {
			.concat("accept",Result);
		}
		else {
			.concat("reject",Result);
		}.
		
	
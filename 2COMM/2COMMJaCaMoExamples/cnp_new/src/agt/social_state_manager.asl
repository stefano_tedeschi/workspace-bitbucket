{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

+!start
	<- .wait(2000);
	   lookupArtifact("C1",C1);
	   focus(C1);
	   +art_id("C1",C1);
	   lookupArtifact("C2",C2);
	   focus(C2);
	   +art_id("C2",C2).


+propose(AgentName)
	<- assertPropose.

+refuse(AgentName)
	<- releaseC1(AgentName).
	   
+accept(AgentName)
	 : art_id("C1",C1) & art_id("C2",C2)
	<- assertAccept[artifact_id(C1)];
	   assertAccept[artifact_id(C2)].

+reject(AgentName)
	<- releaseC2(AgentName);
	   assertReject.

+done(AgentName)
	<- assertDone.
	
+failure(AgentName)
	<- assertFailure.
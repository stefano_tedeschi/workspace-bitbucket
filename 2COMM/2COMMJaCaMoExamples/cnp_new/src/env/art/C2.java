package art;


import jade.lang.acl.ACLMessage;

import java.util.Arrays;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import twocomm.core.automated.AutomatedSocialState;
import cartago.AgentId;
import cartago.OPERATION;

public class C2 extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(C2.class);
	public static String ARTIFACT_TYPE = "C2";
	public static String DEBTOR_ROLE = "debtorC2";
	public static String CREDITOR_ROLE = "creditorC2";

	private RoleId debtor;
	private RoleId creditor;
	
	static {
		addEnabledRole(DEBTOR_ROLE, DebtorC2.class);
		addEnabledRole(CREDITOR_ROLE, CreditorC2.class);
	}

	public C2() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);
		
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	@Override
	protected void enact(String roleName) {
		super.enact(roleName);
		if(roleName.equals("debtorC2")) {
			debtor = getEnactedRolesIds().get(getEnactedRolesIds().size()-1);
		}
		else if(roleName.equals("creditorC2")) {
			creditor = getEnactedRolesIds().get(getEnactedRolesIds().size()-1);
		}
	}

	// ROLES OPERATIONS

	@OPERATION
	public void createC2() {
		RoleId agent = getRoleIdByPlayerName(getOpUserName());
		if(!agent.equals(debtor)) {
			failed("Only the debtor can create a commitment");
		}
 		try {
			createCommitment(new Commitment(debtor, creditor, "accept", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			logger.trace("OPERATION PERFORMED: createC2 by "+debtor.toString());
		} catch (MissingOperandException e) {
			log(Arrays.toString(e.getStackTrace()));
		} catch (WrongOperandsNumberException e) {
			log(Arrays.toString(e.getStackTrace()));
		}
	}
	
	@OPERATION
	public void releaseC2() {
		RoleId agent = getRoleIdByPlayerName(getOpUserName());
		if (!agent.equals(creditor)) {
			failed("Only the creditor can release a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		releaseCommitment(c);
		logger.trace("OPERATION PERFORMED: releaseC2 by " + agent.toString());
	}
	
	@OPERATION
	public void assertAccept() {
		try {
			assertFact(new Fact("accept"));
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void assertDone() {
		try {
			assertFact(new Fact("done"));
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void assertFailure() {
		try {
			assertFact(new Fact("failure"));
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}

	}
	
	// BEGIN INNER CLASSES for ROLES
		// cnp.Initiator role

		public class DebtorC2 extends PARole {

			public DebtorC2(String playerName, IPlayer player) {
				super(DEBTOR_ROLE, player);
			}
		}

		// cnp.Participant role

		public class CreditorC2 extends PARole {

			public CreditorC2(String playerName, IPlayer player) {
				super(CREDITOR_ROLE, player);
			}



		}

		public interface CNPInitiatorObserver extends ProtocolObserver {

		}

		public interface CNPParticipantObserver extends ProtocolObserver {

		}

}

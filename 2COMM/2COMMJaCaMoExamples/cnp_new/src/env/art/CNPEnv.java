package art;

import cartago.Artifact;
import cartago.OPERATION;

public class CNPEnv extends Artifact {
	
	
	
	@OPERATION
	public void cfp() {
		defineObsProperty("cfp", getCurrentOpAgentId().getAgentName());
	}
	
	@OPERATION
	public void propose() {
		defineObsProperty("propose", getCurrentOpAgentId().getAgentName());
	}
	
	@OPERATION
	public void refuse() {
		defineObsProperty("refuse", getCurrentOpAgentId().getAgentName());
	}
	
	@OPERATION
	public void accept() {
		defineObsProperty("accept", getCurrentOpAgentId().getAgentName());
	}
	
	@OPERATION
	public void reject() {
		defineObsProperty("reject", getCurrentOpAgentId().getAgentName());
	}
	
	@OPERATION
	public void done() {
		defineObsProperty("done", getCurrentOpAgentId().getAgentName());
	}
	
	@OPERATION
	public void failure() {
		defineObsProperty("failure", getCurrentOpAgentId().getAgentName());
	}
	
}

package item;


import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.BusinessArtifact;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.AgentId;
import cartago.OPERATION;
import cartago.LINK;


public class NetbillNormativeState extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(NetbillNormativeState.class);
	public static String ARTIFACT_TYPE = "NetbillNormativeState";
	public static String GENERIC_ROLE = "generic";

	static {
		addEnabledRole(GENERIC_ROLE, Generic.class);
	}

	public NetbillNormativeState() {
		super();
		socialState = new AutomatedSocialState(this);
	}
	
	@OPERATION
	public void init(String itemName, int maxQuantity)  {

    }

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	// ROLES OPERATIONS

	// merchant.quote 
	@LINK
	public void quote(RoleId merchant, RoleId customerId, int price, int quantity) {

		try {
			Commitment c = new Commitment(merchant, customerId, 
					new Fact("acceptedQuotation", price, quantity, customerId), new Fact("goods",customerId));			
			createCommitment(c);
			if (this.socialState.existsFact(new Fact("goods",customerId)))  {
				satisfyCommitment(c);
			}
			createCommitment(new Commitment(merchant, customerId, new Fact("paid",customerId), new Fact("receipt", customerId)));
			assertFact(new Fact("quotation", price,quantity,customerId));
			logger.trace("OPERATION PERFORMED: QUOTE ("+price+","+quantity+") by " + merchant.getPlayerName());
		} catch (MissingOperandException e) {
			log(Arrays.toString(e.getStackTrace()));
		}
	}

	// merchant.ship
	@LINK
	public void ship(String customerString, int quantity) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {
			assertFact(new Fact("goods",customerString));
			logger.trace("OPERATION PERFORMED: SHIP by " + getOpUserName());
			
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// merchant.emitReceipt
	@LINK
	public void emitReceipt(String customerString) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {			
			
			assertFact(new Fact("receipt", customer.toString()));
			logger.trace("OPERATION PERFORMED: RECEIPT by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	// customer.request
	@LINK
	public void request(RoleId customer, RoleId merchant, int quantity) {
		try {			
			assertFact(new Fact("requestedQuote", quantity, customer.toString()));
			logger.trace("OPERATION PERFORMED: REQUESTED_QUOTE OF " + quantity + " by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// customer.accept
	@LINK
	public void accept(int price, int quantity, String merchantRole) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(merchantRole).get(0);
		try {
			assertFact(new Fact("acceptedQuotation", price, quantity, customer.toString()));
			createCommitment(new Commitment(customer, merchant, new Fact("goods",customer.toString()), new Fact("paid",customer.toString())));			
			
			logger.trace("OPERATION PERFORMED: ACCEPTED_QUOTATION of " + price + " by " + customer);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.reject
	@LINK
	public void reject(String price, String merchantRole) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(merchantRole).get(0);
		try {
			assertFact(new Fact("rejectedQuotation", price, customer.toString()));
			createCommitment(new Commitment(customer, merchant, "goods", "paid"));
			logger.trace("OPERATION PERFORMED: REJECT by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.sendEPO
	@LINK
	public void sendEPO(int creditCardNumber, String merchantRole) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(merchantRole).get(0);
		try {			
			
			assertFact(new Fact("paid", customer.toString()));
			assertFact(new Fact("EPO", creditCardNumber, customer.toString()));
		
			logger.trace("OPERATION PERFORMED: EPO by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	// INNER CLASSES for ROLES
	// generic role
	public class Generic extends PARole {

		public Generic(String playerName, IPlayer player) {
			super(GENERIC_ROLE, player);
		}
	}
}
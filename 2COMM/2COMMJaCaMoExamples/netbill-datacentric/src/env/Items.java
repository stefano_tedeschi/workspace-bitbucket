package item;


import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.BusinessArtifact;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.*;

public class Items extends BusinessArtifact {

	protected Logger logger = LogManager.getLogger(Items.class);
	public static String ARTIFACT_TYPE = "Items";
	public static String MERCHANT_ROLE = "merchant";
	public static String CUSTOMER_ROLE = "customer";
	
	private ArtifactId normativeArtifact = null;

	static {
		addEnabledRole(MERCHANT_ROLE, Merchant.class);
		addEnabledRole(CUSTOMER_ROLE, Customer.class);
	}

	public Items() {
		super();
		socialState = new AutomatedSocialState(this);
	}
	
	@INTERNAL_OPERATION
	void createNormativeArtifact() {
		
		if (normativeArtifact == null) {
			//Create normativeArtifact artifact
			try {
				normativeArtifact = makeArtifact("NetbillNormativeState", 
						"item.NetbillNormativeState", ArtifactConfig.DEFAULT_CONFIG);
				defineObsProperty("netBillNormativeStateId",      normativeArtifact.getName());
			} catch (OperationException e) {
				failed("There was problem to create a timeout artifact");				
			}
		}
	}
	
	@OPERATION
	public void init(String itemName, int maxQuantity)  {
        // observable properties   
		
        defineObsProperty("itemName",      itemName);
        defineObsProperty("maxQuantity",   maxQuantity);           
    }

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	// ROLES OPERATIONS

	// merchant.quote 
	@OPERATION
	public void quote(int price, int quantity, String customer) {
		
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customerId = getRoleIdByRoleName(customer);
		
				
		try {			
			assertFact(new Fact("quotation", price,quantity,customer));
			execLinkedOp(normativeArtifact, "quote", merchant, customerId, price, quantity);
			logger.trace("FACT ASSERTED: QUOTATION ("+price+","+quantity+") by " + merchant.getPlayerName());
		} catch (MissingOperandException|OperationException e) {
			log(Arrays.toString(e.getStackTrace()));
		}
	}

	// merchant.ship
	@OPERATION
	public void ship(String customerString, int quantity) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {
			assertFact(new Fact("goods",customerString));
			logger.trace("OPERATION PERFORMED: SHIP by " + getOpUserName());
			
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// merchant.emitReceipt
	@OPERATION
	public void emitReceipt(String customerString) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {			
			
			assertFact(new Fact("receipt", customer.toString()));
			logger.trace("OPERATION PERFORMED: RECEIPT by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	// customer.request
	@OPERATION
	public void request(int quantity) {
		createNormativeArtifact();
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {			
			assertFact(new Fact("requestedQuote", quantity, customer.toString()));
			System.out.println(normativeArtifact);
			execLinkedOp(normativeArtifact, "request", customer, merchant, quantity);
			logger.trace("OPERATION PERFORMED: REQUESTED_QUOTE OF " + quantity + " by " + getOpUserName());
		} catch (MissingOperandException|OperationException e) {
			e.printStackTrace();
		}
	}

	// customer.accept
	@OPERATION
	public void accept(int price, int quantity) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			assertFact(new Fact("acceptedQuotation", price, quantity, customer.toString()));		
			logger.trace("OPERATION PERFORMED: ACCEPTED_QUOTATION of " + price + " by " + customer);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.reject
	@OPERATION
	public void reject(String price) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			assertFact(new Fact("rejectedQuotation", price, customer.toString()));
			logger.trace("OPERATION PERFORMED: REJECT by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.sendEPO
	@OPERATION
	public void sendEPO(int creditCardNumber) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {			
			
			assertFact(new Fact("paid", customer.toString()));
			assertFact(new Fact("EPO", creditCardNumber, customer.toString()));
		
			logger.trace("OPERATION PERFORMED: EPO by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// INNER CLASSES for ROLES
	// merchant role
	public class Merchant extends PARole {

		public Merchant(String playerName, IPlayer player) {
			super(MERCHANT_ROLE, player);
		}
	}

	// customer role
	public class Customer extends PARole {
		public Customer(String playerName, IPlayer player) {
			super(CUSTOMER_ROLE, player);
		}
	}

	public interface MerchantObserver extends ProtocolObserver {	}

	public interface CustomerObserver extends ProtocolObserver {	}

}
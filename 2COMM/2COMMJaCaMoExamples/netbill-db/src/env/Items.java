package item;


import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Statement;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.Role;
import twocomm.core.BusinessArtifact;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.AgentId;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Items extends BusinessArtifact {

	protected Logger logger = LogManager.getLogger(Items.class);
	public static String ARTIFACT_TYPE = "Items";
	public static String MERCHANT_ROLE = "merchant";
	public static String CUSTOMER_ROLE = "customer";

	private Connection conn;
	private Statement stmt;
	private ResultSet rs;
	
	private String itemName;
	
	static {
		addEnabledRole(MERCHANT_ROLE, Merchant.class);
		addEnabledRole(CUSTOMER_ROLE, Customer.class);
	}

	public Items() {
		super();
		socialState = new AutomatedSocialState(this);
	}			
	
	@OPERATION
	public void init(String itemName, int maxQuantity)  {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
       
        try {
            conn =
               DriverManager.getConnection("jdbc:mysql://localhost/items?" +
                                           "user=twocomm&password=fredkapu");
            stmt = conn.createStatement();
            String statement = "INSERT INTO item (Name, Description, Merchant, MaxQuantity) "+
            		"VALUES (\""+itemName+"\", \"prova descrizione\", \"merchantX\","+maxQuantity+")";
             if(stmt.execute(statement))
            	rs = stmt.getResultSet();
            // observable properties   
            defineObsProperty("itemName",      itemName);
            defineObsProperty("maxQuantity",   maxQuantity);  
            this.itemName = itemName;
          
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
           
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) { } // ignore

                rs = null;
            }      
        }        
    }

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	// ROLES OPERATIONS
	
	// merchant.setMerchant
	@OPERATION
	public void setMerchant(String roleId) {
		String statement = "UPDATE item SET Merchant=\" "+roleId+"\""+
        		" WHERE Name = \""+getObsProperty("itemName").getValue()+"\"";
		System.out.println(statement);
         try {
			stmt.execute(statement);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// merchant.quote 
	@OPERATION
	public void quote(int price, int quantity, String customer) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customerId = getRoleIdByRoleName(customer);
		
				
		try {
			Commitment c = new Commitment(merchant, customerId, 
					new Fact("acceptedQuotation", price, quantity, customer), new Fact("goods",customer));			
			createCommitment(c);
			if (this.socialState.existsFact(new Fact("goods",customer)))  {
				satisfyCommitment(c);
			}
			createCommitment(new Commitment(merchant, customerId, new Fact("paid",customer), new Fact("receipt", customer)));
			assertFact(new Fact("quotation", price,quantity,customer));
			logger.trace("OPERATION PERFORMED: QUOTE ("+price+","+quantity+") by " + merchant.getPlayerName());
		} catch (MissingOperandException e) {
			log(Arrays.toString(e.getStackTrace()));
		}
		
		String statement = "UPDATE quotation SET Price="+price+", Status=\"Quoted\""+
        		" WHERE ItemName = \""+itemName+"\"";
		System.out.println(statement);
         try {
			stmt.execute(statement);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// merchant.ship
	@OPERATION
	public void ship(String customerString, int quantity) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {
			assertFact(new Fact("goods",customerString));
			logger.trace("OPERATION PERFORMED: SHIP by " + getOpUserName());
			
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// merchant.emitReceipt
	@OPERATION
	public void emitReceipt(String customerString) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByRoleName(customerString);
		try {			
			
			assertFact(new Fact("receipt", customer.toString()));
			logger.trace("OPERATION PERFORMED: RECEIPT by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	// customer.request
	@OPERATION
	public void request(int quantity) {
		if (quantity < Integer.parseInt(getObsProperty("maxQuantity").getValue()+"")) {
			RoleId customer = getRoleIdByPlayerName(getOpUserName());
			RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
			
			try {			
				assertFact(new Fact("requestedQuote", quantity, customer.toString()));
				logger.trace("OPERATION PERFORMED: REQUESTED_QUOTE OF " + quantity + " by " + getOpUserName());
			} catch (MissingOperandException e) {
				e.printStackTrace();
			}
			
			
	         
	        String statement = "INSERT INTO quotation (ItemName, Status, Quantity, Customer) values "
	        		+"(\""+itemName+"\",\"Offered\","+quantity+", \""+customer+"\")";
	        System.out.println(statement);
	         try {
				stmt.execute(statement);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// customer.accept
	@OPERATION
	public void accept(int price, int quantity) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			assertFact(new Fact("acceptedQuotation", price, quantity, customer.toString()));
			createCommitment(new Commitment(customer, merchant, new Fact("goods",customer.toString()), new Fact("paid",customer.toString())));			
			
			logger.trace("OPERATION PERFORMED: ACCEPTED_QUOTATION of " + price + " by " + customer);
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.reject
	@OPERATION
	public void reject(String price) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {
			assertFact(new Fact("rejectedQuotation", price, customer.toString()));
			createCommitment(new Commitment(customer, merchant, "goods", "paid"));
			logger.trace("OPERATION PERFORMED: REJECT by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.sendEPO
	@OPERATION
	public void sendEPO(int creditCardNumber) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {			
			
			assertFact(new Fact("paid", customer.toString()));
			assertFact(new Fact("EPO", creditCardNumber, customer.toString()));
		
			logger.trace("OPERATION PERFORMED: EPO by " + getOpUserName());
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// INNER CLASSES for ROLES
	// merchant role
	public class Merchant extends PARole {

		public Merchant(String playerName, IPlayer player) {
			super(MERCHANT_ROLE, player);
		}
	}

	// customer role
	public class Customer extends PARole {
		public Customer(String playerName, IPlayer player) {
			super(CUSTOMER_ROLE, player);
		}
	}

	public interface MerchantObserver extends ProtocolObserver {	}

	public interface CustomerObserver extends ProtocolObserver {	}

}
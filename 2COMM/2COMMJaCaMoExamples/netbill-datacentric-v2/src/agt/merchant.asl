{ include("$jacamoJar/templates/common-cartago.asl") }

+focused(_,purchase,PID)
	<- 	enact("merchant")[artifact_id(PID)].
      
+netBillNormativeStateId(Id)
	<- focus(Id).
		
+enacted(Id,"merchant",Role_Id)
	<-	+enactment_id(Role_Id).
		
+requestedQuote(Quantity, Customer_Id)[artifact_id(NSID)]
     : netBillNormativeStateId(NSID) &
       purchaseId(PID)
	<- .print("Providing quotation to ",Customer_Id,"...");
	   quote(1000, Quantity, Customer_Id)[artifact_id(PID)].

+cc(My_Role_Id, Customer_Role_Id, AcceptedQuotation, Goods,"DETACHED")[artifact_id(NSID)]
	 : enactment_id(My_Role_Id) &
	   netBillNormativeStateId(NSID) &
       purchaseId(PID)
	   & jia.getAcceptedQuotationComponentsPrice(AcceptedQuotation,Price)
	   & jia.getAcceptedQuotationComponentsQuantity(AcceptedQuotation,Quantity)
	   & jia.getAcceptedQuotationComponentsCustomer(AcceptedQuotation,Customer_Role_Id)
	<- .print("Shipping goods...");
	   ship(Customer_Role_Id)[artifact_id(PID)].

+cc(My_Role_Id, Customer_Role_Id, Paid, Receipt,"DETACHED")[artifact_id(NSID)]
	:  enactment_id(My_Role_Id) &
	   netBillNormativeStateId(NSID) &
       purchaseId(PID)
		& .term2string(Term1,Paid)
		& Term1 = paid(CustomerString)
		& .term2string(CustomerString,S)
		& S == Customer_Role_Id
	<- 	.print("Emitting receipt...");
	    emitReceipt(Customer_Role_Id)[artifact_id(PID)].

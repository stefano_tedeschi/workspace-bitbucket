{ include("$jacamoJar/templates/common-cartago.asl") }

+focused(_,purchase,PID)
	<- 	enact("customer")[artifact_id(PID)].

+netBillNormativeStateId(Id)
	<- focus(Id).
	
+enacted(Id,"customer",My_Role_Id)
	<-	+enactment_id(My_Role_Id);
		!buy(10).
	
+!buy(Quantity)
	 : itemName(Name) &
	   purchaseId(PID)
	<- .print("Requesting quotation for ",Quantity," units of ",Name,"...");
	   request(Quantity)[artifact_id(PID)].

+cc(Merchant_Role_Id, My_Role_Id, AcceptedQuotation, Goods,"CONDITIONAL")[artifact_id(NSID)]
	:  	enactment_id(My_Role_Id) &
	    netBillNormativeStateId(NSID) &
        purchaseId(PID)
		& jia.getAcceptedQuotationComponentsPrice(AcceptedQuotation,Price)
		& jia.getAcceptedQuotationComponentsQuantity(AcceptedQuotation,Quantity)
		& jia.getAcceptedQuotationComponentsCustomer(AcceptedQuotation,My_Role_Id)
	<-  // --- Code for evaluating quotation
		.print("Accepting quotation...")
		accept(Price,Quantity)[artifact_id(PID)].
//	
+cc(My_Role_Id, Merchant_Role_Id, _, Paid,"DETACHED")[artifact_id(NSID)]
	:  enactment_id(My_Role_Id) &
	   netBillNormativeStateId(NSID) &
       purchaseId(PID)        
		& .term2string(Term1,Paid)
		& Term1 = paid(My_Role)
	<- 	.print("Paying goods...");
	    sendEPO(53530331)[artifact_id(PID)].

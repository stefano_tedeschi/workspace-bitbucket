package jia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
import jason.environment.grid.Location;
import jason.asSyntax.Atom;
import jason.asSyntax.Structure;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.StringTermImpl;



public class getAcceptedQuotationComponentsCustomer extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] terms) throws Exception {
        try {
        	// acceptedQuotation(1000,10,customer2)
            String t = terms[0]+""; // pick(12,12) AND drop(12,12)
            t = t.substring(19, t.indexOf(")",4));
            String[] components = t.split(",");
//            System.out.println(components[0]+components[1]+components[2]);
            return un.unifies(terms[1], new StringTermImpl(components[2]));
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }
}

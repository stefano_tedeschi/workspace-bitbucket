package mining;


import jason.stdlib.println;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.Commitment;
import twocomm.core.SocialStateElementType;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;
import cartago.OPERATION;


public class MiningProtocol extends ProtocolArtifact {

	public static String ARTIFACT_TYPE = "MiningProtocol";
	public static String MINER_ROLE = "miner";
	
	
	private ArrayList<Gold> golds = new ArrayList<Gold>();
	private int numberOfMiners = 4;
	private HashMap<Gold,Integer> bids = new HashMap<Gold,Integer>();
	
	static {
		addEnabledRole(MINER_ROLE, Miner.class);
	}

	public MiningProtocol() {
		super();
	}

	@Override
	public String getArtifactType() {
		// TODO Auto-generated method stub
		return ARTIFACT_TYPE;
	}
	
	@OPERATION
    public void init() {
        super.init();
    }

	
	
	
	@OPERATION
	public void stampa(String test) {
		System.out.println(test);
			
	}
	
	
	@OPERATION
	public void communicateGoldPosition(int x, int y) {
		logger.debug("Comunicazione gold "+x+" "+y);
		Gold g = new Gold(x,y);
		boolean toAdd = true;
		for (Gold g1 : golds)
			if (g1.equals(g)) {
				toAdd = false;
				break;
			}
		if (toAdd) {
			golds.add(g);
			logger.debug("Golds: ");
			for (Gold g1 : golds) {
				logger.debug(g1);
			}
			defineObsProperty("gold", x, y);
		}
	}
	
	
	@OPERATION
	public void commitToDropGold(int x, int y) {
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());
		RoleId groupMiner = new RoleId(MINER_ROLE);
		Gold g = new Gold(x,y);
		
		
		Fact ant, cons; boolean first = false;
		try {			
			for (Gold actualG : golds) {
				if (actualG.equals(g) && !actualG.isBooked()) {
					actualG.book();
					first = true;
					System.out.println("Gold booked, "+actualG.x+" "+actualG.y);
				}
			}
			
			
			ant = new Fact("gold_assigned", x, y, askingMiner.toString());
			cons = new Fact("drop", x, y);
			Commitment groupCommit = new Commitment(askingMiner, groupMiner, ant, cons);
			createCommitment(groupCommit);
			golds.add(g);
			
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@OPERATION
	public void commitToDropGoldImmediately(int x, int y) {
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());
		RoleId groupMiner = new RoleId(MINER_ROLE);
		Gold g = new Gold(x,y);
		
		
		Fact ant, cons; boolean first = false;
		try {			
			for (Gold actualG : golds) {
				if (actualG.equals(g) && !actualG.isBooked()) {
					actualG.book();
					first = true;
					System.out.println("Gold booked, "+actualG.x+" "+actualG.y);
				}
			}
			
			golds.add(g);
			cons = new Fact("drop", x, y);
			ant = new Fact("true");
			Commitment groupCommit = new Commitment(askingMiner, groupMiner, ant, cons);
			groupCommit.setStatus(LifeCycleState.DETACHED);
			// rilascio eventuali commitment di altri
			for (Commitment c : socialState.retrieveCommitmentsByConsequent(cons)) {
				if (!c.getDebtor().equals(askingMiner)) {
					System.out.println("COMM DA RILASCIARE: "+c);
					releaseCommitment(c);
				}
			}
			
			// e aggiungo il mio che vado subito a perseguire
			createCommitment(groupCommit);
			
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	@OPERATION
	public void changeGoldToPursue(int x, int y) { // x e y sono le coordinate del NUOVO gold
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());
		RoleId groupMiner = new RoleId(MINER_ROLE);
		Gold g = new Gold(x,y);
		Fact ant = null, cons = null;
		
		try {
			cons = new Fact("drop", x, y);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Commitment c : socialState.retrieveCommitmentsByDebtorRoleId(askingMiner)) {
			if (c.getLifeCycleStatus()==LifeCycleState.DETACHED && !c.getConsequent().equals(cons)) {
				logger.debug("COMM DA CANCELLARE: "+c);
				cancelCommitment(c);
			}
		}
		try {
			ant = new Fact("true");
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Commitment groupCommit = new Commitment(askingMiner, groupMiner, ant, cons);
		groupCommit.setStatus(LifeCycleState.DETACHED);
		// e aggiungo il mio che vado subito a conseguire
		createCommitment(groupCommit);
	}
	
	
	@OPERATION
	public void announcePick(int x, int y) {
		try {
			Fact f = new Fact("pick",x,y);
			assertFact(f);
			if (hasObsPropertyByTemplate("gold", x, y))
				removeObsPropertyByTemplate("gold", x, y);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@OPERATION
	public void announceDrop(int x, int y) {
		Gold g = new Gold(x, y);
		Fact f = null;
		try {
			f = new Fact("drop", x, y);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());
		// recupero commitment che hanno richiedente come debitore
		List<Commitment> comms = socialState.retrieveCommitmentsByDebtorRoleId(askingMiner);
		System.out.println("AskingMiner: "+askingMiner+", number of commitments as debtor: "+comms.size());
		// quel commitment che e' detached avra' il conseguente relativo all'oro che si sta per rilasciare
		for (Commitment comm : comms) {
			if (comm.getLifeCycleStatus() == LifeCycleState.DETACHED) {
				f = (Fact)(comm.getConsequent());
				satisfyCommitment(comm);
				System.out.println("Commitment satisfied: "+comm);
			}
				
		}
		assertFact(f);
		// faccio la satisfy di tutti gli altri commitments per quell'oro
		for (Commitment c : socialState.retrieveCommitmentsByConsequent(f)) {
			if (c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				satisfyCommitment(c);
				System.out.println("Commitment satisfied: "+c);
			}
		}		
		golds.remove(g);
		
	}
	
	// operazione che fa la bid con la distanza e crea commitment a: se mi si assegna il gold, allora mi impegno a portarlo
	// al depot
	@OPERATION
	public void bid(int x, int y, int dist) {
		try {
			Fact ant = null, cons = null;
			RoleId biddingMiner = getRoleIdByPlayerName(getOpUserName());
			RoleId groupMiner = new RoleId(MINER_ROLE);
			// recupero l'oro per cui si fa la bid
			Gold goldToBid = new Gold(x,y);
			for (Gold g1 : golds) {
				if (g1.equals(goldToBid)) {
					goldToBid = g1;
					break;
				}
					
			}
			goldToBid.bid(biddingMiner, dist);
			
			// creo il commitment della bid
			cons = new Fact("drop", x, y);
			ant = new Fact("gold_assigned", x, y, biddingMiner.toString());
			Commitment groupCommit = new Commitment(biddingMiner, groupMiner, ant, cons);
			createCommitment(groupCommit);
			
			//controllo se son arrivate tutte le bid
			if (goldToBid.getNumberOfBidders() == numberOfMiners && goldToBid.getWinningBidder()!=null) {
				ant = new Fact("gold_assigned", x, y, goldToBid.getWinningBidder().toString());
				assertFact(ant);
				for (Commitment comm : socialState.retrieveCommitmentsByAntecedent(ant)) {
					if (comm.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
						detachCommitment(comm);
						logger.debug("BID FINITE, GOLD ASSEGNATO A "+comm.getDebtor().toString());
					}
				}
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@OPERATION
	public void ignore(int x, int y) {
		try {
			Fact ant = null, cons = null;
			RoleId biddingMiner = getRoleIdByPlayerName(getOpUserName());
			RoleId groupMiner = new RoleId(MINER_ROLE);
			// recupero l'oro per cui si fa la bid
			Gold goldToBid = new Gold(x,y);
			for (Gold g1 : golds) {
				if (g1.equals(goldToBid)) {
					goldToBid = g1;
					break;
				}
					
			}
			goldToBid.bid(biddingMiner, 1000);
			
			// creo il commitment della bid
			cons = new Fact("drop", x, y);
			ant = new Fact("gold_assigned", x, y, biddingMiner.toString());
			
			
			//controllo se son arrivate tutte le bid, nel qual caso recupero il vincitore
			if (goldToBid.getNumberOfBidders() == numberOfMiners && goldToBid.getWinningBidder()!=null) {
				ant = new Fact("gold_assigned", x, y, goldToBid.getWinningBidder().toString());
				assertFact(ant);
				for (Commitment comm : socialState.retrieveCommitmentsByDebtorRoleId(goldToBid.getWinningBidder())) {
					if (comm.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
						detachCommitment(comm);
						logger.debug("BID FINITE, GOLD ASSEGNATO A "+comm.getDebtor().toString());
					}
				}
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
		
	}
	
	
	// BEGIN INNER CLASSES for ROLES


	public class Miner extends PARole {

		public Miner(String playerName, IPlayer player) {
			super(MINER_ROLE, player);
		}
	}


	

	public interface MinerObserver extends ProtocolObserver {

	}

	
	private class Gold {
		
		int x;
		int y;
		boolean booked = false;
		int numberOfBidders = 0;
		//HashMap<Integer, RoleId> bidders = new HashMap<Integer, RoleId>();
		RoleId winningBidder;
		int lowestBid = 1000;
		
		public Gold (int x, int y) {
			this.x = x;
			this.y = y;
			
		}

		public int getX() {
			return x;
		}



		public int getY() {
			return y;
		}

		
		public int getNumberOfBidders() {
			return this.numberOfBidders;
		}
		
		
		// in questa versione non si fa controllo che stesso agente faccia piu' di una bid
		public void bid(RoleId role, int distance) {
			//bidders.put(distance, role);
			numberOfBidders++;
			if (distance < lowestBid) {
				winningBidder = role;
				lowestBid = distance;
			}
		}
		
		public RoleId getWinningBidder() {
			return this.winningBidder;
		}
		
		public boolean isBooked() {
			return booked;
		}

		public void book() {
			this.booked = true;
		}

		public boolean equals(Gold g) {
			return this.x == g.x && this.y == g.y;
		}
		
		public String toString() {
			return this.x + ","+this.y;
		}
		
	}
	

}

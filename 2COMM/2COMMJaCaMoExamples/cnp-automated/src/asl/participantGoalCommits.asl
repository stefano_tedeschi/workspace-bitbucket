// Agent participant in project cnp.mas2j

/* Initial beliefs and rules */

/* Initial goals */

!participate.

/* Plans */

+!participate : true
	<- 	focusWhenAvailable("cnp");
		.wait(1000); // serve per evitare l'errore di concurrentmodification cartago
		enact("participant").

+enacted(Id,"participant",My_Role_Id)
	<-	+enactment_id(My_Role_Id). // serve perche' enact e' una signal e va aggiunta come mental note

+cc(Initiator_Role_Id, My_Role_Id, "propose", "(accept OR reject)","CONDITIONAL")
	:	enactment_id(My_Role_Id) 
        & task(Task, Initiator_Role_Id)
	<-  !setup_proposal(Task, Initiator_Role_Id);
		?cc(Initiator_Role_Id, My_Role_Id, "true", "(accept OR reject)", "DETACHED").   
        
		
+!setup_proposal(Task, Initiator_Role_Id)
	: 	enactment_id(My_Role_Id)
	<-	!prepare_proposal(Task, Prop, Cost);
		propose(Prop, Cost, Initiator_Role_Id);
		+my_proposal(Prop, Cost, Initiator_Role_Id);
		?cc(My_Role_Id, Initiator_Role_Id, "accept", "(done OR failure)", "CONDITIONAL").

+cc(My_Role_Id, Initiator_Role_Id, "true", "(done OR failure)", "DETACHED")
	: enactment_id(My_Role_Id) &
      accept(My_Role_Id) 
	<-	.print("******* I AM THE WINNER *********");

        ?my_proposal(Prop, Cost, Initiator_Role_Id);
        !doneORfailure(Prop, Cost, Initiator_Role_Id);
        ?cc(My_Role_Id, Initiator_Role_Id,"true","(done OR failure)", "SATISFIED").   
		
+!doneORfailure(Prop, Cost, Initiator_Role_Id)
	<-	!compute_result(Prop, Cost, Result);
		.print("The result is: ",Result);
		if (Result == "fail")
		{
			failure(Initiator_Role_Id);
		}
		else
		{
			done(Result, Initiator_Role_Id);
		};
        // Meglio qui il test seguente o sopra (stessa domanda precedente).
		?cc(My_Role_Id, Initiator_Role_Id, "true", "(done OR failure)", "DETACHED").


+cc(My_Role_Id, Initiator_Role_Id, "true", "(done OR failure)", "DETACHED")
	: 	enactment_id(My_Role_Id) &
	    not accept(Prop, Cost, Initiator_Role_Id)
	<-	.print("proposal rejected").
	
+!prepare_proposal(Task, Prop, Cost)
	<-	//.print("Preparing proposal");
		.my_name(My_Id);
		.concat("proposal-",My_Id,Prop);
		.random(C);
		Cost = math.round(C*100).
		
		//refuse(My_Role_Id, Id).

+!compute_result(Prop, Cost, Result)
	<-	.random(Succ);
		if (Succ < 0.5) {
			.concat("fail",Result);
		}
		else {
			.concat(Prop,"-",Cost,"-done",Result);
		}.

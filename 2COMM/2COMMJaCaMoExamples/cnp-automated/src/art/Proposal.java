package art;

import twocomm.core.RoleId;

public class Proposal {

	private String proposalContent = "";
	private int cost;
	private RoleId role;
	
	public Proposal(String prop, int cost) {
		this.proposalContent = prop;
		this.cost = cost;
	}
	
	public void setRoleId(RoleId role) {
		this.role = role;
	}
	
	public RoleId getRoleId() {
		return this.role;
	}
	
	public String getProposalContent() {
		return proposalContent;
	}
	
	public int getCost() {
		return this.cost;
	}
	
	public String toString() {
		return "Proposal "+proposalContent;
	}
	
}

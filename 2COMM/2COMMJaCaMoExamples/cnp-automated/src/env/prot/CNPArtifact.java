package prot;

import jade.lang.acl.ACLMessage;

import java.util.Arrays;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.AgentId;
import cartago.OPERATION;

public class CNPArtifact extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(CNPArtifact.class);
	public static String ARTIFACT_TYPE = "CNP";
	public static String INITIATOR_ROLE = "initiator";
	public static String PARTICIPANT_ROLE = "participant";
	private long millis;
	private int numberMaxProposals = 1;
	private int actualProposals = 0;

	static {
		addEnabledRole(INITIATOR_ROLE, Initiator.class);
		addEnabledRole(PARTICIPANT_ROLE, Participant.class);
	}

	public CNPArtifact() {
		super();
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}


	// ROLES OPERATIONS

	@OPERATION
	public void cfp(String task) {
		millis = new Date().getTime();
		RoleId initiator = getRoleIdByPlayerName(getOpUserName());
		this.defineObsProperty("task", task, initiator.toString());
		RoleId dest = new RoleId(PARTICIPANT_ROLE);
 		try {
			createAllCommitments(new Commitment(initiator, dest, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			assertFact(new Fact("cfp", initiator.toString(), task));
			logger.trace("OPERATION PERFORMED: CFP by "+initiator.getPlayerName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			log(Arrays.toString(e.getStackTrace()));
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			log(Arrays.toString(e.getStackTrace()));
		}
	}

	@OPERATION
	public void accept(String prop) {
		RoleId participant = getRoleIdByRoleName(prop);
		// VERSIONE CON SIGNAL PARTICOLARE, CHE RICHIEDE QUINDI LE REJECT A TUTTI GLI ALTRI
		//signal(participant.getPlayerAgentId(), "accept");
		// VERSIONE CON PUBBLICAZIONE DI CHI HA VINTO
		System.out.println(""+prop);
		defineObsProperty("accept", participant.toString());
		try {
			assertFact(new Fact("accept",participant.toString()));

			//**** DEBUG: CAMBIO STATO DI VITA A COMMITMENT GIUSTO, SARA' IL SOCIAL STATE CHE DOVRA' GESTIRLO
			for (Commitment com : socialState.retrieveAllCommitments()) {
				if (com.getAntecedent().equals(new Fact("accept"))) {
						removeObsPropertyByTemplate("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
						com.setStatus(LifeCycleState.DETACHED);
						com.setAntecedent(new Fact("true"));
						defineObsProperty("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
				}
			}

			logger.trace("OPERATION PERFORMED: ACCEPT by "+getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// NELLA VERSIONE CON PUBBLICAZIONE RISULTATO NON SERVE MANDARE LA REJECT AGLI ALTRI
//	@OPERATION
//	public void reject(String part) {
//		String initiator = getOpUserName();
//		RoleId initiatorId = getRoleIdByPlayerName(initiator);
//		RoleId participant = getRoleIdByRoleCanonicalName(part);
//
//		signal(participant.getPlayerAgentId(), "reject");
//		try {
//			releaseCommitment(new Commitment(participant, initiatorId, "accept", new CompositeExpression(
//					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
//			logger.trace("OPERATION PERFORMED: REJECT by "+initiator);
//		} catch (MissingOperandException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (WrongOperandsNumberException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	@OPERATION
	public void propose(String prop, int cost, String init) {
		Proposal p = new Proposal(prop, cost);
		RoleId participant = getRoleIdByPlayerName(getOpUserName());
		RoleId initiator = getRoleIdByRoleName(init);
		p.setRoleId(participant);
		// VERSIONE CON SIGNAL PARTICOLARE
		// signal(initiator.getPlayerAgentId(), "proposal", p.getProposalContent(), p.getCost(), participant.getCanonicalName());
		defineObsProperty("proposal", p.getProposalContent(), p.getCost(), participant.toString());
 		try {
			createCommitment(new Commitment(participant, initiator, "accept", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			assertFact(new Fact("propose", participant, prop));

			//**** DEBUG: CAMBIO STATO DI VITA A COMMITMENT GIUSTO, SARA' IL SOCIAL STATE CHE DOVRA' GESTIRLO
			for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(participant)) {
				if (com.getAntecedent().equals(new Fact("propose")) && com.getLifeCycleStatus().equals(LifeCycleState.CONDITIONAL)) {
						removeObsPropertyByTemplate("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
						com.setStatus(LifeCycleState.DETACHED);
						com.setAntecedent(new Fact("true"));
						actualProposals++;
						defineObsProperty("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());

				}
			}
			//System.out.println("PROPOSALS: "+actualProposals);
			if (actualProposals == numberMaxProposals) {
				RoleId groupParticipant = new RoleId(PARTICIPANT_ROLE);
				createCommitment(new Commitment(initiator, groupParticipant, new CompositeExpression(
						LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
//				defineObsProperty("cc", initiator.getCanonicalName(), "GroupParticipant",
//						"true", "(accept OR reject)", "DETACHED");

			}



			logger.trace("OPERATION PERFORMED: PROPOSE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//
//	@INTERNAL_OPERATION
//	public void check
	// DA AGGIUNGERE: CHECK SU NUMERO PROPOSTE E SU TEMPO

//	if (actualProposals==numberMaxProposals)
//		//
//		defineObsProperty("cc", com.getDebtor().getCanonicalName(), com.getCreditor().getCanonicalName(),
//				com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());

	@OPERATION
	public void refuse(RoleId participant, RoleId initiator) {
		RoleMessage refusal = new RoleMessage();
		refusal.setRoleSender(participant);
		refusal.setRoleReceiver(initiator);
		refusal.setPerformative(ACLMessage.REFUSE);
		send(refusal);

		try {
			releaseCommitment(new Commitment(initiator, participant, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			assertFact(new Fact("refuse", participant));
			logger.trace("OPERATION PERFORMED: REFUSE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@OPERATION
	public void done(String result, String init) {
		RoleId rolePart = getRoleIdByPlayerName(getOpUserName());
		//signal(initiator.getPlayerAgentId(), "done", result);
		defineObsProperty("done", result);
		long now = new Date().getTime();
		log("TIME ELAPSED: "+((now-millis)/1000));
		try {
			for (Commitment com : socialState.retrieveAllCommitments()) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))
								&& com.getDebtor().equals(rolePart)) {
						removeObsPropertyByTemplate("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
						com.setStatus(LifeCycleState.SATISFIED);
						defineObsProperty("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
				}
			}

			assertFact(new Fact("done", result));
			logger.trace("OPERATION PERFORMED: DONE by "+getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@OPERATION
	public void failure(String init) {
		RoleId rolePart = getRoleIdByPlayerName(getOpUserName());
		//signal(initiator.getPlayerAgentId(), "done", result);
		defineObsProperty("failure", rolePart.toString());
		long now = new Date().getTime();
		log("TIME ELAPSED: "+((now-millis)/1000));
		try {
			for (Commitment com : socialState.retrieveAllCommitments()) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))
								&& com.getDebtor().equals(rolePart)) {
						removeObsPropertyByTemplate("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
						com.setStatus(LifeCycleState.SATISFIED);
						defineObsProperty("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
				}
			}

			assertFact(new Fact("failure", rolePart.toString()));
			logger.trace("OPERATION PERFORMED: FAILURE by "+getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// BEGIN INNER CLASSES for ROLES
		// cnp.Initiator role

		public class Initiator extends PARole {

			public Initiator(String playerName, IPlayer player) {
				super(INITIATOR_ROLE, player);
			}
		}

		// cnp.Participant role

		public class Participant extends PARole {

			public Participant(String playerName, IPlayer player) {
				super(PARTICIPANT_ROLE, player);
			}



		}

		public interface CNPInitiatorObserver extends ProtocolObserver {

		}

		public interface CNPParticipantObserver extends ProtocolObserver {

		}

}

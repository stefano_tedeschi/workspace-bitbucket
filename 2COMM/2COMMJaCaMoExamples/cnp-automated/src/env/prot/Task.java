package prot;

public class Task {
	
	private String taskName = "";
	private Object result = null;
	
	public Task(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public void setResult(Object obj) {
		this.result = obj;
	}
	
	public Object getResult() {
		return this.result;
	}
	
	public String toString() {
		return "task "+taskName;
	}
}

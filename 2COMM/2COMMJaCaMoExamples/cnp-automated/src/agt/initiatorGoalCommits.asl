{ include("$jacamoJar/templates/common-cartago.asl") }

!startCNP.

/* Plans */

+!startCNP : true
	<- 	//makeArtifact("cnp","art.CNPArtifact",[],C);
		//focus(C);
		enact("initiator").
		
+enacted(Id,"initiator",Role_Id)
	<-	+enactment_id(Role_Id);
		println("Role initiator successfully enacted!");
		!solveTask("task-one").
		

+!solveTask(Task)
	: enactment_id(My_Role_Id)
    <-  +task(Task);
    	.wait(2000);
    	println("Making a CFP for task-one...");
        cfp("task-one").


+cc(My_Role_Id, "participant", "true", "(accept OR reject)", "DETACHED") 
	: 	enactment_id(My_Role_Id)
	<-	.print("Proposals reached");
	    !acceptORreject.

+!acceptORreject
	: 	not evaluated // da ricontrollare: serve solo per evitare la riesecuzione del piano
	<-	+evaluated;	    
		.wait(2000);
	    .findall(proposal(Content,Cost,Id),proposal(Content,Cost,Id),Proposals);
	    .count(proposal(Content,Cost,Id),ProposalsNumb);
	   	.print(ProposalsNumb);
		.min(Proposals,proposal(Proposal,Cost,Winner_Role_Id));
		println("The winner is: ", Winner_Role_Id);	
		+winner(Winner_Role_Id);	
		accept(Winner_Role_Id);
	    ?cc(My_Role_Id, Winner_Role_Id, "true", "(accept OR reject)", "DETACHED"). // questa da errore
		// ci sara' solo accept oppure reject		
		

+done(Participant_role_id, Result)
    :   winner(Participant_role_id)
	<-	.print("Task resolved: ",Result).
	
+failure(Participant_role_id)
    :   winner(Participant_role_id)
	<-	.print("Task failed by ",Participant_role_id).	

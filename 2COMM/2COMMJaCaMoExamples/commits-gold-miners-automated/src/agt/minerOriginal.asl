// miner agent

{ include("$jacamoJar/templates/common-cartago.asl") }

/* 
 * Agent with commitment-based strategy
 * 
 * Based on implementation developed by Joao Leite and Rafael Bordini, Jomi Hubner and Maicon Zatelli
 */

/* beliefs */
last_dir(null). // the last movement I did
free.
!enact.

+!enact
	<-	enact("miner").
	
+enacted(Id,"miner",Role_Id)
	<-	+enactment_id(Role_Id);
		.my_name(Me);
		.print(Me, " as ",Role_Id).
		

// commitment da prendere ogni volta che sono libero
// e valuto i gold che conosco
// quindi rimangono tutti i commitment presi,
// ma solo uno si attiva perch� � l'artefatto
// che pubblica quell'atomo che � l'antecedente
// di un unico commitment tra i diversi relativi a quel gold
// 
// da gestire Release e Cancel. Quando 

/* rules */


// le regole non funzionano coi commitment, poich� le public properties degli artefatti compaiono
// nella KB degli agenti ma non triggerano le regole

//i_go_for_gold(X,Y)
//	:- gold(X,Y)
//	   & enactment_id(Role_Id)
//	   & gold_assigned(X,Y,Assigned_Role_Id) & Role_Id == Assigned_Role_Id.


/* When free, agents wonder around. This is encoded with a plan that executes 
 * when agents become free (which happens initially because of the belief "free" 
 * above, but can also happen during the execution of the agent (as we will see below).
 *   
 * The plan simply gets two random numbers within the scope of the size of the grid 
 * (using an internal action jia.random), and then calls the subgoal go_near. Once the
 * agent is near the desired position, if free, it deletes and adds the atom free to 
 * its belief base, which will trigger the plan to go to a random location again.
 */

+free : gsize(_,W,H) & jia.random(RX,W-1) & jia.random(RY,H-1) 
   <-  .print("I am going to go near (",RX,",", RY,")");
       !go_near(RX,RY).
+free  // gsize is unknown yet
   <- .wait(100); -+free.
   
/* When the agent comes to believe it is near the location and it is still free, 
 * it updates the atom "free" so that it can trigger the plan to go to a random 
 * location again.
 */
+near(X,Y) : free <- -+free.







/* Gold-searching Plans */

/* The following plan encodes how an agent should deal with a newly found piece 
 * of gold, when it is not carrying gold and it is free. 
 * The first step changes the belief so that the agent no longer believes it is free. 
 * Then it adds the belief that there is gold in position X,Y, and 
 * prints a message. Finally, it calls a plan to handle that piece of gold.
 */

// **************** PERCEZIONE *****************

// sono libero: faccio la bid
@goldpercepted[atomic]
+cell(X,Y,gold) 
	:	pos(MyX, MyY) &
	 	enactment_id(My_Role_Id) &
		not cc(My_Role_Id,_,RenounceGroup,PickThenDrop,"CONDITIONAL") &
		not cc(My_Role_Id,_,RenounceGroup,PickThenDrop,"DETACHED") // non ci deve essere ne' conditional ne' detached
		// qui controllo che non ci sia ALCUN commitment conditional o detached con me debitore
	<- 	jia.dist(X,Y,MyX,MyY,Dist);
		.print("sono LIBERO. Trovato oro ",X," ",Y," faccio BID");
		bid(X,Y,Dist).
	

		
// se non sono libero (quindi sto andando a prendere un oro) ma non ne sto portando nessuno, gli do priorita'
@goldperceptedswitch[atomic]
+cell(X,Y,gold) 
	:	cc(My_Role_Id,OtherRoleId,Renounce,PickThenDrop,"DETACHED")
		& not carrying(OldX,OldY)
		& enactment_id(My_Role_Id)
		& .term2string(Renounce, T)
		& jia.getCoord1(T, OldX)
		& jia.getCoord2(T, OldY) 
		& not changed & pos(MyX, MyY)
		& X \== OldX & Y \== OldY // l'oro percepito non e' quello gia' mio obiettivo		
	<- 	.print("ho un COMMITMENT DETACHED in ",OldX,OldY,". Trovato oro ",X," ",Y," faccio WITHDRAW e BID");
		.drop_intention(handle(gold(_,_)));
		+changed;
		-handling_gold(OldX,OldY);
		jia.dist(X,Y,MyX,MyY,Dist);
		withdraw(OldX,OldY);
		bid(X,Y,Dist).
		


//+cell(X,Y,gold) // se sto gia' portando oro, calcolo distanza fino a depot e poi fino a gold
//	:	cc(My_Role_Id,_,Renounce,PickThenDrop,"DETACHED") 
//		& pos(MyX, MyY)
//		& enactment_id(My_Role_Id)
//		& .term2string(Renounce, T)
//		& jia.getCoord1(T, OldX)
//		& jia.getCoord2(T, OldY)
//		& carrying(OldX,OldY)
//		& not bid(X,Y,_,My_Role_Id)
//	<- 	.print("sto TRASPORTANDO, faccio bid con calcolo distanza totale per ",X,"," ,Y);
//		jia.dist(MyX,MyY,0,0,Dist1);
//		jia.dist(0,0,X,Y,Dist2);
//		bid(X,Y,Dist1+Dist2).
		
//+cell(X,Y,gold) // se sto gia' portando oro, mi faccio un appunto per una futura bid. pero' cos� gli altri?
//	:	cc(My_Role_Id,_,Renounce,PickThenDrop,"DETACHED") 
//		& pos(MyX, MyY)
//		& enactment_id(My_Role_Id)
//		& .term2string(Renounce, T)
//		& jia.getCoord1(T, OldX)
//		& jia.getCoord2(T, OldY)
//		& carrying(OldX,OldY)
//		& not bid(X,Y,_,My_Role_Id)
//	<- 	.print("sto TRASPORTANDO, faccio mental note per ",X,"," ,Y).
		
// ******** COMUNICAZIONE

// qui invece faccio rinuncia per un gold per cui sono piu' lontano, ed io ho gia' un commitment condizionale per quel gold,
// cioe' un altro agente si sta proponendo ed e' piu' vicino di me
+cc(Other, My_Role_Id, Renounce, _, "CONDITIONAL")
	:	enactment_id(My_Role_Id)
		& .term2string(Renounce, T)
		& jia.getCoord1(T, X)
		& jia.getCoord2(T, Y)
		& (cc(My_Role_Id, Other, _, _, "DETACHED") | cc(My_Role_Id, Other2, _, _, "CONDITIONAL"))
		
	<-	.print("Stampa ",X,Y);	
		
		.print("COMMITTATO, faccio RINUNCIA");
		renounce(X, Y).





// arriva comunicazione e sono libero: faccio bid
// se noto che la mia offerta e' piu' bassa di quella attuale
// pero' cosa fare se un altro agente ancora potrebbe offrire una bid piu' bassa?
// perche' si attiva piu' volte questo piano?? perche' ci sono tanti commitment
//@commitgoldfree[atomic]
+cc(Other_Role_Id,My_Role_Id,Renounce,_,"CONDITIONAL")
	: 	pos(MyX, MyY) & enactment_id(My_Role_Id)
		& .term2string(Renounce, T)
		& jia.getCoord1(T, X1)
		& jia.getCoord2(T, Y1)
		& not cc(My_Role_Id, Other, _, _, "DETACHED")
		& not cc(My_Role_Id, Other, _, _, "CONDITIONAL")
		& bid(X1,Y1,Other_Dist,Other_Role_Id) // ci deve essere la bid di chi ha creato il commitment	
		//& not renounce(X1,Y1,My_Role_Id) // non devo aver gia' fatto la renounce per questo	
	<- 	jia.dist(X1,Y1,MyX,MyY,Dist);
	    .print("Sono LIBERO, COMMITMENT RILEVATO per ",X1," ",Y1);
	    .print("Other: ",Other_Role_Id);
		if (Dist < Other_Dist) {
			.print("faccio bid");			
			bid(X1,Y1,Dist);
		}
		else {
			.print("faccio renounce per ",X1,Y1);			
			renounce(X1,Y1);
		}.
		
// qui invece faccio rinuncia per un gold per cui sono piu' lontano, ed io ho gia' un commitment condizionale per quel gold,
// cioe' un altro agente si sta proponendo ed e' piu' vicino di me
//+cc(Other, My_Role_Id, Renounce, PickAndDrop, "CONDITIONAL")
//	:	enactment_id(My_Role_Id)
//		& .term2string(Renounce, T)
//		& jia.getCoord1(T, X1)
//		& jia.getCoord2(T, X2)
//		& not cc(My_Role_Id, Other, _, PickAndDrop2, "DETACHED")
//		& cc(My_Role_Id, Other, Renounce2, PickAndDrop, "CONDITIONAL")
//		& .term2string(Renounce2, T1)
//		& jia.getCoord1(T1, X1)
//		& jia.getCoord2(T1, X2)
//		& pos(MyX, MyY)
//		& bid(X, Y, Dist, My_Role_Id)
//		& bid(X, Y, DistOther, Other)
//		& Dist > DistOther
//	<-	.print("Stampa ",X1,X2);	
//		.print("COMMITTATO per ",X,Y,"ma agente ",Other,"e' piu' vicino di me, faccio WITHDRAW e RINUNCIA");
//		withdraw(X, Y); 
//		renounce(X, Y).
		
		

		
		
// se c'e' un commitment gia' detached per me,
//// rinuncio a fare bid per l'oro per cui e' arrivato il commitment
//+cc(Other, My_Role_Id, Renounce, PickAndDrop, "CONDITIONAL")
//	:	enactment_id(My_Role_Id)
//		& cc(My_Role_Id, Other, Renounce2, PickAndDrop2, "DETACHED")
//		& .term2string(Renounce2, T)
//		& jia.getCoord1(T, X)
//		& jia.getCoord2(T, Y)
//		& .term2string(Renounce, T1)
//		& jia.getCoord1(T1, NewX)
//		& jia.getCoord2(T1, NewY)
//		& not bid(NewX, NewY, _, My_Role_Id) // non ho gia' fatto la bid di questo nuovo oro
//		& not renounce(NewX, NewY, My_Role_Id)
//	<-	.print("Sono DETACHED di ",X,",",Y,", faccio RENOUNCE di ",NewX,", ",NewY," verso ",Other);
//		.print("Commit: ",Renounce,", ",PickAndDrop);
//		.print("mio commit: ",Renounce2,", ",PickAndDrop2 );
//		renounce(NewX, NewY).


// se qualcuno ha fatto violazione vuol dire che c'e' un oro "orfano"
// per cui fare la bid
+cc(Other, My_Role_Id, _, PickAndDrop, "VIOLATED")
	:	enactment_id(My_Role_Id)
		& not cc(My_Role_Id, "miner", _, PickAndDrop2, "DETACHED")
		& pos(MyX, MyY)
		& bid(X,Y,OtherDist,Other)
	<-	jia.dist(MyX, MyY, X, Y, Dist);
		bid(X, Y, Dist). 
			

// se il mio commitment e' detached, inizio la gestione dell'oro
// non devo reagire pero' a TUTTI i commitment che diventano detached
@p2gold[atomic] 
+cc(My_Role_Id, Other_Role_Id, _, PickAndDrop, "DETACHED")
	:	enactment_id(My_Role_Id)	
		& bid(X,Y,My_Dist,My_Role_Id)
		& free
	<-	.print("DETACHED per ",X," ",Y);
		-free;
		!init_handle(gold(X,Y)).	


     
/* The next plans encode how to handle a piece of gold.
 * The first one drops the desire to be near some location, 
 * which could be true if the agent was just randomly moving around looking for gold.
 * The second one simply calls the goal to handle the gold.
 * The third plan is the one that actually results in dealing with the gold. 
 * It raises the goal to go to position X,Y, then the goal to pickup the gold, 
 * then to go to the position of the depot, and then to drop the gold and remove 
 * the belief that there is gold in the original position. 
 * Finally, it prints a message and raises a goal to choose another gold piece.
 * The remaining two plans handle failure.
 */     

@pih1[atomic]
+!init_handle(Gold) 
  :  .desire(near(_,_)) 
  <- .print("Dropping near(_,_) desires and intentions to handle ",Gold);
     .drop_desire(near(_,_));
     !init_handle(Gold).
@pih2[atomic]
+!init_handle(Gold)
  :  pos(X,Y)
  <- .print("Going for ",Gold);
     !!handle(Gold). // must use !! to perform "handle" as not atomic

+!handle(gold(X,Y)) 
  <- .print("Handling ",gold(X,Y)," now.");
  	 +handling_gold(X,Y);
     !pos(X,Y);
     !ensure(pick,gold(X,Y));
     !pos(0,0);
     !ensure(drop, 0);
     -handling_gold(X,Y);
     .print("Finish handling ",gold(X,Y));
     -future_gold(X,Y);
     !!choose_gold.

// if ensure(pick/drop) failed, pursue another gold
-!handle(G) : G
  <- .print("failed to catch gold ",G);
     .abolish(G); // ignore source
     !!choose_gold.
-!handle(G) : true
  <- .print("failed to handle ",G,", it isn't in the BB anyway");
     !!choose_gold.
//
///* The next plans deal with picking up and dropping gold. */
//
+!ensure(pick,_) : pos(X,Y)
  <- pick; 
     ?carrying_gold;
     +carrying(X,Y); 
     //-gold(X,Y);
     -changed;
     announcePick(X,Y).
// fail if no gold there or not carrying_gold after pick! 
// handle(G) will "catch" this failure.


// qui recupero anche le coordinate dell'oro che sto trasportando,
// cos� posso soddisfare tutti i commitment che ne fanno parte
+!ensure(drop, _) : carrying(X,Y) & pos(0,0)
  <- drop;  	 
  	 +dropped(X,Y);
  	 -carrying(X,Y);
  	 announceDrop(X, Y).



/* The next plans encode how the agent can choose the next gold piece 
 * to pursue (the closest one to its current position) or, 
 * if there is no known gold location, makes the agent believe it is free.
 */
+!choose_gold 
  :  not future_gold(_,_)
  <- -+free.
//
//// Finished one gold, but others left
//// find the closest gold among the known options, 
//// in particolare ci dev'essere commitment mio a prenderlo,
//// nel qual caso rendo vero l'antecedente e chiedo di prenotarlo.
//// non deve essere gi� prenotato da altri
+!choose_gold 
  :  future_gold(X,Y) & enactment_id(My_Role_Id)
  	<-	.print("sono LIBERO ma ho una mental note per ",X, ", ",Y,", faccio BID");
  		.findall(future_gold(X,Y),future_gold(X,Y),LG);
	     !calc_gold_distance(LG,LD);
	     .length(LD,LLD); LLD > 0;
	     .print("Gold distances: ",LD,LLD);
	     .min(LD,d(_,future_gold(X,Y)));
	     .print("Next gold is ",X,Y);	     
	     bid(X,Y).
	     //!!init_handle(gold(X,Y)).
	     
	     
-!choose_gold <- .print("sono libero."); -+free.
//
+!calc_gold_distance([],[]).
+!calc_gold_distance([gold(GX,GY)|R],[d(D,gold(GX,GY))|RD]) 
  :  pos(IX,IY)
  <- jia.dist(IX,IY,GX,GY,D);
     !calc_gold_distance(R,RD).
+!calc_gold_distance([_|R],RD) 
  <- !calc_gold_distance(R,RD).


/* The following plans encode how an agent should go to near a location X,Y. 
 * Since the location might not be reachable, the plans succeed 
 * if the agent is near the location, given by the internal action jia.neighbour, 
 * or if the last action was skip, which happens when the destination is not 
 * reachable, given by the plan next_step as the result of the call to the 
 * internal action jia.get_direction.
 * These plans are only used when exploring the grid, since reaching the 
 * exact location is not really important.
 */

+!go_near(X,Y) : free
  <- -near(_,_); 
     -last_dir(_); 
     !near(X,Y).


// I am near to some location if I am near it 
+!near(X,Y) : (pos(AgX,AgY) & jia.neighbour(AgX,AgY,X,Y)) 
   <- .print("I am at ", "(",AgX,",", AgY,")", " which is near (",X,",", Y,")");
      +near(X,Y).
   
// I am near to some location if the last action was skip 
// (meaning that there are no paths to there)
+!near(X,Y) : pos(AgX,AgY) & last_dir(skip) 
   <- .print("I am at ", "(",AgX,",", AgY,")", " and I can't get to' (",X,",", Y,")");
      +near(X,Y).

+!near(X,Y) : not near(X,Y)
   <- !next_step(X,Y);
      !near(X,Y).
+!near(X,Y) : true 
   <- !near(X,Y).


/* These are the plans to have the agent execute one step in the direction of X,Y.
 * They are used by the plans go_near above and pos below. It uses the internal 
 * action jia.get_direction which encodes a search algorithm. 
 */

+!next_step(X,Y) : pos(AgX,AgY) // I already know my position
   <- jia.get_direction(AgX, AgY, X, Y, D);
      -+last_dir(D);
      D.
+!next_step(X,Y) : not pos(_,_) // I still do not know my position
   <- !next_step(X,Y).
-!next_step(X,Y) : true  // failure handling -> start again!
   <- -+last_dir(null);
      !next_step(X,Y).
  

/* The following plans encode how an agent should go to an exact position X,Y. 
 * Unlike the plans to go near a position, this one assumes that the 
 * position is reachable. If the position is not reachable, it will loop forever.
 */

+!pos(X,Y) : pos(X,Y) 
   <- .print("I've reached ",X,"x",Y).
+!pos(X,Y) : not pos(X,Y)
   <- !next_step(X,Y);
      !pos(X,Y).
      
      

/* end of a simulation */

+end_of_simulation(S,_) : true 
  <- .drop_all_desires; 
     .abolish(gold(_,_));
     .abolish(picked(_));
     -+free;
     .print("-- END ",S," --").

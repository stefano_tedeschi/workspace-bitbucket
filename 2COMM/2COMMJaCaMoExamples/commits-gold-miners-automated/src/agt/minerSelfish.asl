// miner agent

{ include("$jacamoJar/templates/common-cartago.asl") }

/* 
 * By Joao Leite
 * Based on implementation developed by Rafael Bordini, Jomi Hubner and Maicon Zatelli
 */

/* beliefs */
last_dir(null). // the last movement I did
free.
!enact.

+!enact
	<-	enact("miner").
	
+enacted(Id,"miner",Role_Id)
	<-	+enactment_id(Role_Id);
		.my_name(Me);
		.print(Me, " as ",Role_Id).
		

// commitment da prendere ogni volta che sono libero
// e valuto i gold che conosco
// quindi rimangono tutti i commitment presi,
// ma solo uno si attiva perch� � l'artefatto
// che pubblica quell'atomo che � l'antecedente
// di un unico commitment tra i diversi relativi a quel gold
// 
// da gestire Release e Cancel. Quando 

/* rules */


// le regole non funzionano coi commitment, poich� le public properties degli artefatti compaiono
// nella KB degli agenti ma non triggerano le regole

//i_go_for_gold(X,Y)
//	:- gold(X,Y)
//	   & enactment_id(Role_Id)
//	   & gold_assigned(X,Y,Assigned_Role_Id) & Role_Id == Assigned_Role_Id.


/* When free, agents wonder around. This is encoded with a plan that executes 
 * when agents become free (which happens initially because of the belief "free" 
 * above, but can also happen during the execution of the agent (as we will see below).
 *   
 * The plan simply gets two random numbers within the scope of the size of the grid 
 * (using an internal action jia.random), and then calls the subgoal go_near. Once the
 * agent is near the desired position, if free, it deletes and adds the atom free to 
 * its belief base, which will trigger the plan to go to a random location again.
 */

+free : gsize(_,W,H) & jia.random(RX,W-1) & jia.random(RY,H-1) 
   <-  .print("I am going to go near (",RX,",", RY,")");
       !go_near(RX,RY).
+free  // gsize is unknown yet
   <- .wait(100); -+free.
   
/* When the agent comes to believe it is near the location and it is still free, 
 * it updates the atom "free" so that it can trigger the plan to go to a random 
 * location again.
 */
+near(X,Y) : free <- -+free.







/* Gold-searching Plans */

/* The following plan encodes how an agent should deal with a newly found piece 
 * of gold, when it is not carrying gold and it is free. 
 * The first step changes the belief so that the agent no longer believes it is free. 
 * Then it adds the belief that there is gold in position X,Y, and 
 * prints a message. Finally, it calls a plan to handle that piece of gold.
 */

// **************** PERCEZIONE *****************

// sono libero: mi committo io direttamente
@goldpercepted[atomic]
+cell(X,Y,gold) 
	:	free & not gold(X,Y) & enactment_id(My_Role_Id)
		& not cc(My_Role_Id,_,Assigned,Drop,"DETACHED")
	<- 	+gold(X,Y);
		commitToDropGoldImmediately(X,Y);
		.print("commited to immediately take gold in ,",X,Y).
		
		
		
// se non sono libero (quindi sto andando a prendere un oro) ma non ne sto portando nessuno, gli do priorita'
// devo comunicare gold della vecchia posizione
@goldperceptedswitch[atomic]
+cell(X,Y,gold) 
	:	not free & not carrying(_,_)  & not gold(X,Y) 
		& handling_gold(OldX,OldY) & not changed
	<- 	.drop_intention(handle(gold(_,_)));
		+gold(X,Y);
		+changed;
		-handling_gold(OldX,OldY);
		.print("change commitment for new gold ,",X,Y, " dropping old ",OldX,OldY);
		changeGoldToPursue(X,Y).
		//communicateGoldPosition(OldX,OldY).


+cell(X,Y,gold) // se sto gia' portando oro
	:	not gold(X,Y)
	<- 	.print("I am a selfish miner, i will NOT communicate gold position",X,Y).
		//communicateGoldPosition(X,Y).
		
		
// ******** COMUNICAZIONE
// arriva comunicazione e sono libero: faccio bid
@commitgoldfree[atomic]
+gold(X,Y)
	: 	free & pos(MyX, MyY) & not bet(X,Y) // cos� evito che il cambio di source me lo attivi due volte
	<- 	jia.dist(X,Y,MyX,MyY,Dist);
		print("Comunicazione oro arrivata per ",X," ",Y," e la mia distanza e' ",Dist);
		bid(X,Y,Dist);
		+bet(X,Y).
		
// se non sono libero faccio la ignore	
+gold(X,Y)
	:	not bet(X,Y)
	<-	.print("Comunicazione per ",X, " ",Y," arrivata ma sono occupato. Invio bid finta.");
		ignore(X,Y);
		+bet(X,Y).
	



		
// se mio commit diventa detached, vuol dire che quell'oro lo vado a recuperare io
@p2gold[atomic]  
+cc(My_Role_Id,_,_,Drop,"DETACHED")
	:	enactment_id(My_Role_Id)
		& .term2string(Term1,Drop)
		& Term1 = drop(X,Y)
		// & free
	<- 	.print("Sono committato a prendere l'oro in ",X," ",Y);
		-free;
		!init_handle(gold(X,Y)).


     
/* The next plans encode how to handle a piece of gold.
 * The first one drops the desire to be near some location, 
 * which could be true if the agent was just randomly moving around looking for gold.
 * The second one simply calls the goal to handle the gold.
 * The third plan is the one that actually results in dealing with the gold. 
 * It raises the goal to go to position X,Y, then the goal to pickup the gold, 
 * then to go to the position of the depot, and then to drop the gold and remove 
 * the belief that there is gold in the original position. 
 * Finally, it prints a message and raises a goal to choose another gold piece.
 * The remaining two plans handle failure.
 */     

@pih1[atomic]
+!init_handle(Gold) 
  :  .desire(near(_,_)) 
  <- .print("Dropping near(_,_) desires and intentions to handle ",Gold);
     .drop_desire(near(_,_));
     !init_handle(Gold).
@pih2[atomic]
+!init_handle(Gold)
  :  pos(X,Y)
  <- .print("Going for ",Gold);
     !!handle(Gold). // must use !! to perform "handle" as not atomic

+!handle(gold(X,Y)) 
  <- .print("Handling ",gold(X,Y)," now.");
  	 +handling_gold(X,Y);
     !pos(X,Y);
     !ensure(pick,gold(X,Y));
     !pos(0,0);
     !ensure(drop, 0);
     -handling_gold(X,Y);
     .print("Finish handling ",gold(X,Y));
     !!choose_gold.

// if ensure(pick/drop) failed, pursue another gold
-!handle(G) : G
  <- .print("failed to catch gold ",G);
     .abolish(G); // ignore source
     !!choose_gold.
-!handle(G) : true
  <- .print("failed to handle ",G,", it isn't in the BB anyway");
     !!choose_gold.
//
///* The next plans deal with picking up and dropping gold. */
//
+!ensure(pick,_) : pos(X,Y) & gold(X,Y)
  <- pick; 
     ?carrying_gold;
     +carrying(X,Y); 
     -gold(X,Y);
     -changed;
     announcePick(X,Y).
// fail if no gold there or not carrying_gold after pick! 
// handle(G) will "catch" this failure.


// qui recupero anche le coordinate dell'oro che sto trasportando,
// cos� posso soddisfare tutti i commitment che ne fanno parte
+!ensure(drop, _) : carrying(X,Y) & pos(0,0)
  <- drop;  	 
  	 +dropped(X,Y);
  	 -carrying(X,Y);
  	 announceDrop(X, Y).



/* The next plans encode how the agent can choose the next gold piece 
 * to pursue (the closest one to its current position) or, 
 * if there is no known gold location, makes the agent believe it is free.
 */
+!choose_gold 
  :  not gold(_,_)
  <- -+free.
//
//// Finished one gold, but others left
//// find the closest gold among the known options, 
//// in particolare ci dev'essere commitment mio a prenderlo,
//// nel qual caso rendo vero l'antecedente e chiedo di prenotarlo.
//// non deve essere gi� prenotato da altri
+!choose_gold 
  :  gold(X,Y) & enactment_id(My_Role_Id)
  	<-	.print("cerco un nuovo gold");
  		.findall(gold(X,Y),gold(X,Y),LG);
	     !calc_gold_distance(LG,LD);
	     .length(LD,LLD); LLD > 0;
	     .print("Gold distances: ",LD,LLD);
	     .min(LD,d(_,gold(X,Y)));
	     .print("Next gold is ",X,Y);	     
	     commitToDropGoldImmediately(X,Y).
	     //!!init_handle(gold(X,Y)).
	     
	     
-!choose_gold <- .print("sono libero."); -+free.
//
+!calc_gold_distance([],[]).
+!calc_gold_distance([gold(GX,GY)|R],[d(D,gold(GX,GY))|RD]) 
  :  pos(IX,IY)
  <- jia.dist(IX,IY,GX,GY,D);
     !calc_gold_distance(R,RD).
+!calc_gold_distance([_|R],RD) 
  <- !calc_gold_distance(R,RD).


/* The following plans encode how an agent should go to near a location X,Y. 
 * Since the location might not be reachable, the plans succeed 
 * if the agent is near the location, given by the internal action jia.neighbour, 
 * or if the last action was skip, which happens when the destination is not 
 * reachable, given by the plan next_step as the result of the call to the 
 * internal action jia.get_direction.
 * These plans are only used when exploring the grid, since reaching the 
 * exact location is not really important.
 */

+!go_near(X,Y) : free
  <- -near(_,_); 
     -last_dir(_); 
     !near(X,Y).


// I am near to some location if I am near it 
+!near(X,Y) : (pos(AgX,AgY) & jia.neighbour(AgX,AgY,X,Y)) 
   <- .print("I am at ", "(",AgX,",", AgY,")", " which is near (",X,",", Y,")");
      +near(X,Y).
   
// I am near to some location if the last action was skip 
// (meaning that there are no paths to there)
+!near(X,Y) : pos(AgX,AgY) & last_dir(skip) 
   <- .print("I am at ", "(",AgX,",", AgY,")", " and I can't get to' (",X,",", Y,")");
      +near(X,Y).

+!near(X,Y) : not near(X,Y)
   <- !next_step(X,Y);
      !near(X,Y).
+!near(X,Y) : true 
   <- !near(X,Y).


/* These are the plans to have the agent execute one step in the direction of X,Y.
 * They are used by the plans go_near above and pos below. It uses the internal 
 * action jia.get_direction which encodes a search algorithm. 
 */

+!next_step(X,Y) : pos(AgX,AgY) // I already know my position
   <- jia.get_direction(AgX, AgY, X, Y, D);
      -+last_dir(D);
      D.
+!next_step(X,Y) : not pos(_,_) // I still do not know my position
   <- !next_step(X,Y).
-!next_step(X,Y) : true  // failure handling -> start again!
   <- -+last_dir(null);
      !next_step(X,Y).
  

/* The following plans encode how an agent should go to an exact position X,Y. 
 * Unlike the plans to go near a position, this one assumes that the 
 * position is reachable. If the position is not reachable, it will loop forever.
 */

+!pos(X,Y) : pos(X,Y) 
   <- .print("I've reached ",X,"x",Y).
+!pos(X,Y) : not pos(X,Y)
   <- !next_step(X,Y);
      !pos(X,Y).
      
      

/* end of a simulation */

+end_of_simulation(S,_) : true 
  <- .drop_all_desires; 
     .abolish(gold(_,_));
     .abolish(picked(_));
     -+free;
     .print("-- END ",S," --").

package jia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
import jason.environment.grid.Location;
import jason.asSyntax.Atom;
import jason.asSyntax.Structure;
import jason.asSyntax.NumberTermImpl;



public class getCoord2 extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] terms) throws Exception {
        try {
        	String t = terms[0]+"";// pick(12,12) AND drop(12,12)
        	//System.out.println("Stampa termine: "+t);
            t = t.substring(8, t.indexOf(")",4)); //12,12
            String t1 = t.substring(0,t.indexOf(","));
            String t2 = t.substring(t.indexOf(",")+1,t.length());  
            return un.unifies(terms[1], new NumberTermImpl(Double.parseDouble(t2)));
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }
}

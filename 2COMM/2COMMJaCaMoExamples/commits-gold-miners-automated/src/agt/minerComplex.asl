// miner agent

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("miner.asl") }

// se sto gia' portando oro, lo comunico agli altri tramite bid "finta"
// R3
+cell(X,Y,gold) 
	:	cc(My_Role_Id,_,Renounce,Drop,"DETACHED") 
		& enactment_id(My_Role_Id)
		& .term2string(Drop, T)
		& jia.getCoord1(T, OldX)
		& jia.getCoord2(T, OldY)
		//& carrying(OldX,OldY) // provo a mettere PICK invece di CARRYING
		& not offer(X,Y,_,My_Role_Id)
		& pick(OldX,OldY)
	<- 	.print("sto TRASPORTANDO, faccio OFFER FINTA per ",X,"," ,Y);
		.print("Valore Pic: ",OldX,OldY);
		volunteer(X,Y,1000).

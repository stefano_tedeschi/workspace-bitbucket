package mining;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.Commitment;
import twocomm.core.SocialStateElementType;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.LogicalExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import cartago.OPERATION;


import jason.asSyntax.Term;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Structure;


public class MiningProtocol extends ProtocolArtifact {

	public static String ARTIFACT_TYPE = "MiningProtocol";
	public static String MINER_ROLE = "miner";			
	private int numberOfMiners = 4;
	private final int TEMP_LIMIT = 0;
	
	private boolean [][] golds = new boolean[50][50];
	
	
	static {
		addEnabledRole(MINER_ROLE, Miner.class);
	}

	public MiningProtocol() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);
	}

	@Override
	public String getArtifactType() {	
		return ARTIFACT_TYPE;
	}
	
	@OPERATION
    public void init() {
        super.init();
    }
	
	
	@OPERATION
	public void withdraw(int x, int y) { // x e y sono le coordinate del vecchio gold
	
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());
		CompositeExpression cons;
		Fact f;
		List<Commitment> comms = socialState.retrieveCommitmentsByDebtorRoleId(askingMiner);
		if (comms != null) {
			for (Commitment c : comms) {
				cons = (CompositeExpression)(c.getConsequent());
				f = (Fact)(cons.getLeft());
				if ((int)(f.getArguments()[0]) == x && (int)(f.getArguments()[1]) == y) {
					//System.out.println("COMM DA CANCELLARE: "+c); 
						cancelCommitment(c);		
				}
			}		
		}
		golds[x][y] = false;
	}
	
	
	@OPERATION
	public void agree(int x, int y) {
		RoleId agreeMiner = getRoleIdByPlayerName(getOpUserName());	
		try {
			Fact f = new Fact("agree", x, y, agreeMiner.toString());
			assertFact(f);					
			
					
			// controllo se ho tutte le renounces
//			List<Fact> allFacts = getAllFactsWithName(f);
//			int renounces = 0;
//			
//			for (Fact f1 : allFacts) {
//				
//				if ((int)(f1.getArguments()[0]) == x && (int)(f1.getArguments()[1]) == y) {
//					renounces++;
//				}
//			}
//			if (renounces == numberOfMiners-1) assertFact(new Fact("renounceGroup",x,y));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void announcePick(int x, int y) {
		RoleId pickingMiner = getRoleIdByPlayerName(getOpUserName());	
		try {
			Fact f = new Fact("pick",x,y);
			assertFact(f);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void announceDrop(int x, int y) {
		RoleId droppingMiner = getRoleIdByPlayerName(getOpUserName());	
		Fact f = null;
		try {
			f = new Fact("drop", x, y);
			assertFact(f);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
		
	// operazione che fa la OFFER con la distanza e crea commitment a: se mi si assegna il gold, allora mi impegno a portarlo
	// al depot
	@OPERATION
	public void offer(int x, int y, int dist) {
		try {
			CompositeExpression cons = null;
//			Fact ant = new Fact("renounceGroup",x,y);
			RoleId offeringMiner = getRoleIdByPlayerName(getOpUserName());
			RoleId groupMiner = new RoleId(MINER_ROLE);			
			
			
			ArrayList<RoleId> enactedMiners = getRoleIdByGenericRoleName(MINER_ROLE);
			LogicalExpression antec = new Fact("true");
//			CompositeExpression antec = new CompositeExpression(LogicalOperatorType.AND,
//					new Fact("renounce",x,y,enactedMiners.get(0)), new Fact("renounce",x,y,enactedMiners.get(1)));
			
			
			for (int i = 0;i<numberOfMiners-TEMP_LIMIT;i++) {
				if (!(enactedMiners.get(i).equals(offeringMiner))) { // non aggiungo elementi all'espressione per il ROleId richiedente
					CompositeExpression acc = new CompositeExpression(LogicalOperatorType.AND, 
							new Fact("agree",x,y,enactedMiners.get(i).toString()),antec);
							antec = acc;
				}
						
			}
			
			cons = new CompositeExpression(LogicalOperatorType.THEN, new Fact("pick", x, y), new Fact("drop", x, y));
			//ant = new Fact("renounceGroup", x, y); // questa rappresenta semplicemente l'AND di tutte le renounce degli agenti
			Commitment groupCommit = new Commitment(offeringMiner, groupMiner, antec, cons);			
			
			createAllCommitments(groupCommit);	
			assertFact(new Fact("offer",x,y,dist,offeringMiner.toString()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@OPERATION
	public void volunteer(int x, int y, int dist) {
		try {
			
			boolean temp = false;
			synchronized (golds) {
				temp = golds[x][y];
				golds[x][y] = true;
			}
			if (temp) return;
			
			CompositeExpression cons = null;
//			Fact ant = new Fact("renounceGroup",x,y);
			RoleId volunteerMiner = getRoleIdByPlayerName(getOpUserName());
			RoleId groupMiner = new RoleId(MINER_ROLE);			
			
			ArrayList<RoleId> enactedMiners = getRoleIdByGenericRoleName(MINER_ROLE);
			LogicalExpression antec = new Fact("true");
//			CompositeExpression antec = new CompositeExpression(LogicalOperatorType.AND,
//					new CompositeExpression(LogicalOperatorType.AND, new Fact("renounce"), new Fact("true")), new Fact("true"));
//			CompositeExpression antec = new CompositeExpression(LogicalOperatorType.AND,
//					new Fact("renounce",x,y,enactedMiners.get(0)), new Fact("renounce",x,y,enactedMiners.get(1)));
//			
			
			for (int i = 0;i<numberOfMiners-TEMP_LIMIT;i++) {
				if (!(enactedMiners.get(i).equals(volunteerMiner))) { // non aggiungo elementi all'espressione per il ROleId richiedente
					CompositeExpression acc = new CompositeExpression(LogicalOperatorType.AND, 
							new Fact("agree",x,y,enactedMiners.get(i).toString()),antec);
							antec = acc;
				}
						
			}
			
			cons = new CompositeExpression(LogicalOperatorType.THEN, new Fact("pick", x, y), new Fact("drop", x, y));
			//ant = new Fact("renounceGroup", x, y); // questa rappresenta semplicemente l'AND di tutte le renounce degli agenti
			Commitment groupCommit = new Commitment(volunteerMiner, groupMiner, antec, cons);			
			
			createAllCommitments(groupCommit);	
			assertFact(new Fact("offer",x,y,dist,volunteerMiner.toString()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public class Miner extends PARole {

		public Miner(String playerName, IPlayer player) {
			super(MINER_ROLE, player);
		}
	}
}

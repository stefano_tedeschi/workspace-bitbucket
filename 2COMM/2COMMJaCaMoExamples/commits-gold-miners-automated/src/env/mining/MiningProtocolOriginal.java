package mining;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.Commitment;
import twocomm.core.SocialStateElementType;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import cartago.OPERATION;


import jason.asSyntax.Term;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Structure;


public class MiningProtocolOriginal extends ProtocolArtifact {

	public static String ARTIFACT_TYPE = "MiningProtocol";
	public static String MINER_ROLE = "miner";
	
	
	private ArrayList<Gold> golds = new ArrayList<Gold>();
	
	private int numberOfMiners = 4;
	private boolean[] renounces = new boolean[numberOfMiners];
	
	static {
		addEnabledRole(MINER_ROLE, Miner.class);
	}

	public MiningProtocolOriginal() {
		super();
		socialState = new AutomatedSocialState(this);
	}

	@Override
	public String getArtifactType() {	
		// TODO Auto-generated method stub
		return ARTIFACT_TYPE;
	}
	
	@OPERATION
    public void init() {
        super.init();
    }

	
	
	
	@OPERATION
	public void stampa(String test) {
		System.out.println(test);
			
	}
	
	
	
	
	
	@OPERATION
	public void withdraw(int x, int y) { // x e y sono le coordinate del vecchio gold
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());
				
		for (Commitment c : socialState.retrieveCommitmentsByDebtorRoleId(askingMiner)) {
			logger.debug("COMM DA CANCELLARE: "+c); // TODO: controllo con X e Y
				cancelCommitment(c);			
		}		
	}
	
	
	@OPERATION
	public void renounce(int x, int y) {
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());	
		try {
			Fact f = new Fact("renounce", x, y, askingMiner.toString());
			assertFact(f);					
			
			// controllo se ho tutte le renounces
			List<Fact> allFacts = getAllFactsWithName(f);
			int renounces = 0;
			
			for (Fact f1 : allFacts) {
				
				if ((int)(f1.getArguments()[0]) == x && (int)(f1.getArguments()[1]) == y) {
					renounces++;
				}
			}
			System.out.println("numero rinunce: "+renounces+" per "+x+","+y);
			if (renounces == numberOfMiners-1) {
				assertFact(new Fact("renounceGroup",x,y));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				System.out.println(socialState);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void announcePick(int x, int y) {
		try {
			Fact f = new Fact("pick",x,y);
			assertFact(f);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void announceDrop(int x, int y) {
		Gold g = new Gold(x, y);
		Fact f = null;
		try {
			f = new Fact("drop", x, y);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RoleId askingMiner = getRoleIdByPlayerName(getOpUserName());
		// recupero commitment che hanno richiedente come debitore
		List<Commitment> comms = socialState.retrieveCommitmentsByDebtorRoleId(askingMiner);
		
		// quel commitment che e' detached avra' il conseguente relativo all'oro che si sta per rilasciare
//		for (Commitment comm : comms) {
//			if (comm.getLifeCycleStatus() == LifeCycleState.DETACHED) {
//				f = (Fact)(comm.getConsequent());
//			}
//				
//		}
		assertFact(f);	
		golds.remove(g);
//		try {
//			Thread.sleep(500);
//		} catch (Exception e) {}
//		System.out.println(socialState);
		
	}
	
		
	// operazione che fa la bid con la distanza e crea commitment a: se mi si assegna il gold, allora mi impegno a portarlo
	// al depot
	@OPERATION
	public void bid(int x, int y, int dist) {
		try {
			CompositeExpression cons = null;
			Fact ant = null;
			RoleId biddingMiner = getRoleIdByPlayerName(getOpUserName());
			RoleId groupMiner = new RoleId(MINER_ROLE);
			
			Gold goldToBid = new Gold(x,y);
			for (Gold g1 : golds) {
				if (g1.equals(goldToBid)) {
					goldToBid = g1;
					break;
				}
					
			}		
			 
			
			
			goldToBid.bid(biddingMiner, dist);
			
			cons = new CompositeExpression(LogicalOperatorType.THEN, new Fact("pick", x, y), new Fact("drop", x, y));
			ant = new Fact("renounceGroup", x, y); // questa rappresenta semplicemente l'AND di tutte le renounce degli agenti
			Commitment groupCommit = new Commitment(biddingMiner, groupMiner, ant, cons);
			
			System.out.println("Richiesta creazione commitment:");
			System.out.println(groupCommit);
			System.out.println("Agente richiedente: "+biddingMiner);
			
			
			createAllCommitments(groupCommit);	
			assertFact(new Fact("bid",x,y,dist,biddingMiner.toString()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}
	

	
	
	// BEGIN INNER CLASSES for ROLES


	public class Miner extends PARole {

		public Miner(String playerName, IPlayer player) {
			super(MINER_ROLE, player);
		}
	}


	

	public interface MinerObserver extends ProtocolObserver {

	}

	
	private class Gold {
		
		int x;
		int y;
		boolean booked = false;
		int numberOfBidders = 0;
		//HashMap<Integer, RoleId> bidders = new HashMap<Integer, RoleId>();
		RoleId winningBidder;
		int lowestBid = 1000;
		
		public Gold (int x, int y) {
			this.x = x;
			this.y = y;
			
		}

		public int getX() {
			return x;
		}



		public int getY() {
			return y;
		}

		
		public int getNumberOfBidders() {
			return this.numberOfBidders;
		}
		
		
		// in questa versione non si fa controllo che stesso agente faccia piu' di una bid
		public void bid(RoleId role, int distance) {
			//bidders.put(distance, role);
			numberOfBidders++;
			if (distance < lowestBid) {
				winningBidder = role;
				lowestBid = distance;
			}
		}
		
		public RoleId getWinningBidder() {
			return this.winningBidder;
		}
		
		public boolean isBooked() {
			return booked;
		}

		public void book() {
			this.booked = true;
		}

		public boolean equals(Gold g) {
			return this.x == g.x && this.y == g.y;
		}
		
		public String toString() {
			return this.x + ","+this.y;
		}
		
	}
	

}

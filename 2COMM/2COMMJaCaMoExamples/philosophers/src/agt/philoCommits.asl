{ include("$jacamoJar/templates/common-cartago.asl") }

counter(0).

!start.

+!start
	<- 	//focusWhenAvailable("philo");
		enact("philosopher").

+enacted(Id,"philosopher",Role_Id)
	<-	+enactment_id(Role_Id);
		.my_name(Me);
		.print(Me, " as ",Role_Id);
		in("philo_init",Me,Left,Right);
		+my_left_fork(Left);
		+my_right_fork(Right);
		println(Me," ready.");
		!!living.

+!living
	: counter(C) 
	<- 	print(C);
		!thinking;
		!eating.

+!eating
	:	my_left_fork(F1) & my_right_fork(F2) & counter(C)
	<- 	.my_name(Me); ?enactment_id(Role_Id);
		println(Me, " ", Role_Id, " asking for eating with ", F1, " and ", F2, " for the ",C,"th time.");
		askForks(F1, F2, C).

+cc(My_Role_Id, "philosopher", AvailableForks, ReturnForks, "DETACHED")
	:	enactment_id(My_Role_Id) & my_left_fork(F1) & my_right_fork(F2) & counter(C)
		& .term2string(Term1,AvailableForks) & Term1 = available(F1,F2,C)
	    & .term2string(Term2,ReturnForks) & Term2 = returnForks(F1,F2,C)
	<- 	!eat(F1, F2, C);
	    returnForks(F1, F2, C).

+cc(My_Role_Id, "philosopher", AvailableForks, ReturnForks, "SATISFIED")
	:	enactment_id(My_Role_Id) & my_left_fork(F1) & my_right_fork(F2) & counter(C)
	    & .term2string(Term1,AvailableForks) & Term1 = available(F1,F2,C)
	    & .term2string(Term2,ReturnForks) & Term2 = returnForks(F1,F2,C) 
	<- 	?counter(C);
		-+counter(C+1);
		!living.

+!eat(F1, F2, C)
    :   my_left_fork(F1) & my_right_fork(F2) & available(F1, F2, C) & counter(C)
	<-	.my_name(Me);
		?enactment_id(Role_Id); println(Me, " ", Role_Id, " eating with ", F1, " and ", F2, ".").
		

+!thinking
	: counter(C)
	<-	.my_name(Me); ?enactment_id(Role_Id); println(Me, " ", Role_Id, " thinking, time ",C).
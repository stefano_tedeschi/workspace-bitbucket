package dinner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.ProtocolArtifact.PARole;
import twocomm.core.ProtocolArtifact.ProtocolObserver;
import twocomm.core.RoleId;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.Fact;
import twocomm.exception.MissingOperandException;
import cartago.AgentId;
import cartago.OPERATION;

public class PhiloProtocol extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(PhiloProtocol.class);
	public static String ARTIFACT_TYPE = "PhiloProtocol";
	public static String WAITER_ROLE = "waiter";
	public static String PHILOSOPHER_ROLE = "philosopher";

	private int numberOfPhilosophers = 0;
	
	static {
		addEnabledRole(WAITER_ROLE, Waiter.class);
		addEnabledRole(PHILOSOPHER_ROLE, Philosopher.class);
	}

	public PhiloProtocol() {
		super();
	}

	@Override
	public String getArtifactType() {
		// TODO Auto-generated method stub
		return ARTIFACT_TYPE;
	}

	
	// ROLES OPERATIONS
	// PHILOSOPHER
	@OPERATION
	public void askForks(int forkLeft, int forkRight, int counter) {
		RoleId askingPhilo = getRoleIdByPlayerName(getOpUserName());
		logger.trace("OPERATION PERFORMED: ASK_FORKS by "+askingPhilo.getPlayerName());
		String preCond = "available("+forkLeft+","+forkRight+","+counter+")";
 		try {
 			//ArrayList<Commitment> addedCommits = createAllCommitments(new Commitment(askingPhilo, dest, preCond, "eat"));
 			// Il commit di gruppo si pu� creare subito qui, a questo punto tanto li ho creati tutti di sicuro
 			RoleId groupPhilo = new RoleId(PHILOSOPHER_ROLE);
 			Fact cons = new Fact("returnForks",forkLeft,forkRight,counter);
 			Commitment groupCommit = new Commitment(askingPhilo, groupPhilo, preCond, cons);
 			createCommitment(groupCommit);
 			inImpl("ticket");
 			inImpl("fork", forkLeft);
 			inImpl("fork", forkRight);
 			// in realt� qui lo asserisce la prima volta. le altre?
 			// non posso usare il counter dell'agente perch� questo available non � privato ma
 			// sociale.
 			assertFact(new Fact("available",forkLeft,forkRight,counter));
// 			for (Commitment com : addedCommits) {
// 				removeObsPropertyByTemplate("cc", com.getDebtor().getCanonicalName(), com.getCreditor().getCanonicalName(),
//						com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
//				com.setStatus(LifeCycleState.DETACHED);
//				//com.setAntecedent(new Fact("true"));
//				defineObsProperty("cc", com.getDebtor().getCanonicalName(), com.getCreditor().getCanonicalName(),
//						com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
// 			}
 			removeObsPropertyByTemplate("cc", groupCommit.getDebtor().toString(), groupCommit.getCreditor().toString(),
 					groupCommit.getAntecedent().toString(), groupCommit.getConsequent().toString(), groupCommit.getLifeCycleStatus().toString());
 			groupCommit.setStatus(LifeCycleState.DETACHED);
 			defineObsProperty("cc", groupCommit.getDebtor().toString(), groupCommit.getCreditor().toString(),
 					groupCommit.getAntecedent().toString(), groupCommit.getConsequent().toString(),
 					groupCommit.getLifeCycleStatus().toString());
			assertFact(new Fact("askForks", askingPhilo, forkLeft, forkRight, counter));
			
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void returnForks(int forkLeft, int forkRight, int counter) {
		RoleId askingPhilo = getRoleIdByPlayerName(getOpUserName());
		try {
			assertFact(new Fact("returnForks",forkLeft,forkRight,counter));
			removeObsPropertyByTemplate("available", forkLeft, forkRight, counter);
			
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(askingPhilo)) {
				if (com.getLifeCycleStatus().equals(LifeCycleState.DETACHED)) {
						removeObsPropertyByTemplate("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());
						com.setStatus(LifeCycleState.SATISFIED);
						defineObsProperty("cc", com.getDebtor().toString(), com.getCreditor().toString(),
								com.getAntecedent().toString(), com.getConsequent().toString(), com.getLifeCycleStatus().toString());

				}
			}
			
			outImpl("fork", forkLeft);
			outImpl("fork", forkRight);
			outImpl("ticket");	
			//defineObsProperty("cleaned", forkLeft, forkRight, counter);
			
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	// WAITER:
	/*
	 * Il problema qui � che non basta avere due forchette "qualsiasi". Ogni filosofo 
	 * condivide una forchetta col suo vicino, e si deve catturare questo aspetto, onde
	 * evitare di "snaturare" l'essenza del problema. Questo coincide con una situazione
	 * in cui le risorse sono condivise in potenza a due a due tra tutti i partecipanti,
	 * in modo circolare (l'ultimo partecipante condivide col primo). E' quindi
	 * avere, con n filosofi, n-1 problemi di sincronizzazione, reciprocamente
	 * influenti. Il Waiter prepara l'ambiente in base al numero che richiede all'artefatto.
	 */
	@OPERATION
	public void prepareTable(int numberOfPhilosophers) {
		this.numberOfPhilosophers = numberOfPhilosophers;
		for (int i=0;i<numberOfPhilosophers;i++) {
			outImpl("fork", i);
			outImpl("philo_init", "philosopher"+(i+1), i, (i+1)%numberOfPhilosophers);
		}
		for (int i=0;i<numberOfPhilosophers-1;i++) {
			outImpl("ticket");
		}
			
	}
	
	
	
	
	
	// BEGIN INNER CLASSES for ROLES


	public class Waiter extends PARole {

		public Waiter(String playerName, IPlayer player) {
			super(WAITER_ROLE, player);
		}
	}



	public class Philosopher extends PARole {

		public Philosopher(String playerName, IPlayer player) {
			super(PHILOSOPHER_ROLE, player);
		}

	}

	public interface WaiterObserver extends ProtocolObserver {

	}

	public interface PhilosopherObserver extends ProtocolObserver {

	}

}

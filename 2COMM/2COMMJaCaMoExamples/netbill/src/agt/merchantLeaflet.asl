{ include("$jacamoJar/templates/common-cartago.asl") }

!pay.

/* Plans */

+!pay : true
	<- 	enact("merchant").
		
+enacted(Id,"merchant",Role_Id)
	<-	.wait(500);
		+enactment_id(Role_Id);
		sendQuote("item", "5000"). //mando subito una quote per l'oggetto

+cc(My_Role_Id, Customer_Role_Id, _, "goods","DETACHED")
	:	enactment_id(My_Role_Id) 
	<-  sendGoods("item").
		
+cc(My_Role_Id, Customer_Role_Id, "paid", "receipt","DETACHED")
	:	enactment_id(My_Role_Id)
	<-  sendReceipt.

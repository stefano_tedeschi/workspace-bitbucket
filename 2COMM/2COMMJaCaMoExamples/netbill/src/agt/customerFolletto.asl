{ include("$jacamoJar/templates/common-cartago.asl") }

!buy.

/* Plans */

+!buy : true
	<- 	enact("customer").

+enacted(Id,"customer",My_Role_Id)
	<-	+enactment_id(My_Role_Id).

// ho solo visto la merce arrivare. chiedo la quote
+goods(Good)
	:	enactment_id(My_Role_Id)
	<-	sendRequest(Good).
	
+cc(Merchant_Role_Id, My_Role_Id, "paid", "receipt","CONDITIONAL")
	:	enactment_id(My_Role_Id) 
        & sendQuote(Item,Price,Merchant_Role_Id)
	<-  sendAccept(Item, Price);
		sendEPO. 
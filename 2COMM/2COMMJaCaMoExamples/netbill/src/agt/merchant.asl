{ include("$jacamoJar/templates/common-cartago.asl") }

!sell.

/* Plans */

+!sell : true
	<- 	enact("merchant").
		
+enacted(Id,"merchant",Role_Id)
	<-	+enactment_id(Role_Id).
		
// quando vedo una requestedQuote, mando una quote. Prezzo a 5000
+requestedQuote(Item,Merchant)
	<-	sendQuote(Item, "5000").


+cc(My_Role_Id, Customer_Role_Id, _, "goods","DETACHED")
	:	enactment_id(My_Role_Id) 
	<-  sendGoods("item").
		
+cc(My_Role_Id, Customer_Role_Id, "paid", "receipt","DETACHED")
	:	enactment_id(My_Role_Id)
	<-  sendReceipt.

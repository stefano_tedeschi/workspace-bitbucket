{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

/* Plans */

+!start : true
	<- 	enact("merchant").
		
+enacted(Id,"merchant",Role_Id)
	<-	+enactment_id(Role_Id);
	!sell.
	
// ************** VERSIONE UNIFICATA DEL MERCHANT
// LASCIARE NON COMMENTATO SOLO UNO TRA I SEGUENTI BLOCCHI DI CODICE

// caso 1, base
+!sell <- true. 

// caso 2, leaflet
//+!sell <- sendQuote("item", "5000"). 

// caso 3, folletto
//+!sell <- sendGoods("item"). 
//********************  DA QUI IN POI IL CODICE RIMANE LO STESSO

		
// quando vedo una requestedQuote, mando una quote. Prezzo a 5000
+requestedQuote(Item,Merchant)
	<-	sendQuote(Item, "5000").


+cc(My_Role_Id, Customer_Role_Id, _, "goods","DETACHED")
	:	enactment_id(My_Role_Id) 
	<-  sendGoods("item").
		
+cc(My_Role_Id, Customer_Role_Id, "paid", "receipt","DETACHED")
	:	enactment_id(My_Role_Id)
	<-  sendReceipt.

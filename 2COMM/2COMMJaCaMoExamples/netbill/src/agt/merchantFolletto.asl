{ include("$jacamoJar/templates/common-cartago.asl") }

!sell.

/* Plans */

+!sell : true
	<- 	enact("merchant").
		
+enacted(Id,"merchant",Role_Id)
	<-	.wait(500);
		+enactment_id(Role_Id);
		sendGoods("item").

// la sendQuote genera solo requestQuote, quindi posso usare solo questo fatto per la sendQuote
+requestedQuote(Item,Merchant)
	<-	sendQuote(Item, "5000").
	
+cc(My_Role_Id, Customer_Role_Id, "paid", "receipt","DETACHED")
	:	enactment_id(My_Role_Id)
	<-  sendReceipt.

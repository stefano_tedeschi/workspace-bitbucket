{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

/* Plans */

+!start : true
	<- 	enact("customer").

+enacted(Id,"customer",My_Role_Id)
	<-	+enactment_id(My_Role_Id); // serve perche' enact e' una signal e va aggiunta come mental note
		!buy.

// ************** VERSIONE UNIFICATA DEL CUSTOMER
// LASCIARE NON COMMENTATO SOLO UNO TRA I SEGUENTI BLOCCHI DI CODICE
// caso 1, base
+!buy
	<-	sendRequest("item").
	
// casi 2 e 3, leaflet e folletto
//+!buy
//	<-	true.
//********************  DA QUI IN POI IL CODICE RIMANE LO STESSO

+goods(Good)
	:	enactment_id(My_Role_Id)
	& not acceptedQuotation(Item,Price)
	<-	.print("ole");
		sendRequest(Good).

// qui si lavora sui commitment. questo agente
// risponde al commitment del mercant di inviare la merce se riceve il pagamento 

+cc(Merchant_Role_Id, My_Role_Id, AcceptedQuotation, "goods","CONDITIONAL")
	:	enactment_id(My_Role_Id) 
        & sendQuote(Item,Price,Merchant_Role_Id)
        & .term2string(Term1,AcceptedQuotation)
        & Term1 = acceptedQuotation(A,B)
        & not goods(Item)
	<-  .print("accetto, cc era conditional");
		sendAccept(Item, Price).   

+cc(Merchant_Role_Id, My_Role_Id, AcceptedQuotation, "goods", "SATISFIED")
    :    enactment_id(My_Role_Id)
        & .term2string(Term1,AcceptedQuotation)
        & Term1 = acceptedQuotation(A,B)
        & sendQuote(Item,Price,Merchant_Role_Id)
        & not acceptedQuotation(_,_)
    <-  .print("accetto, cc era satisfied");
    	sendAccept(Item, Price). 

+cc(My_Role_Id, Merchant_Role_Id, "goods", "paid","DETACHED")
	:	enactment_id(My_Role_Id)        
	<-  sendEPO.  
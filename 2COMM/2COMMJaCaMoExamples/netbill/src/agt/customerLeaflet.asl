{ include("$jacamoJar/templates/common-cartago.asl") }

!startRequest.

/* Plans */

+!startRequest : true
	<- 	enact("customer").

+enacted(Id,"customer",My_Role_Id)
	<-	+enactment_id(My_Role_Id).

+cc(Merchant_Role_Id, My_Role_Id, AcceptedQuotation, "goods","CONDITIONAL")
	:	enactment_id(My_Role_Id) 
        & sendQuote(Item,Price,Merchant_Role_Id)
        & .term2string(Term1,AcceptedQuotation)
        & Term1 = acceptedQuotation(A,B)
	<-  sendAccept(Item, Price).  
	
+cc(My_Role_Id, Merchant_Role_Id, "goods", "paid","DETACHED")
	:	enactment_id(My_Role_Id)        
	<-  sendEPO.  
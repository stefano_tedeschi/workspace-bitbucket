package prot;

import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.Commitment;
import twocomm.core.LifeCycleState;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.AgentId;
import cartago.OPERATION;

public class NetbillProtocol extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(NetbillProtocol.class);
	public static String ARTIFACT_TYPE = "Netbill";
	public static String MERCHANT_ROLE = "merchant";
	public static String CUSTOMER_ROLE = "customer";

	static {
		addEnabledRole(MERCHANT_ROLE, Merchant.class);
		addEnabledRole(CUSTOMER_ROLE, Customer.class);
	}

	public NetbillProtocol() {
		super();
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	// ROLES OPERATIONS

	// merchant.sendQuote eccezione se item � diverso da quello della sendrequest
	@OPERATION
	public void sendQuote(String item, String price) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByGenericRoleName(CUSTOMER_ROLE).get(0);
				
		try {
			Commitment c = new Commitment(merchant, customer, 
					new Fact("acceptedQuotation", item, price), "goods");			
			createCommitment(c);
			if (this.socialState.existsFact(new Fact("goods")))  {
				satisfyCommitment(c);
			}
			createCommitment(new Commitment(merchant, customer, "paid", "receipt"));
			assertFact(new Fact("sendQuote", item, price, merchant.toString()));
			assertFact(new Fact("quotation", item, price, merchant.toString()));
			logger.trace("OPERATION PERFORMED: SEND_QUOTE ("+item+","+price+") by " + merchant.getPlayerName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			log(Arrays.toString(e.getStackTrace()));
		}
	}

	// merchant.sendGoods
	@OPERATION
	public void sendGoods(String item) {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByGenericRoleName(CUSTOMER_ROLE).get(0);
		try {
			assertFact(new Fact("goods")); // commitment deve diventare satisfied
			assertFact(new Fact("goods",item));
			//createCommitment(new Commitment(merchant, customer, "paid", "receipt"));
			
			// COMMITMENT DA SODDISFARE: C(merc,cust,accepted...,goods)
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			if (socialState.retrieveCommitmentsByDebtorRoleId(merchant) != null) {
				for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(merchant)) {
					if (com.getConsequent().equals(new Fact("goods"))
							&& com.getCreditor().equals(customer)) {
						arrComm.add(com);
					}
				}
				for (Commitment com : arrComm) {
					satisfyCommitment(com);				
				}
				// COMMITMENT DA DETACH: C(cust,merch,goods,paid)
				arrComm = new ArrayList<Commitment>();
				List<Commitment> testComm = socialState.retrieveCommitmentsByCreditorRoleId(merchant);
				System.out.println("prova");
				
				for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(merchant)) {
					if (com.getAntecedent().equals(new Fact("goods"))
							&& com.getCreditor().equals(merchant)) {
						arrComm.add(com);
					}
				}
				for (Commitment com : arrComm) {
					detachCommitment(com);				
				}
				logger.trace("OPERATION PERFORMED: SEND_GOODS by " + getOpUserName());
			}
			else logger.trace("OPERATION PERFORMED: SEND_GOODS NO COMMITMENTS by " + getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// merchant.receipt
	@OPERATION
	public void sendReceipt() {
		RoleId merchant = getRoleIdByPlayerName(getOpUserName());
		RoleId customer = getRoleIdByGenericRoleName(CUSTOMER_ROLE).get(0);
		try {			
			
			assertFact(new Fact("receipt", merchant.toString()));
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			
			
			// Commitment con conseguente RECEIPT diventano satisfied
			arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(merchant)) {
				if (com.getConsequent().equals(new Fact("receipt"))
						&& com.getDebtor().equals(merchant)) {
					arrComm.add(com);
				}
			}
			for (Commitment com : arrComm) {
				satisfyCommitment(com);
			}
			logger.trace("OPERATION PERFORMED: SEND_RECEIPT by " + getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// customer.sendRequest
	@OPERATION
	public void sendRequest(String item) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {			
			// NON CI VA ANCHE COMMITMENT A FARE ACCEPT O REJECT?
			assertFact(new Fact("requestedQuote", item, customer.toString()));
			logger.trace("OPERATION PERFORMED: SEND_REQUEST by " + getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// customer.sendAccept FORSE NON SERVE DI NUOVO ITEM
	@OPERATION
	public void sendAccept(String item, String price) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		// defineObsProperty("accept", participant.toString());
		try {
			assertFact(new Fact("acceptedQuotation", price, customer.toString()));
			createCommitment(new Commitment(customer, merchant, "goods", "paid"));
			
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();			
			// Commitment con antecedente acceptedQuotation diventano detached
			for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(customer)) {
				if (com.getConsequent().equals(new Fact("goods"))
						&& com.getDebtor().equals(merchant)) {
					arrComm.add(com);
				}
			}
			for (Commitment com : arrComm) {
				detachCommitment(com);
			}
			
			// SE GOODS E' GIA' PRESENTE NEL SOCIAL STATE, COMMITMENT CREATO DIVENTA SUBITO DETACHED
			arrComm = new ArrayList<Commitment>();			
			// Commitment con antecedente acceptedQuotation diventano detached
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(customer)) {
				if (com.getAntecedent().equals(new Fact("goods"))
						&& com.getCreditor().equals(merchant)) {
					arrComm.add(com);
				}
			}
			for (Commitment com : arrComm) {
				detachCommitment(com);
			}
			
			logger.trace("OPERATION PERFORMED: SEND_ACCEPT "+item+" "+price+" by " + getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.sendReject FORSE NON SERVE DI NUOVO ITEM
	@OPERATION
	public void sendReject(String item, String price) {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		// defineObsProperty("accept", participant.toString());
		try {
			assertFact(new Fact("rejectedQuotation", price, customer.toString()));
			createCommitment(new Commitment(customer, merchant, "goods", "paid"));
			logger.trace("OPERATION PERFORMED: SEND_REJECT by " + getOpUserName());
			// TODO release dei commitments
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			logger.trace("Commitment already added: "+e.toString());
		}
	}
	
	// customer.sendEPO
	@OPERATION
	public void sendEPO() {
		RoleId customer = getRoleIdByPlayerName(getOpUserName());
		RoleId merchant = getRoleIdByGenericRoleName(MERCHANT_ROLE).get(0);
		try {			
			
			assertFact(new Fact("paid", customer.toString()));
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			
			// Commitment con antecedente paid diventano detached
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(merchant)) {
				if (com.getAntecedent().equals(new Fact("paid"))
						&& com.getDebtor().equals(merchant)) {
					arrComm.add(com);
				}
			}
			for (Commitment com : arrComm) {
				detachCommitment(com);
			}
			// Commitment con conseguente paid diventano satisfied
			arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(merchant)) {
				if (com.getConsequent().equals(new Fact("paid"))
						&& com.getCreditor().equals(merchant)) {
					arrComm.add(com);
				}
			}
			for (Commitment com : arrComm) {
				satisfyCommitment(com);
			}
			logger.trace("OPERATION PERFORMED: PAY by " + getOpUserName());
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// BEGIN INNER CLASSES for ROLES
	// cnp.Initiator role

	public class Merchant extends PARole {

		public Merchant(String playerName, IPlayer player) {
			super(MERCHANT_ROLE, player);
		}
	}

	// cnp.Participant role

	public class Customer extends PARole {

		public Customer(String playerName, IPlayer player) {
			super(CUSTOMER_ROLE, player);
		}

	}

	public interface MerchantObserver extends ProtocolObserver {

	}

	public interface CustomerObserver extends ProtocolObserver {

	}

}

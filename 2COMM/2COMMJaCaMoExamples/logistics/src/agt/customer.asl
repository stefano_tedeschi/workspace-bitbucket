{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("common.asl") }

!start.

+!start
	 : focused(_,c1,C1ArtId) &
	   focused(_,c2,C2ArtId)
	<- enact("creditorC1")[artifact_id(C1ArtId)];
	   enact("debtorC2")[artifact_id(C2ArtId)].
	   
+cc(_,MyId,"pay500(s)","at(pkg,D)","CONDITIONAL")
	 : enactment_id(MyId,c1,ArtId)
	<- println("I'm interested in the product! Creating C2...");
	   createC2;
	   println("Paying 500 for the product...");
	   pay(500,"s").
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("common.asl") }

+pay(What,Who)
	 : focused(_,c1,C1ArtId) &
	   focused(_,c2,C2ArtId) &
	   focused(_,c3,C3ArtId) &
	   focused(_,c4,C4ArtId) &
	   focused(_,c5,C5ArtId) &
	   focused(_,c6,C6ArtId) &
	   focused(_,c7,C7ArtId) &
	   focused(_,c8,C8ArtId)
	<- .concat("pay",What,Pred);
	   assertPay(Pred,Who)[artifact_id(C1ArtId)];
	   assertPay(Pred,Who)[artifact_id(C2ArtId)];
	   assertPay(Pred,Who)[artifact_id(C3ArtId)];
	   assertPay(Pred,Who)[artifact_id(C4ArtId)];
	   assertPay(Pred,Who)[artifact_id(C5ArtId)];
	   assertPay(Pred,Who)[artifact_id(C6ArtId)];
	   assertPay(Pred,Who)[artifact_id(C7ArtId)];
	   assertPay(Pred,Who)[artifact_id(C8ArtId)].
	   
+currentLocation(Where)
	 : focused(_,c1,C1ArtId) &
	   focused(_,c2,C2ArtId) &
	   focused(_,c3,C3ArtId) &
	   focused(_,c4,C4ArtId) &
	   focused(_,c5,C5ArtId) &
	   focused(_,c6,C6ArtId) &
	   focused(_,c7,C7ArtId) &
	   focused(_,c8,C8ArtId) &
	   packageName(What)
	<- assertAt(What,Where)[artifact_id(C1ArtId)];
	   assertAt(What,Where)[artifact_id(C2ArtId)];
	   assertAt(What,Where)[artifact_id(C3ArtId)];
	   assertAt(What,Where)[artifact_id(C4ArtId)];
	   assertAt(What,Where)[artifact_id(C5ArtId)];
	   assertAt(What,Where)[artifact_id(C6ArtId)];
	   assertAt(What,Where)[artifact_id(C7ArtId)];
	   assertAt(What,Where)[artifact_id(C8ArtId)];.
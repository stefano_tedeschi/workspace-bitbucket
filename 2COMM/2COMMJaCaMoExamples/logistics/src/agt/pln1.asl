{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("common.asl") }

!start.

+!start
	 : focused(_,c5,C5ArtId) &
	   focused(_,c6,C6ArtId) &
	   focused(_,c7,C7ArtId)
	<- enact("debtorC5")[artifact_id(C5ArtId)];
	   enact("debtorC6")[artifact_id(C6ArtId)];
	   enact("creditorC7")[artifact_id(C7ArtId)];
	   
	   .wait(500);
	   println("Offering my services for (B-C)...");
	   createC5.

+cc(_,MyId,"(at(pkg,C) AND pay50(t3))","at(pkg,D)","CONDITIONAL")
	 : enactment_id(MyId,c7,_)
	<- println("I have a deal with t3 for (C-D). I can offer my services for both (B-C) and (C-D)...")
	   createC6.
	
+cc(MyId,_,"(at(pkg,B) AND pay200(p))","at(pkg,D)","DETACHED")
	 : enactment_id(MyId,c6,_) &
	   packageName(What)
	<- println("Moving package to C...");
	   move(What,"C");
	   println("Paying t3 for the last step (C-D)");
	   pay(50,"t3").
	   
+cc(MyId,_,"(at(pkg,B) AND pay100(p))","at(pkg,C)","DETACHED")
	 : enactment_id(MyId,c6,_) &
	   packageName(What)
	<- println("Moving package to C...");
	   move(What,"C").
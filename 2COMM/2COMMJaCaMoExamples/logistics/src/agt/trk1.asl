{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("common.asl") }

!start.

+!start
	 : focused(_,c3,C3ArtId)
	<- enact("debtorC3")[artifact_id(C3ArtId)];
	   
	   .wait(500);
	   
	   println("I offer my services for (A-B), creating C3...");
	   createC3.

+cc(MyId,_,"(at(pkg,A) AND pay50(t1))","at(pkg,B)","DETACHED")
	 : packageName(What)
	<- println("Moving goods from A to B...");
	   move(What,"B").
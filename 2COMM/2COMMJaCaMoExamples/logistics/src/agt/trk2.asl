{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("common.asl") }

!start.

+!start
	 : focused(_,c4,C4ArtId)
	<- enact("creditorC4")[artifact_id(C4ArtId)].

+cc(_,MyId,"at(pkg,B)","pay20(t2)","CONDITIONAL")
	 : enactment_id(MyId,c4,ArtId)
	<- releaseC4.
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("common.asl") }

!start.

+!start
	 : focused(_,c1,C1ArtId) &
	   focused(_,c2,C2ArtId) &
	   focused(_,c3,C3ArtId) &
	   focused(_,c4,C4ArtId) &
	   focused(_,c5,C5ArtId) &
	   focused(_,c6,C6ArtId) &
	   focused(_,c8,C8ArtId)
	<- enact("debtorC1")[artifact_id(C1ArtId)];
	   enact("creditorC2")[artifact_id(C2ArtId)];
	   enact("creditorC3")[artifact_id(C3ArtId)];
	   enact("debtorC4")[artifact_id(C4ArtId)];
	   enact("creditorC5")[artifact_id(C5ArtId)];
	   enact("creditorC6")[artifact_id(C6ArtId)];
	   enact("debtorC8")[artifact_id(C8ArtId)];
	   
	   .wait(1000);
	   
	   println("Creating commitment C1...");
	   createC1.

// The seller waits for the payment before shipping the package
+cc(MyId,_,"pay500(s)","at(pkg,D)","DETACHED")
	 : enactment_id(MyId,c1,ArtId) &
	   packageName(Name)
	<- println("Payment received from the customer! Sending the package...");
	   move(Name,"A");
	   println("Creating C4, contracting with t2 for (A-B)...");
	   createC4;
	   println("Creating C8, contracting with t4 for (C-D)...");
	   createC8;
	   .wait(1000);
	   !timeoutC8.

+cc(MyIdC4,_,"at(pkg,B)","pay20(t2)","TERMINATED")
	 : enactment_id(MyIdC4,c4,_) &
	   enactment_id(MyIdC3,c3,_) &
	   cc(_,MyIdC3,"(at(pkg,A) AND pay50(t1))","at(pkg,B)","CONDITIONAL")
	<- println("t2 refused my proposal for (A-B), I have to rely on t1. Paying it 50...");
	   pay(50,"t1").
	   
+!timeoutC8
	 : enactment_id(MyId,c8,_) &
	   cc(MyId,_,"at(pkg,D)","pay50(t4)","CONDITIONAL")
	<- println("t4 didn't answer for (C-D), I have to rely on p for both (B-C) and (C-D). Paying it 200...");
	   cancelC8;
	   pay(200,p);
	   releaseC5.
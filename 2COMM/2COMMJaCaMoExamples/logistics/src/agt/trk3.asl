{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("common.asl") }

!start.

+!start
	 : focused(_,c7,C7ArtId)
	<- enact("debtorC7")[artifact_id(C7ArtId)];
	
	   .wait(500);
	   
	   println("Offering my services to p for (C-D)...")
	   createC7.
	   
+cc(MyId,_,"(at(pkg,C) AND pay50(t3))","at(pkg,D)","DETACHED")
	 : enactment_id(MyId,c7,_) &
	   packageName(What)
	<- println("Moving package to D...");
	   move(What,"D").
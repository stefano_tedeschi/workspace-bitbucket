package art;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.OPERATION;

public class C5 extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(C5.class);
	public static String ARTIFACT_TYPE = "C5";
	public static String DEBTOR_ROLE = "debtorC5";
	public static String CREDITOR_ROLE = "creditorC5";

	private RoleId debtor;
	private RoleId creditor;

	static {
		addEnabledRole(DEBTOR_ROLE, DebtorC5.class);
		addEnabledRole(CREDITOR_ROLE, CreditorC5.class);
	}

	public C5() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);

	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	@Override
	protected void enact(String roleName) {
		super.enact(roleName);
		if (roleName.equals("debtorC5")) {
			debtor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		} else if (roleName.equals("creditorC5")) {
			creditor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		}
	}

	// ROLES OPERATIONS

	@OPERATION
	public void createC5() {
		RoleId agent = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can create a commitment");
		}
		try {
			createCommitment(new Commitment(debtor, creditor, new CompositeExpression(LogicalOperatorType.AND, new Fact("at", "pkg", "B"), new Fact("pay100", getCurrentOpAgentId().getAgentName())), new Fact("at", "pkg", "C")));
			logger.trace("OPERATION PERFORMED: createC5 by " + agent.toString());
		} catch (MissingOperandException e) {
			log(Arrays.toString(e.getStackTrace()));
		} catch (WrongOperandsNumberException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	public void releaseC5() {
		RoleId agent = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(creditor)) {
			failed("Only the creditor can release a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		releaseCommitment(c);
		logger.trace("OPERATION PERFORMED: releaseC5 by " + agent.toString());
	}
	
	@OPERATION
	public void cancelC5() {
		RoleId agent = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can cancel a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		cancelCommitment(c);
		logger.trace("OPERATION PERFORMED: cancelC5 by " + agent.toString());
	}
	
	@OPERATION
	public void assertPay(String pred, String who) {
		RoleId agentId = getRoleIdByRoleName(who);
		try {
			if(agentId != null) {
				assertFact(new Fact(pred, agentId));
			}
			else {
				assertFact(new Fact(pred, who));
			}
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void assertAt(String what, String where) {
		try {
			assertFact(new Fact("at", what, where));
		} catch (

		MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// BEGIN INNER CLASSES for ROLES
	// cnp.Initiator role

	public class DebtorC5 extends PARole {

		public DebtorC5(String playerName, IPlayer player) {
			super(DEBTOR_ROLE, player);
		}
	}

	// cnp.Participant role

	public class CreditorC5 extends PARole {

		public CreditorC5(String playerName, IPlayer player) {
			super(CREDITOR_ROLE, player);
		}

	}

	public interface DebtorC5Observer extends ProtocolObserver {

	}

	public interface CreditorC5Observer extends ProtocolObserver {

	}

}

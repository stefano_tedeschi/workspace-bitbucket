package art;

import cartago.Artifact;
import cartago.OPERATION;

public class Logistics extends Artifact {
	
	private String packageName;
	private Location currentLocation;
	
	private enum Location {
		A,B,C,D
	}
	
	@OPERATION
	public void init(String packageName) {
		this.packageName = packageName;
		defineObsProperty("packageName", packageName);
	}
	
	@OPERATION
	public void pay(int what, String who) {
		defineObsProperty("pay", what, who);
	}
	
	@OPERATION
	public void move(String pkg, String where) {
		if(!packageName.equals(pkg)) {
			failed("Wrong package name");
		}
		else if(where.equals("A")) {
			currentLocation = Location.A;
			defineObsProperty("currentLocation", Location.A.toString());
		}
		else if(where.equals("B")) {
			if(currentLocation == Location.A) {
				currentLocation = Location.B;
				removeObsProperty("currentLocation");
				defineObsProperty("currentLocation", Location.B.toString());
			}
			else {
				failed("Wrong position");
			}
		}
		else if(where.equals("C")) {
			if(currentLocation == Location.B) {
				currentLocation = Location.C;
				removeObsProperty("currentLocation");
				defineObsProperty("currentLocation", Location.C.toString());
			}
			else {
				failed("Wrong position");
			}
		}
		else if(where.equals("D")) {
			if(currentLocation == Location.C) {
				currentLocation = Location.D;
				removeObsProperty("currentLocation");
				defineObsProperty("currentLocation", Location.D.toString());
			}
			else {
				failed("Wrong position");
			}
		}
	}
	
}

package art;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleId;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.core.Commitment;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.OPERATION;

public class C3 extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(C3.class);
	public static String ARTIFACT_TYPE = "C3";
	public static String DEBTOR_ROLE = "debtorC3";
	public static String CREDITOR_ROLE = "creditorC3";

	private RoleId debtor;
	private RoleId creditor;

	static {
		addEnabledRole(DEBTOR_ROLE, DebtorC3.class);
		addEnabledRole(CREDITOR_ROLE, CreditorC3.class);
	}

	public C3() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);

	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}

	@Override
	protected void enact(String roleName) {
		super.enact(roleName);
		if (roleName.equals("debtorC3")) {
			debtor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		} else if (roleName.equals("creditorC3")) {
			creditor = getEnactedRolesIds().get(getEnactedRolesIds().size() - 1);
		}
	}

	// ROLES OPERATIONS

	@OPERATION
	public void createC3() {
		RoleId agent = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can create a commitment");
		}
		try {
			createCommitment(new Commitment(debtor, creditor, new CompositeExpression(LogicalOperatorType.AND, new Fact("at","pkg","A"), new Fact("pay50", getCurrentOpAgentId().getAgentName())), new Fact("at","pkg","B")));
			logger.trace("OPERATION PERFORMED: createC3 by " + agent.toString());
		} catch (MissingOperandException e) {
			log(Arrays.toString(e.getStackTrace()));
		} catch (WrongOperandsNumberException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	public void releaseC3() {
		RoleId agent = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(creditor)) {
			failed("Only the creditor can release a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		releaseCommitment(c);
		logger.trace("OPERATION PERFORMED: releaseC3 by " + agent.toString());
	}
	
	@OPERATION
	public void cancelC3() {
		RoleId agent = getRoleIdByPlayerName(getCurrentOpAgentId().getAgentName());
		if (!agent.equals(debtor)) {
			failed("Only the debtor can cancel a commitment");
		}
		Commitment c = socialState.retrieveCommitmentsByCreditorRoleId(agent).get(0);
		cancelCommitment(c);
		logger.trace("OPERATION PERFORMED: cancelC3 by " + agent.toString());
	}
	
	@OPERATION
	public void assertPay(String pred, String who) {
		RoleId agentId = getRoleIdByRoleName(who);
		try {
			if(agentId != null) {
				assertFact(new Fact(pred, agentId));
			}
			else {
				assertFact(new Fact(pred, who));
			}
		} catch (MissingOperandException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void assertAt(String what, String where) {
		try {
			assertFact(new Fact("at", what, where));
		} catch (

		MissingOperandException e) {
			e.printStackTrace();
		}
	}

	// BEGIN INNER CLASSES for ROLES
	// cnp.Initiator role

	public class DebtorC3 extends PARole {

		public DebtorC3(String playerName, IPlayer player) {
			super(DEBTOR_ROLE, player);
		}
	}

	// cnp.Participant role

	public class CreditorC3 extends PARole {

		public CreditorC3(String playerName, IPlayer player) {
			super(CREDITOR_ROLE, player);
		}

	}

	public interface DebtorC3Observer extends ProtocolObserver {

	}

	public interface CreditorC3Observer extends ProtocolObserver {

	}

}

{ include("$jacamoJar/templates/common-cartago.asl") }

!start.

/* Plans */

@p1[atomic]
+!start : true
	<- println("Enacting role hirer...");
	   enact("hirer").
	
+enacted(Id,"hirer",My_Role_Id)
	<- println("Enactment done! My role id is ", My_Role_Id);
	   +enactment_id(My_Role_Id).
	
+positionId(PosId) : normativeStateId(NSId)
	<- focus(PosId);
	   focus(NSId);
	   !post_job.
		
+!post_job : positionId(PosId)
	<- .wait(2000);
	   println("Posting job...");
	   postJob[artifact_id(PosId)].	   

@p2[atomic]
//+offerAccepted(_)[artifact_id(NSId)]
+cc(My_Role_Id,Evaluator_Role_Id,Ant,Cons,"DETACHED")[artifact_id(NSId)]
	 : enactment_id(My_Role_Id) &
	   positionStatus("POSITION_OPEN") &
	   normativeStateId(NSId) &
	   positionId(PosId) &
	   .concat("(postJob(",My_Role_Id,") THEN offerAccepted(",Evaluator_Role_Id,"))",Ant) &
	   .concat("positionFilled(",My_Role_Id,")",Cons)
	<- println("The position has been filled. Well done!");
	   updateFilled[artifact_id(PosId)].


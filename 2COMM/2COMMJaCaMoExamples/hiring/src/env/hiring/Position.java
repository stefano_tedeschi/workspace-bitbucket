package hiring;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.IPlayer;
import twocomm.core.BusinessArtifact;
import twocomm.core.RoleId;
import twocomm.core.logic.Fact;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.exception.MissingOperandException;
import cartago.*;
import hiring.exceptions.IllegalArtifactStatusException;

public class Position extends BusinessArtifact {

	protected Logger logger = LogManager.getLogger(Position.class);
	
	public static String ARTIFACT_TYPE = "Position";
	
	private ArtifactId normativeArtifact;
	private ArtifactId workspaceManager;
	
	private RoleId hirer;
	private ArrayList<RoleId> evaluators = new ArrayList<>();
	
	private PositionStatus status = PositionStatus.POSITION_NULL;
	
	@OPERATION
	public void init() {
		defineObsProperty("positionStatus", status.toString());
	}
	
	@LINK
	void initNormativeArtifact() {
		if (normativeArtifact == null) {
			
			try {
				normativeArtifact = lookupArtifact("hiringNormativeState");	
			} catch (CartagoException e) {
				e.printStackTrace();	
			}

		}
	}
	
	@LINK
	void initWorkspaceManager() {
		if (workspaceManager == null) {
			
			try {
				workspaceManager = lookupArtifact("workspaceManager");	
			} catch (CartagoException e) {
				e.printStackTrace();	
			}

		}
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}
	
	@LINK
	public void setHirer(RoleId hirerId) throws OperationException {
		
		if(hirer == null) {
			hirer = hirerId;
		}
		else {
			failed("Hirer already set!");
		}

	}
	
	@OPERATION
	public void addEvaluator(RoleId evaluatorId) {

		if(!evaluators.contains(evaluatorId)) {
			evaluators.add(evaluatorId);
		}
		else {
			failed("Evaluator already added!");
		}
	}

	// ROLES OPERATIONS

	@OPERATION
	public void postJob() throws IllegalArtifactStatusException, OperationException, MissingOperandException {

		if(status != PositionStatus.POSITION_NULL) {
			throw new IllegalArtifactStatusException(status);
		}
		
		RoleId hirerId = getPlayerRoleId(getCurrentOpAgentId().getAgentName());
		assertFact(new Fact("postJob", hirerId.toString()));
		logger.trace("POSITION: fact asserted postJob(" + hirerId.toString() + ")");
		
		status = PositionStatus.POSITION_OPEN;
		removeObsProperty("positionStatus");
		defineObsProperty("positionStatus", status.toString());
		
		execLinkedOp(normativeArtifact, "postJob", hirerId);
	
	}
	
	@OPERATION
	public void updateFilled() throws IllegalArtifactStatusException, MissingOperandException, OperationException {

		if(status != PositionStatus.POSITION_OPEN) {
			throw new IllegalArtifactStatusException(status);
		}
		
		RoleId hirerId = getPlayerRoleId(getCurrentOpAgentId().getAgentName());
		assertFact(new Fact("positionFilled", hirerId.toString()));
		logger.trace("POSITION: fact asserted positionFilled(" + hirerId.toString() + ")");
		
		status = PositionStatus.POSITION_FILLED;
		removeObsProperty("positionStatus");
		defineObsProperty("positionStatus", status.toString());
		
		execLinkedOp(normativeArtifact, "updateFilled", hirerId);
		
	}
	
	@OPERATION
	public void updateAbandoned() throws IllegalArtifactStatusException, MissingOperandException, OperationException {
		
		if(status != PositionStatus.POSITION_OPEN) {
			throw new IllegalArtifactStatusException(status);
		}
		
		RoleId hirerId = getPlayerRoleId(getCurrentOpAgentId().getAgentName());
		assertFact(new Fact("positionAbandoned", hirerId.toString()));
		logger.trace("POSITION: fact asserted positionAbandoned(" + hirerId.toString() + ")");
		
		status = PositionStatus.POSITION_ABANDONED;
		removeObsProperty("positionStatus");
		defineObsProperty("positionStatus", status.toString());
		
		execLinkedOp(normativeArtifact, "updateAbandoned", hirerId);
		
	}
	
	private RoleId getPlayerRoleId(String playerName) throws OperationException {
		OpFeedbackParam<RoleId> r = new OpFeedbackParam<>();
		execLinkedOp(workspaceManager, "getPlayerRoleId", getCurrentOpAgentId().getAgentName(), r);
		return r.get();
	}

}
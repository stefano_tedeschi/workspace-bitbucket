package hiring;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.LifeCycleState;
import twocomm.core.CoordinationArtifact;
import twocomm.core.RoleId;
import twocomm.core.logic.*;
import twocomm.core.automated.AutomatedSocialState;
import twocomm.core.automated.AutomatedSocialStateSingleThreaded;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.AgentId;
import cartago.OPERATION;
import cartago.OperationException;
import cartago.LINK;


public class HiringNormativeState extends CoordinationArtifact {

	protected Logger logger = LogManager.getLogger(HiringNormativeState.class);
	public static String ARTIFACT_TYPE = "HiringNormativeState";
	public static String GENERIC_ROLE = "generic";

	public HiringNormativeState() {
		super();
		socialState = new AutomatedSocialStateSingleThreaded(this);
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}
	
	@LINK
	public void initCommitments(RoleId hirerId, RoleId evaluatorId, RoleId candidateId) throws MissingOperandException, WrongOperandsNumberException {
		
		LogicalExpression ant;
		LogicalExpression cons;
		
		Fact postJob = new Fact("postJob", hirerId.toString());
		Fact positionFilled = new Fact("positionFilled", hirerId.toString());
		//Fact positionAbandoned = new Fact("positionAbandoned", hirerId.toString());
		Fact apply = new Fact("apply", candidateId.toString());
		Fact screenInterview = new Fact("screenInterview", evaluatorId.toString());
		Fact positionClosed = new Fact("msgPositionClosed", evaluatorId.toString());
		Fact rejectionNotice = new Fact("msgRejectionNotice", evaluatorId.toString());
		Fact makeOffer = new Fact("makeOffer", evaluatorId.toString());
		Fact responseYes = new Fact("responseYes", candidateId.toString());
		Fact responseNo = new Fact("responseNo", candidateId.toString());
		Fact offerAccepted = new Fact("offerAccepted", evaluatorId.toString());
		Fact offerRejected = new Fact("offerRejected", evaluatorId.toString());
		//Fact timeout = new Fact("timeout3months", hirerId.toString());
		
		ant = new CompositeExpression(LogicalOperatorType.THEN, postJob, apply);
		LogicalExpression evaluateCandidate = new CompositeExpression(LogicalOperatorType.OR, positionClosed,
				new CompositeExpression(LogicalOperatorType.THEN, screenInterview,
						new CompositeExpression(LogicalOperatorType.OR, rejectionNotice,
								new CompositeExpression(LogicalOperatorType.THEN, makeOffer,
										new CompositeExpression(LogicalOperatorType.OR, 
												new CompositeExpression(LogicalOperatorType.THEN, responseYes, offerAccepted), 
												new CompositeExpression(LogicalOperatorType.THEN, responseNo, offerRejected)
										)
								)
						)
				)
		);
		cons = new CompositeExpression(LogicalOperatorType.OR, new CompositeExpression(LogicalOperatorType.THEN, apply, evaluateCandidate), positionFilled);
		Commitment c1 = new Commitment(evaluatorId, hirerId, ant, cons);
		createCommitment(c1);
		
		ant = positionFilled;
		//cons = new CompositeExpression(LogicalOperatorType.OR,
		//			new CompositeExpression(LogicalOperatorType.THEN, positionFilled, positionClosed),
		//			new CompositeExpression(LogicalOperatorType.THEN, offerAccepted, positionFilled)
		//	   );
		cons = new CompositeExpression(LogicalOperatorType.THEN, positionFilled, positionClosed);
		Commitment c2 = new Commitment(evaluatorId, hirerId, ant, cons);
		createCommitment(c2);
		
		ant = new CompositeExpression(LogicalOperatorType.THEN, postJob, apply);
		LogicalExpression informOutcome = new CompositeExpression(LogicalOperatorType.THEN, apply,
				new CompositeExpression(LogicalOperatorType.OR, positionClosed, 
						new CompositeExpression(LogicalOperatorType.OR, rejectionNotice, makeOffer)));
		cons = informOutcome;
		Commitment c3 = new Commitment(evaluatorId, candidateId, ant, cons);
		createCommitment(c3);
		
		ant = makeOffer;
		cons = new CompositeExpression(LogicalOperatorType.THEN, makeOffer, new CompositeExpression(LogicalOperatorType.OR, responseYes, responseNo));
		Commitment c4 = new Commitment(candidateId, evaluatorId, ant, cons);
		createCommitment(c4);
		
		ant = new CompositeExpression(LogicalOperatorType.THEN, postJob, offerAccepted);
		cons = positionFilled;
		Commitment c5 = new Commitment(hirerId, evaluatorId, ant, cons);
		createCommitment(c5);

	}

	@LINK
	public void terminateCommitments(RoleId roleId) {
		ArrayList<Commitment> comms = new ArrayList<>();
		comms.addAll(socialState.retrieveCommitmentsByDebtorRoleId(roleId));
		comms.addAll(socialState.retrieveCommitmentsByCreditorRoleId(roleId));
		for(Commitment c : comms) {
			if(c.getLifeCycleStatus() == LifeCycleState.CONDITIONAL) {
				releaseCommitment(c);
			}
		}
	}
	
	@LINK
	public void postJob(RoleId hirerId) throws MissingOperandException {
		assertFact(new Fact("postJob", hirerId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted postJob(" + hirerId.toString() + ")");
	}
	
	@LINK
	public void updateFilled(RoleId hirerId) throws MissingOperandException {
		assertFact(new Fact("positionFilled", hirerId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted positionFilled(" + hirerId.toString() + ")");
	}
	
	@LINK
	public void updateAbandoned(RoleId hirerId) throws MissingOperandException {
		assertFact(new Fact("positionAbandoned", hirerId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted positionAbandoned(" + hirerId.toString() + ")");
	}
	
	@LINK
	public void apply(RoleId candidateId) throws MissingOperandException {
		assertFact(new Fact("apply", candidateId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted apply(" + candidateId.toString() + ")");
	}
	
	@LINK
	public void screenInterview(RoleId evaluatorId) throws MissingOperandException {
		assertFact(new Fact("screenInterview", evaluatorId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted screenInterview(" + evaluatorId.toString() + ")");
	}
	
	@LINK
	public void positionClosed(RoleId evaluatorId) throws MissingOperandException {
		assertFact(new Fact("msgPositionClosed", evaluatorId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted msgPositionClosed(" + evaluatorId.toString() + ")");
	}
	
	@LINK
	public void rejectionNotice(RoleId evaluatorId) throws MissingOperandException {
		assertFact(new Fact("msgRejectionNotice", evaluatorId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted msgRejectionNotice(" + evaluatorId.toString() + ")");
	}
		
	@LINK
	public void makeOffer(RoleId evaluatorId) throws MissingOperandException {
		assertFact(new Fact("makeOffer", evaluatorId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted makeOffer(" + evaluatorId.toString() + ")");
	}
	
	@LINK
	public void responseYes(RoleId candidateId) throws MissingOperandException {
		assertFact(new Fact("responseYes", candidateId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted responseYes(" + candidateId.toString() + ")");
	}
	
	@LINK
	public void responseNo(RoleId candidateId) throws MissingOperandException {
		assertFact(new Fact("responseNo", candidateId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted responseNo(" + candidateId.toString() + ")");
	}
	
	@LINK
	public void offerAccepted(RoleId evaluatorId) throws MissingOperandException {
		assertFact(new Fact("offerAccepted", evaluatorId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted offerAccepted(" + evaluatorId.toString() + ")");
	}
	
	@LINK
	public void offerRejected(RoleId evaluatorId) throws MissingOperandException {
		assertFact(new Fact("offerRejected", evaluatorId.toString()));
		logger.trace("NORMATIVE STATE: fact asserted offerRejected(" + evaluatorId.toString() + ")");
	}

}
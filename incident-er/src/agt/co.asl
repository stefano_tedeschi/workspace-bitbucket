/* Initial goals */

!start.

/* Plans */

+!start
	<- .wait(100);
       
       .send(c, achieve, adoptRole(customer));
       .send(am, achieve, adoptRole(key_account_manager));
       .send(fls, achieve, adoptRole(first_level_support));
       .send(sls, achieve, adoptRole(second_level_support));
       .send(dev, achieve, adoptRole(developer)).

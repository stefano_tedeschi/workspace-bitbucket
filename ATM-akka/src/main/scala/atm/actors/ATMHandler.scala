package atm.actors

import akka.actor.SupervisorStrategy.{Escalate, Restart, Stop}
import akka.actor.{Actor, ActorRef, ActorSystem, AllForOneStrategy, OneForOneStrategy, PoisonPill, Props}
import akka.dispatch.sysmsg.Terminate
import akka.event.Logging
import atm.messages._

class ATMHandler extends Actor {

  val log = Logging(context.system, this)

  val rh = context.actorOf(Props[RequestHandler], "RequestHandler")
  val mk = context.actorOf(Props[MoneyKeeper], "MoneyKeeper")

  def receive = {
    case InitializeATM =>
      rh ! ObtainAmount
    case AmountObtained(amount) =>
      mk ! ProvideMoney(amount)
    case MoneyProvided =>
      println("Withdrawal complete")
      rh ! PoisonPill
      mk ! PoisonPill
  }

  override val supervisorStrategy = AllForOneStrategy() {
    case _: NotANumberException =>
      println("TRY LATER")
      Stop
  }

}

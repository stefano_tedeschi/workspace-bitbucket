package atm.actors

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor.{Actor, OneForOneStrategy, Props}
import akka.event.Logging
import atm.messages._

class MoneyKeeper extends Actor {

  val log = Logging(context.system, this)

  def receive = {
    case ProvideMoney(amount) =>
      if(amount == 1)
        println("Withdraw " + amount + " euro within 10 seconds")
      else
        println("Withdraw " + amount + " euros within 10 seconds")
      Thread.sleep(10000)
      context.parent ! MoneyProvided
  }

}
package atm.actors

import akka.actor.{Actor, Props}
import akka.event.Logging
import atm.messages._

import java.util.Scanner

class Parser extends Actor {

  val log = Logging(context.system, this)

  def receive = {
    case ParseAmount(amountString) =>
      if (amountString == null) throw new NotANumberException
      var result = 0
      var negative = false
      var i = 0
      val len = amountString.length
      var limit = -Integer.MAX_VALUE
      var multmin = 0
      var digit = 0
      if (len > 0) {
        val firstChar = amountString.charAt(0)
        if (firstChar < '0') { // Possible leading "+" or "-"
          if (firstChar == '-') {
            negative = true
            limit = Integer.MIN_VALUE
          }
          else if (firstChar != '+') throw new NotANumberException
          if (len == 1) { // Cannot have lone "+" or "-"
            throw new NotANumberException
          }
          i += 1
        }
        multmin = limit / 10
        while ( {
          i < len
        }) { // Accumulating negatively avoids surprises near MAX_VALUE
          digit = Character.digit(amountString.charAt({
            i += 1; i - 1
          }), 10)
          if (digit < 0) throw new NotANumberException
          if (result < multmin) throw new NotANumberException
          result *= 10
          if (result < limit + digit) throw new NotANumberException
          result -= digit
        }
      }
      else throw new NotANumberException
      if (!negative)
        result = -result
      context.parent ! ParsingDone(result)
  }

}
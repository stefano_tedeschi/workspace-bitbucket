package atm.actors

import akka.actor.{Actor, Props}
import akka.event.Logging
import atm.messages._

import java.util.Scanner

class Reader extends Actor {

  val log = Logging(context.system, this)

  def receive = {
    case GetAmountAsString =>
      println("Insert amount...")
      val input = new Scanner(System.in)
      val amountString = input.nextLine()
      context.parent ! AmountString(amountString)
  }

  override def postRestart(reason: Throwable): Unit = {
    super.postRestart(reason)
    context.parent ! Restarted
  }

}
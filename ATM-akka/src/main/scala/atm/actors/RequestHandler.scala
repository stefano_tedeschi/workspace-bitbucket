package atm.actors

import akka.actor.SupervisorStrategy.{Restart, Escalate}
import akka.actor.{Actor, AllForOneStrategy, Props}
import akka.event.Logging
import atm.messages._

import java.util.Scanner

class RequestHandler extends Actor {

  val log = Logging(context.system, this)

  var attempts = 0

  val r = context.actorOf(Props[Reader], "Reader")
  val p = context.actorOf(Props[Parser], "Parser")

  def receive = {
    case ObtainAmount | Restarted =>
      r ! GetAmountAsString
    case AmountString(amountString) =>
      p ! ParseAmount(amountString)
    case ParsingDone(amountInt) =>
      context.parent ! AmountObtained(amountInt)
  }

  override val supervisorStrategy = AllForOneStrategy() {
    case _: NotANumberException =>
      if(attempts < 2) {
        attempts += 1
        Restart
      }
      else {
        Escalate
      }
  }

}

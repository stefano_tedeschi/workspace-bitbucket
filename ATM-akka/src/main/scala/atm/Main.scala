package atm

import akka.actor.{ActorSystem, Props}
import atm.actors.ATMHandler
import atm.messages._

object Main extends App {

  val system = ActorSystem("atm")
  val companyActor = system.actorOf(Props[ATMHandler], name = "actors.ATMHandler")
  companyActor ! InitializeATM

}

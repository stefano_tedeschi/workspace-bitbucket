package atm.messages

trait Message

case class InitializeATM() extends Message
case class ObtainAmount() extends Message
case class AmountObtained(amount: Int) extends Message
case class ProvideMoney(amount: Int) extends Message
case class MoneyProvided() extends Message
case class GetAmountAsString() extends Message
case class AmountString(amount: String) extends Message
case class ParseAmount(amount: String) extends Message
case class ParsingDone(amount: Int) extends Message
case class Restarted() extends Message

case class NotANumberException() extends Exception

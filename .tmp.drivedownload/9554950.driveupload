package cnp.base;

import java.util.ArrayList;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cnp.base.initiator.InitiatorRequirements;
import cnp.base.participant.ParticipantRequirements;
import twocomm.core.Commitment;
import twocomm.core.IPlayer;
import twocomm.core.LifeCycleState;
import twocomm.core.ProtocolArtifact;
import twocomm.core.RoleAction;
import twocomm.core.RoleId;
import twocomm.core.RoleMessage;
import twocomm.core.SocialEvent;
import twocomm.core.logic.CompositeExpression;
import twocomm.core.logic.Fact;
import twocomm.core.logic.LogicalOperatorType;
import twocomm.core.typing.RoleType;
import twocomm.exception.CommitmentNotFoundInInteractionStateException;
import twocomm.exception.MissingOperandException;
import twocomm.exception.WrongOperandsNumberException;
import cartago.CartagoException;
import cartago.OPERATION;
import cartago.Op;
import cartago.util.agent.ActionFailedException;

public class CNPArtifact extends ProtocolArtifact {

	protected Logger logger = LogManager.getLogger(CNPArtifact.class);
	public static String ARTIFACT_TYPE = "CNP";	
	public static String INITIATOR_ROLE = "Initiator";
	public static String PARTICIPANT_ROLE = "Participant";	
	private long millis;
	private int numberMaxProposals = 3;
	private int actualProposals = 0;
	private boolean acceptingProposals = true;
	private Task t = null;
	private ArrayList<Proposal> proposals = new ArrayList<Proposal>();
	
	static {
		addEnabledRole(INITIATOR_ROLE, Initiator.class);
		addEnabledRole(PARTICIPANT_ROLE, Participant.class);
	}
	
	public CNPArtifact() {
		super();
	}

	@Override
	public String getArtifactType() {
		return ARTIFACT_TYPE;
	}	
	
	
	// ROLES OPERATIONS
	
	@OPERATION
	public void cfp(Task task) {
		millis = new Date().getTime();
		RoleId initiator = getRoleIdByRoleName(getOpUserName());
		RoleId dest = new RoleId(PARTICIPANT_ROLE, RoleId.GROUP_ROLE);		
		
		try {
			t = task;
			assertFact(new Fact("cfp", initiator, task));
			
			createAllCommitments(new Commitment(initiator, dest, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			
			logger.trace("OPERATION PERFORMED: CFP by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void accept(Proposal proposal) {
		
		RoleId initiator = getRoleIdByRoleName(getOpUserName());
 		RoleId part = proposal.getRoleId();
 		
		
		try {
			assertFact(new Fact("accept", part));
			//**** DEBUG: CAMBIO STATO DI VITA A COMMITMENT GIUSTO, SARA' IL SOCIAL STATE CHE DOVRA' GESTIRLO
			// QUI METTO A DETACHED IL COMMITMENT CHE HA COME DEBITORE IL PARTICIPANT, TIPO C(P,I,ACCEPT,DONE OR FAILURE)
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(initiator)) {
				if (com.getAntecedent().equals(new Fact("accept")) && com.getLifeCycleStatus().equals(LifeCycleState.CONDITIONAL)
						&& com.getDebtor().equals(part)) {
					arrComm.add(com);
				}
			}
			Commitment cToAdd;
			for (Commitment com : arrComm) {
//				cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), new Fact("true"), com.getConsequent(), LifeCycleState.DETACHED);	
//				socialState.removeCommitment(com);
//				createCommitment(cToAdd);
				detachCommitment(com);
			}
			logger.trace("OPERATION PERFORMED: ACCEPT by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void reject(Proposal proposal) {
		
		RoleId dest = proposal.getRoleId();
		RoleId initiator = getRoleIdByRoleName(getOpUserName());
		
		try {
			assertFact(new Fact("reject", dest));
			releaseCommitment(new Commitment(proposal.getRoleId(), initiator, "accept", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			logger.trace("OPERATION PERFORMED: REJECT by "+initiator);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void propose(Proposal prop, RoleId initiator) {
		if (!acceptingProposals)
			failed("No more proposals allowed.");
		RoleId participant = getRoleIdByRoleName(getOpUserName());
		proposals.add(prop);
		
		try {
			assertFact(new Fact("propose", participant, prop));
			createCommitment(new Commitment(participant, initiator, new Fact("accept"), new CompositeExpression(
					LogicalOperatorType.OR, new Fact("done"), new Fact("failure"))));
			//**** DEBUG: CAMBIO STATO DI VITA A COMMITMENT GIUSTO, SARA' IL SOCIAL STATE CHE DOVRA' GESTIRLO
			// QUI, CAMBIO IL COMMITMENT CHE VEDE IL PARTICIPANT COME DEBITORE E LO METTO A DETACHED
			for (Commitment com : socialState.retrieveCommitmentsByCreditorRoleId(participant)) {
				if (com.getAntecedent().equals(new Fact("propose")) && com.getLifeCycleStatus().equals(LifeCycleState.CONDITIONAL)) {
//					Commitment cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), new Fact("true"), com.getConsequent(), LifeCycleState.DETACHED);	
//					socialState.removeCommitment(com);
//					createCommitment(cToAdd);	
					detachCommitment(com);
					actualProposals++;						
				}
			}
			
			// asserisco il commitment di gruppo se raggiungo max numero.
			if (actualProposals == numberMaxProposals) {
				acceptingProposals = false;
				RoleId groupParticipant = new RoleId(PARTICIPANT_ROLE, RoleId.GROUP_ROLE);
				createCommitment(new Commitment(initiator, groupParticipant, new CompositeExpression(
						LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
				
			}
			
			logger.trace("OPERATION PERFORMED: PROPOSE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@OPERATION
	public void refuse(RoleId participant, RoleId initiator) {
		
		try {
			releaseCommitment(new Commitment(initiator, participant, "propose", new CompositeExpression(
					LogicalOperatorType.OR, new Fact("accept"), new Fact("reject"))));
			assertFact(new Fact("refuse", participant));
			logger.trace("OPERATION PERFORMED: REFUSE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void done(Task task, RoleId initiator) {
		
		RoleId participant = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("done"));
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(participant)) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
					arrComm.add(com);
				}
			}
			for (Commitment com :arrComm) {
				satisfyCommitment(com);
//				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
//					Commitment cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), com.getAntecedent(), com.getConsequent(), LifeCycleState.SATISFIED);	
//					socialState.removeCommitment(com);
//					createCommitment(cToAdd);				
//				}
			}			
			logger.trace("OPERATION PERFORMED: DONE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void failure(RoleId initiator) {
		RoleId participant = getRoleIdByRoleName(getOpUserName());
		try {
			assertFact(new Fact("failure"));
			ArrayList<Commitment> arrComm = new ArrayList<Commitment>();
			for (Commitment com : socialState.retrieveCommitmentsByDebtorRoleId(participant)) {
				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
					arrComm.add(com);
				}
			}
			for (Commitment com :arrComm) {
				satisfyCommitment(com);
//				if (com.getConsequent().equals(new CompositeExpression(LogicalOperatorType.OR, new Fact("done"), new Fact("failure")))) {
//					Commitment cToAdd = new Commitment(com.getDebtor(), com.getCreditor(), com.getAntecedent(), com.getConsequent(), LifeCycleState.SATISFIED);	
//					socialState.removeCommitment(com);
//					createCommitment(cToAdd);				
//				}
			}	
			
			logger.trace("OPERATION PERFORMED: FAILURE by "+participant);
		} catch (MissingOperandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongOperandsNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	// BEGIN INNER CLASSES for ROLES
	// cnp.Initiator role
	
	@RoleType(requirements = InitiatorRequirements.class)
	public class Initiator extends PARole {
		
		public Initiator(IPlayer player) {
			super(INITIATOR_ROLE, player);
		}

		@RoleAction
		public void cfp(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("cfp", task));
			} catch (ActionFailedException e) {
				logger.error("Action CFP failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		// accepting to particular role
		@RoleAction
		public void accept(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("accept", proposal));
			} catch (ActionFailedException e) {
				logger.error("Action ACCEPT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		@RoleAction
		public void reject(Proposal proposal) {
			try {
				doAction(this.getArtifactId(), new Op("reject", proposal));
			} catch (ActionFailedException e) {
				logger.error("Action REJECT failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public ArrayList<Proposal> getProposals() {
			return proposals;
		}
		
	}
	
	// cnp.Participant role
	
	@RoleType(requirements = ParticipantRequirements.class)
	public class Participant extends PARole {
		
		public Participant(IPlayer player) {
			super(PARTICIPANT_ROLE, player);
		}

		public Task getTask() {
			return t;
		}
		
		@RoleAction
		public void propose(Proposal proposal, RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("propose", proposal, initiator));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		@RoleAction
		public void refuse(Task task) {
			try {
				doAction(this.getArtifactId(), new Op("refuse", task, this.getRoleId()));
			} catch (ActionFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		@RoleAction
		public void done(Task task, RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("done", task, initiator));
			} catch (ActionFailedException e) {
				logger.error("Action DONE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		@RoleAction
		public void failure(RoleId initiator) {
			try {
				doAction(this.getArtifactId(), new Op("failure", initiator));
			} catch (ActionFailedException e) {
				logger.error("Action FAILURE failed: "+e.getFailureMsg());
				e.printStackTrace();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
	}
	
	public interface CNPInitiatorObserver extends ProtocolObserver {
		
	}
	
	public interface CNPParticipantObserver extends ProtocolObserver {
		
	}

}

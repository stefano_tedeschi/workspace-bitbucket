package ora4mas.nopl.accountability;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cartago.OPERATION;
import cartago.ObsProperty;
import moise.common.MoiseException;
import ora4mas.nopl.JasonTermWrapper;
import ora4mas.nopl.OrgArt;

public class AccountabilityBoard extends OrgArt {

    protected Logger logger = Logger.getLogger(AccountabilityBoard.class.getName());

    // Agent - Roles
    private HashMap<String, List<String>> pendingRoles; //Roles in call for proposal
    private HashMap<String, List<String>> refusedRoles; //Refused roles

    // Agent - Roles
    private HashMap<String, List<String>> pendingRoleProposals;  //Role proposals not yet accepted nor rejected
    private HashMap<String, List<String>> acceptedRoleProposals; //Accepted role proposals
    private HashMap<String, List<String>> rejectedRoleProposals; //Rejected role proposals

    // Agent - [(Group, Scheme), ...]
    private HashMap<String, Set<Power>> successfulPowerAcquisitions;
    private HashMap<String, Set<Power>> failedPowerAcquisitions;

    // Agent - (Goal, Provision)
    private HashMap<String, HashMap<String, String>> pendingProvisions;
    private HashMap<String, HashMap<String, String>> acceptedProvisions;
    private HashMap<String, HashMap<String, String>> rejectedProvisions;

    //Agent - Goals
    private HashMap<String, List<String>> pendingGoals;   // Goals in call for proposals
    private HashMap<String, List<String>> refusedGoals;   // Refused goals
    private HashMap<String, List<String>> requestedGoals; // Requested goals
    private HashMap<String, List<String>> agreedGoals;    // Agreed goals
    
    private List<String> failedGoals;
    private List<String> achievedGoals;

    private List<String> holdingProvisions;
    private List<String> failedProvisions;

    public void init() {

        oeId = getCreatorId().getWorkspaceId().getName();

        pendingRoles = new HashMap<>();
        refusedRoles = new HashMap<>();
        
        pendingRoleProposals = new HashMap<>();
        acceptedRoleProposals = new HashMap<>();
        rejectedRoleProposals = new HashMap<>();

        successfulPowerAcquisitions = new HashMap<>();
        failedPowerAcquisitions = new HashMap<>();

        pendingProvisions = new HashMap<>();
        acceptedProvisions = new HashMap<>();
        rejectedProvisions = new HashMap<>();

        pendingGoals = new HashMap<>();
        refusedGoals = new HashMap<>();
        requestedGoals = new HashMap<>();
        agreedGoals = new HashMap<>();
        
        failedGoals = new ArrayList<>();
        achievedGoals = new ArrayList<>();

        holdingProvisions = new ArrayList<>();
        failedProvisions = new ArrayList<>();

    }

    /** ROLE ADOPTION **/

    @OPERATION
    public void callForRole(String addressee, String role) throws MoiseException {
        callForRole(getOpUserName(), addressee, role);
    }

    private void callForRole(String agent, String addressee, String role) throws MoiseException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can perform a call for role");
        }
        List<String> addresseePendingRoles = pendingRoles.get(addressee);
        if (addresseePendingRoles != null && addresseePendingRoles.contains(role)) {
            throw new OpNotAllowedException("Another call is still pending for agent " + addressee + " and role " + role);
        }
        if(addresseePendingRoles == null) {
            addresseePendingRoles = new ArrayList<>();
            pendingRoles.put(addressee, addresseePendingRoles);
        }
        addresseePendingRoles.add(role);
        defineObsProperty("pendingRole",new JasonTermWrapper(addressee), new JasonTermWrapper(role));
    }

    @OPERATION
    public void refuseRole(String role) throws MoiseException {
        refuseRole(getOpUserName(), role);
    }

    private void refuseRole(String agent, String role) throws MoiseException {
        List<String> agentPendingRoles = pendingRoles.get(agent);
        if (agentPendingRoles == null || !agentPendingRoles.contains(role)) {
            throw new OpNotAllowedException("The call for role to be refused is not pending");
        }
        List<String> agentRefusedRoles = refusedRoles.get(agent);
        if (agentRefusedRoles == null) {
            agentRefusedRoles = new ArrayList<String>();
            refusedRoles.put(agent, agentRefusedRoles);
        }
        agentPendingRoles.remove(role);
        //removeObsPropertyByTemplate("pendingRole", new JasonTermWrapper(agent), new JasonTermWrapper(role));
        agentRefusedRoles.add(role);
        defineObsProperty("refusedRole", new JasonTermWrapper(agent), new JasonTermWrapper(role));
    }

    @OPERATION
    public void proposeForRole(String role) throws MoiseException {
        proposeForRole(getOpUserName(), role);
    }

    private void proposeForRole(String agent, String role) throws MoiseException {
        List<String> agentPendingRoles = pendingRoles.get(agent);
        if (agentPendingRoles == null || !agentPendingRoles.contains(role)) {
            throw new OpNotAllowedException("The role for which the agent is trying to propose itself is not pending");
        }
        List<String> agentRejectedRoleProposals = rejectedRoleProposals.get(agent);
        if (agentRejectedRoleProposals != null && agentRejectedRoleProposals.contains(role)) {
            throw new OpNotAllowedException(
                    "The agent has already made a proposal for this role and the proposal has been rejected");
        }
        List<String> agentAcceptedRoleProposals = acceptedRoleProposals.get(agent);
        if (agentAcceptedRoleProposals != null && agentAcceptedRoleProposals.contains(role)) {
            throw new OpNotAllowedException(
                    "The agent has already made a proposal for this role and the proposal has been accepted");
        }
        List<String> agentPendingRoleProposals = pendingRoleProposals.get(agent);
        if (agentPendingRoleProposals != null && agentPendingRoleProposals.contains(role)) {
            throw new OpNotAllowedException(
                    "The agent has already made a proposal for this role and the proposal is pending");
        }
        if (agentPendingRoleProposals == null) {
            agentPendingRoleProposals = new ArrayList<>();
            pendingRoleProposals.put(agent, agentPendingRoleProposals);
        }
        agentPendingRoleProposals.add(role);
        defineObsProperty("proposedForRole", new JasonTermWrapper(agent), new JasonTermWrapper(role));
    }

    @OPERATION
    public void rejectProposalForRole(String proposer, String role) throws MoiseException {
        rejectProposalForRole(getOpUserName(), proposer, role);
    }

    private void rejectProposalForRole(String agent, String proposer, String role) throws MoiseException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can reject role proposals");
        }
        List<String> proposerPendingRoleProposals = pendingRoleProposals.get(proposer);
        if (proposerPendingRoleProposals == null || !proposerPendingRoleProposals.contains(role)) {
            throw new OpNotAllowedException("No pending proposal made by agent " + proposer + " for role " + role);
        }
        List<String> proposerRejectedRoleProposals = rejectedRoleProposals.get(proposer);
        if (proposerRejectedRoleProposals == null) {
            proposerRejectedRoleProposals = new ArrayList<>();
            rejectedRoleProposals.put(proposer, proposerRejectedRoleProposals);
        }
        proposerPendingRoleProposals.remove(role);
        proposerRejectedRoleProposals.add(role);
        defineObsProperty("roleProposalRejected", new JasonTermWrapper(proposer), new JasonTermWrapper(role));
    }

    @OPERATION
    public void acceptProposalForRole(String proposer, String role) throws MoiseException {
        acceptProposalForRole(getOpUserName(), proposer, role);
    }

    private void acceptProposalForRole(String agent, String proposer, String role) throws MoiseException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can accept role proposals");
        }
        List<String> proposerPendingRoleProposals = pendingRoleProposals.get(proposer);
        if (proposerPendingRoleProposals == null || !proposerPendingRoleProposals.contains(role)) {
            throw new OpNotAllowedException("No pending proposal made by " + proposer + " for role " + role);
        }
        List<String> proposerAcceptedRoleProposals = acceptedRoleProposals.get(proposer);
        if (proposerAcceptedRoleProposals == null) {
            proposerAcceptedRoleProposals = new ArrayList<>();
            acceptedRoleProposals.put(proposer, proposerAcceptedRoleProposals);
        }
        proposerPendingRoleProposals.remove(role);
        proposerAcceptedRoleProposals.add(role);
        defineObsProperty("roleProposalAccepted", new JasonTermWrapper(proposer), new JasonTermWrapper(role));
    }

    @OPERATION
    public void declareAdoptionSuccess(String role, String group, String scheme) throws MoiseException {
        declareAdoptionSuccess(getOpUserName(), role, group, scheme);
    }
    private void declareAdoptionSuccess(String agent, String role, String group, String scheme) throws MoiseException {
        //List<String> agentPendingRoles = pendingRoles.get(agent);
        //if (agentPendingRoles == null || !agentPendingRoles.contains(role)) {
        //    throw new OpNotAllowedException("The role the agent is trying to adopt is not pending");
        //}
        //List<String> agentAcceptedRoleProposals = acceptedRoleProposals.get(agent);
        //if (agentAcceptedRoleProposals == null || !agentAcceptedRoleProposals.contains(role)) {
        //    throw new OpNotAllowedException("No proposal for role " + role + " made by agent " + agent + " was accepted");
        //}
        Power newP = new Power(role, group, scheme);
        Set<Power> agentFailedPowerAcquisitions = failedPowerAcquisitions.get(agent);
        if (agentFailedPowerAcquisitions != null && agentFailedPowerAcquisitions.contains(newP)) {
            throw new OpNotAllowedException("The agent has already failed to acquire the powers for role " + role
                    + " resulting from group " + group + " and scheme " + scheme);
        }
        Set<Power> agentSuccessfulPowerAcquisitions = successfulPowerAcquisitions.get(agent);
        if (agentSuccessfulPowerAcquisitions != null) {
            if (agentSuccessfulPowerAcquisitions.contains(newP)) {
                throw new OpNotAllowedException("The agent has already acquired the powers for role " + role
                        + " resulting from group " + group + " and scheme " + scheme);
            }
            agentSuccessfulPowerAcquisitions.add(newP);
        } else {
            agentSuccessfulPowerAcquisitions = new HashSet<Power>();
            agentSuccessfulPowerAcquisitions.add(newP);
            successfulPowerAcquisitions.put(agent, agentSuccessfulPowerAcquisitions);
        }
        //agentPendingRoles.remove(role);
        //removeObsPropertyByTemplate("pendingRole", new JasonTermWrapper(agent), new JasonTermWrapper(role));
        defineObsProperty("powerAcquired", new JasonTermWrapper(agent), new JasonTermWrapper(role),
                new JasonTermWrapper(group), new JasonTermWrapper(scheme));
    }

    @OPERATION
    public void declareAdoptionFailure(String role, String group, String scheme) throws MoiseException {
        declareAdoptionFailure(getOpUserName(), role, group, scheme);
    }
    private void declareAdoptionFailure(String agent, String role, String group, String scheme) throws OpNotAllowedException {
        List<String> agentPendingRoles = pendingRoles.get(agent);
        if (agentPendingRoles == null || !agentPendingRoles.contains(role)) {
            throw new OpNotAllowedException("The role the agent failed to adopt is not pending");
        }
        List<String> agentAcceptedRoleProposals = acceptedRoleProposals.get(agent);
        if (agentAcceptedRoleProposals == null || !agentAcceptedRoleProposals.contains(role)) {
            throw new OpNotAllowedException("No proposal for role " + role + " made by agent " + agent + " was accepted");
        }
        Power newP = new Power(role, group, scheme);
        Set<Power> agentSuccessfulPowerAcquisitions = successfulPowerAcquisitions.get(agent);
        if (agentSuccessfulPowerAcquisitions != null && agentSuccessfulPowerAcquisitions.contains(newP)) {
            throw new OpNotAllowedException("The agent has already acquired the powers for role " + role
                    + " resulting from group " + group + " and scheme " + scheme);
        }
        Set<Power> agentFailedPowerAcquisitions = failedPowerAcquisitions.get(agent);
        if (agentFailedPowerAcquisitions != null) {
            if (agentFailedPowerAcquisitions.contains(newP)) {
                throw new OpNotAllowedException("The agent has already failed to acquire the powers for role " + role
                        + " resulting from group " + group + " and scheme " + scheme);
            }
            agentFailedPowerAcquisitions.add(newP);
        } else {
            agentFailedPowerAcquisitions = new HashSet<Power>();
            agentFailedPowerAcquisitions.add(newP);
            failedPowerAcquisitions.put(agent, agentFailedPowerAcquisitions);
        }
        agentPendingRoles.remove(role);
        //removeObsPropertyByTemplate("pendingRole", new JasonTermWrapper(agent), new JasonTermWrapper(role));
        defineObsProperty("powerAcquisitionFailed", new JasonTermWrapper(agent), new JasonTermWrapper(role),
                new JasonTermWrapper(group), new JasonTermWrapper(scheme));
    }

    /** GOAL AGREEMENT **/

    @OPERATION
    public void callForGoal(String addressee, String goal) throws MoiseException {
        callForGoal(getOpUserName(), addressee, goal);
    }

    private void callForGoal(String agent, String addressee, String goal) throws OpNotAllowedException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can perform a call for goal");
        }
        List<String> addresseePendingGoals = pendingGoals.get(addressee);
        if (addresseePendingGoals != null && addresseePendingGoals.contains(goal)) {
            throw new OpNotAllowedException("Another call is still pending for this goal");
        }
        if(addresseePendingGoals == null) {
            addresseePendingGoals = new ArrayList<>();
            pendingGoals.put(addressee, addresseePendingGoals);
        }
        addresseePendingGoals.add(goal);
        defineObsProperty("pendingGoal",new JasonTermWrapper(addressee), new JasonTermWrapper(goal));
        
    }

    @OPERATION
    public void refuseGoal(String goal) throws MoiseException {
        refuseGoal(getOpUserName(), goal);
    }

    private void refuseGoal(String agent, String goal) throws MoiseException {
        List<String> agentPendingGoals = pendingGoals.get(agent);
        if (agentPendingGoals == null || !agentPendingGoals.contains(goal)) {
            throw new OpNotAllowedException("The call for goal to be refused is not pending");
        }
        List<String> agentRefusedGoals = refusedGoals.get(agent);
        if (agentRefusedGoals == null) {
            agentRefusedGoals = new ArrayList<String>();
            refusedRoles.put(agent, agentRefusedGoals);
        }
        agentPendingGoals.remove(goal);
        //removeObsPropertyByTemplate("pendingGoal", new JasonTermWrapper(agent), new JasonTermWrapper(goal));
        agentRefusedGoals.add(goal);
        defineObsProperty("refusedGoal", new JasonTermWrapper(agent), new JasonTermWrapper(goal));
    }

    @OPERATION
    public void proposeProvision(String goal, String provision) throws MoiseException {
        proposeProvision(getOpUserName(), goal, provision);
    }

    private void proposeProvision(String agent, String goal, String provision) throws MoiseException {
        List<String> agentPendingGoals = pendingGoals.get(agent);
        if (agentPendingGoals == null || !agentPendingGoals.contains(goal)) {
            throw new OpNotAllowedException("The goal for which the agent is proposing a provision is not pending");
        }
        HashMap<String, String> agentAcceptedProvisions = acceptedProvisions.get(agent);
        if (agentAcceptedProvisions != null && agentAcceptedProvisions.containsKey(goal)) {
            throw new ProvisionException("A provision was already accepted for this goal");
        } 
        HashMap<String, String> agentRejectedProvisions = rejectedProvisions.get(agent);
        if (agentRejectedProvisions != null && agentRejectedProvisions.containsKey(goal)) {
            throw new ProvisionException("A provision was already rejected for this goal");
        }
        HashMap<String, String> agentPendingProvisions = pendingProvisions.get(agent);
        if (agentPendingProvisions != null && agentPendingProvisions.containsKey(goal)) {
            throw new ProvisionException("Another provision is already pending for this goal");
        } 
        if (agentPendingProvisions == null) {
            agentPendingProvisions = new HashMap<String, String>();
            pendingProvisions.put(agent, agentPendingProvisions);
        }
        agentPendingProvisions.put(goal, provision);
        defineObsProperty("pendingProvision", new JasonTermWrapper(agent), new JasonTermWrapper(goal),
                new JasonTermWrapper(provision));
    }

    @OPERATION
    public void acceptProvision(String proposer, String goal, String provision) throws MoiseException {
        acceptProvision(getOpUserName(), proposer, goal, provision);
    }

    private void acceptProvision(String agent, String proposer, String goal, String provision) throws MoiseException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can accept a provision");
        }
        HashMap<String, String> proposerPendingProvisions = pendingProvisions.get(proposer);
        if (proposerPendingProvisions != null) {
            if (proposerPendingProvisions.get(goal).equals(provision)) {
                proposerPendingProvisions.remove(goal);
                //removeObsPropertyByTemplate("pendingProvision", new JasonTermWrapper(proposer),
                //        new JasonTermWrapper(goal), new JasonTermWrapper(provision));
                HashMap<String, String> proposerAcceptedProvisions = acceptedProvisions.get(proposer);
                if (proposerAcceptedProvisions == null) {
                    proposerAcceptedProvisions = new HashMap<String, String>();
                    acceptedProvisions.put(proposer, proposerAcceptedProvisions);
                }
                proposerAcceptedProvisions.put(goal, provision);
                defineObsProperty("acceptedProvision", new JasonTermWrapper(proposer), new JasonTermWrapper(goal),
                        new JasonTermWrapper(provision));
            } else {
                throw new ProvisionException("The provision to accept is not pending");
            }
        } else {
            throw new ProvisionException("The provision to accept is not pending");
        }
    }

    @OPERATION
    public void rejectProvision(String proposer, String goal, String provision) throws MoiseException {
        rejectProvision(getOpUserName(), proposer, goal, provision);
    }

    private void rejectProvision(String agent, String proposer, String goal, String provision) throws MoiseException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can accept a provision");
        }
        HashMap<String, String> proposerPendingProvisions = pendingProvisions.get(proposer);
        if (proposerPendingProvisions != null) {
            if (proposerPendingProvisions.get(goal).equals(provision)) {
                proposerPendingProvisions.remove(goal);
                //removeObsPropertyByTemplate("pendingProvision", new JasonTermWrapper(proposer),
                //        new JasonTermWrapper(goal), new JasonTermWrapper(provision));
                HashMap<String, String> proposerRejectedProvisions = rejectedProvisions.get(proposer);
                if (proposerRejectedProvisions == null) {
                    proposerRejectedProvisions = new HashMap<String, String>();
                    rejectedProvisions.put(proposer, proposerRejectedProvisions);
                }
                proposerRejectedProvisions.put(goal, provision);
                defineObsProperty("rejectedProvision", new JasonTermWrapper(proposer), new JasonTermWrapper(goal),
                        new JasonTermWrapper(provision));
            } else {
                throw new ProvisionException("The provision to reject is not pending");
            }
        } else {
            throw new ProvisionException("The provision to reject is not pending");
        }
    }

    @OPERATION
    public void requestGoal(String addressee, String goal) throws MoiseException {
        requestGoal(getOpUserName(), addressee, goal);
    }

    private void requestGoal(String agent, String addressee, String goal) throws OpNotAllowedException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can request the achievement of a goal");
        }
        List<String> addresseePendingGoals = pendingGoals.get(addressee);
        if (addresseePendingGoals == null || !addresseePendingGoals.contains(goal)) {
            throw new OpNotAllowedException("Requested goal was not included in the call for proposal");
        }
        List<String> addresseeRequestedGoals = requestedGoals.get(addressee);
        if (addresseeRequestedGoals != null && addresseeRequestedGoals.contains(goal)) {
            throw new OpNotAllowedException("Another request is still open for this goal");
        }
        addresseePendingGoals.remove(goal);
        removeObsPropertyByTemplate("pendingGoal", new JasonTermWrapper(addressee), new JasonTermWrapper(goal));
        if (addresseeRequestedGoals == null) {
            addresseeRequestedGoals = new ArrayList<>();
            requestedGoals.put(addressee, addresseeRequestedGoals);
        }
        addresseeRequestedGoals.add(goal);
        defineObsProperty("requestedGoal", new JasonTermWrapper(addressee), new JasonTermWrapper(goal));
    }

    @OPERATION
    public void agreeGoal(String goal) throws MoiseException {
        agreeGoal(getOpUserName(), goal);
    }

    private void agreeGoal(String agent, String goal) throws MoiseException {
        List<String> agentRequestedGoals = requestedGoals.get(agent);
        if (agentRequestedGoals == null || !agentRequestedGoals.contains(goal)) {
            throw new OpNotAllowedException("Impossible to agree to achieve a goal that is not requested");
        }
        //if (refusedGoals.get(agent).contains(goal)) {
        //    throw new OpNotAllowedException("Agent has already refused the goal");
        //}
        //if (agreedGoals.get(agent).contains(goal)) {
        //    throw new OpNotAllowedException("Agent has already agreed to achieve the goal");
        //}
        HashMap<String, String> agentAcceptedProvisions = acceptedProvisions.get(agent);
        if (!agentAcceptedProvisions.containsKey(goal)) {
            throw new OpNotAllowedException("Agent's provisions have not been accepted");
        }
        agentRequestedGoals.remove(goal);
        //removeObsPropertyByTemplate("requestedGoal", new JasonTermWrapper(agent), new JasonTermWrapper(goal));
        List<String> agentAgreedGoals = agreedGoals.get(agent);
        if (agentAgreedGoals != null) {
            agentAgreedGoals.add(goal);
        } else {
            agentAgreedGoals = new ArrayList<>();
            agentAgreedGoals.add(goal);
            agreedGoals.put(agent, agentAgreedGoals);
        }
        defineObsProperty("agreedGoal", new JasonTermWrapper(agent), new JasonTermWrapper(goal));
    }
    
    @OPERATION
    public void informProvision(String goal, String provision) throws MoiseException {
        informProvision(getOpUserName(), goal, provision);
    }
    private void informProvision(String agent, String goal, String provision) throws OpNotAllowedException, ProvisionException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can declare the truth value of a provision");
        }
        //if (failedProvisions.contains(provision)) {
        //    throw new ProvisionException("The provision has already been declared false");
        //}
        //if (holdingProvisions.contains(provision)) {
        //    throw new ProvisionException("The provision has been already declared true");
        //}
        holdingProvisions.add(provision);
        defineObsProperty("holdingProvision", new JasonTermWrapper(goal), new JasonTermWrapper(provision));
    }
    
    @OPERATION
    public void failureProvision(String provision) throws MoiseException {
        failureProvision(getOpUserName(), provision);
    }
    private void failureProvision(String agent, String provision) throws OpNotAllowedException, ProvisionException {
        if (!agent.equals(ownerAgent)) {
            throw new OpNotAllowedException("Only the organization owner can declare the truth value of a provision");
        }
        if (failedProvisions.contains(provision)) {
            throw new ProvisionException("The provision has already been declared false");
        }
        if (holdingProvisions.contains(provision)) {
            throw new ProvisionException("The provision has been already declared true");
        }
        failedProvisions.add(provision);
        defineObsProperty("failedProvision", new JasonTermWrapper(provision));
    }
    
    @OPERATION
    public void informGoal(String goal) throws MoiseException {
        informGoal(getOpUserName(), goal);
    }
    private void informGoal(String agent, String goal) throws OpNotAllowedException, GoalException {
        Set<String> agents = agreedGoals.keySet();
        boolean found = false;
        for (String a : agents) {
            List<String> aAgreedGoals = agreedGoals.get(a);
            if(aAgreedGoals != null && aAgreedGoals.contains(goal)) {
                found = true;
                aAgreedGoals.remove(goal);
            }
        }
        if (!found) {
            throw new GoalException("The goal " + goal + " has not been agreed");
        }
        //if (failedGoals.contains(goal)) {
        //    throw new GoalException("The goal has already been declared failed");
        //}
        //if (achievedGoals.contains(goal)) {
        //    throw new GoalException("The goal has been already declared achieved");
        //}
        achievedGoals.add(goal);
        defineObsProperty("achievedGoal", new JasonTermWrapper(goal));
    }
    
    @OPERATION
    public void failureGoal(String goal) throws MoiseException {
        failureGoal(getOpUserName(), goal);
    }
    private void failureGoal(String agent, String goal) throws OpNotAllowedException, GoalException {
        Set<String> agents = requestedGoals.keySet();
        boolean found = false;
        for (String a : agents) {
            List<String> aRequestedGoals = requestedGoals.get(a);
            if(aRequestedGoals != null && aRequestedGoals.contains(goal)) {
                found = true;
                aRequestedGoals.remove(goal);
            }
        }
        if (!found) {
            throw new GoalException("The goal has not been requested");
        }
        if (failedGoals.contains(goal)) {
            throw new GoalException("The goal has already been declared failed");
        }
        if (achievedGoals.contains(goal)) {
            throw new GoalException("The goal has been already declared achieved");
        }
        failedGoals.add(goal);
        defineObsProperty("failedGoal", new JasonTermWrapper(goal));
    }

//  @OPERATION
//  public void printAccountabilityArtifactState() {
//      System.out.println(printProvisions());
//  }
//
//  private String printProvisions() {
//
//      Set<String> app = pendingProvisions.keySet();
//      Set<String> aap = acceptedProvisions.keySet();
//      Set<String> arp = rejectedProvisions.keySet();
//      Set<String> agents = new HashSet<String>();
//      agents.addAll(app);
//      agents.addAll(aap);
//      agents.addAll(arp);
//
//      String s = "****PROVISIONS****\n";
//      for (String agent : agents) {
//
//          s += "\t" + agent + ":\n";
//
//          List<Pair<String, String>> agentPendingProvisions = pendingProvisions.get(agent);
//          s += "\t\tPending provisions: " + agentPendingProvisions + "\n";
//
//          List<Pair<String, String>> agentAcceptedProvisions = acceptedProvisions.get(agent);
//          s += "\t\tAccepted provisions: " + agentAcceptedProvisions + "\n";
//
//          List<Pair<String, String>> agentRejectedProvisions = rejectedProvisions.get(agent);
//          s += "\t\tRejected provisions: " + agentRejectedProvisions + "\n";
//
//      }
//
//      return s;
//
//  }

    @Override
    public Element getAsDOM(Document document) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getStyleSheetName() {
        return "noplAccountabilityArtifactInstance";
    }

}

/*
    This program was automatically generated from
    the organisation specification 'testsg'
    on febbraio 27, 2018 - 00:41:31

    This is a MOISE tool, see more at http://moise.sourceforge.net

*/

scope organisation(testsg) {


   // Role hierarchy
   subrole(r2,soc).
   subrole(r3,soc).
   subrole(r4,soc).
   subrole(r1,soc).

   // f* rules implement the role hierarchy transitivity
   // t* rules implement the transitivity of some relations

   // fplay(A,R,G) is true if A play R in G or if A play a subrole of R in G
   fplay(A,R,G) :- play(A,R,G).
   fplay(A,R,G) :- subrole(R1,R) & fplay(A,R1,G).

   // fcompatible(R1,R2,S) is true if R1 or its sub-roles are compatible with R2 in scope S
   fcompatible(R1,R2,S) :- tsubrole(R1,R2).
   fcompatible(R1,R2,S) :- tsubrole(R1,R1a) & tsubrole(R2,R2a) & compatible(R1a,R2a,S).
   fcompatible(R1,R2,S) :- tcompatible(R1,R2,S,[R1,R2]).
   tcompatible(R1,R2,S,Path) :- compatible(R1,R3,S) & not .member(R3,Path) & tcompatible(R3,R2,S,[R3|Path]).
   tsubrole(R,R).
   tsubrole(R1,R2)    :- subrole(R1,R2).
   tsubrole(R1,R2)    :- subrole(R1,R3) & tsubrole(R3,R2).


scope group(g1) {

   // ** Facts from OS
   subgroup_cardinality(g2,1,1).
   subgroup_cardinality(g4,1,1).


   // ** Rules
   rplayers(R,G,V)    :- .count(play(_,R,G),V).
   well_formed(G) :-
      .count(subgroup(_,g2,G),Sg2) & Sg2 >= 1 & Sg2 <= 1 &
      .count(subgroup(_,g4,G),Sg4) & Sg4 >= 1 & Sg4 <= 1 &
      .findall(GInst, subgroup(GInst,_,G), ListSubgroups) & all_subgroups_well_formed(ListSubgroups).
   all_subgroups_well_formed([]).
   all_subgroups_well_formed([H|T]) :- subgroup_well_formed(H) & all_subgroups_well_formed(T).

   // ** Properties check 
   norm role_in_group:  
           play(Agt,R,Gr) &
           group_id(Gr) &
           not role_cardinality(R,_,_)
        -> fail(role_in_group(Agt,R,Gr)).
   norm role_cardinality:  
           group_id(Gr) &
           role_cardinality(R,_,RMax) &
           rplayers(R,Gr,RP) &
           RP > RMax
        -> fail(role_cardinality(R,Gr,RP,RMax)).
   norm role_compatibility:  
           play(Agt,R1,Gr) &
           play(Agt,R2,Gr) &
           group_id(Gr) &
           R1 < R2 &
           not fcompatible(R1,R2,gr_inst)
        -> fail(role_compatibility(R1,R2,Gr)).
   norm well_formed_responsible:  
           responsible(Gr,S) &
           not well_formed(Gr)
        -> fail(well_formed_responsible(Gr)).
   norm subgroup_in_group:  
           group_id(Gr) &
           subgroup(G,GT,Gr) &
           not subgroup_cardinality(GT,_,_)
        -> fail(subgroup_in_group(G,GT,Gr)).
   norm subgroup_cardinality:  
           group_id(Gr) &
           subgroup_cardinality(SG,_,SGMax) &
           .count(subgroup(_,SG,Gr),SGP) &
           SGP > SGMax
        -> fail(subgroup_cardinality(SG,Gr,SGP,SGMax)).
} // end of group g1


// ** Group g2, subgroup of g1
scope group(g2) {

   // ** Facts from OS
   role_cardinality(r2,1,2147483647).
   subgroup_cardinality(g3,1,2147483647).


   // ** Rules
   rplayers(R,G,V)    :- .count(play(_,R,G),V).
   well_formed(G) :-
      rplayers(r2,G,Vr2) & Vr2 >= 1 & Vr2 <= 2147483647 &
      .count(subgroup(_,g3,G),Sg3) & Sg3 >= 1 & Sg3 <= 2147483647 &
      .findall(GInst, subgroup(GInst,_,G), ListSubgroups) & all_subgroups_well_formed(ListSubgroups).
   all_subgroups_well_formed([]).
   all_subgroups_well_formed([H|T]) :- subgroup_well_formed(H) & all_subgroups_well_formed(T).

   // ** Properties check 
   norm role_in_group:  
           play(Agt,R,Gr) &
           group_id(Gr) &
           not role_cardinality(R,_,_)
        -> fail(role_in_group(Agt,R,Gr)).
   norm role_cardinality:  
           group_id(Gr) &
           role_cardinality(R,_,RMax) &
           rplayers(R,Gr,RP) &
           RP > RMax
        -> fail(role_cardinality(R,Gr,RP,RMax)).
   norm role_compatibility:  
           play(Agt,R1,Gr) &
           play(Agt,R2,Gr) &
           group_id(Gr) &
           R1 < R2 &
           not fcompatible(R1,R2,gr_inst)
        -> fail(role_compatibility(R1,R2,Gr)).
   norm well_formed_responsible:  
           responsible(Gr,S) &
           not well_formed(Gr)
        -> fail(well_formed_responsible(Gr)).
   norm subgroup_in_group:  
           group_id(Gr) &
           subgroup(G,GT,Gr) &
           not subgroup_cardinality(GT,_,_)
        -> fail(subgroup_in_group(G,GT,Gr)).
   norm subgroup_cardinality:  
           group_id(Gr) &
           subgroup_cardinality(SG,_,SGMax) &
           .count(subgroup(_,SG,Gr),SGP) &
           SGP > SGMax
        -> fail(subgroup_cardinality(SG,Gr,SGP,SGMax)).
} // end of group g2


// ** Group g3, subgroup of g2
scope group(g3) {

   // ** Facts from OS
   role_cardinality(r3,1,2147483647).


   // ** Rules
   rplayers(R,G,V)    :- .count(play(_,R,G),V).
   well_formed(G) :-
      rplayers(r3,G,Vr3) & Vr3 >= 1 & Vr3 <= 2147483647 &
      .findall(GInst, subgroup(GInst,_,G), ListSubgroups) & all_subgroups_well_formed(ListSubgroups).
   all_subgroups_well_formed([]).
   all_subgroups_well_formed([H|T]) :- subgroup_well_formed(H) & all_subgroups_well_formed(T).

   // ** Properties check 
   norm role_in_group:  
           play(Agt,R,Gr) &
           group_id(Gr) &
           not role_cardinality(R,_,_)
        -> fail(role_in_group(Agt,R,Gr)).
   norm role_cardinality:  
           group_id(Gr) &
           role_cardinality(R,_,RMax) &
           rplayers(R,Gr,RP) &
           RP > RMax
        -> fail(role_cardinality(R,Gr,RP,RMax)).
   norm role_compatibility:  
           play(Agt,R1,Gr) &
           play(Agt,R2,Gr) &
           group_id(Gr) &
           R1 < R2 &
           not fcompatible(R1,R2,gr_inst)
        -> fail(role_compatibility(R1,R2,Gr)).
   norm well_formed_responsible:  
           responsible(Gr,S) &
           not well_formed(Gr)
        -> fail(well_formed_responsible(Gr)).
   norm subgroup_in_group:  
           group_id(Gr) &
           subgroup(G,GT,Gr) &
           not subgroup_cardinality(GT,_,_)
        -> fail(subgroup_in_group(G,GT,Gr)).
   norm subgroup_cardinality:  
           group_id(Gr) &
           subgroup_cardinality(SG,_,SGMax) &
           .count(subgroup(_,SG,Gr),SGP) &
           SGP > SGMax
        -> fail(subgroup_cardinality(SG,Gr,SGP,SGMax)).
} // end of group g3




// ** Group g4, subgroup of g1
scope group(g4) {

   // ** Facts from OS
   role_cardinality(r4,1,2147483647).


   // ** Rules
   rplayers(R,G,V)    :- .count(play(_,R,G),V).
   well_formed(G) :-
      rplayers(r4,G,Vr4) & Vr4 >= 1 & Vr4 <= 2147483647 &
      .findall(GInst, subgroup(GInst,_,G), ListSubgroups) & all_subgroups_well_formed(ListSubgroups).
   all_subgroups_well_formed([]).
   all_subgroups_well_formed([H|T]) :- subgroup_well_formed(H) & all_subgroups_well_formed(T).

   // ** Properties check 
   norm role_in_group:  
           play(Agt,R,Gr) &
           group_id(Gr) &
           not role_cardinality(R,_,_)
        -> fail(role_in_group(Agt,R,Gr)).
   norm role_cardinality:  
           group_id(Gr) &
           role_cardinality(R,_,RMax) &
           rplayers(R,Gr,RP) &
           RP > RMax
        -> fail(role_cardinality(R,Gr,RP,RMax)).
   norm role_compatibility:  
           play(Agt,R1,Gr) &
           play(Agt,R2,Gr) &
           group_id(Gr) &
           R1 < R2 &
           not fcompatible(R1,R2,gr_inst)
        -> fail(role_compatibility(R1,R2,Gr)).
   norm well_formed_responsible:  
           responsible(Gr,S) &
           not well_formed(Gr)
        -> fail(well_formed_responsible(Gr)).
   norm subgroup_in_group:  
           group_id(Gr) &
           subgroup(G,GT,Gr) &
           not subgroup_cardinality(GT,_,_)
        -> fail(subgroup_in_group(G,GT,Gr)).
   norm subgroup_cardinality:  
           group_id(Gr) &
           subgroup_cardinality(SG,_,SGMax) &
           .count(subgroup(_,SG,Gr),SGP) &
           SGP > SGMax
        -> fail(subgroup_cardinality(SG,Gr,SGP,SGMax)).
} // end of group g4


} // end of organisation testsg

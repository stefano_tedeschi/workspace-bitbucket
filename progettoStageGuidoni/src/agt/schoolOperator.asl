// Agent schoolOperator in project progettoStage

/* Initial beliefs and rules */

/* Initial goals */

!inform_number_inj.
!inform_situation_inj.
!inform_site.
!inform_hydrant.

/* Plans */

+!inform_number_inj
	<- !execute
.

+!inform_situation_inj
	<- !execute
.

+!inform_site
	<- !execute
.

+!inform_hydrant
	<- !execute
.

+!execute
	<-  println;
		.wait(4000);
		lookupArtifact(em_group, GrId);
		focus(GrId);
		adoptRole(schoolOperator);
.

+!informated_1
	<- aidInformedOnNumberOfInjured
.

+!informated_2
	<- aidInformedOnSituationOfInjured
.

+!f_informed_site
	<- firefightersInformedOnFireSite
.

+!f_informed_hydrant
	<- firefightersInformedOnHydrantPosition
.

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
{ include("$moiseJar/asl/org-obedient.asl") }

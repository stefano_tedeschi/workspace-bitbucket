// Agent technician in project progettoStage

/* Initial beliefs and rules */

/* Initial goals */

!inj_first_aid.
!exclude_tech.
!remove_voltage.
!design_help.

/* Plans */

+!inj_first_aid
	<- !execute
.

+!exclude_tech
	<- !execute
.

+!remove_voltage
	<- !execute	
.

+!design_help
	<- !execute
.

+!execute
	<-  println;
		.wait(2000);
		lookupArtifact(em_group, GrId);
		focus(GrId);
		adoptRole(technician);
.

+!first_aid_inj
	<- firstAidProvided
.

+!tech_excl
	<- technologicalSystemsExcluded
.
 +!voltage_removed
 	<- voltageRemoved
.
 
 +!disable_people_designated
 	<- twoPeopleDesignatedForEachDisablePerson
.

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
{ include("$moiseJar/asl/org-obedient.asl") }

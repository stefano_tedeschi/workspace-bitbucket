// Agent teacher in project progettoStage

/* Initial goals */

!turn_off_pc.
!no_elevator.
!no_back.
!no_block.
!inj_first_aid.
!design_trans.

/* Plans */

+!turn_off_pc	
	<- !execute
.
	
+!no_elevator
	<-!execute
.

+!no_back
	<-!execute
.

+!no_block
	<-!execute
.	

+!inj_first_aid
	<- !execute
.

+!design_trans
	<- !execute
.

+!execute
	<-  println;
		.wait(6000);
		lookupArtifact(em_group, GrId);
		focus(GrId);
		adoptRole(teacher);
.
		
+!pc_off
	<-offPc 
.

+!no_elevator_used
	<-notUseElevator
.

+!no_turning_back
	<- notTurningBack
.

+!no_blocked_accesses
	<- notBlockedAccesses
.

+!first_aid_inj
	<- firstAidProvided
.

+!d_transporter
	<- designatedTransporters
.

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
{ include("$moiseJar/asl/org-obedient.asl") }

// Agent student in project progettoStage

/* Initial beliefs and rules */

/* Initial goals */

!close_windows.
!no_elevator.
!no_back.
!no_block.
/* Plans */

+!close_windows	
	<- !execute
.

+!no_elevator
	<-!execute
. 

+!no_back
	<-!execute
.

+!no_block
	<-!execute
.	
	
	
+!execute
	<- println;
      	.wait(4000);
		lookupArtifact(em_group, GrId);
		focus(GrId);
		adoptRole(classPresident);
.

+!windows_closed
	<- .wait(1000)
		closeWindows 
.

+!no_elevator_used
	<- .wait(1500)
		notUseElevator
.

+!no_turning_back
	<- .wait(1500)
		notTurningBack
.

+!no_blocked_accesses
	<- .wait(1500)
		notBlockedAccesses
.
	
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
{ include("$moiseJar/asl/org-obedient.asl") }

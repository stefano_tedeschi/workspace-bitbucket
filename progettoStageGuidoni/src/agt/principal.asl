/* Initial goals */

!calling.

/* Plans */

+!calling
	<- !execute
.

+!execute
	<-println("*Creating Group and Scheme...*")
	
      //create the group
      .my_name(Me)
      
      //we have to use the same id for OrgBoard and Workspace
      makeArtifact(ora4mas, "ora4mas.nopl.OrgBoard", ["src/org/emergencyplan.xml"], OrgArtId)[wid(WOrg)];
      focus(OrgArtId);
      createGroup(em_group, emergency_group, GrArtId);
      debug(inspector_gui(on))[artifact_id(GrArtId)];
      adoptRole(principal)[artifact_id(GrArtId)];
      focus(GrArtId);
      
      // create the GUI artifact
      makeArtifact("emergencygui", "simulator.Emergency");
      
      // create the scheme
      createScheme(emsch, emergency_sch, SchArtId);
      debug(inspector_gui(on))[artifact_id(SchArtId)];
      focus(SchArtId);
      addSchemeWhenFormationOk("emsch")[artifact_id(GrArtId)];    
      .

+!call_made
	<- callRescue
.

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
{ include("$moiseJar/asl/org-obedient.asl") }

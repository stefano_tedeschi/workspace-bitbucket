package simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

public class InfoNumberInj implements EmergencyPart{
	public void draw(Dimension size, Graphics2D g) {
		Color dgreen = new Color(0, 139, 0);
        g.setColor(dgreen);
		g.drawString("Info Number Inj OK", 140, 240);
	}
}

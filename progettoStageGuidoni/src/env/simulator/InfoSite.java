package simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

public class InfoSite implements EmergencyPart{
	public void draw(Dimension size, Graphics2D g) {
		Color dgreen = new Color(0, 139, 0);
        g.setColor(dgreen);
		g.drawString("Firefighters Informed On Fire Site OK", 140, 280);
	}
}

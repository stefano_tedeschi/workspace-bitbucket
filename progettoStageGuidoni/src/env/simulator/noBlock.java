package simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

public class noBlock implements EmergencyPart{
	public void draw(Dimension size, Graphics2D g) {
		Color dgreen = new Color(0, 139, 0);
        g.setColor(dgreen);
		g.drawString("No Blocked Accesses OK", 80, 120);
	}
}

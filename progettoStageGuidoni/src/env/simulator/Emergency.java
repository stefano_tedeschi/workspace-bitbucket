package simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import cartago.OPERATION;
import cartago.tools.GUIArtifact;

class EmergencyPanel extends JPanel {

    EmergencyView view;

    public EmergencyPanel(EmergencyView view){
        this.view = view;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

           g.setColor(Color.WHITE);
           Dimension size = getSize();
           g.fillRect(0, 0, size.width, size.height);

           for (EmergencyPart part: view.getParts()){
               part.draw(size,(Graphics2D)g);
           }
    }
}

class EmergencyView extends JFrame {

    EmergencyPanel emergencyPanel;
    ArrayList<EmergencyPart> partsToDraw;

    public EmergencyView(){
        setTitle("-- Emergency Over --");
        setSize(600,400);

        partsToDraw = new ArrayList<EmergencyPart>();
        emergencyPanel  = new EmergencyPanel(this);
        setContentPane(emergencyPanel);
    }

    public synchronized void addPart(EmergencyPart part){
        partsToDraw.add(part);
        repaint();
    }

    public synchronized ArrayList<EmergencyPart> getParts(){
        return (ArrayList<EmergencyPart>)partsToDraw.clone();
    }
}


public class Emergency extends GUIArtifact{
	
	EmergencyView view;
	
	@Override 
	public void init() {
		view = new EmergencyView();
		view.setVisible(true);
	}
	
	@OPERATION void callRescue(){
		view.addPart(new Call());
    }
	
	@OPERATION void offPc(){
		view.addPart(new Pc());
    }
	
	@OPERATION void closeWindows(){
		view.addPart(new Windows());
    }
	
	@OPERATION void notUseElevator(){
		view.addPart(new noElevator());
    }
	
	@OPERATION void notTurningBack(){
		view.addPart(new noBack());
    }
	
	@OPERATION void notBlockedAccesses(){
		view.addPart(new noBlock());
    }

	@OPERATION void firstAidProvided() {
		view.addPart(new InjFirstAid());
	}
	
	@OPERATION void designatedTransporters(){
		view.addPart(new DesignTransporters());
    }
	
	@OPERATION void aidInformedOnNumberOfInjured(){
		view.addPart(new InfoNumberInj());
    }
	
	@OPERATION void aidInformedOnSituationOfInjured(){
		view.addPart(new InfoSitInj());
    }
	
	@OPERATION void technologicalSystemsExcluded(){
		view.addPart(new ExcludeTech());
    }
	
	@OPERATION void voltageRemoved(){
        view.addPart(new RemoveVoltage());
    }
	
	@OPERATION void firefightersInformedOnFireSite(){
		view.addPart(new InfoSite());
    }
	
	@OPERATION void firefightersInformedOnHydrantPosition(){
		view.addPart(new InfoHydrant());
    }
	
	@OPERATION void twoPeopleDesignatedForEachDisablePerson(){
		view.addPart(new DesignHelp());
    }

}

package simulator;

import java.awt.Dimension;
import java.awt.Graphics2D;

interface EmergencyPart {
	abstract public void draw(Dimension size, Graphics2D g);
}

package simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

public class DesignTransporters implements EmergencyPart{
	public void draw(Dimension size, Graphics2D g) {
		Color dred = new Color(178, 0, 0);
        g.setColor(dred);
		g.drawString("Transporters Inj Designated OK", 120, 160);
	}
}

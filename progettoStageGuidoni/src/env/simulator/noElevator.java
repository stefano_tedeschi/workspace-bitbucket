package simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

public class noElevator implements EmergencyPart{
	public void draw(Dimension size, Graphics2D g) {
		Color dgreen = new Color(0, 139, 0);
        g.setColor(dgreen);
		g.drawString("No Elevator Used OK", 80, 80);
	}
}

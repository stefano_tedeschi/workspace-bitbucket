package simulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

public class Pc implements EmergencyPart{
	public void draw(Dimension size, Graphics2D g) {
		Color dred = new Color(178, 0, 0);
        g.setColor(dred);
		g.drawString("Pc Off OK", 40, 40);
	}
}

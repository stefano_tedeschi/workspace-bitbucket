// Agent sample_agent in project test_request

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start
	 : true
	<- .wait(1000);
	   .print("hello world.");
	   request(m1,rep);
	   println("REQUEST FATTA").

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
{ include("$moiseJar/asl/org-obedient.asl") }

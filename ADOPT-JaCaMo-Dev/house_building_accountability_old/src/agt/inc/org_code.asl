// Common code for the organisational part of the system

task_roles("SitePreparation",  [site_prep_contractor]).
task_roles("FloorsWalls",      [bricklayer]).
task_roles("Roof",             [roofer]).
task_roles("WindowsDoors",     [window_fitter, door_fitter]).
task_roles("Plumbing",         [plumber]).
task_roles("ElectricalSystem", [electrician]).
task_roles("Painting",         [painter]).

provision(site_prepared, provision_for_site_prepared).
provision(floors_laid, provision_for_floors_laid).
provision(walls_built, provision_for_walls_built).
provision(roof_built, provision_for_roof_built).
provision(windows_fitted, provision_for_windows_fitted).
provision(doors_fitted, provision_for_doors_fitted).
provision(electrical_system_installed, provision_for_electrical_system_installed).
provision(plumbing_installed, provision_for_plumbing_installed).
provision(exterior_painted, provision_for_exterior_painted).
provision(interior_painted, provision_for_interior_painted).

+!contract(Task,OrgName,GroupName,AccArtName)
    : task_roles(Task, Roles)
   <- !in_ora4mas;
      
      lookupArtifact(OrgName, OrgId);
      focus(OrgId);
      
      lookupArtifact(AccArtName, AccArtId);
      focus(AccArtId);
      
      lookupArtifact(GroupName, GroupId);
      focus(GroupId);
      
      for (.member( Role, Roles) ) {
      	!apply_for_role(Role);
      }.
      
+!apply_for_role(Role)
	: my_name(Ag) & playerAccepted(Me, Role) & Ag == Me
   <- println("I have already been accepted for role ",Role).

+!apply_for_role(Role)
   <- for(role_mission(Role,S,M)) {
          for (mission_goal(M,G)) {
          	  ?provision(G,P);
          	  proposeProvisionForGoal(G,P);
          }
      }
      proposeForRole(Role).
      
+playerAccepted(Ag, Role)
	: .my_name(Ag)
   <- println("La mia candidatura per il ruolo ",Role," � stata accettata. Adotto il ruolo.");
      adoptRole(Role)[artifact_id(GroupId)].
    
-!contract(Service,GroupName)[error(E),error_msg(Msg),code(Cmd),code_src(Src),code_line(Line)]
   <- .print("Failed to sign the contract for ",Service,"/",GroupName,": ",Msg," (",E,"). command: ",Cmd, " on ",Src,":", Line).


+!in_ora4mas : in_ora4mas.
+!in_ora4mas : .intend(in_ora4mas)
   <- .wait({+in_ora4mas},100,_); 
      !in_ora4mas.
@lin[atomic]    
+!in_ora4mas
   <- joinWorkspace("ora4mas",_);
      +in_ora4mas.

{ include("$jacamoJar/templates/common-moise.asl") }
{ include("$jacamoJar/templates/org-obedient.asl") }

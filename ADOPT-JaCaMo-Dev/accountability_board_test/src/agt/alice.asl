// Agent sample_agent in project accountability_board_test

/* Initial beliefs and rules */

acceptableProvision(goal, acceptable_provision).

/* Initial goals */

!start.

/* Plans */

+!start 
	: .my_name(Me)
   <- println("Creating workspace...")
	  createWorkspace("ora4mas");
      joinWorkspace("ora4mas",WOrg);
	       
      println("Creating accountability artifact...")
	  makeArtifact(acc_art, "ora4mas.nopl.accountability.AccountabilityBoard", [], AccArtId)[wid(WOrg)];
      setOwner(Me)[artifact_id(AccArtId)];
      focus(AccArtId);
      
      proposeProvision(goal, unacceptable_provision);
      printAccountabilityArtifactState;
      
      .wait(1000);
      printAccountabilityArtifactState;
      
      proposeProvision(goal, acceptable_provision);
      printAccountabilityArtifactState;
      
      .wait(1000);
      printAccountabilityArtifactState;
      
      .

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }


{ include("$jacamoJar/templates/org-accountable.asl") }
{ include("$jacamoJar/templates/agent-accountable.asl") }

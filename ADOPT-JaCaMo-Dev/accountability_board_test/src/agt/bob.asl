// Agent bob in project accountability_board_test

+accountability_artifact(AccArtName)
   <- lookupArtifact(AccArtName,AccArtId);
      focus(AccArtId);
      !negotiateGoal.

+!negotiateGoal
   <- println("Negotiating goal...");
      .

{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

// uncomment the include below to have an agent compliant with its organisation
//{ include("$jacamoJar/templates/org-obedient.asl") }

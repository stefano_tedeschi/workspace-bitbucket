package wall;


import cartago.Artifact;
import cartago.OPERATION;
import cartago.ObsProperty;

/**
 *      Artifact that implements the auction. 
 */
public class WallArt extends Artifact {
    
    
    @OPERATION public void init()  {
        // observable properties   
        defineObsProperty("spackled", "no");
        defineObsProperty("painted", "no");   
    }

    @OPERATION public void prepareWallWithDarkSpackle() {
    	updateObsProperty("spackled", "yes");
    	defineObsProperty("wall_grey");
    }
    
    @OPERATION public void prepareWallWithTraditionalSpackle() {
    	updateObsProperty("spackled", "yes");
    	defineObsProperty("wall_white");
    }

    @OPERATION public void paintWall() {
        updateObsProperty("painted", "yes");
    }
    
}


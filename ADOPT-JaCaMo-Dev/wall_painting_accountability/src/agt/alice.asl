provision(wall_painted,wall_white).

+!wall_painted
	: wall_white
   <- println("painting the wall...");
      paintWall;
      println("done!").
   
+!wall_painted
	: not wall_white
   <- println("Unable to paint the wall. It is not white.");
      .fail.

{ include("common.asl") }
{ include("org_code.asl") }
{ include("$jacamoJar/templates/agent-accountable.asl") }
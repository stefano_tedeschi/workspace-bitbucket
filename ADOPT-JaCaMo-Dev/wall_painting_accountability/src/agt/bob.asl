+!wall_prepared
   <- //println("preparing the wall with the new dark spackle...");
      //prepareWallWithDarkSpackle;
      //println("done!");
      println("preparing the wall with the traditional spackle...");
      prepareWallWithTraditionalSpackle;
      println("done!");
      .

{ include("common.asl") }
{ include("org_code.asl") }
{ include("$jacamoJar/templates/agent-accountable.asl") }

+!wall_painted
	: wall_white
   <- println("painting the wall...");
      paintWall;
      println("done!").
   
+!wall_painted
	: not wall_white
   <- println("Unable to paint the wall. It is not white.");
      .fail.

// obligation to achieve a goal
+obligation(Ag,_,What,_)
    : .my_name(Ag) & (satisfied(_,wall_painted)=What | done(_,wall_painted,Ag)=What)
   <- println("proposing a provision for ",wall_painted,"...");
      proposeProvision(wall_painted,wall_white).

+acceptedProvision(Ag,wall_painted,wall_white)
	: .my_name(Ag) & obligation(Ag,_,What,_) & (satisfied(_,wall_painted)=What | done(_,wall_painted,Ag)=What)
   <- println("my provision ",wall_white," for ",wall_painted," has been accepted! accepting the goal...");
      agreeGoal(wall_painted, wall_white).
      
+confirmedProvision(wall_white)
	: .my_name(Ag) &
	  obligation(Ag,_,What,_) &
	  (satisfied(_,wall_painted)=What | done(_,wall_painted,Ag)=What) &
	  agreedGoal(Ag,wall_painted,wall_white)
   <- println("working to achieve ",wall_painted,"...");
      !wall_painted;
      goalAchieved(wall_painted);
      println("goal achieved!").

{ include("common.asl") }
{ include("org_code.asl") }
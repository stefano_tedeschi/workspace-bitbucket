+!wall_prepared
   <- println("preparing the wall with the new dark spackle...");
      prepareWallWithDarkSpackle;
      println("done!");
      //println("preparing the wall with the traditional spackle...");
      //prepareWallWithTraditionalSpackle;
      //println("done!");
      .

// obligation to achieve a goal
+obligation(Ag,_,What,_)
    : .my_name(Ag) & (satisfied(_,wall_prepared)=What | done(_,wall_prepared,Ag)=What)
   <- println("accepting goal ",wall_prepared,"...");
      agreeGoal(wall_prepared);
      println("working to achieve ",wall_prepared,"...");
      !wall_prepared;
      goalAchieved(wall_prepared);
      println("goal achieved!").

{ include("common.asl") }
{ include("org_code.asl") }
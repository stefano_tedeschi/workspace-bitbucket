{ include("common.asl") } 
{ include("org_code.asl") }

!have_work_done.


/* Plans */

+!have_work_done
   <-    
      // create the group
      .my_name(Me);
      createWorkspace("ora4mas");
      joinWorkspace("ora4mas",WOrg);
     
      makeArtifact(myorg, "ora4mas.nopl.OrgBoard", ["src/org/wall-os.xml"], OrgArtId)[wid(WOrg)];
      focus(OrgArtId);
      
      createGroup(w_group, wall_group, GrArtId);
      debug(inspector_gui(on))[artifact_id(GrArtId)];
      focus(GrArtId);
      
      adoptRole(house_owner)[artifact_id(GroupId)];
      
      makeArtifact("wall", "wall.WallArt");
      lookupArtifact("wall",WallArtId);
      focus(WallArtId);
      
      .send(bob, achieve, contract(wall_preparation,"myorg","w_group","wall"));
      .send(alice, achieve, contract(wall_painting,"myorg","w_group","wall"));
      
      createScheme(w_scheme, wall_scheme, SchArtId);
      debug(inspector_gui(on))[artifact_id(SchArtId)];
      focus(SchArtId);
      
      ?formationStatus(ok)[artifact_id(GrArtId)];
      addScheme("w_scheme")[artifact_id(GrArtId)].
      
-!have_wall_painted[error(E),error_msg(Msg),code(Cmd),code_src(Src),code_line(Line)]
   <- .print("Failed to complete the work due to: ",Msg," (",E,"). Command: ",Cmd, " on ",Src,":", Line).    

+?formationStatus(ok)[artifact_id(G)]
   <- .wait({+formationStatus(ok)[artifact_id(G)]}).

+!work_done
   <- println("*** Finished ***").

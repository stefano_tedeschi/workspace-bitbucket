{ include("common.asl") }
{ include("org_code.asl") }

+!wall_prepared
   <- println("Preparing the wall with the new dark spackle...");
      prepareWallWithDarkSpackle;
      println("Done!");
      //println("Preparing the wall with the traditional spackle...");
      //prepareWallWithTraditionalSpackle;
      //println("Done!");
      .
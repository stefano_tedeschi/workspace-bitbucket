{ include("common.asl") }
{ include("org_code.asl") }

+!wall_painted
	: wall_white
   <- println("Painting the wall...");
      paintWall;
      println("Done!").
   
+!wall_painted
   <- println("Unable to paint the wall. It is not white.");
      .fail.
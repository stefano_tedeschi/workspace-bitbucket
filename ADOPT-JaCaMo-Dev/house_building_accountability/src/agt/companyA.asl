
my_price(300). // initial belief

acceptableGoal(plumbing_installed).

// to make the agent refuse the goal comment the following line
//acceptableGoal(air_conditioning_installed).

// to install the air conditioning, walls must be built
provision(air_conditioning_installed,walls_built).

!discover_art("auction_for_Plumbing").

+currentBid(V)[artifact_id(Art)]         // there is a new value for current bid
    : not i_am_winning(Art)  &           // I am not the current winner
      my_price(P) & P < V                // I can offer a better bid
   <- //.print("my bid in auction artifact ", Art, " is ",P);
      bid( P ).                          // place my bid offering a cheaper service

+!plumbing_installed
   <- installPlumbing.
   
+!air_conditioning_installed
	: walls_built
   <- installAirConditioning.
      
{ include("common.asl") }
{ include("org_code.asl") }
{ include("agent-accountable.asl") }
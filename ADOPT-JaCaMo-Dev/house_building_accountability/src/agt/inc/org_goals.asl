// plan to execute organisational goals

acceptableGoal(site_prepared).
acceptableGoal(floors_laid).
acceptableGoal(walls_built).
acceptableGoal(roof_built).
acceptableGoal(windows_fitted).
acceptableGoal(doors_fitted).
acceptableGoal(electrical_system_installed).
acceptableGoal(plumbing_installed).
acceptableGoal(exterior_painted).
acceptableGoal(interior_painted).

+!site_prepared      // the goal (introduced by the organisational obligation) 
   <- prepareSite.   // simulation of the action (in GUI artifact)

+!floors_laid                   <- layFloors.
+!walls_built                   <- buildWalls.
+!roof_built                    <- buildRoof.
+!windows_fitted                <- fitWindows.
+!doors_fitted                  <- fitDoors.
+!electrical_system_installed   <- installElectricalSystem.
+!plumbing_installed            <- installPlumbing.
+!exterior_painted              <- paintExterior.
+!interior_painted              <- paintInterior.

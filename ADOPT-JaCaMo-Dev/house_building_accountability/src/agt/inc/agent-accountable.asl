// refuse goal      
+pendingGoal(Ag,Goal)
    : .my_name(Ag) & not acceptableGoal(Goal)
   <- println("Call for goal ",Goal," unacceptable, refusing it...");
      refuseGoal(Goal).

// agree goal, no provision
+pendingGoal(Ag,Goal)
    : .my_name(Ag) & acceptableGoal(Goal) & not provision(Goal,Prov)
   <- println("Proposing provision true_prov for goal ",Goal,"...");
   	  proposeProvision(Goal, true_prov).

//propose provision
+pendingGoal(Ag,Goal)
    : .my_name(Ag) & acceptableGoal(Goal) & provision(Goal,Prov)
   <- println("Proposing provision ",Prov," for goal ",Goal,"...");
      proposeProvision(Goal,Prov).

//agree goal, provision accepted      
+requestedGoal(Ag,Goal)
	: .my_name(Ag) &
	  acceptedProvision(Ag,Goal,_)
   <- println("Agreeing to achieve goal ",Goal,"...");
      agreeGoal(Goal).

//provision confirmed, must satisfy goal
+holdingProvision(Goal,Prov)
	: .my_name(Ag) &
	  agreedGoal(Ag,Goal) &
	  acceptedProvision(Ag,Goal,Prov)
   <- println("Working to achieve ",Goal,"...");
      !Goal[scheme(Scheme)];
      goalAchieved(Goal)[artifact_id(ArtId)];
      println("Goal ",Goal," achieved! Informing the organization...");
      informGoal(Goal).
      

// an unknown type of obligation was received
//+obligation(Ag,Norm,What,DeadLine)
//   : .my_name(Ag)
//   <- .print("I am obliged to ",What,", but I don't know what to do!").
   
// keep focused on schemes that my groups are responsible for
@l_focus_on_my_scheme[atomic]
+schemes(L)[artifact_name(_,Group), workspace(_,_,W)]
    : .my_name(Ag) &
      play(Ag,Role,Group)
   <- //cartago.set_current_wsp(W);
      for ( .member(Scheme,L) ) {
         lookupArtifact(Scheme,ArtId)[wid(W)];
         focus(ArtId)[wid(W)];
         println("Declaring power adoption as player of role ",Role," in group ",Group," on scheme ",Scheme,"...");
         declareAdoptionSuccess(Role,Group,Scheme);
         .concat(Group,".",Scheme,NBName);
         lookupArtifact(NBName,NBId)[wid(W)];
         focus(NBId)[wid(W)];
      }.
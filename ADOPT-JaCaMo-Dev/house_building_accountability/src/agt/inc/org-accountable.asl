+obligation(Ag,_,What,_)[artifact_id(ArtId)]
    : (satisfied(Scheme,Goal)=What | done(Scheme,Goal,Ag)=What)
   <- println("Sending call for goal ",Goal," to agent ",Ag,"...");
      callForGoal(Ag,Goal).

+pendingProvision(Agent,Goal,Provision)
    : acceptableProvision(Goal,Provision)
   <- println("New provision proposal ",Provision," for goal ",Goal," by ",Agent,". Accepting it...")
      acceptProvision(Agent,Goal,Provision);
      requestGoal(Agent,Goal).
   
+pendingProvision(Agent,Goal,Provision)
	: not acceptableProvision(Goal,Provision)
   <- println("New provision proposal ",Provision," for goal ",Goal," by ",Agent,". Rejecting it...")
      rejectProvision(Agent,Goal,Provision).
      
+agreedGoal(Ag,Goal)
	: acceptedProvision(Ag,Goal,Prov) &
	  Prov
   <- println("Provision ",Prov," for goal ",Goal," (agreed by ",Ag,") holds. Informing the agents...");
      informProvision(Goal,Prov).
      
+refusedGoal(Ag,Goal)
    : acceptedProvision(Ag,Goal,Prov)
   <- println("Agent ",Ag," refused to pursue goal ",Goal," even if the provision ",Prov, " it proposed was accepted. Too bad!").
   
+refusedGoal(Ag,Goal)
    : not acceptedProvision(Ag,Goal,Prov)
   <- println("Agent ",Ag," refused to pursue goal ",Goal," without proposing any provision. I'm sorry...").   
   
+Prov
	: acceptedProvision(Ag,Goal,Prov) & 
	  not achievedGoal(Goal)
   <- println("Provision ",Prov," for goal ",Goal," (agreed by ",Ag,") holds. Informing the agents...");
      informProvision(Goal,Prov).

// Common code for the organisational part of the system

task_roles("SitePreparation",  [site_prep_contractor]).
task_roles("FloorsWalls",      [bricklayer]).
task_roles("Roof",             [roofer]).
task_roles("WindowsDoors",     [window_fitter, door_fitter]).
task_roles("Plumbing",         [plumber]).
task_roles("ElectricalSystem", [electrician]).
task_roles("Painting",         [painter]).

+contract(GroupName,AccArtName,HouseArtName)
   <- !in_ora4mas;
      lookupArtifact(GroupName, GroupId);
      focus(GroupId);
      lookupArtifact(AccArtName, AccArtId);
      focus(AccArtId);
      lookupArtifact(HouseArtName, HouseArtId);
      focus(HouseArtId).

+!contract(Task,GroupName,AccArtName,HouseArtName)
    : task_roles(Task,Roles)
   <- !in_ora4mas;
      lookupArtifact(GroupName, GroupId);
      focus(GroupId);
      lookupArtifact(AccArtName, AccArtId);
      focus(AccArtId);
      lookupArtifact(HouseArtName, HouseArtId);
      focus(HouseArtId);
      for ( .member( Role, Roles) ) {
         adoptRole(Role)[artifact_id(GroupId)];
      }.
    
-!contract(Service,GroupName)[error(E),error_msg(Msg),code(Cmd),code_src(Src),code_line(Line)]
   <- .print("Failed to sign the contract for ",Service,"/",GroupName,": ",Msg," (",E,"). command: ",Cmd, " on ",Src,":", Line).

+!in_ora4mas : in_ora4mas.
+!in_ora4mas : .intend(in_ora4mas)
   <- .wait({+in_ora4mas},100,_); 
      !in_ora4mas.
@lin[atomic]    
+!in_ora4mas
   <- joinWorkspace("ora4mas",_);
      +in_ora4mas.
      
// obligation to commit to a mission
+obligation(Ag,Norm,committed(Ag,Mission,Scheme),Deadline)[artifact_id(ArtId),workspace(_,_,W)]
    : .my_name(Ag)
   <- .print("I am obliged to commit to ",Mission," on ",Scheme,"... doing so");
      commitMission(Mission)[artifact_name(Scheme), wid(W)].

//{ include("$jacamoJar/templates/common-moise.asl") }

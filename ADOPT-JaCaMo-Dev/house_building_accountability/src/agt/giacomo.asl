// Agent Giacomo, who wants to build the house

/* Initial beliefs and rules */

// walls_built is an acceptable provision for the plumbing_installed goal
acceptableProvision(air_conditioning_installed,walls_built).
// true_prov is an acceptable provision for every goal
acceptableProvision(_,true_prov).

acceptableGoal(house_built).

// counts the number of tasks based on the observable properties of the auction artifacts
number_of_tasks(NS) :- .findall( S, task(S), L) & .length(L,NS).

/* Initial goals */

!have_a_house.


/* Plans */

+!have_a_house 
   <- !contract; // hire the companies that will build the house
      !execute.  // (simulates) the execution of the construction
      
-!have_a_house[error(E),error_msg(Msg),code(Cmd),code_src(Src),code_line(Line)]
   <- .print("Failed to build a house due to: ",Msg," (",E,"). Command: ",Cmd, " on ",Src,":", Line).

   
/* Plans for Contracting */

+!contract
   <- !create_auction_artifacts;
      !wait_for_bids.
 
+!create_auction_artifacts
   <-  !create_auction_artifact("SitePreparation", 2000); // 2000 is the maximum value I can pay for the task
       !create_auction_artifact("FloorsWalls",     1000);
       !create_auction_artifact("Roof",            2000);
       !create_auction_artifact("WindowsDoors",    2500);
       !create_auction_artifact("Plumbing",         500);
       !create_auction_artifact("ElectricalSystem", 500);
       !create_auction_artifact("Painting",        1200).
       
+!create_auction_artifact(Task,MaxPrice)
   <- .concat("auction_for_",Task,ArtName);
      makeArtifact(ArtName, "tools.AuctionArt", [Task, MaxPrice], ArtId); 
      focus(ArtId).
-!create_auction_artifact(Task,MaxPrice)[error_code(Code)]
   <- .print("Error creating artifact ", Code).
       
+!wait_for_bids
   <- println("Waiting bids for 5 seconds...");
      .wait(5000); // use an internal deadline of 5 seconds to close the auctions
      !show_winners.
   
+!show_winners     
   <- for ( currentWinner(Ag)[artifact_id(ArtId)] ) {
         ?currentBid(Price)[artifact_id(ArtId)]; // check the current bid
         ?task(Task)[artifact_id(ArtId)];          // and the task it is for
         println("Winner of task ", Task," is ", Ag, " for ", Price)
      }.
      
/* Plans for managing the execution of the house construction */

+!execute
    : .my_name(Me)
   <- println;
      println("*** Execution Phase ***");
      println;    
      
      // create workspace
      createWorkspace("ora4mas");
      joinWorkspace("ora4mas",WOrg);
      
      // create accountability manager
      makeArtifact(bh_acc, "ora4mas.nopl.accountability.AccountabilityBoard", [], AccArtId)[wid(WOrg)];
      setOwner(Me)[artifact_id(AccArtId)];
      focus(AccArtId);
      
      // create group
      makeArtifact(bh_group, "ora4mas.nopl.GroupBoard",["src/org/group-os.xml", house_group], GrArtId)[wid(WOrg)];
      debug(inspector_gui(on))[artifact_id(GrArtId)];
      focus(GrArtId);
      
      adoptRole(house_owner)[artifact_id(GrArtId)];
      
      // create GUI artifact
      makeArtifact(housegui, "simulator.House",[],HouseArtId);
      focus(HouseArtId);
      
      !contract_winners(bh_group,bh_acc,housegui);
      
      // create scheme
      //makeArtifact("bhsch", "ora4mas.nopl.SchemeBoard",["src/org/house-os.xml", build_house_sch], SchArtId)[wid(WOrg)];
      makeArtifact(bh_scheme, "ora4mas.nopl.SchemeBoard",["src/org/house-os-aci.xml", build_house_sch], SchArtId)[wid(WOrg)];
      debug(inspector_gui(on))[artifact_id(SchArtId)];
      focus(SchArtId);
      
      // ensure to wait until the group is well formed
      ?formationStatus(ok)[artifact_id(GrArtId)];
      addScheme(bh_scheme)[artifact_id(GrArtId)];
      
      .
      
+!contract_winners(GroupName,AccArtName,HouseArtName)
    : number_of_tasks(NS) &
      .findall( ArtId, currentWinner(A)[artifact_id(ArtId)] & A \== "no_winner", L) &
      .length(L, NS)
   <- for ( currentWinner(Ag)[artifact_id(ArtId)] ) {
            ?task(Task)[artifact_id(ArtId)];    
            println("Contracting ",Ag," for ", Task);
            .send(Ag, achieve, contract(Task,GroupName,AccArtName,HouseArtName))
      }.
      
+!contract_winners(_)
   <- println("** I didn't find enough builders!");
      .fail.      

// plans to wait until the group is well formed
+?formationStatus(ok)[artifact_id(G)]
   <- .wait({+formationStatus(ok)[artifact_id(G)]}).

+!house_built
   <- println("*** Finished ***").

{ include("common.asl") }
{ include("org_code.asl") }
{ include("agent-accountable.asl") }
{ include("org-accountable.asl") }